-- ----------------------------
-- Records of sys_param
-- ----------------------------
INSERT INTO portal.sys_param VALUES ('ERP', 'MANAGE', '0', '项目线', '0', null, null, null);
INSERT INTO portal.sys_param VALUES ('ERP', 'MANAGE', '1', '部门线', '1', null, null, null);
INSERT INTO portal.sys_param VALUES ('EVALUATION', 'MODEL', '0', '上月', '1', null, null, null);
INSERT INTO portal.sys_param VALUES ('EVALUATION', 'MODEL', '1', '本月', '0', null, null, null);
INSERT INTO portal.sys_param VALUES ('VACATION', 'PLACE', '0', '在京', '0', null, null, null);
INSERT INTO portal.sys_param VALUES ('VACATION', 'PLACE', '1', '离京', '0', null, null, null);
INSERT INTO portal.sys_param VALUES ('VACATION', 'STATE', '0', '未审批', '0', null, null, null);
INSERT INTO portal.sys_param VALUES ('VACATION', 'STATE', '1', '审批同意', '0', null, null, null);
INSERT INTO portal.sys_param VALUES ('VACATION', 'STATE', '2', '审批不同意', '0', null, null, null);
INSERT INTO portal.sys_param VALUES ('VACATION', 'STATE', '9', '取消', '0', null, null, null);
INSERT INTO portal.sys_param VALUES ('WEEKLY_REPORT', 'STATE', '0', '延时', '0', null, null, null);
INSERT INTO portal.sys_param VALUES ('WEEKLY_REPORT', 'STATE', '1', '进行中', '0', null, null, null);
INSERT INTO portal.sys_param VALUES ('WEEKLY_REPORT', 'STATE', '2', '完成', '0', null, null, null);
