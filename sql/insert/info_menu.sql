-- ----------------------------
-- Records of info_menu
-- ----------------------------
INSERT INTO portal.info_menu VALUES ('10', '周报管理', null, null, '0', '1');
INSERT INTO portal.info_menu VALUES ('11', '周报填写', '/erp/weeklyReport/init', '10', '1', '1');
INSERT INTO portal.info_menu VALUES ('12', '周报查询', '/erp/weeklyReport/batchView', '10', '1', '1');
INSERT INTO portal.info_menu VALUES ('20', '工时管理', null, null, '0', '0');
INSERT INTO portal.info_menu VALUES ('21', '工时填报', null, '20', '1', '0');
INSERT INTO portal.info_menu VALUES ('22', '工时审批', null, '20', '1', '0');
INSERT INTO portal.info_menu VALUES ('23', '工时查询', null, '20', '1', '0');
INSERT INTO portal.info_menu VALUES ('30', '绩效管理', null, null, '0', '1');
INSERT INTO portal.info_menu VALUES ('31', '绩效填报', '/erp/evaluation/self', '30', '1', '1');
INSERT INTO portal.info_menu VALUES ('32', '绩效审批', '/erp/evaluation/check', '30', '1', '1');
INSERT INTO portal.info_menu VALUES ('33', '绩效查询', '/erp/evaluation/signView', '30', '1', '1');
INSERT INTO portal.info_menu VALUES ('34', '绩效结果', '/erp/evaluation/batchView', '30', '1', '1');
INSERT INTO portal.info_menu VALUES ('40', '休假管理', null, null, '0', '1');
INSERT INTO portal.info_menu VALUES ('41', '休假申请', '/erp/vacation/apply', '40', '1', '1');
INSERT INTO portal.info_menu VALUES ('42', '休假审批', '/erp/vacation/audit', '40', '1', '1');
INSERT INTO portal.info_menu VALUES ('80', '签到管理', null, null, '0', '0');
INSERT INTO portal.info_menu VALUES ('81', '日签到', '/erp/sign/daySign', '80', '1', '0');
INSERT INTO portal.info_menu VALUES ('82', '周签到', '/erp/sign/weekSign', '80', '1', '0');
INSERT INTO portal.info_menu VALUES ('83', '月视图', '/erp/sign/monthView', '80', '1', '0');
INSERT INTO portal.info_menu VALUES ('90', '系统管理', null, null, '0', '1');
INSERT INTO portal.info_menu VALUES ('91', '修改密码', '/erp/login/changePwd', '90', '1', '1');
