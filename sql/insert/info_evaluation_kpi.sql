-- ----------------------------
-- Records of info_evaluation_kpi
-- ----------------------------
INSERT INTO portal.info_evaluation_kpi VALUES ('10101000', '执行力', '30', '10100000');
INSERT INTO portal.info_evaluation_kpi VALUES ('10102000', '问题解决能力', '10', '10100000');
INSERT INTO portal.info_evaluation_kpi VALUES ('10103000', '业务洞察能力', '10', '10100000');
INSERT INTO portal.info_evaluation_kpi VALUES ('10104000', '计划组织、管理能力', '10', '10100000');
INSERT INTO portal.info_evaluation_kpi VALUES ('10201000', '专业知识技能', '10', '10200000');
INSERT INTO portal.info_evaluation_kpi VALUES ('10202000', '学习创新', '10', '10200000');
INSERT INTO portal.info_evaluation_kpi VALUES ('10301000', '团队合作、沟通协作', '10', '10300000');
INSERT INTO portal.info_evaluation_kpi VALUES ('10401000', '企业文化价值认同感', '10', '10400000');
