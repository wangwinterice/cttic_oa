ALTER TABLE portal.info_employee 
ADD COLUMN status VARCHAR(1) NULL AFTER dept_id,
ADD COLUMN cert_num VARCHAR(20) NULL AFTER status,
ADD COLUMN post VARCHAR(2) NULL AFTER cert_num,
ADD COLUMN sex VARCHAR(1) NULL AFTER post,
ADD COLUMN birthday DATE NULL AFTER sex,
ADD COLUMN address VARCHAR(1000) NULL AFTER birthday,
ADD COLUMN zip VARCHAR(6) NULL AFTER address,
ADD COLUMN in_company DATE NULL AFTER zip;

INSERT INTO portal.info_menu (menu_id, menu_name, menu_type, state) VALUES ('60', '人事管理', '0', '1');
INSERT INTO portal.info_menu (menu_id, menu_name, menu_url, parent_menu_id, menu_type, state) VALUES ('61', '信息管理', '/erp/employee/init', '60', '1', '1');
INSERT INTO portal.info_menu (menu_id, menu_name, menu_url, parent_menu_id, menu_type, state) VALUES ('62', '信息查询', '/erp/employee/query', '60', '1', '1');
INSERT INTO portal.info_role_menu (role_id, menu_id) VALUES ('2', '60');
INSERT INTO portal.info_role_menu (role_id, menu_id) VALUES ('2', '61');
INSERT INTO portal.info_role_menu (role_id, menu_id) VALUES ('2', '62');


ALTER TABLE `portal`.`info_employee` 
CHANGE COLUMN `birthday` `birthday` VARCHAR(10) NULL DEFAULT NULL ,
CHANGE COLUMN `in_company` `in_company` VARCHAR(10) NULL DEFAULT NULL ;
