UPDATE portal.info_menu m set m.menu_url='/erp/manHour/dealCurrent' ,m.state=1,m.menu_type=1 where m.menu_id='21';
UPDATE portal.info_menu m set m.menu_url='/erp/manHour/appInit' ,m.state=1,m.menu_type=1  where m.menu_id='22';
UPDATE portal.info_menu m set m.menu_url='/erp/manHour/init' ,m.state=1,m.menu_type=1  where m.menu_id='23';


DROP TABLE IF EXISTS busi_man_hour;
CREATE TABLE busi_man_hour (
  id bigint(12) NOT NULL DEFAULT '0',
  employee_id int(11) DEFAULT NULL,
  begin_date varchar(8) DEFAULT NULL,
  end_date varchar(8) DEFAULT NULL,
  isTB int(1) DEFAULT NULL,
  isSP int(1) DEFAULT NULL,
  week_id varchar(16) DEFAULT NULL,
  real_begin_date varchar(8) DEFAULT NULL,
  real_end_date varchar(8) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS busi_man_hour_detail;
CREATE TABLE busi_man_hour_detail (
  manHour_id bigint(12) NOT NULL DEFAULT '0',
  prj_id int(11) NOT NULL,
  man_hour varchar(60) DEFAULT NULL,
  prj_name varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  total int(8) DEFAULT NULL,
  app_man_hour varchar(60) DEFAULT NULL,
  app_total int(8) DEFAULT NULL,
  PRIMARY KEY (manHour_id,prj_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE portal.busi_weekly_report_record 
ADD COLUMN task_begin_date VARCHAR(10) NULL AFTER oper_time,
ADD COLUMN task_end_date VARCHAR(10) NULL AFTER task_begin_date,
ADD COLUMN plan_begin_date VARCHAR(10) NULL AFTER task_end_date,
ADD COLUMN plan_end_date VARCHAR(10) NULL AFTER plan_begin_date;

