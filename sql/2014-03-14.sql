INSERT INTO portal.info_role (role_id, role_name) VALUES ('3', '报销记录员');
INSERT INTO portal.info_menu (menu_id, menu_name, menu_type, state) VALUES ('50', '报销管理', '0', '1');
INSERT INTO portal.info_menu (menu_id, menu_name, menu_url, parent_menu_id, menu_type, state) VALUES ('51', '报销申请', '/erp/expenseAccount/apply', '50', '1', '1');
INSERT INTO portal.info_menu (menu_id, menu_name, menu_url, parent_menu_id, menu_type, state) VALUES ('52', '报销确认', '/erp/expenseAccount/confirm', '50', '1', '1');
INSERT INTO portal.info_menu (menu_id, menu_name, menu_url, parent_menu_id, menu_type, state) VALUES ('53', '报销记录', '/erp/expenseAccount/oper', '50', '1', '1');
INSERT INTO portal.info_role_menu (role_id, menu_id) VALUES ('1', '50');
INSERT INTO portal.info_role_menu (role_id, menu_id) VALUES ('1', '51');
INSERT INTO portal.info_role_menu (role_id, menu_id) VALUES ('1', '52');
INSERT INTO portal.info_role_menu (role_id, menu_id) VALUES ('3', '53');
INSERT INTO portal.info_employee_role (employee_id, role_id) VALUES ('20', '3');
INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state) VALUES ('EXPENSE_ACCOUNT', 'EXPENSE_TYPE', '0', '燃油费', '0');
INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state) VALUES ('EXPENSE_ACCOUNT', 'EXPENSE_TYPE', '1', '打车费', '0');
INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state) VALUES ('EXPENSE_ACCOUNT', 'STATE', '0', '已撤销', '0');
INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state) VALUES ('EXPENSE_ACCOUNT', 'STATE', '1', '已申请', '0');
INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state, parent_type_code, parent_param_code, parent_column_value) VALUES ('EXPENSE_ACCOUNT', 'STATE', '2', '已收单', '0', 'EXPENSE_ACCOUNT', 'STATE', '1');
INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state, parent_type_code, parent_param_code, parent_column_value) VALUES ('EXPENSE_ACCOUNT', 'STATE', '3', '已报销', '0', 'EXPENSE_ACCOUNT', 'STATE', '2');
INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state, parent_type_code, parent_param_code, parent_column_value) VALUES ('EXPENSE_ACCOUNT', 'STATE', '4', '已打款', '0', 'EXPENSE_ACCOUNT', 'STATE', '3');
INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state, parent_type_code, parent_param_code, parent_column_value) VALUES ('EXPENSE_ACCOUNT', 'STATE', '5', '已收款', '0', 'EXPENSE_ACCOUNT', 'STATE', '4');
INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state, parent_type_code, parent_param_code, parent_column_value) VALUES ('EXPENSE_ACCOUNT', 'OPER', '0', '收单', '0', 'EXPENSE_ACCOUNT', 'STATE', '1');
INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state, parent_type_code, parent_param_code, parent_column_value) VALUES ('EXPENSE_ACCOUNT', 'OPER', '1', '报销', '0', 'EXPENSE_ACCOUNT', 'STATE', '2');
INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state, parent_type_code, parent_param_code, parent_column_value) VALUES ('EXPENSE_ACCOUNT', 'OPER', '2', '打款', '0', 'EXPENSE_ACCOUNT', 'STATE', '3');
INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state, parent_type_code, parent_param_code, parent_column_value) VALUES ('EXPENSE_ACCOUNT', 'OPER', '3', '收款', '0', 'EXPENSE_ACCOUNT', 'STATE', '4');
INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state, parent_type_code, parent_param_code, parent_column_value) VALUES ('EXPENSE_ACCOUNT', 'STATE', '6', '结束', '0', 'EXPENSE_ACCOUNT', 'STATE', '5');
INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state, parent_type_code, parent_param_code, parent_column_value) VALUES ('EXPENSE_ACCOUNT', 'OPER', '4', '结束', '0', 'EXPENSE_ACCOUNT', 'STATE', '5');


CREATE TABLE portal.busi_expense_account_record (
  record_id int(11) NOT NULL AUTO_INCREMENT,
  employee_id int(11) DEFAULT NULL,
  expense_type varchar(4) DEFAULT NULL,
  expense_amount decimal(11,2) DEFAULT NULL,
  expense_desc varchar(45) DEFAULT NULL,
  state int(1) DEFAULT NULL,
  apply_time datetime DEFAULT NULL,
  PRIMARY KEY (record_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

