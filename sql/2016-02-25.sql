CREATE TABLE portal.info_project (
  prj_id decimal(16,0) DEFAULT NULL,
  prj_code varchar(16) COLLATE utf8_bin DEFAULT NULL,
  prj_name varchar(128) COLLATE utf8_bin DEFAULT NULL,
  state char(1) COLLATE utf8_bin DEFAULT NULL,
  manager_employee_id int(11) DEFAULT NULL,
  id int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
CREATE TABLE portal.info_project_employee (
  prj_id decimal(16,0) NOT NULL,
  employee_id int(11) NOT NULL,
  state varchar(1) COLLATE utf8_bin NOT NULL,
  id int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state) VALUES ('ERP', 'DATE_TYPE', '0', '工作日', '0');
INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state) VALUES ('ERP', 'DATE_TYPE', '1', '休息日', '0');

CREATE TABLE portal.info_date (
  id int(11) NOT NULL AUTO_INCREMENT,
  year varchar(4) COLLATE utf8_bin DEFAULT NULL,
  month varchar(2) COLLATE utf8_bin DEFAULT NULL,
  week varchar(2) COLLATE utf8_bin DEFAULT NULL,
  day varchar(2) COLLATE utf8_bin DEFAULT NULL,
  date_type varchar(2) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '1', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '2', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '3', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '4', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '5', '1');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '6', '1');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '7', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '8', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '9', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '10', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '11', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '12', '1');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '13', '1');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '14', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '15', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '16', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '17', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '18', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '19', '1');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '20', '1');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '21', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '22', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '23', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '24', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '25', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '26', '1');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '27', '1');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '28', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '29', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '30', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '03', '31', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '1', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '2', '1');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '3', '1');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '4', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '5', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '6', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '7', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '8', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '9', '1');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '10', '1');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '11', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '12', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '13', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '14', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '15', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '16', '1');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '17', '1');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '18', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '19', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '20', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '21', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '22', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '23', '1');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '24', '1');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '25', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '26', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '27', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '28', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '29', '0');
INSERT INTO portal.info_date (year, month, day, date_type) VALUES ('2016', '04', '30', '1');

INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state) VALUES ('VACATION', 'TYPE_MARK', '11', '△', '0');
INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state) VALUES ('VACATION', 'TYPE_MARK', '21', '√', '0');
INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state) VALUES ('VACATION', 'TYPE_MARK', '22', 'A', '0');
INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state) VALUES ('VACATION', 'TYPE_MARK', '23', 'P', '0');
INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state) VALUES ('VACATION', 'TYPE_MARK', '31', '〇', '0');
INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state) VALUES ('VACATION', 'TYPE_MARK', '32', '㊤', '0');
INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state) VALUES ('VACATION', 'TYPE_MARK', '33', '㊦', '0');
INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state) VALUES ('VACATION', 'TYPE_MARK', '41', '□', '0');
INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state) VALUES ('VACATION', 'TYPE_MARK', '51', 'M', '0');
INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state) VALUES ('VACATION', 'TYPE_MARK', '61', 'X', '0');
INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state) VALUES ('VACATION', 'TYPE_MARK', '71', 'L', '0');
INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state) VALUES ('VACATION', 'TYPE_MARK', '81', 'E', '0');

UPDATE portal.info_menu SET menu_name='考勤管理' WHERE menu_id='40';
INSERT INTO portal.info_menu (menu_id, menu_name, menu_url, parent_menu_id, menu_type, state) VALUES ('43', '考勤填报', '/erp/vacation/report', '40', '1', '1');
INSERT INTO portal.info_menu (menu_id, menu_name, menu_url, parent_menu_id, menu_type, state) VALUES ('44', '考勤统计', '/erp/vacation/state', '40', '1', '1');

INSERT INTO portal.info_role_menu (role_id, menu_id) VALUES ('1', '43');
INSERT INTO portal.info_role_menu (role_id, menu_id) VALUES ('2', '44');
DELETE FROM portal.info_role_menu WHERE role_id='2' and menu_id='42';

INSERT INTO portal.info_menu (menu_id, menu_name, menu_url, parent_menu_id, menu_type, state) VALUES ('13', '工时审核', '/erp/weeklyReport/check', '10', '1', '1');
INSERT INTO portal.info_role_menu (role_id, menu_id) VALUES ('1', '13');

INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc, state) VALUES ('PROJECT', 'STATE', '0', '有效', '0');
INSERT INTO portal.sys_param (type_code, param_code, column_value, column_desc) VALUES ('PROJECT', 'STATE', '1', '作废');

ALTER TABLE portal.busi_weekly_report_record 
ADD COLUMN approval_flag CHAR(1) NULL AFTER plan_end_date;
