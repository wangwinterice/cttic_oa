CREATE SCHEMA portal;

drop table if exists portal.busi_employee_sign;
CREATE TABLE portal.busi_employee_sign (
  employee_id int(11) NOT NULL,
  sign_in_date varchar(8) NOT NULL,
  am_sign_in char(1) DEFAULT NULL,
  am_working_place char(1) DEFAULT NULL,
  am_leave_reason char(1) DEFAULT NULL,
  pm_sign_in char(1) DEFAULT NULL,
  pm_working_place char(1) DEFAULT NULL,
  pm_leave_reason char(1) DEFAULT NULL,
  PRIMARY KEY (employee_id,sign_in_date)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

drop table if exists portal.busi_employee_vacation_record;
CREATE TABLE portal.busi_employee_vacation_record (
  record_id int(11) NOT NULL AUTO_INCREMENT,
  employee_id int(11) DEFAULT NULL,
  begin_date varchar(10) DEFAULT NULL,
  end_date varchar(10) DEFAULT NULL,
  number_of_days varchar(3) DEFAULT NULL,
  place char(1) DEFAULT NULL,
  reason varchar(1000) DEFAULT NULL,
  state char(1) DEFAULT NULL,
  check_employee_id int(11) DEFAULT NULL,
  check_date datetime DEFAULT NULL,
  PRIMARY KEY (record_id)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

drop table if exists portal.busi_evaluation_record;
CREATE TABLE portal.busi_evaluation_record (
  record_id bigint(20) NOT NULL,
  employee_id int(11) NOT NULL,
  evaluation_date varchar(6) NOT NULL,
  state char(1) NOT NULL COMMENT '0 个人已评分\n1 上级已评分',
  result_description varchar(4000) DEFAULT NULL,
  PRIMARY KEY (record_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

drop table if exists portal.busi_evaluation_result;
CREATE TABLE portal.busi_evaluation_result (
  record_id bigint(20) NOT NULL,
  evaluation_type varchar(24) NOT NULL,
  evaluation_item varchar(10) NOT NULL,
  evaluation_value varchar(6) NOT NULL,
  PRIMARY KEY (record_id,evaluation_type,evaluation_item)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

drop table if exists portal.busi_weekly_report_record;
CREATE TABLE portal.busi_weekly_report_record (
  record_id varchar(16) NOT NULL,
  employee_id int(11) DEFAULT NULL,
  begin_date varchar(8) DEFAULT NULL,
  end_date varchar(8) DEFAULT NULL,
  oper_time datetime DEFAULT NULL,
  PRIMARY KEY (record_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

drop table if exists portal.busi_weekly_report_result;
CREATE TABLE portal.busi_weekly_report_result (
  result_id int(2) NOT NULL,
  result_type varchar(12) NOT NULL,
  result_code varchar(45) NOT NULL,
  result_value varchar(4000) DEFAULT NULL,
  record_id varchar(16) NOT NULL,
  PRIMARY KEY (result_id,result_type,result_code,record_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

drop table if exists portal.info_department;
CREATE TABLE portal.info_department (
  dept_id int(11) NOT NULL,
  dept_name varchar(450) DEFAULT NULL,
  dept_code varchar(45) DEFAULT NULL,
  parent_dept_id varchar(45) DEFAULT NULL,
  manager_employee_id int(11) DEFAULT NULL,
  PRIMARY KEY (dept_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

drop table if exists portal.info_employee;
CREATE TABLE portal.info_employee (
  employee_id int(11) NOT NULL,
  employee_name varchar(45) NOT NULL,
  employee_no int(11) NOT NULL,
  mobile_phone varchar(45) NOT NULL,
  email varchar(45) NOT NULL,
  employee_pwd varchar(45) NOT NULL,
  dept_id int(11) DEFAULT NULL,
  PRIMARY KEY (employee_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

drop table if exists portal.info_employee_role;
CREATE TABLE portal.info_employee_role (
  employee_id int(11) NOT NULL,
  role_id int(11) NOT NULL,
  PRIMARY KEY (employee_id,role_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

drop table if exists portal.info_evaluation_dimension;
CREATE TABLE portal.info_evaluation_dimension (
  dimension_id int(11) NOT NULL,
  dimension_name varchar(45) NOT NULL,
  weight_function varchar(2) NOT NULL,
  template_id int(11) DEFAULT NULL,
  PRIMARY KEY (dimension_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

drop table if exists portal.info_evaluation_kpi;
CREATE TABLE portal.info_evaluation_kpi (
  kpi_id int(11) NOT NULL,
  kpi_name varchar(45) NOT NULL,
  kpi_score varchar(2) NOT NULL,
  demension_id int(11) NOT NULL,
  PRIMARY KEY (kpi_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

drop table if exists portal.info_evaluation_kpi_standard;
CREATE TABLE portal.info_evaluation_kpi_standard (
  standard_id int(11) NOT NULL,
  standard_name varchar(128) NOT NULL,
  standard_score_scope varchar(45) NOT NULL,
  kpi_id int(11) NOT NULL,
  PRIMARY KEY (standard_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

drop table if exists portal.info_evaluation_template;
CREATE TABLE portal.info_evaluation_template (
  template_id int(11) NOT NULL,
  template_name varchar(45) NOT NULL,
  PRIMARY KEY (template_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

drop table if exists portal.info_menu;
CREATE TABLE portal.info_menu (
  menu_id int(11) NOT NULL,
  menu_name varchar(45) NOT NULL,
  menu_url varchar(45) DEFAULT NULL,
  parent_menu_id int(11) DEFAULT NULL,
  menu_type char(1) NOT NULL,
  state varchar(45) NOT NULL,
  PRIMARY KEY (menu_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

drop table if exists portal.info_role;
CREATE TABLE portal.info_role (
  role_id int(11) NOT NULL,
  role_name varchar(45) NOT NULL,
  PRIMARY KEY (role_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

drop table if exists portal.info_role_menu;
CREATE TABLE portal.info_role_menu (
  role_id int(11) NOT NULL,
  menu_id int(11) NOT NULL,
  PRIMARY KEY (role_id,menu_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

drop table if exists portal.sys_param;
CREATE TABLE portal.sys_param (
  type_code varchar(45) NOT NULL,
  param_code varchar(45) NOT NULL,
  column_value varchar(45) NOT NULL,
  column_desc varchar(45) DEFAULT NULL,
  state char(1) DEFAULT NULL COMMENT '0 有效\n1 无效',
  parent_type_code varchar(45) DEFAULT NULL,
  parent_param_code varchar(45) DEFAULT NULL,
  parent_column_value varchar(45) DEFAULT NULL,
  PRIMARY KEY (type_code,param_code,column_value)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--add@2014/5/12
CREATE TABLE `info_salary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `base_salary` decimal(10,2) NOT NULL COMMENT '基本工资',
  `performance_salary` decimal(10,2) NOT NULL COMMENT '绩效工资',
  `eff_date` datetime NOT NULL,
  `exp_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


