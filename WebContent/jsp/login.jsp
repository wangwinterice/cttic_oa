<%@page contentType="text/html; charset=utf-8"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>北京国交信通科技发展有限公司软件工程院智能办公系统</title>
<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	$(function() {
		$('#loginBtn').bind('click', function () {
			var employeeNo = $('#employeeNo').val();
			if ('' == employeeNo) {
				alert("工号不能为空");
				return;
			}
			var employeePwd = $('#employeePwd').val();
			if ('' == employeePwd) {
				alert("密码不能为空");
				return;
			}
			employeePwd = hex_md5(employeePwd);
			$.ajax({ 
				type: "POST", 
				url: "<%=basePath%>/erp/login/loginIn",
				data : 'EMPLOYEE_NO=' + employeeNo + '&EMPLOYEE_PWD=' + employeePwd,
				async : false,
				success : function(returnStr) {
					var jsonReturnStr = JSON.parse(returnStr);
					if (jsonReturnStr.STATUS_CODE == '1') {
						window.location = "<%=basePath%>/erp/init/index";
					} else {
						alert(jsonReturnStr.STATUS_INFO);
					}
				}
			});
		});
		SlideShow(3500);
	});
	
	function SlideShow(slidetime) {
		var slideContainer = document.getElementById("slideContainer"); 
		var img_row = document.getElementById("slidesImgs").getElementsByTagName("li"); 
		var slideBar = document.getElementById("slideBar"); 
		var slideBar_li = slideBar.getElementsByTagName("li"); 
		var img_length = img_row.length; 
		var slidetime = slidetime || 3000; 
		var e = lastI = 0; 
		var j;
		var m;
		function setTime() {
			m = setInterval(function() {
				e = e + 1 >= img_length ? e + 1 - img_length : e + 1;
				g();
			}, slidetime);
		}
		function clearsolid() {
			clearInterval(m);
		}
		function g() {
			img_row[lastI].style.display = "none";
			slideBar_li[lastI].className = "";
			img_row[e].style.display = "block";
			slideBar_li[e].className = "on";
			lastI = e;
		}
		img_row[e].style.display = "block";
		slideContainer.onmouseover = clearsolid;
		slideContainer.onmouseout = setTime;
		slideBar.onmouseover = function(i) {
			j = i ? i.target : window.event.srcElement;
			if (j.nodeName === "LI") {
				e = parseInt(j.innerHTML, 10) - 1;
				g();
			}
		};
		setTime();
	};
</script>
</head>
<body style="background: #E7EAEB;"
	onload="javascript:document.form1.PASSWORD.focus();" scroll="auto">
	<div style="margin-top: 30px; margin-bottom: 30px;">
		<div
			style="background-image:url('<%=basePath%>/image/bj_top.jpg');background-repeat:no-repeat;background-position:center bottom;height:7px;width:1008px;margin:0px auto;"></div>
		<div
			style="background-image:url('<%=basePath%>/image/bj_conten.jpg');height:600px;width:1008px;margin:0px auto;">
			<div style="height: 12px; width: 1000px; margin: 0px auto;"></div>
			<div
				style="background:url('<%=basePath%>/image/login_logo.jpg') left top no-repeat;height:44px;margin:0px 23px;"></div>
			<div style="height: 330px; width: 1000px; margin: 0px auto;">
				<div style="height: 17px;"></div>
				<div style="height: 330px; width: 954px; margin: 0px auto;">
					<div style="width: 954px;" class="wrap_div">
						<!-- 照片幻灯片 start-->
						<div class="comiis_wrapad" id="slideContainer">
							<div id="frameHlicAe" class="frame cl">
								<div class="temp"></div>
								<div class="block">
									<div class="cl">
										<ul class="slideshow" id="slidesImgs">
											<li><img src="<%=basePath%>/image/bg1.jpg" width="954px"
												height="330px !important" alt="" /></li>
											<li><img src="<%=basePath%>/image/bg2.jpg" width="954px"
												height="330px !important" alt="" /></li>
											<li><img src="<%=basePath%>/image/bg3.jpg" width="954px"
												height="330px !important" alt="" /></li>
											<li><img src="<%=basePath%>/image/bg4.jpg" width="954px"
												height="330px !important" alt="" /></li>
											<li><img src="<%=basePath%>/image/bg5.jpg" width="954px"
												height="330px !important" alt="" /></li>
											<li><img src="<%=basePath%>/image/bg6.jpg" width="954px"
												height="330px !important" alt="" /></li>
										</ul>
									</div>
									<div class="slidebar" id="slideBar">
										<ul>
											<li class="on">1</li>
											<li>2</li>
											<li>3</li>
											<li>4</li>
											<li>5</li>
											<li>6</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div style="height: 192px; width: 954px; margin: 0px auto;">
					<div style="height: 192px; width: 608px; float: left;">
						<div style="height: 40px;"></div>
						<div style="height: 64px; width: 608px; float: left;">
							<img src="<%=basePath%>/image/3.png"
								style="float: left; margin: 0px; padding: 0px;">
							<div
								style="height: 64px; float: left; text-align: left; font-size: 15px; font-weight: bold; color: #898989">
								<div style="margin-top: 8px;">*&nbsp;欢迎登录北京国交信通科技发展有限公司软件工程院智能办公系统。</div>
								<table>
									<tr>
										<td style="height: 6px;"></td>
									</tr>
								</table>
								<div>*&nbsp;如账号登录遇到问题,请联系系统管理员。</div>
							</div>
						</div>
					</div>
					<div style="height: 192px; width: 346px; float: left;">
						<div style="height: 9px;"></div>
						<div
							style="height:183px;width:346px;background:url('<%=basePath%>/image/logo_k.jpg');">
							<div style="height: 30px;"></div>
							<div align=center>
								<table width="291" align="center" cellspacing="0"
									cellpadding="0">
									<tr bgcolor="#FFFFFF">
										<td nowrap width="80"
											style="border-top: 1px solid #D4D7C7; border-bottom: 1px solid #D4D7C7; border-left: 1px solid #D4D7C7; border-right: 0px; padding-left: 10px; font-weight: bold; font-size: 16px; text-align: right;">工&nbsp;&nbsp;号：</td>
										<td width="220"
											style="border-top: 1px solid #D4D7C7; border-bottom: 1px solid #D4D7C7; border-right: 1px solid #D4D7C7; border-left: 0px;">
											<input type="text" id="employeeNo" maxlength="20"
											onmouseover="this.focus()" onfocus="this.select()" value=""
											style="height: 30px; width: auto; font-size: 18px; padding: 0px; margin: 0px; line-height: 30px; border: 0px;">
										</td>
									</tr>
									<tr style="height: 3px;">
										<td colspan=2></td>
									</tr>
									<tr bgcolor="#FFFFFF">
										<td nowrap
											style="border-top: 1px solid #D4D7C7; border-bottom: 1px solid #D4D7C7; border-left: 1px solid #D4D7C7; border-right: 0px; padding-left: 10px; font-weight: bold; font-size: 16px; text-align: right;">密&nbsp;&nbsp;码：</td>
										<td
											style="border-top: 1px solid #D4D7C7; border-bottom: 1px solid #D4D7C7; border-right: 1px solid #D4D7C7; border-left: 0px; font-weight: bold;">
											<input type="password" id="employeePwd"
											onmouseover="this.focus()" onfocus="this.select()" value=""
											style="height: 30px !important; width: auto; font-size: 18px; border: 0px;">
										</td>
									</tr>
									<tr>
										<td colspan=2 style="height: 8px;"></td>
									<tr>
									<tr>
										<td colspan=2 align=center>
											<div style="text-align: center">
												<input type="button" title="登录" value="" id="loginBtn"
													style="background-image: url('<%=basePath%>/image/dl2.jpg'); width: 291px; height: 43px !important; border: 0px;" />
											</div>
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div
			style="background:url('<%=basePath%>/image/bj_bottom.jpg');background-repeat:no-repeat;background-position:center top;height:7px;width:1008px;margin:0px auto;"></div>
		<div style="height: 100%;"></div>
		<div class="msg">
			<div></div>
			<div></div>
			<div></div>
		</div>
	</div>
</body>
</html>