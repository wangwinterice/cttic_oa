<%@page contentType="text/html; charset=utf-8"%>
<%@page import="com.cttic.erp.bean.ext.InfoOperBeanExt"%>
<%@page import="com.cttic.erp.common.util.InfoOperUtil"%>
<%@page import="com.cttic.erp.bean.InfoMenuBean"%>
<%@page import="java.util.List"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
	InfoOperBeanExt infoOperBean = InfoOperUtil.getCurrOperInfo(request);
	List<InfoMenuBean> infoMenuBeanList = infoOperBean.getInfoMenuBeanLists();
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>北京国交信通科技发展有限公司软件工程院智能办公系统-首页</title>
<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	$(function() {
		$('#MAIN_WINDOW').attr('height', document.documentElement.clientHeight-120);
		<%
		for (InfoMenuBean infoMenuBean : infoMenuBeanList) {
			if ("1".equals(infoMenuBean.getMenu_type()) && null != infoMenuBean.getMenu_url()) {
		%>
		$('#MENU_<%=infoMenuBean.getMenu_id()%>').click(function(){
			$('#MENU_LISE_<%=infoMenuBean.getParent_menu_id()%>').menubutton({
				disabled:true
			});
			$('#MAIN_TITLE').html('<%=infoMenuBean.getMenu_name()%>');
			$('#MAIN_WINDOW').attr('src', "<%=basePath%><%=infoMenuBean.getMenu_url()%>");
		});
		<%
			}
		}
		%>
		$('#loginOut').bind('click', function () {
			$.ajax({ 
				type: "POST", 
				url: "<%=basePath%>/erp/login/loginOut",
				async : false,
				success : function(returnStr) {
					var jsonReturnStr = JSON.parse(returnStr);
					if (jsonReturnStr.STATUS_CODE == '1') {
						alert(jsonReturnStr.STATUS_INFO);
						window.location = "<%=basePath%>/erp/init/login";
					} else {
						alert(jsonReturnStr.STATUS_INFO);
					}
				}
			});
		});
	});
</script>
</head>
<body background="<%=basePath%>/image/main_bg2.jpg">
	<table style="width:100%;">
		<tr>
			<td style="width:85%; height:50px">
				<img src="<%=basePath%>/image/topbar_left_bg3.png"></img>
			</td>
			<td style="width:15%" nowrap>
				姓名：<%= infoOperBean.getInfoEmployeeBean().getEmployee_name() %>
				&nbsp;&nbsp;工号：<%= infoOperBean.getInfoEmployeeBean().getEmployee_no() %>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" id="loginOut">退出</a>
			</td>
		<tr>
		<tr>
			<td style="background: url('<%=basePath%>/image/shortcut_bg.png') repeat-x top left; height:25px" colspan="2">
			<% 
			for (InfoMenuBean parentInfoMenuBean : infoMenuBeanList) {
				if ("0".equals(parentInfoMenuBean.getMenu_type())) {
					%>
					<a href="#" id="MENU_<%= parentInfoMenuBean.getMenu_id() %>" class="easyui-menubutton" 
						menu="#MENU_LIST_<%= parentInfoMenuBean.getMenu_id() %>"><%= parentInfoMenuBean.getMenu_name() %></a>
					<div id="MENU_LIST_<%= parentInfoMenuBean.getMenu_id() %>" style="width:150px;">
					<%
					for (InfoMenuBean infoMenuBean : infoMenuBeanList) {
						if (infoMenuBean.getParent_menu_id() == parentInfoMenuBean.getMenu_id()) {
					%>
						<div id="MENU_<%= infoMenuBean.getMenu_id() %>"><%= infoMenuBean.getMenu_name() %></div>
					<%
						}
					}
					%>
					</div>
					<%
				}
			}
			%>
			</td>
		</tr>
		<tr>
			<td style="width:100%; height:25px" colspan="2" >
				您当前的位置：<span id="MAIN_TITLE">首页</span>
			</td>
		</tr>
		<tr>
			<td style="width:100%;" colspan="2">
				<iframe id="MAIN_WINDOW" width="100%" height="" src=""></iframe>
			</td>
		</tr>
	</table>
</body>
</html>