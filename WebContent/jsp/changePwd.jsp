<%@page contentType="text/html; charset=utf-8"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>密码修改</title>
<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	$(function() {
		$('#saveBtn').bind('click', function (){
			var oldEmployeePwd = $('#oldEmployeePwd').val();
			var newEmployeePwd = $('#newEmployeePwd').val();
			var confirmEmployeePwd = $('#confirmEmployeePwd').val();
			if ("" == oldEmployeePwd) {
				alert("旧密码不能为空");
				return;
			}
			if ("" == newEmployeePwd) {
				alert("新密码不能为空");
				return;
			}
			if ("" == confirmEmployeePwd) {
				alert("密码确认不能为空");
				return;
			}
			if (newEmployeePwd != confirmEmployeePwd) {
				alert("两次输入的新密码不一致");
				return;
			}
			oldEmployeePwd = hex_md5(oldEmployeePwd);
			newEmployeePwd = hex_md5(newEmployeePwd);
			$.ajax({ 
				type: "POST", 
				url: "<%=basePath%>/erp/login/savePwd",
				data : 'OLD_EMPLOYEE_PWD=' + oldEmployeePwd + '&NEW_EMPLOYEE_PWD=' + newEmployeePwd,
				async : false,
				success : function(returnStr) {
					var jsonReturnStr = JSON.parse(returnStr);
					if (jsonReturnStr.STATUS_CODE == '1') {
						alert(jsonReturnStr.STATUS_INFO);
						parent.location.reload();
						//top.document.location.href = "<%=basePath%>/erp/init/index";
					} else {
						alert(jsonReturnStr.STATUS_INFO);
					}
				}
			});
		});
	});
</script>
</head>
<body>
		<div style="text-align: center">
			<table width="291" align="center" cellspacing="0" cellpadding="0">
				<tr style="height: 160px;">
					<td colspan=2></td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td nowrap width="80"
						style="border-top: 1px solid #D4D7C7; border-bottom: 1px solid #D4D7C7; border-left: 1px solid #D4D7C7; border-right: 0px; padding-left: 10px; font-weight: bold; font-size: 16px; text-align: right;">
						旧&nbsp;密&nbsp;码:</td>
					<td width="220"
						style="border-top: 1px solid #D4D7C7; border-bottom: 1px solid #D4D7C7; border-right: 1px solid #D4D7C7; border-left: 0px;">
						<input type="password" id="oldEmployeePwd" maxlength="20"
						onmouseover="this.focus()" onfocus="this.select()" value=""
						style="height: 30px; width: auto; font-size: 18px; padding: 0px; margin: 0px; line-height: 30px; border: 0px;"></span><br>
					</td>
				</tr>
				<tr style="height: 3px;">
					<td colspan=2></td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td nowrap width="80"
						style="border-top: 1px solid #D4D7C7; border-bottom: 1px solid #D4D7C7; border-left: 1px solid #D4D7C7; border-right: 0px; padding-left: 10px; font-weight: bold; font-size: 16px; text-align: right;">
						新&nbsp;密&nbsp;码:</td>
					<td width="220"
						style="border-top: 1px solid #D4D7C7; border-bottom: 1px solid #D4D7C7; border-right: 1px solid #D4D7C7; border-left: 0px;">
						<input type="password" id="newEmployeePwd" maxlength="20"
						onmouseover="this.focus()" onfocus="this.select()" value=""
						style="height: 30px; width: auto; font-size: 18px; padding: 0px; margin: 0px; line-height: 30px; border: 0px;"></span><br>
					</td>
				</tr>
				<tr style="height: 3px;">
					<td colspan=2></td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td nowrap width="80"
						style="border-top: 1px solid #D4D7C7; border-bottom: 1px solid #D4D7C7; border-left: 1px solid #D4D7C7; border-right: 0px; padding-left: 10px; font-weight: bold; font-size: 16px; text-align: right;">
						密码确认:</td>
					<td width="220"
						style="border-top: 1px solid #D4D7C7; border-bottom: 1px solid #D4D7C7; border-right: 1px solid #D4D7C7; border-left: 0px;">
						<input type="password" id="confirmEmployeePwd" maxlength="20"
						onmouseover="this.focus()" onfocus="this.select()" value=""
						style="height: 30px; width: auto; font-size: 18px; padding: 0px; margin: 0px; line-height: 30px; border: 0px;"></span><br>
					</td>
				</tr>
				<tr style="height: 3px;">
					<td colspan=2></td>
				</tr>
			</table>
			<a href="#" class="easyui-linkbutton" id="saveBtn">&nbsp;&nbsp;保&nbsp;&nbsp;&nbsp;&nbsp;存&nbsp;&nbsp;</a>
		</div>
</body>
</html>