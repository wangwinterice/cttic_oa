<%@page contentType="text/html; charset=utf-8"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<script type="text/javascript">
	$(function() {
		<%
		for (int count = 2; count < 11; count++) {
		%>
			$('#TASK_ROW_<%=count%>_${SELECT_EMPLOYEE.employee_id}').hide();	
			$('#PLAN_ROW_<%=count%>_${SELECT_EMPLOYEE.employee_id}').hide();
		<%
		}
		%>
		<c:forEach items="${WEEKLY_REPORT_RESULT}" var="resultList">
			$("#${resultList.result_type}_${resultList.result_code}_${resultList.result_id}_${SELECT_EMPLOYEE.employee_id}").html('${resultList.result_value}');
			$('#${resultList.result_type}_ROW_${resultList.result_id}_${SELECT_EMPLOYEE.employee_id}').show();
		</c:forEach>
	});
</script>
	<table border="0" align="center">
		<tr>
			<td>
				<table border="1" bordercolor="#7FB0DE" cellpadding="1"
					cellspacing="0" align="center">
					<tr height="30px">
						<td rowspan="2" align="center" nowrap>WBS ID</td>
						<td rowspan="2" align="center" nowrap>参与项目</td>
						<td rowspan="2" align="center" nowrap>任务描述</td>
						<td colspan="7" align="center" width="280px" nowrap>工时填报</td>
					</tr>
					<tr height="15px">
						<c:forEach items="${TASK_DATE_LIST}" var="taskDate">
						<td align="center">${taskDate}</td>
						</c:forEach>
					</tr>
						<%
							for (int count = 1; count < 11; count++) {
						%>
						<tr id="TASK_ROW_<%=count%>_${SELECT_EMPLOYEE.employee_id}" style="height:35px;">
							<td align='center'><%=count%></td>
							<td id='TASK_PRJ_ID_<%=count%>_${SELECT_EMPLOYEE.employee_id}' style="width:80px;"></td>
							<td id='TASK_TASK_DESC_<%=count%>_${SELECT_EMPLOYEE.employee_id}' style="width:300px;"></td>
							<td id='TASK_WORK_TIME_1_<%=count%>_${SELECT_EMPLOYEE.employee_id}' align="center" style="width:35px;">0</td>
							<td id='TASK_WORK_TIME_2_<%=count%>_${SELECT_EMPLOYEE.employee_id}' align="center" style="width:35px;">0</td>
							<td id='TASK_WORK_TIME_3_<%=count%>_${SELECT_EMPLOYEE.employee_id}' align="center" style="width:35px;">0</td>
							<td id='TASK_WORK_TIME_4_<%=count%>_${SELECT_EMPLOYEE.employee_id}' align="center" style="width:35px;">0</td>
							<td id='TASK_WORK_TIME_5_<%=count%>_${SELECT_EMPLOYEE.employee_id}' align="center" style="width:35px;">0</td>
							<td id='TASK_WORK_TIME_6_<%=count%>_${SELECT_EMPLOYEE.employee_id}' align="center" style="width:35px;">0</td>
							<td id='TASK_WORK_TIME_7_<%=count%>_${SELECT_EMPLOYEE.employee_id}' align="center" style="width:35px;">0</td>
						</tr>
						<%
							}
						%>
				</table>
			</td>
			<td align="center" valign="top">
				<table border="1" bordercolor="#7FB0DE" cellpadding="1"
					cellspacing="0" align="center">
					<tr height="45px">
						<td align="center" nowrap>序号</td>
						<td align="center" nowrap>参与项目</td>
						<td align="center" nowrap>计划描述</td>
					</tr>
						<%
							for (int count = 1; count < 11; count++) {
						%>
						<tr id="PLAN_ROW_<%=count%>_${SELECT_EMPLOYEE.employee_id}" style="height:35px;">
							<td align='center'><input type='hidden' id='PLAN_SEQ_NO_<%=count%>_${SELECT_EMPLOYEE.employee_id}' value='<%=count%>' /><%=count%></td>
							<td id='PLAN_PRJ_ID_<%=count%>_${SELECT_EMPLOYEE.employee_id}' style="width:80px;"></td>
							<td id='PLAN_PLAN_DESC_<%=count%>_${SELECT_EMPLOYEE.employee_id}' style="width:300px;"></td>
						</tr>
						<%
							}
						%>
				</table>
			</td>
		</tr>
	</table>
