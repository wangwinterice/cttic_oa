<%@page contentType="text/html; charset=utf-8"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>${DATE_DESC}周报填写</title>
<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	$(function() {
		<%
		for (int count = 2; count < 11; count++) {
		%>
			$('#TASK_ROW_<%=count%>').hide();	
			$('#PLAN_ROW_<%=count%>').hide();
		<%
		}
		%>
		$('#saveBtn').bind('click', function () {
			var paramJson = {};
			var paramJsonAttr = new Array();
			var taskJson = {};
			<%for (int count = 1; count < 11; count++) {%>
				taskJson = {};
				taskJson['WBS_ID'] = $('#TASK_WBS_ID_<%=count%>').val();
				taskJson['TASK_WORK_TIME_1'] = $('#TASK_WORK_TIME_1_<%=count%>').val();
				taskJson['TASK_WORK_TIME_2'] = $('#TASK_WORK_TIME_2_<%=count%>').val();
				taskJson['TASK_WORK_TIME_3'] = $('#TASK_WORK_TIME_3_<%=count%>').val();
				taskJson['TASK_WORK_TIME_4'] = $('#TASK_WORK_TIME_4_<%=count%>').val();
				taskJson['TASK_WORK_TIME_5'] = $('#TASK_WORK_TIME_5_<%=count%>').val();
				taskJson['TASK_WORK_TIME_6'] = $('#TASK_WORK_TIME_6_<%=count%>').val();
				taskJson['TASK_WORK_TIME_7'] = $('#TASK_WORK_TIME_7_<%=count%>').val();
				paramJsonAttr.push(taskJson);
			<%}%>
			//alert(JSON.stringify(paramJsonAttr));
			paramJson['TASK'] = paramJsonAttr;
			//alert(JSON.stringify(paramJson));
			$.ajax({ 
				type: "POST", 
				url: "<%=basePath%>/erp/weeklyReport/checksave?OPER_TYPE=${OPER_TYPE}&BEGIN_DATE=${BEGIN_DATE}&END_DATE=${END_DATE}&SELECT_EMPLOYEE=${SELECT_EMPLOYEE.employee_id}",
				data : {"INPUT_PARAM" : JSON.stringify(paramJson)},
				async : false,
				success : function(returnStr) {
					var jsonReturnStr = JSON.parse(returnStr);
					if (jsonReturnStr.STATUS_CODE == '1') {
						alert(jsonReturnStr.STATUS_INFO);
						//window.close();
						window.location = '<%=basePath%>/erp/weeklyReport/checkedit?OPER_TYPE=EDIT&BEGIN_DATE=${BEGIN_DATE}&END_DATE=${END_DATE}&SELECT_EMPLOYEE=${SELECT_EMPLOYEE.employee_id}';
					} else {
						alert(jsonReturnStr.STATUS_INFO);
					}
				}
			});
		});
		<c:forEach items="${WEEKLY_REPORT_RESULT}" var="resultList">
			$("#${resultList.result_type}_${resultList.result_code}_${resultList.result_id}").val('${resultList.result_value}'.split("<br>").join("\r\n"));
			$("#${resultList.result_type}_${resultList.result_code}_${resultList.result_id}").html('${resultList.result_value}');
			$('#${resultList.result_type}_ROW_${resultList.result_id}').show();
		</c:forEach>
	});
</script>
</head>
<body>
	<table border="0" align="center">
		<tr>
			<td align="center" valign="top">
				<table>
					<tr>
						<td><h3>本周总结</h3></td>
						<td>
							${TASK_BEGIN_DATE}~${TASK_END_DATE}
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center">
							<table border="1" bordercolor="#7FB0DE" cellpadding="1"
								cellspacing="0" align="center">
								<tr height="30px">
									<td rowspan="2" align="center" nowrap>WBS ID</td>
									<td rowspan="2" align="center" nowrap>参与项目</td>
									<td rowspan="2" align="center" nowrap>任务描述</td>
									<td colspan="7" align="center" width="280px" nowrap>工时填报</td>
								</tr>
								<tr height="15px">
									<c:forEach items="${TASK_DATE_LIST}" var="taskDate">
									<td align="center">${taskDate}</td>
									</c:forEach>
								</tr>
									<%
										for (int count = 1; count < 11; count++) {
									%>
									<tr id="TASK_ROW_<%=count%>">
										<td align='center'><input type='hidden' id='TASK_WBS_ID_<%=count%>' value='<%=count%>' /><%=count%></td>
										<td id='TASK_PRJ_ID_<%=count%>' style="width:80px;"></td>
										<td id='TASK_TASK_DESC_<%=count%>' style="width:300px;"></td>
										<td align="center"><input type="text" id='TASK_WORK_TIME_1_<%=count%>' value="0" style="width:35px;height:35px;border:0;" /></td>
										<td align="center"><input type="text" id='TASK_WORK_TIME_2_<%=count%>' value="0" style="width:35px;height:35px;border:0;" /></td>
										<td align="center"><input type="text" id='TASK_WORK_TIME_3_<%=count%>' value="0" style="width:35px;height:35px;border:0;" /></td>
										<td align="center"><input type="text" id='TASK_WORK_TIME_4_<%=count%>' value="0" style="width:35px;height:35px;border:0;" /></td>
										<td align="center"><input type="text" id='TASK_WORK_TIME_5_<%=count%>' value="0" style="width:35px;height:35px;border:0;" /></td>
										<td align="center"><input type="text" id='TASK_WORK_TIME_6_<%=count%>' value="0" style="width:35px;height:35px;border:0;" /></td>
										<td align="center"><input type="text" id='TASK_WORK_TIME_7_<%=count%>' value="0" style="width:35px;height:35px;border:0;" /></td>
									</tr>
									<%
										}
									%>
							</table>
						</td>
					</tr>
				</table>
			</td>
			<td align="center" valign="top">
				<table>
					<tr>
						<td><h3>下周计划</h3></td>
						<td>
							${PLAN_BEGIN_DATE}~${PLAN_END_DATE}
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center">
							<table border="1" bordercolor="#7FB0DE" cellpadding="1"
								cellspacing="0" align="center">
								<tr height="45px">
									<td align="center" nowrap>序号</td>
									<td align="center" nowrap>参与项目</td>
									<td align="center" nowrap>计划描述</td>
								</tr>
									<%
										for (int count = 1; count < 11; count++) {
									%>
									<tr id="PLAN_ROW_<%=count%>" style="height:37px;">
										<td align='center'><input type='hidden' id='PLAN_SEQ_NO_<%=count%>' value='<%=count%>' /><%=count%></td>
										<td id='PLAN_PRJ_ID_<%=count%>' style="width:80px;"></td>
										<td id='PLAN_PLAN_DESC_<%=count%>' style="width:300px;"></td>
									</tr>
									<%
										}
									%>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table width="90%">
		<tr>
			<td align="right"><a href="#" class="easyui-linkbutton"
				id="saveBtn">保&nbsp;存</a>
			</td>
		</tr>
	</table>
</body>
</html>