<%@page contentType="text/html; charset=utf-8"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	$(function() {
		$('#date_select').combo({
			required : true,
			editable : false
		});
		$('#date_list').appendTo($('#date_select').combo('panel'));
		$('#date_select').combo('setValue', '${CURR_DATE.column_value}').combo('setText',
				'${CURR_DATE.column_desc}').combo('hidePanel');
		$('#date_list input').click( function() {
			var v = $(this).val();
			var s = $(this).next('span').text();
			$('#date_select').combo('setValue', v).combo('setText', s).combo('hidePanel');
			window.location = '<%=basePath%>/erp/weeklyReport/viewinit?SELECT_DATE=' 
		        + $(this).val()
		        + '&SELECT_PRJ='
		        + $('#prj_select').val();
		});
		$('#prj_select').bind('change', function () {
			window.location = '<%=basePath%>/erp/weeklyReport/viewinit?SELECT_DATE=' 
		        + $('#date_select').combo('getValue')
		        + '&SELECT_PRJ='
		        + $('#prj_select').val();
		});
		<c:forEach items="${EMPLOYEE_LIST}" var="employee">
		$('#weeklyReport_${fn:substring(CURR_DATE.column_value, 0, 8)}${fn:substring(CURR_DATE.column_value, 8, 16)}_${employee.employee_id}').load('<%=basePath%>/erp/weeklyReport/detail?BEGIN_DATE=${fn:substring(CURR_DATE.column_value, 0, 8)}&END_DATE=${fn:substring(CURR_DATE.column_value, 8, 16)}&SELECT_EMPLOYEE=${employee.employee_id}');
		</c:forEach>
	});
</script>
</head>
<body>
	<select id="date_select" style="width: 350px"></select>
	<div id="date_list">
		<input type="radio" name="lang" value="">
		<span>---请选择---</span><br />
		<c:forEach items="${DATE_LIST}" var="dateList">
			<input type="radio" name="lang" value="${dateList.beginDate}${dateList.endDate}">
			<span>${dateList.year}年${dateList.month}月第${dateList.week}周${dateList.beginDateDesc}~${dateList.endDateDesc}</span>
			<br />
		</c:forEach>
	</div>
	<select id='prj_select'>
		<option value=""></option>
		<c:forEach items="${PRJ_LIST}" var="prjList">
			<option value="${prjList.prj_id}"
			<c:if test="${SELECT_PRJ ==  prjList.prj_id}">selected</c:if>
			>${prjList.prj_name}</option>
		</c:forEach>
	</select>
	<table width="100%" border="0" align="center">
		<tr>
			<td width="10%"></td>
			<td width="60%">
				<h3>
					本周总结&nbsp;${TASK_BEGIN_DATE}~${TASK_END_DATE}
				</h3>
			</td>
			<td width="30%">
				<h3>
					下周计划&nbsp;${PLAN_BEGIN_DATE}~${PLAN_END_DATE}
				</h3>
			</td>
		</tr>
		<c:forEach items="${EMPLOYEE_LIST}" var="employee">
		<tr>
			<td width="10%" align="center">${employee.employee_name}</td>
			<td width="90%" colspan="2">
				<div id="weeklyReport_${fn:substring(CURR_DATE.column_value, 0, 8)}${fn:substring(CURR_DATE.column_value, 8, 16)}_${employee.employee_id}"></div>
			</td>
		</tr>
		</c:forEach>
	</table>
</body>
</html>