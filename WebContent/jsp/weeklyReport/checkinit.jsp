<%@page contentType="text/html; charset=utf-8"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	$(function() {
		$('#date_select').combo({
			required : true,
			editable : false
		});
		$('#date_list').appendTo($('#date_select').combo('panel'));
		$('#date_select').combo('setValue', '${CURR_DATE.column_value}').combo('setText',
				'${CURR_DATE.column_desc}').combo('hidePanel');
		$('#date_list input').click( function() {
			var v = $(this).val();
			var s = $(this).next('span').text();
			$('#date_select').combo('setValue', v).combo('setText', s).combo('hidePanel');
			window.location = '<%=basePath%>/erp/weeklyReport/checkinit?SELECT_DATE=' 
		        + $(this).val()
		        + '&SELECT_PRJ='
		        + $('#prj_select').val();
		});
		$('#prj_select').bind('change', function () {
			window.location = '<%=basePath%>/erp/weeklyReport/checkinit?SELECT_DATE=' 
		        + $('#date_select').combo('getValue')
		        + '&SELECT_PRJ='
		        + $('#prj_select').val();
		});
		<c:forEach items="${EMPLOYEE_LIST}" var="employee">
		var url = '<%=basePath%>/erp/weeklyReport/checkedit?BEGIN_DATE=${fn:substring(CURR_DATE.column_value, 0, 8)}&END_DATE=${fn:substring(CURR_DATE.column_value, 8, 16)}&SELECT_EMPLOYEE=${employee.employee_id}';	
		var iframeS = "<iframe src='" 
			        + url 
			        + "' frameborder='0' marginheight='0' marginwidth='0' width='99%' height='300px'></iframe>"
		$('#weeklyReport_${fn:substring(CURR_DATE.column_value, 0, 8)}${fn:substring(CURR_DATE.column_value, 8, 16)}_${employee.employee_id}').html(iframeS);
		</c:forEach>
	});
</script>
</head>
<body>
	<select id="date_select" style="width: 350px"></select>
	<div id="date_list">
		<input type="radio" name="lang" value="">
		<span>---请选择---</span><br />
		<c:forEach items="${DATE_LIST}" var="dateList">
			<input type="radio" name="lang" value="${dateList.beginDate}${dateList.endDate}">
			<span>${dateList.year}年${dateList.month}月第${dateList.week}周${dateList.beginDateDesc}~${dateList.endDateDesc}</span>
			<br />
		</c:forEach>
	</div>
	<select id='prj_select'>
		<option value=""></option>
		<c:forEach items="${PRJ_LIST}" var="prjList">
			<option value="${prjList.prj_id}"
			<c:if test="${SELECT_PRJ ==  prjList.prj_id}">selected</c:if>
			>${prjList.prj_name}</option>
		</c:forEach>
	</select>
	<table width="100%" border="1" align="center">
		<c:forEach items="${EMPLOYEE_LIST}" var="employee">
		<tr>
			<td width="10%">${employee.employee_name}</td>
			<td width="90%" id="weeklyReport_${fn:substring(CURR_DATE.column_value, 0, 8)}${fn:substring(CURR_DATE.column_value, 8, 16)}_${employee.employee_id}">
			</td>
		</tr>	
		</c:forEach>
	</table>
</body>
</html>