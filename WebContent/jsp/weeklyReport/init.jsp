<%@page contentType="text/html; charset=utf-8"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	$(function() {
		$('#date_select').combo({
			required : true,
			editable : false
		});
		$('#date_list').appendTo($('#date_select').combo('panel'));
		$('#date_select').combo('setValue', '${CURR_DATE.column_value}').combo('setText',
				'${CURR_DATE.column_desc}').combo('hidePanel');
		$('#date_list input').click( function() {
			var v = $(this).val();
			var s = $(this).next('span').text();
			$('#date_select').combo('setValue', v).combo('setText', s).combo('hidePanel');
			window.location = '<%=basePath%>/erp/weeklyReport/init?SELECT_DATE=' + $(this).val();
		});
		var url = '<%=basePath%>/erp/weeklyReport/';	
		var editFlag = "";
		<c:if test="${empty WEEKLY_REPORT_RECORD}">
			url += 'edit?OPER_TYPE=NEW&';
			editFlag = "edit";
		</c:if>
		<c:if test="${!empty WEEKLY_REPORT_RECORD}">
			<c:if test="${WEEKLY_REPORT_RECORD.approval_flag == '0'}">	
				url += 'edit?OPER_TYPE=EDIT&';
				editFlag = "edit";
			</c:if>
			
			<c:if test="${WEEKLY_REPORT_RECORD.approval_flag == '1'}">	
				url += 'detail?';
				editFlag = "detail";
			</c:if>
		</c:if>
		url += 'BEGIN_DATE=${fn:substring(CURR_DATE.column_value, 0, 8)}&END_DATE=${fn:substring(CURR_DATE.column_value, 8, 16)}&SELECT_EMPLOYEE=${WEEKLY_REPORT_RECORD.employee_id}';
		var iframeS = "<iframe src='" 
            + url 
            + "' frameborder='0' marginheight='0' marginwidth='0' width='99%' height='500px'></iframe>"
		if (editFlag == "edit") {
            $('#weeklyReport_${fn:substring(CURR_DATE.column_value, 0, 8)}${fn:substring(CURR_DATE.column_value, 8, 16)}').html(iframeS);
		} else if (editFlag == "detail") {
            $('#weeklyReport_${fn:substring(CURR_DATE.column_value, 0, 8)}${fn:substring(CURR_DATE.column_value, 8, 16)}').load(url);
		}
	});
</script>
</head>
<body>
	<table width="100%" align="center">
		<tr>
			<td>
				<select id="date_select" style="width: 350px"></select>
				<div id="date_list">
					<input type="radio" name="lang" value="">
					<span>---请选择---</span><br />
					<c:forEach items="${DATE_LIST}" var="dateList">
						<input type="radio" name="lang" value="${dateList.beginDate}${dateList.endDate}">
						<span>${dateList.year}年${dateList.month}月第${dateList.week}周${dateList.beginDateDesc}~${dateList.endDateDesc}</span>
						<br />
					</c:forEach>
				</div>
			</td>
		</tr>
		<tr>
			<td id="weeklyReport_${fn:substring(CURR_DATE.column_value, 0, 8)}${fn:substring(CURR_DATE.column_value, 8, 16)}">
			</td>
		</tr>
	</table>
</body>
</html>