<%@page contentType="text/html; charset=utf-8"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>周报查询</title>
<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	$(function() {
		$('#employee_select').combo({
			required : true,
			editable : false
		});
		$('#employee_list').appendTo($('#employee_select').combo('panel'));
		<c:if test="${!empty SELECT_EMPLOYEE}">
			$('#employee_select').combo('setValue', '${SELECT_EMPLOYEE.employee_no}').combo('setText',
				'姓名：${SELECT_EMPLOYEE.employee_name}    工号：${SELECT_EMPLOYEE.employee_no}').combo('hidePanel');
		</c:if>
		$('#employee_list input').click( function() {
			var v = $(this).val();
			var s = $(this).next('span').text();
			$('#employee_select').combo('setValue', v).combo('setText',
							s).combo('hidePanel');
			window.location = '<%=basePath%>/erp/weeklyReport/batchView?EMPLOYEE_ID=' + $(this).val();
		});
		<c:forEach items="${WEEKLYREPORT_DATE_LIST}" var="dateList">
			$('#viewBtn${dateList.beginDate}${dateList.endDate}').bind('click', function () {
				var url = '<%=basePath%>/erp/weeklyReport/view?OPER_TYPE=VIEW&EMPLOYEE_ID=${SELECT_EMPLOYEE.employee_id}&BEGIN_DATE=${dateList.beginDate}&END_DATE=${dateList.endDate}&DATE_DESC=${dateList.beginDateDesc}~${dateList.endDateDesc}';
				window.showModalDialog(url, 'edit', 'dialogWidth:1000px;dialogHeight:700px;center:yes;help:yes;resizable:yes;status:yes');
			}).hide();
			$('#exportBtn${dateList.beginDate}${dateList.endDate}').bind('click', function () {
				$.ajax({
					type : "POST", 
					url : '<%=basePath%>/erp/weeklyReport/generatorOneExcel?EMPLOYEE_ID=${SELECT_EMPLOYEE.employee_id}',
					async : false,
					data: {
						'BEGIN_DATE' : '${dateList.beginDate}',
						'END_DATE' : '${dateList.endDate}'
					},
					success : function(returnStr) {
						var jsonReturnStr = JSON.parse(returnStr);
						var url = "<%=basePath%>/erp/weeklyReport/downloadExcel?EMPLOYEE_ID=${SELECT_EMPLOYEE.employee_id}"
						      + "&_FILE_NAME=" + jsonReturnStr.RETURN_INFO;
						var iTop = (window.screen.availHeight-30-200)/2; //获得窗口的垂直位置;
						var iLeft = (window.screen.availWidth-10-270)/2; //获得窗口的水平位置;
						var styleCss = 'height=160, width=270, top='+iTop+', left='+iLeft+', toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, status=no';
						window.open(url, '', styleCss);
					}
				});
			}).hide();
		</c:forEach>
		<c:forEach items="${WEEKLY_REPORT_RECORD}" var="recordList">
			$('#STATE_${recordList.begin_date}${recordList.end_date}').html("已填报");
			$('#viewBtn${recordList.begin_date}${recordList.end_date}').show();
			$('#exportBtn${recordList.begin_date}${recordList.end_date}').show();
		</c:forEach>
		$('#exportBtn').bind('click', function () {
			if (null == '${SELECT_EMPLOYEE.employee_id}' || '' == '${SELECT_EMPLOYEE.employee_id}') {
				alert("请先选择员工");
				return false;
			}
			$.ajax({
				type : "POST", 
				url : '<%=basePath%>/erp/weeklyReport/generatorExcel?EMPLOYEE_ID=${SELECT_EMPLOYEE.employee_id}',
				async : false,
				data: {
				},
				success : function(returnStr) {
					var jsonReturnStr = JSON.parse(returnStr);
					var url = "<%=basePath%>/erp/weeklyReport/downloadExcel?EMPLOYEE_ID=${SELECT_EMPLOYEE.employee_id}"
					      + "&_FILE_NAME=" + jsonReturnStr.RETURN_INFO;
					var iTop = (window.screen.availHeight-30-200)/2; //获得窗口的垂直位置;
					var iLeft = (window.screen.availWidth-10-270)/2; //获得窗口的水平位置;
					var styleCss = 'height=160, width=270, top='+iTop+', left='+iLeft+', toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, status=no';
					window.open(url, '', styleCss);
				}
			});
		});
		$('#batchExportBtn').bind('click', function () {
			var url = '<%=basePath%>/erp/weeklyReport/batchExport';
			window.showModalDialog(url, 'batchExport', 'dialogWidth:1000px;dialogHeight:700px;center:yes;help:yes;resizable:yes;status:yes');
		});
	});
</script>
</head>
<body>
	<br>
	<br>
	<table align="center">
		<tr>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<select id="employee_select" style="width: 300px"></select>
				<div id="employee_list">
					<div style="color: #99BBE8; background: #fafafa; padding: 5px;">---请选择---</div>
					<c:forEach items="${EMPLOYEE_LIST}" var="employee">
						<input type="radio" name="lang" value="${employee.employee_no}">
						<span>姓名：${employee.employee_name}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;工号：${employee.employee_no}</span>
						<br />
					</c:forEach>
				</div>
			</td>
			<td>
				<a href='#' class='easyui-linkbutton' id='exportBtn'>按人导出</a>
				<a href='#' class='easyui-linkbutton' id='batchExportBtn'>按周导出</a>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table border="1" width="600px" bordercolor="#7FB0DE" cellpadding="1"
					cellspacing="0" align="center">
					<tr>
						<td align="center">年份</td>
						<td align="center">月份</td>
						<td align="center">周</td>
						<td align="center">起始~截止日期</td>
						<td align="center">填写状态</td>
						<td align="center">操作</td>
					</tr>
					<c:forEach items="${WEEKLYREPORT_DATE_LIST}" var="dateList">
						<tr>
							<td name="${dateList.year}">${dateList.year}年</td>
							<td name="${dateList.month}">${dateList.month}月</td>
							<td name="${dateList.week}">第${dateList.week}周</td>
							<td id="${dateList.endDate}">${dateList.beginDateDesc}~${dateList.endDateDesc}</td>
							<td id="STATE_${dateList.beginDate}${dateList.endDate}" align="center">未填报</td>
							<td id="OPER_${dateList.beginDate}${dateList.endDate}" align="center">
								<a href='#' class='easyui-linkbutton' id='viewBtn${dateList.beginDate}${dateList.endDate}'>查看</a>
								<a href='#' class='easyui-linkbutton' id='exportBtn${dateList.beginDate}${dateList.endDate}'>导出</a>
							</td>
						</tr>
					</c:forEach>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>