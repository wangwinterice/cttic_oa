<%@page contentType="text/html; charset=utf-8"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>${DATE_DESC}周报展示</title>
<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	$(function() {
		<%
		for (int count = 1; count < 11; count++) {
		%>
			$('#TASK_ROW_<%=count%>').hide();	
			$('#PLAN_ROW_<%=count%>').hide();
		<%
			}
		%>
		<c:forEach items="${WEEKLY_REPORT_RESULT}" var="resultList">
			$("#${resultList.result_type}_${resultList.result_code}_${resultList.result_id}").html('${resultList.result_value}');
			<c:if test="${resultList.result_code == 'STATE'}">
				<c:forEach items="${WEEKLY_REPORT_STATE}" var="stateList">
					<c:if test="${resultList.result_value == stateList.column_value}">
						$("#${resultList.result_type}_${resultList.result_code}_${resultList.result_id}").html('${stateList.column_desc}');
					</c:if>
				</c:forEach>
			</c:if>
			<c:if test="${resultList.result_code == 'PRJ_ID'}">
				<c:forEach items="${PRJ_LIST}" var="prjList">
					<c:if test="${resultList.result_value == prjList.prj_id}">
						$("#${resultList.result_type}_${resultList.result_code}_${resultList.result_id}").html('${prjList.prj_name}');
					</c:if>
				</c:forEach>
			</c:if>
			$('#${resultList.result_type}_ROW_${resultList.result_id}').show();
			$('#${resultList.result_type}_ROW_${resultList.result_id}').show();
		</c:forEach>
		$('#closeBtn').bind('click', function () {
			window.close();
		});
	});
</script>
</head>
<body>
	<br>
	<table border="0" align="center">
		<tr>
			<td align="center">
				<h1>本周工作总结</h1>${DATE_DESC}
			</td>
		</tr>
		<tr>
			<td align="center">
				<table border="1" bordercolor="#7FB0DE" cellpadding="1"
					cellspacing="0" align="center">
					<tr>
						<td align="center" height="50px" width="30px" nowrap>WBS ID</td>
						<td align="center" height="50px" nowrap>参与项目</td>
						<td align="center" height="50px" width="480px" nowrap>任务描述</td>
						<td align="center" height="50px" width="60px" nowrap>状态</td>
						<td align="center" height="50px" width="60px" nowrap>开始时间</td>
						<td align="center" height="50px" width="60px" nowrap>完成时间</td>
						<td align="center" height="50px" width="170px" nowrap>备注</td>
					</tr>
						<%
							for (int count = 1; count < 11; count++) {
						%>
						<tr id="TASK_ROW_<%=count%>">
							<td align='center'><%=count%></td>
							<td id='TASK_PRJ_ID_<%=count%>'></td>
							<td id='TASK_TASK_DESC_<%=count%>'></td>
							<td id='TASK_STATE_<%=count%>'></td>
							<td id='TASK_BEGIN_TIME_<%=count%>'></td>
							<td id='TASK_END_TIME_<%=count%>'></td>
							<td id='TASK_OUTPUT_<%=count%>'></td>
						</tr>
						<%
							}
						%>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<table border="0" align="center">
		<tr>
			<td align="center">
				<h1>下周工作计划</h1>${PLAN_DATE_DESC}
			</td>
		</tr>
		<tr>
			<td align="center">
				<table border="1" bordercolor="#7FB0DE" cellpadding="1"
					cellspacing="0" align="center">
					<tr>
						<td align="center" height="50px" width="30px" nowrap>序号</td>
						<td align="center" height="50px" nowrap>参与项目</td>
						<td align="center" height="50px" width="480px" nowrap>计划描述</td>
						<td align="center" height="50px" width="60px" nowrap>依赖</td>
						<td align="center" height="50px" width="60px" nowrap>计划开始时间</td>
						<td align="center" height="50px" width="60px" nowrap>计划完成时间</td>
						<td align="center" height="50px" width="170px" nowrap>备注</td>
					</tr>
						<%
							for (int count = 1; count < 11; count++) {
						%>
						<tr id="PLAN_ROW_<%=count%>">
							<td align='center'><%=count%></td>
							<td id='PLAN_PRJ_ID_<%=count%>'></td>
							<td id='PLAN_PLAN_DESC_<%=count%>'></td>
							<td id='PLAN_DEPEND_ON_<%=count%>'></td>
							<td id='PLAN_PLAN_BEGIN_TIME_<%=count%>'></td>
							<td id='PLAN_PLAN_END_TIME_<%=count%>'></td>
							<td id='PLAN_OUTPUT_<%=count%>'></td>
						</tr>
						<%
							}
						%>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<table width="90%">
		<tr>
			<td align="right"><a href="#" class="easyui-linkbutton"
				id="closeBtn">&nbsp;&nbsp;关&nbsp;&nbsp;&nbsp;&nbsp;闭&nbsp;&nbsp;</a>
			</td>
		</tr>
	</table>
</body>
</html>