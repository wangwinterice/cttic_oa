<%@page contentType="text/html; charset=utf-8"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>周报查询</title>
<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	$(function() {
		$('#exportBtn').bind('click', function () {
			$.ajax({
				type : "POST", 
				url : '<%=basePath%>/erp/weeklyReport/batchGeneratorExcel',
				async : false,
				data: {
					'BEGIN_DATE' : '${dateList.beginDate}',
					'END_DATE' : '${dateList.endDate}'
				},
				success : function(returnStr) {
					var jsonReturnStr = JSON.parse(returnStr);
					var url = "<%=basePath%>/erp/weeklyReport/downloadExcel?EMPLOYEE_ID=${SELECT_EMPLOYEE.employee_id}"
					      + "&_FILE_NAME=" + jsonReturnStr.RETURN_INFO;
					var iTop = (window.screen.availHeight-30-200)/2; //获得窗口的垂直位置;
					var iLeft = (window.screen.availWidth-10-270)/2; //获得窗口的水平位置;
					var styleCss = 'height=160, width=270, top='+iTop+', left='+iLeft+', toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, status=no';
					window.open(url, '', styleCss);
				}
			});
		});
	});
	function batchExport(beginDate, endDate) {
		$.ajax({
			type : "POST", 
			url : '<%=basePath%>/erp/weeklyReport/batchGeneratorExcel',
			async : false,
			data: {
				'BEGIN_DATE' : beginDate,
				'END_DATE' : endDate
			},
			success : function(returnStr) {
				var jsonReturnStr = JSON.parse(returnStr);
				var url = "<%=basePath%>/erp/weeklyReport/downloadExcel?"
				      + "_FILE_NAME=" + jsonReturnStr.RETURN_INFO;
				var iTop = (window.screen.availHeight-30-200)/2; //获得窗口的垂直位置;
				var iLeft = (window.screen.availWidth-10-270)/2; //获得窗口的水平位置;
				var styleCss = 'height=160, width=270, top='+iTop+', left='+iLeft+', toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, status=no';
				window.open(url, '', styleCss);
			}
		});
	}
</script>
</head>
<body>
	<br>
	<br>
	<table align="center">
		<tr>
			<td colspan="2">
				<table border="1" width="600px" bordercolor="#7FB0DE" cellpadding="1"
					cellspacing="0" align="center">
					<tr>
						<td align="center">年份</td>
						<td align="center">月份</td>
						<td align="center">起始~截止日期</td>
						<td align="center">操作</td>
					</tr>
					<c:forEach items="${WEEKLYREPORT_DATE_LIST}" var="dateList">
						<tr>
							<td name="${dateList.year}">${dateList.year}</td>
							<td name="${dateList.month}">${dateList.month}</td>
							<td id="${dateList.endDate}">${dateList.beginDateDesc}~${dateList.endDateDesc}</td>
							<td id="OPER_${dateList.beginDate}${dateList.endDate}" align="center">
								<a href='#' class='easyui-linkbutton' id='exportBtn${dateList.beginDate}${dateList.endDate}' onClick="batchExport('${dateList.beginDate}', '${dateList.endDate}')">导出</a>
							</td>
						</tr>
					</c:forEach>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>