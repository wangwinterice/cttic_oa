<%@page contentType="text/html; charset=utf-8"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>${DATE_DESC}周报填写</title>
<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	$(function() {
		$('#TASK_BEGIN_TIME').datebox('setValue', '${TASK_BEGIN_DATE}');
		$('#TASK_END_TIME').datebox('setValue', '${TASK_END_DATE}');
		$('#PLAN_BEGIN_TIME').datebox('setValue', '${PLAN_BEGIN_DATE}');
		$('#PLAN_END_TIME').datebox('setValue', '${PLAN_END_DATE}');
		<%
		for (int count = 2; count < 11; count++) {
		%>
			$('#TASK_ROW_<%=count%>').hide();	
			$('#PLAN_ROW_<%=count%>').hide();
		<%
		}
		%>
		// 处理任务增删行
		var taskRowCount = 0;
		<%
		for (int count = 1; count < 11; count++) {
		%>
			if ($('#TASK_ROW_<%=count%>').css("display") == "table-row") {
				taskRowCount ++;
			}
			$('#TASK_PRJ_ID_<%=count%>').change(function () {
				if ($('#TASK_PRJ_ID_<%=count%>').val() == '20160521101325') {
					$('#TD_TASK_DESC_<%=count%>').html('<select id="TASK_TASK_DESC_<%=count%>"><option value=""></option><c:forEach items="${PRJ_LIST}" var="prjList"><option value="${prjList.prj_id}">${prjList.prj_name}</option></c:forEach></select></td>');
					
				} else {
					$('#TD_TASK_DESC_<%=count%>').html('<textarea rows="5" cols="30" id="TASK_TASK_DESC_<%=count%>"></textarea>');
				}
			});
		<%
		}
		%>
		if (taskRowCount == 1) {
			$('#DEL_TASK_ROW_' + taskRowCount).hide();
		} else {
			for (var j = 0; j < taskRowCount; j ++) {
				$('#DEL_TASK_ROW_' + j).hide();
			}
			if (taskRowCount == 10) {
				$('#ADD_TASK_ROW').hide();
			}
		}
		// 处理计划增删行
		var planRowCount = 0;
		<%
		for (int count = 1; count < 11; count++) {
		%>
			if ($('#PLAN_ROW_<%=count%>').css("display") == "table-row") {
				planRowCount ++;
			}
		<%
		}
		%>
		if (planRowCount == 1) {
			$('#DEL_PLAN_ROW_' + planRowCount).hide();
		} else {
			for (var j = 0; j < planRowCount; j ++) {
				$('#DEL_PLAN_ROW_' + j).hide();
			}
			if (planRowCount == 10) {
				$('#ADD_PLAN_ROW').hide();
			}
		}
		<c:if test="${'EDIT' == OPER_TYPE}">
			<c:forEach items="${WEEKLY_REPORT_RESULT}" var="resultList">
				$("#${resultList.result_type}_${resultList.result_code}_${resultList.result_id}").val('${resultList.result_value}'.split("<br>").join("\r\n"));
				$("#${resultList.result_type}_${resultList.result_code}_${resultList.result_id}").change();
				$('#${resultList.result_type}_ROW_${resultList.result_id}').show();	
			</c:forEach>
		</c:if>
		$('#saveBtn').bind('click', function () {
			var paramJson = {};
			var paramJsonAttr = new Array();
			var taskJson = {};
			<%for (int count = 1; count < 11; count++) {%>
				taskJson = {};
				taskJson['WBS_ID'] = $('#TASK_WBS_ID_<%=count%>').val();
				taskJson['PRJ_ID'] = $('#TASK_PRJ_ID_<%=count%>').val();
				taskJson['TASK_DESC'] = $('#TASK_TASK_DESC_<%=count%>').val();
				taskJson['TASK_WORK_TIME_1'] = $('#TASK_WORK_TIME_1_<%=count%>').val();
				taskJson['TASK_WORK_TIME_2'] = $('#TASK_WORK_TIME_2_<%=count%>').val();
				taskJson['TASK_WORK_TIME_3'] = $('#TASK_WORK_TIME_3_<%=count%>').val();
				taskJson['TASK_WORK_TIME_4'] = $('#TASK_WORK_TIME_4_<%=count%>').val();
				taskJson['TASK_WORK_TIME_5'] = $('#TASK_WORK_TIME_5_<%=count%>').val();
				taskJson['TASK_WORK_TIME_6'] = $('#TASK_WORK_TIME_6_<%=count%>').val();
				taskJson['TASK_WORK_TIME_7'] = $('#TASK_WORK_TIME_7_<%=count%>').val();
				paramJsonAttr.push(taskJson);
			<%}%>
			//alert(JSON.stringify(paramJsonAttr));
			paramJson['TASK'] = paramJsonAttr;
			paramJsonAttr = new Array();
			var planJson = {};
			<%for (int count = 1; count < 11; count++) {%>
				planJson = {};
				planJson['SEQ_NO'] = $('#PLAN_SEQ_NO_<%=count%>').val();
				planJson['PRJ_ID'] = $('#PLAN_PRJ_ID_<%=count%>').val();
				planJson['PLAN_DESC'] = $('#PLAN_PLAN_DESC_<%=count%>').val();
				paramJsonAttr.push(planJson);
			<%}%>
			paramJson['PLAN'] = paramJsonAttr;
			paramJson['TASK_BEGIN_DATE'] = $('#TASK_BEGIN_TIME').datebox('getValue');
			paramJson['TASK_END_DATE'] = $('#TASK_END_TIME').datebox('getValue');
			paramJson['PLAN_BEGIN_DATE'] = $('#PLAN_BEGIN_TIME').datebox('getValue');
			paramJson['PLAN_END_DATE'] = $('#PLAN_END_TIME').datebox('getValue');
			//alert(JSON.stringify(paramJson));
			$.ajax({ 
				type: "POST", 
				url: "<%=basePath%>/erp/weeklyReport/save?OPER_TYPE=${OPER_TYPE}&BEGIN_DATE=${BEGIN_DATE}&END_DATE=${END_DATE}",
				data : {"INPUT_PARAM" : JSON.stringify(paramJson)},
				async : false,
				success : function(returnStr) {
					var jsonReturnStr = JSON.parse(returnStr);
					if (jsonReturnStr.STATUS_CODE == '1') {
						alert(jsonReturnStr.STATUS_INFO);
						//window.close();
						window.location = '<%=basePath%>/erp/weeklyReport/edit?OPER_TYPE=EDIT&BEGIN_DATE=${BEGIN_DATE}&END_DATE=${END_DATE}';
					} else {
						alert(jsonReturnStr.STATUS_INFO);
					}
				}
			});
		});
		$('#ADD_TASK_ROW').bind('click', function () {
			var count = 0;
			<%
			for (int count = 1; count < 11; count++) {
			%>
				if ($('#TASK_ROW_<%=count%>').css("display") == "table-row") {
					count ++;
				}
			<%
			}
			%>
			if (count == 9) {
				$('#ADD_TASK_ROW').hide();
			}
			$('#TASK_ROW_' + (count + 1)).show();
			for (var i = 0; i < count; i ++) {
				$('#DEL_TASK_ROW_' + count).hide();
			}
		});
		
		<%
		for (int count = 1; count < 11; count++) {
		%>
			$('#DEL_TASK_ROW_<%=count%>').bind('click', function () {
				if(confirm("确定要删除该行吗，一旦删除该行已填写数据将被清空？")) {
					$('#TASK_ROW_<%=count%>').hide();
					$('#TASK_PRJ_ID_<%=count%>').val('');
					$('#TASK_TASK_DESC_<%=count%>').val('');
					$('#TASK_WORK_TIME_1_<%=count%>').val(0);
					$('#TASK_WORK_TIME_2_<%=count%>').val(0);
					$('#TASK_WORK_TIME_3_<%=count%>').val(0);
					$('#TASK_WORK_TIME_4_<%=count%>').val(0);
					$('#TASK_WORK_TIME_5_<%=count%>').val(0);
					$('#TASK_WORK_TIME_6_<%=count%>').val(0);
					$('#TASK_WORK_TIME_7_<%=count%>').val(0);
					$('#ADD_TASK_ROW').show();
					$('#DEL_TASK_ROW_<%=count-1%>').show();
					var taskRowCount = 0;
					<%
					for (int rowCount = 1; rowCount < 11; rowCount ++) {
					%>
						if ($('#TASK_ROW_<%=rowCount%>').css("display") == "table-row") {
							taskRowCount ++;
						}
					<%
					}
					%>
					if (taskRowCount == 1) {
						$('#DEL_TASK_ROW_1').hide();
					}
				}
			});
		<%
		}
		%>
		$('#ADD_PLAN_ROW').bind('click', function () {
			var count = 0;
			<%
			for (int count = 1; count < 11; count++) {
			%>
				if ($('#PLAN_ROW_<%=count%>').css("display") == "table-row") {
					count ++;
				}	
			<%
			}
			%>
			if (count == 9) {
				$('#ADD_PLAN_ROW').hide();
			}
			$('#PLAN_ROW_' + (count + 1)).show();
			for (var i = 0; i < count; i ++) {
				$('#DEL_PLAN_ROW_' + count).hide();
			}
		});
		
		<%
		for (int count = 1; count < 11; count++) {
		%>
			$('#DEL_PLAN_ROW_<%=count%>').bind('click', function () {
				if(confirm("确定要删除该行吗，一旦删除该行已填写数据将被清空？")) {
					$('#PLAN_ROW_<%=count%>').hide();
					$('#PLAN_PRJ_ID_<%=count%>').val('');
					$('#PLAN_PLAN_DESC_<%=count%>').val('');
					$('#ADD_PLAN_ROW').show();
					$('#DEL_PLAN_ROW_<%=count-1%>').show();
					var planRowCount = 0;
					<%
					for (int rowCount = 1; rowCount < 11; rowCount ++) {
					%>
						if ($('#PLAN_ROW_<%=rowCount%>').css("display") == "table-row") {
							planRowCount ++;
						}
					<%
					}
					%>
					if (planRowCount == 1) {
						$('#DEL_PLAN_ROW_1').hide();
					}
				}
			});
		<%
		}
		%>
	});
	
	function myformatter(date){
		var y = date.getFullYear();
		var m = date.getMonth()+1;
		var d = date.getDate();
		return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
	}
	function myparser(s){
		if (!s) return new Date();
		var ss = (s.split('-'));
		var y = parseInt(ss[0],10);
		var m = parseInt(ss[1],10);
		var d = parseInt(ss[2],10);
		if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
			return new Date(y,m-1,d);
		} else {
			return new Date();
		}
	}
	function checkVal(obj) {
		if (obj.value > 8) {
			alert("工时不能大于8小时");
			obj.focus();
			return;
		}
	}
</script>
</head>
<body>
	<table border="0" align="center">
		<tr>
			<td align="center" valign="top">
				<table>
					<tr>
						<td><h3>本周总结</h3></td>
						<td>
							<input id='TASK_BEGIN_TIME' class='easyui-datebox' style="width:140px" data-options="formatter:myformatter,parser:myparser" readonly/>
							~
							<input id='TASK_END_TIME' class='easyui-datebox' style="width:140px" data-options="formatter:myformatter,parser:myparser" readonly/>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center">
							<table border="1" bordercolor="#7FB0DE" cellpadding="1"
								cellspacing="0" align="center">
								<tr height="30px">
									<td rowspan="2" align="center" nowrap>WBS ID</td>
									<td rowspan="2" align="center" nowrap>参与项目</td>
									<td rowspan="2" align="center" nowrap>任务描述</td>
									<td colspan="7" align="center" width="280px" nowrap>工时填报</td>
									<td rowspan="2" align="center" nowrap><a href="#" id="ADD_TASK_ROW" title="添加一行"><img alt="" src="<%=basePath%>/image/add.gif"></a></td>
								</tr>
								<tr height="15px">
									<c:forEach items="${TASK_DATE_LIST}" var="taskDate">
									<td align="center">${taskDate}</td>
									</c:forEach>
								</tr>
									<%
										for (int count = 1; count < 11; count++) {
									%>
									<tr id="TASK_ROW_<%=count%>">
										<td align='center'><input type='hidden' id='TASK_WBS_ID_<%=count%>' value='<%=count%>' /><%=count%></td>
										<td><select id='TASK_PRJ_ID_<%=count%>'><option value=""></option><c:forEach items="${PRJ_LIST}" var="prjList"><option value="${prjList.prj_id}">${prjList.prj_name}</option></c:forEach></select></td>
										<td id="TD_TASK_DESC_<%=count%>"><textarea rows='5' cols='30' id='TASK_TASK_DESC_<%=count%>'></textarea></td>
										<td align="center"><input type="text" id='TASK_WORK_TIME_1_<%=count%>' value="0" style="width:35px;height:35px;border:0;" onBlur="checkVal(this)" /></td>
										<td align="center"><input type="text" id='TASK_WORK_TIME_2_<%=count%>' value="0" style="width:35px;height:35px;border:0;" onBlur="checkVal(this)" /></td>
										<td align="center"><input type="text" id='TASK_WORK_TIME_3_<%=count%>' value="0" style="width:35px;height:35px;border:0;" onBlur="checkVal(this)" /></td>
										<td align="center"><input type="text" id='TASK_WORK_TIME_4_<%=count%>' value="0" style="width:35px;height:35px;border:0;" onBlur="checkVal(this)" /></td>
										<td align="center"><input type="text" id='TASK_WORK_TIME_5_<%=count%>' value="0" style="width:35px;height:35px;border:0;" onBlur="checkVal(this)" /></td>
										<td align="center"><input type="text" id='TASK_WORK_TIME_6_<%=count%>' value="0" style="width:35px;height:35px;border:0;" onBlur="checkVal(this)" /></td>
										<td align="center"><input type="text" id='TASK_WORK_TIME_7_<%=count%>' value="0" style="width:35px;height:35px;border:0;" onBlur="checkVal(this)" /></td>
										<td align="center"><a href="#" id="DEL_TASK_ROW_<%=count%>" title="删除该行"><img alt="" src="<%=basePath%>/image/clean.gif"></a></td>
									</tr>
									<%
										}
									%>
							</table>
						</td>
					</tr>
				</table>
			</td>
			<td align="center" valign="top">
				<table>
					<tr>
						<td><h3>下周计划</h3></td>
						<td>
							<input id='PLAN_BEGIN_TIME' class='easyui-datebox' style="width:90px" data-options="formatter:myformatter,parser:myparser" readonly/>
							~
							<input id='PLAN_END_TIME' class='easyui-datebox' style="width:90px" data-options="formatter:myformatter,parser:myparser" readonly/>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center">
							<table border="1" bordercolor="#7FB0DE" cellpadding="1"
								cellspacing="0" align="center">
								<tr height="45px">
									<td align="center" nowrap>序号</td>
									<td align="center" nowrap>参与项目</td>
									<td align="center" nowrap>计划描述</td>
									<td align="center" nowrap><a href="#" id="ADD_PLAN_ROW" title="添加一行"><img alt="" src="<%=basePath%>/image/add.gif"></a></td>
								</tr>
									<%
										for (int count = 1; count < 11; count++) {
									%>
									<tr id="PLAN_ROW_<%=count%>">
										<td align='center'><input type='hidden' id='PLAN_SEQ_NO_<%=count%>' value='<%=count%>' /><%=count%></td>
										<td><select id='PLAN_PRJ_ID_<%=count%>'><option value=""></option><c:forEach items="${PRJ_LIST}" var="prjList"><option value="${prjList.prj_id}">${prjList.prj_name}</option></c:forEach></select></td>
										<td><textarea rows='5' cols='30' id='PLAN_PLAN_DESC_<%=count%>'></textarea></td>
										<td align="center"><a href="#" id="DEL_PLAN_ROW_<%=count%>" title="删除该行"><img alt="" src="<%=basePath%>/image/clean.gif"></a></td>
									</tr>
									<%
										}
									%>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table width="90%">
		<tr>
			<td align="right"><a href="#" class="easyui-linkbutton"
				id="saveBtn">保&nbsp;存</a>
			</td>
		</tr>
	</table>
</body>
</html>