<%@page contentType="text/html; charset=utf-8"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
	<title></title>
	<%@ include file="/js/link.jsp"%>
	<script type="text/javascript">
		$(function() {
			$('#employee_select').combo({
				required : true,
				editable : false
			});
			$('#employee_list').appendTo($('#employee_select').combo('panel'));
			<c:if test="${!empty SELECT_EMPLOYEE}">
				$('#employee_select').combo('setValue', '${SELECT_EMPLOYEE.employee_no}')
				                     .combo('setText', '${SELECT_EMPLOYEE.employee_name}')
				                     .combo('hidePanel');
			</c:if>
			$('#employee_list input').click( function() {
				var v = $(this).val();
				var s = $(this).next('span').text();
				$('#employee_select').combo('setValue', v).combo('setText',
								s).combo('hidePanel');
			});
			$("#selectBtn").bind('click', function () {
				$.ajax({
					type: "POST", 
					url: "<%=basePath%>/erp/project/getEmployee",
					data : 
					{
						prj_id : $('#PRJ_ID').val(),
					},
					async : false,
					success : function(returnStr) {
						var jsonReturnStr = JSON.parse(returnStr);
						if (jsonReturnStr.STATUS_CODE == '1') {
							var signEmployee = jsonReturnStr.SIGN_EMPLOYEE;
							var unsignEmployee = jsonReturnStr.UNSIGN_EMPLOYEE;
							var _html = "<table border='0' style='width:100%'>";
							if (signEmployee.length > 0) {
								_html += "<tr><td colspan='6'>已选人员：</td></tr>";
							}
							var cnt = 0;
							$.each(signEmployee, function (i, item) {
								if (cnt == 0) {
									_html += "<tr>";
								}
								_html += "<td><label><input type='checkbox' name='sign_employee' value='" 
								       + item.employee_id + "' checked />" + item.employee_name
								       + "</label>&nbsp;</td>";
								cnt ++;
						        if (cnt == 9) {
									_html += "</tr>";
									cnt = 0;
								}
							});
							_html += "<tr><td colspan='8'>可选人员：</td></tr>";
							var cnt = 0;
							$.each(unsignEmployee, function (i, item) {
								if (cnt == 0) {
									_html += "<tr>";
								}
								_html += "<td><label><input type='checkbox' name='sign_employee' value='" 
								       + item.employee_id + "' />" + item.employee_name
								       + "</label>&nbsp;</td>";
						        cnt ++;
						        if (cnt == 9) {
									_html += "</tr>";
									cnt = 0;
								}
							});
							_html += "</table>";
							$("#div_sign").html(_html);
						} else {
							alert(jsonReturnStr.STATUS_INFO);
						}
					}
				});
			});
			$('#saveBtn').bind('click', function () {
				var prjName = $('#prj_name').val();
				if ("" == prjName) {
					alert("项目名称不能为空");
					return;
				}
				var signEmployee = new Array();
				$(":checkbox[name='sign_employee']").each(function(i, obj){
					if (obj.checked == true) {
						signEmployee.push(obj.value);
					}
				});
				$.ajax({
					type: "POST", 
					url: "<%=basePath%>/erp/project/save",
					data : 
					{
						OPER_TYPE : $('#OPER_TYPE').val(),
						prj_id : $('#PRJ_ID').val(),
						prj_code : $('#prj_code').val(),
						prj_name : prjName,
						manager_employee_id : $('#employee_select').combo('getValue'),
						sign_employee : signEmployee.join(",")
					},
					async : false,
					success : function(returnStr) {
						var jsonReturnStr = JSON.parse(returnStr);
						if (jsonReturnStr.STATUS_CODE == '1') {
							alert(jsonReturnStr.STATUS_INFO);
							window.location = "<%=basePath%>/erp/project/init";
						} else {
							alert(jsonReturnStr.STATUS_INFO);
						}
					}
				});
			});
		});
		
		function editEvent(prjId) {
			$.ajax({
				type: "POST", 
				url: "<%=basePath%>/erp/project/get",
				data : 
				{
					prj_id : prjId
				},
				async : false,
				success : function(returnStr) {
					var jsonReturnStr = JSON.parse(returnStr);
					if (jsonReturnStr.STATUS_CODE == '1') {
						//alert(JSON.stringify(returnStr));
						$('#OPER_TYPE').val('EDIT');
						var prjInfo = jsonReturnStr.RETURN_INFO;
						$('#PRJ_ID').val(prjInfo.prj_id);
						$('#prj_code').val(prjInfo.prj_code);
						$('#prj_name').val(prjInfo.prj_name);
						$('#employee_select').combo('setValue', prjInfo.manager_employee_id)
						                     .combo('setText', prjInfo.manager_employee_name);
						$('#prj_employee').val(prjInfo.employeeNames);
					} else {
						alert(jsonReturnStr.STATUS_INFO);
					}
				}
			});
		}
		
		function closeEvent(prjId) {
			$.ajax({
				type: "POST", 
				url: "<%=basePath%>/erp/project/save",
				data : 
				{
					OPER_TYPE : 'CLOSE',
					prj_id : prjId
				},
				async : false,
				success : function(returnStr) {
					var jsonReturnStr = JSON.parse(returnStr);
					if (jsonReturnStr.STATUS_CODE == '1') {
						alert(jsonReturnStr.STATUS_INFO);
						window.location = "<%=basePath%>/erp/project/init";
					} else {
						alert(jsonReturnStr.STATUS_INFO);
					}
				}
			});
		}
	</script>
	</head>
	<body>
	    <br>
		<table border="1" bordercolor="#7FB0DE" cellpadding="1"
			cellspacing="0" align="center" width="800px" class="content">
			<tr>
				<td class="tabletitle" height="50px">项目编号</td>
				<td class="tabletitle">项目名称</td>
				<td class="tabletitle">项目管理人</td>
				<td class="tabletitle">项目参与人员</td>
				<td class="tabletitle">状态</td>
				<td class="tabletitle">操作</td>
			</tr>
			<c:forEach items="${PROJECT_LIST}" var="project">
				<tr>
					<td>${project.prj_code}</td>
					<td>${project.prj_name}</td>
					<td>${project.manager_employee_name}</td>
					<td>${project.employeeNames}</td>
					<td>${project.state_desc}</td>
					<td align="center">
						<c:if test="${project.state == '0'}">
							<a href='#' class='easyui-linkbutton' onClick="editEvent('${project.prj_id}')">修改</a>
							<a href='#' class='easyui-linkbutton' onClick="closeEvent('${project.prj_id}')">失效</a>
						</c:if>
					</td>
				</tr>
			</c:forEach>
		</table>
		<input type="hidden" id="OPER_TYPE" value="ADD" />
		<input type="hidden" id="PRJ_ID" value="0" />
		<table style="width:100%">
			<tr>
				<td>
					<table border="1" bordercolor="#7FB0DE" cellpadding="1"
						cellspacing="0" align="center" width="800px" class="content">
						<tr>
							<td class="tdtitle" height="50px">项目编码：</td>
							<td>
								<input type="text" id="prj_code" />
							</td>
							<td class="tdtitle">项目名称：</td>
							<td>
								<input type="text" id="prj_name" />
							</td>
						</tr>
						<tr>
							<td class="tdtitle" height="50px">
								项目负责人：
							</td>
							<td>
								<select id="employee_select" style="width: 300px"></select>
								<div id="employee_list">
									<input type="radio" name="lang" value="">
									<span>---请选择---</span><br />
									<c:forEach items="${EMPLOYEE_LIST}" var="employee">
										<input type="radio" name="lang" value="${employee.employee_no}">
										<span>姓名：${employee.employee_name}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;工号：${employee.employee_no}</span>
										<br />
									</c:forEach>
								</div>			
							</td>
							<td class="tdtitle" align="center">项目参与人员：</td>
							<td>
								<textarea rows="3" cols='60' id="prj_employee" readonly></textarea>
								<a href="#" class="easyui-linkbutton" id="selectBtn">选择</a>
								<div id="div_sign"></div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="right">
					<a href="#" class="easyui-linkbutton" id="saveBtn">提&nbsp;交</a>
				</td>
			</tr>
		</table>
	</body>
</html>
