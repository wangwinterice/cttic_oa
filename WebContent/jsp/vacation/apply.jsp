<%@page contentType="text/html; charset=utf-8"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	function getDays(strDateStart, strDateEnd){
	   var strSeparator = "-"; //日期分隔符
	   var oDate1;
	   var oDate2;
	   var iDays;
	   oDate1= strDateStart.split(strSeparator);
	   oDate2= strDateEnd.split(strSeparator);
	   var strDateS = new Date(oDate1[0] + "-" + oDate1[1] + "-" + oDate1[2]);
	   var strDateE = new Date(oDate2[0] + "-" + oDate2[1] + "-" + oDate2[2]);
	   
	   if (strDateS.getTime() > strDateE.getTime()) {
		   alert("[起始时间]不能大于[截止时间]");
		   return 0;
	   }
	   
	   iDays = parseInt(Math.abs(strDateS - strDateE ) / 1000 / 60 / 60 /24);//把相差的毫秒数转换为天数 

	   return iDays + 1;
	}
	
	function getNumberOfDays() {
		$(':radio[name=SELECT_DAY_NUMBER]').each(function (i, obj) {
			if (obj.checked) {
				$('#NUMBER_OF_DAYS').val($(obj).val());
			}
		});
	}
	function setDefault() {
		var beginDate = $('#BEGIN_DATE').datebox('getValue');
		if ("" == beginDate) {
			//alert("[起始时间]不能为空");
			return;
		}
		var endDate = $('#END_DATE').datebox('getValue');
		if ("" == endDate) {
			//alert("[截止时间]不能为空");
			return;
		}
		var numberOfDays = getDays(beginDate, endDate);
		var type = $('#TYPE').val();
		if (type == '22' || type == '23' || type == '32' || type == '33') {
			$('#END_DATE').datebox('setValue', beginDate);
			$('#NUMBER_OF_DAYS').val('0.5');
			$('#SPAN_NUMBER_OF_DAYS').html('<font color="red" size="5">0.5天</font>');
		} else {
			$('#NUMBER_OF_DAYS').val(numberOfDays);
			$('#SPAN_NUMBER_OF_DAYS').html('<font color="red" size="5">' + numberOfDays + '天</font>');
		}
		$('#saveBtn').show();
	}
	$(function() {
		$('#BEGIN_DATE').datebox({
		    onSelect: function(date){
		    	setDefault();
		    }
		});
		$('#END_DATE').datebox({
		    onSelect: function(date){
		    	setDefault();
		    }
		});
		$('#saveBtn').bind('click', function () {
			var paramJson = {};
			var beginDate = $('#BEGIN_DATE').datebox('getValue');
			if ("" == beginDate) {
				alert("[起始时间]不能为空");
				return;
			}
			var endDate = $('#END_DATE').datebox('getValue');
			if ("" == endDate) {
				alert("[截止时间]不能为空");
				return;
			}
			var place = "";
			$(":radio[name=PLACE]").each(function(i, obj){
				if (obj.checked) {
					place = $(obj).val();
				}
			});
			if ("" == place) {
				alert("[休假地]不能为空");
				return;
			}
			var reason = $('#REASON').val();
			if ("" == reason) {
				alert("[休假原因]不能为空");
				return;
			}
			paramJson['RECORD_ID'] = $('#RECORD_ID').val();
			paramJson['TYPE'] = $('#TYPE').val();
			paramJson['BEGIN_DATE'] = beginDate;
			paramJson['END_DATE'] = endDate;
			paramJson['NUMBER_OF_DAYS'] = $('#NUMBER_OF_DAYS').val();
			paramJson['PLACE'] = place;
			paramJson['REASON'] = reason;
			//alert(JSON.stringify(paramJson));
			$.ajax({
				type: "POST", 
				url: "<%=basePath%>/erp/vacation/save?OPER_TYPE=" + $('#OPER_TYPE').val(),
				data : paramJson,
				async : false,
				success : function(returnStr) {
					var jsonReturnStr = JSON.parse(returnStr);
					if (jsonReturnStr.STATUS_CODE == '1') {
						alert(jsonReturnStr.STATUS_INFO);
						window.location = "<%=basePath%>/erp/vacation/apply";
					} else {
						alert(jsonReturnStr.STATUS_INFO);
					}
				}
			});
		}).hide();
	});
	
	function myformatter(date){
		var y = date.getFullYear();
		var m = date.getMonth()+1;
		var d = date.getDate();
		return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
	}
	function myparser(s){
		if (!s) return new Date();
		var ss = (s.split('-'));
		var y = parseInt(ss[0],10);
		var m = parseInt(ss[1],10);
		var d = parseInt(ss[2],10);
		if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
			return new Date(y,m-1,d);
		} else {
			return new Date();
		}
	}
	
	function onSelect(date) {
		$('#NUMBER_OF_DAYS').val();
		$('#SPAN_NUMBER_OF_DAYS').html('');
		$('#saveBtn').hide();
	}
	
	function editEvent(recordId) {
		$('#BEGIN_DATE').datebox('setValue', $('#' + recordId + '_BEGIN_DATE').val());
		$('#END_DATE').datebox('setValue', $('#' + recordId + '_END_DATE').val());
		$('#NUMBER_OF_DAYS').val($('#' + recordId + '_NUMBER_OF_DAYS').val());
		$('#SPAN_NUMBER_OF_DAYS').html('<font color="red" size="5">' + $('#NUMBER_OF_DAYS').val() + '天</font>');
		$(":radio[name=PLACE]").each(function(i, obj){
			if ($('#' + recordId + '_PLACE').val() == $(obj).val()) {
				obj.checked = true;
			}
		});
		$('#REASON').val($('#' + recordId + '_REASON').val().split("<br>").join("\r\n"));
		$('#saveBtn').show();
		$('#OPER_TYPE').val('EDIT');
		$('#RECORD_ID').val(recordId);
	}
	
	function cancelEvent(recordId) {
		$.ajax({
			type: "POST", 
			url: "<%=basePath%>/erp/vacation/save?OPER_TYPE=CANCEL",
			data : {'RECORD_ID' : recordId},
			async : false,
			success : function(returnStr) {
				var jsonReturnStr = JSON.parse(returnStr);
				if (jsonReturnStr.STATUS_CODE == '1') {
					alert(jsonReturnStr.STATUS_INFO);
					window.location = "<%=basePath%>/erp/vacation/apply";
				} else {
					alert(jsonReturnStr.STATUS_INFO);
				}
			}
		});
	}
</script>
</head>
<body>
	<br>
	<br><input type="hidden" id="OPER_TYPE" value="ADD" />
	<input type="hidden" id="RECORD_ID" value="" />
	<table border="1" bordercolor="#7FB0DE" cellpadding="1"
		cellspacing="0" align="center" width="800px" class="content">
		<tr>
			<td class="tdtitle" height="50px">请假类型：</td>
			<td>
				<select id="TYPE" name="TYPE" onChange="setDefault()">
					<c:forEach items="${VACATION_TYPE}" var="vacationType">
						<option value="${vacationType.column_value}">
							${vacationType.column_desc}
						</option>
					</c:forEach>
				</select>
			</td>
			<td class="tdtitle">起始截止时间：</td>
			<td><input id='BEGIN_DATE' class='easyui-datebox'
					data-options="formatter:myformatter,parser:myparser,onSelect:onSelect" />~<input id='END_DATE' class='easyui-datebox'
					data-options="formatter:myformatter,parser:myparser,onSelect:onSelect" /></td>
		</tr>
		<tr>
			<td class="tdtitle" height="50px">请假天数：<input type="hidden" id="NUMBER_OF_DAYS" value="" /></td>
			<td><span id="SPAN_NUMBER_OF_DAYS"></span></td>
			<td class="tdtitle" align="center">休假地：</td>
			<td>
				<c:forEach items="${VACATION_PLACE}" var="vacationPlace">
					<input type="radio" name="PLACE" value="${vacationPlace.column_value}" />
					<span>${vacationPlace.column_desc}</span>&nbsp;&nbsp;
				</c:forEach>
			</td>
		</tr>
		<tr>
			<td class="tdtitle">请假原因：</td>
			<td colspan="3">
				<textarea rows="6" cols='120' id="REASON"></textarea>
				<a href="#" class="easyui-linkbutton"
				id="saveBtn">&nbsp;提&nbsp;&nbsp;交&nbsp;</a>
			</td>
		</tr>
	</table>
	<br>
	<table border="1" bordercolor="#7FB0DE" cellpadding="1"
		cellspacing="0" align="center" width="800px" class="content">
		<tr>
			<td class="tdtitle" colspan="9">[请假申请记录]</td>
		</tr>
		<tr>
			<td class="tabletitle" height="50px">记录编号</td>
			<td class="tabletitle">请假类型</td>
			<td class="tabletitle">起始时间</td>
			<td class="tabletitle">截止时间</td>
			<td class="tabletitle">请假天数</td>
			<td class="tabletitle">休假地</td>
			<td class="tabletitle">请假原因</td>
			<td class="tabletitle">状态</td>
			<td class="tabletitle">操作</td>
		</tr>
		<c:forEach items="${VACATION_RECORD_LIST}" var="vacationRecord">
			<tr>
				<td><input type="hidden" id= "${vacationRecord.record_id}_RECORD_ID" value="${vacationRecord.record_id}" />${vacationRecord.record_id}</td>
				<td><input type="hidden" id= "${vacationRecord.record_id}_TYPE" value="${vacationRecord.type}" />
					<c:forEach items="${VACATION_TYPE}" var="vacationType">
						<c:if test="${vacationRecord.type == vacationType.column_value}">
							${vacationType.column_desc}
						</c:if>
					</c:forEach>
				</td>
				<td><input type="hidden" id= "${vacationRecord.record_id}_BEGIN_DATE" value="${vacationRecord.begin_date}" />${vacationRecord.begin_date}</td>
				<td><input type="hidden" id= "${vacationRecord.record_id}_END_DATE" value="${vacationRecord.end_date}" />${vacationRecord.end_date}</td>
				<td><input type="hidden" id= "${vacationRecord.record_id}_NUMBER_OF_DAYS" value="${vacationRecord.number_of_days}" />${vacationRecord.number_of_days}</td>
				<td><input type="hidden" id= "${vacationRecord.record_id}_PLACE" value="${vacationRecord.place}" />
					<c:forEach items="${VACATION_PLACE}" var="vacationPlace">
						<c:if test="${vacationRecord.place == vacationPlace.column_value}">
							${vacationPlace.column_desc}
						</c:if>
					</c:forEach>
				</td>
				<td><input type="hidden" id= "${vacationRecord.record_id}_REASON" value="${vacationRecord.reason}" />${vacationRecord.reason}</td>
				<td><input type="hidden" id= "${vacationRecord.record_id}_STATE" value="${vacationRecord.state}" />
					<c:forEach items="${VACATION_STATE}" var="vacationState">
						<c:if test="${vacationRecord.state == vacationState.column_value}">
							${vacationState.column_desc}
						</c:if>
					</c:forEach>
				</td>
				<td align="center">
					<c:if test="${vacationRecord.state == '0'}">
						<a href='#' class='easyui-linkbutton' id='editBtn${vacationRecord.record_id}' onClick="editEvent('${vacationRecord.record_id}')">修改</a>
						<a href="#" class="easyui-linkbutton" id="cancelBtn${vacationRecord.record_id}" onClick="cancelEvent('${vacationRecord.record_id}')">撤销</a>
					</c:if>
				</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>