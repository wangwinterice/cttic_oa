<%@page contentType="text/html; charset=utf-8"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	$(function() {
		$('#employee_select').combo({
			required : true,
			editable : false
		});
		$('#employee_list').appendTo($('#employee_select').combo('panel'));
		<c:if test="${!empty SELECT_EMPLOYEE}">
			$('#employee_select').combo('setValue', '${SELECT_EMPLOYEE.employee_no}')
			                     .combo('setText', '姓名：${SELECT_EMPLOYEE.employee_name}    工号：${SELECT_EMPLOYEE.employee_no}')
			                     .combo('hidePanel');
		</c:if>
		$('#employee_list input').click( function() {
			var v = $(this).val();
			var s = $(this).next('span').text();
			$('#employee_select').combo('setValue', v).combo('setText',
							s).combo('hidePanel');
			window.location = '<%=basePath%>/erp/vacation/audit?EMPLOYEE_ID=' + $(this).val();
		});
	});
	
	function auditEvent(recordId, auditType) {
		$.ajax({
			type: "POST", 
			url: "<%=basePath%>/erp/vacation/save?OPER_TYPE=AUDIT",
			data : {'RECORD_ID' : recordId, 'STATE' : auditType},
			async : false,
			success : function(returnStr) {
				var jsonReturnStr = JSON.parse(returnStr);
				if (jsonReturnStr.STATUS_CODE == '1') {
					alert(jsonReturnStr.STATUS_INFO);
					window.location = "<%=basePath%>/erp/vacation/audit?EMPLOYEE_ID=" + $('#employee_select').combo('getValue');
				} else {
					alert(jsonReturnStr.STATUS_INFO);
				}
			}
		});
	}
</script>
</head>
<body>
	<br>
	<br>
	<table border="1" bordercolor="#7FB0DE" cellpadding="1"
		cellspacing="0" align="center" width="800px" class="content">
		<tr>
			<td class="tdtitle" colspan="8">
			[休假申请记录]
			<select id="employee_select" style="width: 300px"></select>
			<div id="employee_list">
				<input type="radio" name="lang" value="">
					<span>---请选择---</span><br />
				<c:forEach items="${EMPLOYEE_LIST}" var="employee">
					<input type="radio" name="lang" value="${employee.employee_no}">
					<span>姓名：${employee.employee_name}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;工号：${employee.employee_no}</span>
					<br />
				</c:forEach>
			</div>			
			</td>
		</tr>
		<tr>
			<td class="tabletitle" height="50px">申请人</td>
			<td class="tabletitle">起始时间</td>
			<td class="tabletitle">截止时间</td>
			<td class="tabletitle">休假天数</td>
			<td class="tabletitle">休假地</td>
			<td class="tabletitle">休假原因</td>
			<td class="tabletitle">状态</td>
			<td class="tabletitle">操作</td>
		</tr>
		<c:forEach items="${VACATION_RECORD_LIST}" var="vacationRecord">
			<tr>
				<td>
					<c:forEach items="${EMPLOYEE_LIST}" var="employee">
						<c:if test="${vacationRecord.employee_id == employee.employee_id}">
							<span>${employee.employee_name}</span>
						</c:if>
					</c:forEach>
				</td>
				<td>${vacationRecord.begin_date}</td>
				<td>${vacationRecord.end_date}</td>
				<td>${vacationRecord.number_of_days}</td>
				<td>
					<c:forEach items="${VACATION_PLACE}" var="vacationPlace">
						<c:if test="${vacationRecord.place == vacationPlace.column_value}">
							${vacationPlace.column_desc}
						</c:if>
					</c:forEach>
				</td>
				<td>${vacationRecord.reason}</td>
				<td>
					<c:forEach items="${VACATION_STATE}" var="vacationState">
						<c:if test="${vacationRecord.state == vacationState.column_value}">
							${vacationState.column_desc}
						</c:if>
					</c:forEach>
				</td>
				<td align="center">
					<c:if test="${vacationRecord.state == '0'}">
						<a href='#' class='easyui-linkbutton' id='auditBtn${vacationRecord.record_id}' onClick="auditEvent('${vacationRecord.record_id}', '1')">同意</a>
						<a href='#' class='easyui-linkbutton' id='auditBtn${vacationRecord.record_id}' onClick="auditEvent('${vacationRecord.record_id}', '2')">不同意</a>
					</c:if>
				</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>