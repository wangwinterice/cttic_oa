<%@page contentType="text/html; charset=utf-8"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	var sum_${SELECT_EMPLOYEE.employee_id}_11 = 0;
	var sum_${SELECT_EMPLOYEE.employee_id}_21 = 0;
	var sum_${SELECT_EMPLOYEE.employee_id}_29 = 0;
	var sum_${SELECT_EMPLOYEE.employee_id}_31 = 0;
	var sum_${SELECT_EMPLOYEE.employee_id}_39 = 0;
	var sum_${SELECT_EMPLOYEE.employee_id}_41 = 0;
	var sum_${SELECT_EMPLOYEE.employee_id}_51 = 0;
	var sum_${SELECT_EMPLOYEE.employee_id}_61 = 0;
	var sum_${SELECT_EMPLOYEE.employee_id}_71 = 0;
	var sum_${SELECT_EMPLOYEE.employee_id}_81 = 0;
	$(function() {
		$('#report_month_select').combo({
			required : true,
			editable : false
		});
		$('#report_month_list').appendTo($('#report_month_select').combo('panel'));
		$('#report_month_select').combo('setValue', '${REPORT_CURR_MONTH.column_value}').combo('setText',
				'${REPORT_CURR_MONTH.column_desc}').combo('hidePanel');
		$('#report_month_list input').click( function() {
			var v = $(this).val();
			var s = $(this).next('span').text();
			$('#report_month_select').combo('setValue', v).combo('setText', s).combo('hidePanel');
			window.location = '<%=basePath%>/erp/vacation/report?REPORT_MONTH=' + $(this).val();
		});
		<c:forEach items="${DATE_LIST}" var="date">
		<c:if test="${date.date_type == 0}">
			sum_${SELECT_EMPLOYEE.employee_id}_11 = sum_${SELECT_EMPLOYEE.employee_id}_11 + 1;
		</c:if>
		</c:forEach>
		<c:forEach items="${VACATION_RECORD_LIST}" var="record">
			<c:if test="${record.type == 22 || record.type == 23}">
			sum_${SELECT_EMPLOYEE.employee_id}_29 = sum_${SELECT_EMPLOYEE.employee_id}_29 + ${record.number_of_days};
			</c:if>
			<c:if test="${record.type == 32 || record.type == 33}">
			sum_${SELECT_EMPLOYEE.employee_id}_39 = sum_${SELECT_EMPLOYEE.employee_id}_39 + ${record.number_of_days};
			</c:if>
			<c:if test="${record.type != 22 && record.type != 23 && record.type != 32 && record.type != 33}">
			sum_${SELECT_EMPLOYEE.employee_id}_${record.type} = sum_${SELECT_EMPLOYEE.employee_id}_${record.type} + ${record.number_of_days};
			</c:if>
			var _day = parseInt(${fn:substring(record.begin_date, 8, 10)});
			$('#type_${SELECT_EMPLOYEE.employee_id}_' + _day).val(${record.type});
		</c:forEach>
		sum_${SELECT_EMPLOYEE.employee_id}_11 = sum_${SELECT_EMPLOYEE.employee_id}_11
		                                      - sum_${SELECT_EMPLOYEE.employee_id}_21
		                                      - sum_${SELECT_EMPLOYEE.employee_id}_29
		                              		  - sum_${SELECT_EMPLOYEE.employee_id}_31
		                            		  - sum_${SELECT_EMPLOYEE.employee_id}_39
		                            		  - sum_${SELECT_EMPLOYEE.employee_id}_41
		                            		  - sum_${SELECT_EMPLOYEE.employee_id}_51
		                            		  - sum_${SELECT_EMPLOYEE.employee_id}_61
		                            		  - sum_${SELECT_EMPLOYEE.employee_id}_71
		                            		  - sum_${SELECT_EMPLOYEE.employee_id}_81;
		$('#sum_${SELECT_EMPLOYEE.employee_id}_11').html(sum_${SELECT_EMPLOYEE.employee_id}_11);
		$('#sum_${SELECT_EMPLOYEE.employee_id}_21').html(sum_${SELECT_EMPLOYEE.employee_id}_21);
		$('#sum_${SELECT_EMPLOYEE.employee_id}_29').html(sum_${SELECT_EMPLOYEE.employee_id}_29);
		$('#sum_${SELECT_EMPLOYEE.employee_id}_31').html(sum_${SELECT_EMPLOYEE.employee_id}_31);
		$('#sum_${SELECT_EMPLOYEE.employee_id}_39').html(sum_${SELECT_EMPLOYEE.employee_id}_39);
		$('#sum_${SELECT_EMPLOYEE.employee_id}_41').html(sum_${SELECT_EMPLOYEE.employee_id}_41);
		$('#sum_${SELECT_EMPLOYEE.employee_id}_51').html(sum_${SELECT_EMPLOYEE.employee_id}_51);
		$('#sum_${SELECT_EMPLOYEE.employee_id}_61').html(sum_${SELECT_EMPLOYEE.employee_id}_61);
		$('#sum_${SELECT_EMPLOYEE.employee_id}_71').html(sum_${SELECT_EMPLOYEE.employee_id}_71);
		$('#sum_${SELECT_EMPLOYEE.employee_id}_81').html(sum_${SELECT_EMPLOYEE.employee_id}_81);
	});
	
	function cal(employee_id, day, val) {
		var paramJson = {};
		paramJson['EMPLOYEE_ID'] = employee_id;
		paramJson['TYPE'] = val;
		var reportMonth = '${REPORT_CURR_MONTH.column_value}';
		var dayStr = day.length > 1 ? day : "0" + day;
		var reportDate = reportMonth.substring(0, 4)
		               + "-"
		               + reportMonth.substring(4, 6)
		               + "-"
		               + dayStr;
		paramJson['BEGIN_DATE'] = reportDate;
		paramJson['END_DATE'] = reportDate;
		var numberOfDays = 0;
		if (val == 21 || val == 31 || val == 41 || val == 51 || val == 61 || val == 71 || val == 81) {
			numberOfDays = 1;
		} else if (val == 22 || val == 23 || val == 32 || val == 33) {
			numberOfDays = 0.5;
		}
		paramJson['NUMBER_OF_DAYS'] = numberOfDays;
		//alert(JSON.stringify(paramJson));
		$.ajax({
			type: "POST", 
			url: "<%=basePath%>/erp/vacation/save?OPER_TYPE=REPORT",
			data : paramJson,
			async : false,
			success : function(returnStr) {
				var jsonReturnStr = JSON.parse(returnStr);
				if (jsonReturnStr.STATUS_CODE == '1') {
					alert(jsonReturnStr.STATUS_INFO);
					window.location = "<%=basePath%>/erp/vacation/report";
				} else {
					alert(jsonReturnStr.STATUS_INFO);
				}
			}
		});
	}
</script>
</head>
<body>
	<br>
	<br>
	<table border="0" cellpadding="1"
		cellspacing="0">
		<tr>
			<td>
				<select id="report_month_select" style="width: 150px"></select>
				<div id="report_month_list">
					<input type="radio" name="lang" value="">
						<span>---请选择---</span><br />
					<c:forEach items="${REPORT_MONTH_LIST}" var="reportMonth">
						<input type="radio" name="lang" value="${reportMonth.column_value}">
						<span>${reportMonth.column_desc}</span>
						<br />
					</c:forEach>
				</div>
			</td>
		</tr>
		<tr>
			<table border="1" cellpadding="1"
				cellspacing="0" align="center">
				<tr>
					<td rowspan="2" colspan="3" style="background-color:#E8F2FC">&nbsp;</td>
					<td colspan="${fn:length(DATE_LIST)}" style="background-color:#E8F2FC" align="center">考勤明细</td>
					<td colspan="10" align="center">月统计（天）</td>
					<td rowspan="3" colspan="3" style="width:100px;background-color:#E8F2FC" nowrap align="center">备注</td>
				</tr>
				<tr>
					<td colspan="${fn:length(DATE_LIST)}" style="background-color:#E8F2FC" align="right">出勤△  事假(√,上A,下P）  病假(〇,上㊤,下㊦)  年假□  婚产M  旷工X  迟到L  早退E&nbsp;</td>
					<td rowspan="2" style="width:60px;" nowrap align="center">出勤</td>
					<td colspan="2" nowrap align="center">事假</td>
					<td colspan="2" nowrap align="center">病假</td>
					<td rowspan="2" style="width:60px;" nowrap align="center">年假</td>
					<td rowspan="2" style="width:60px;" nowrap align="center">婚产</td>
					<td rowspan="2" style="width:60px;" nowrap align="center">旷工</td>
					<td rowspan="2" style="width:60px;" nowrap align="center">迟到</td>
					<td rowspan="2" style="width:60px;" nowrap align="center">早退</td>
				</tr>
				<tr>
					<td nowrap style="width:60px;" align="center">序号</td>
					<td nowrap style="width:60px;" align="center">工号</td>
					<td nowrap style="width:60px;" align="center">姓名</td>
					<c:forEach items="${DATE_LIST}" var="date">
					<td align="center" style="width:40px;background-color:
					<c:if test="${date.date_type == 0}">
					white
					</c:if>
					<c:if test="${date.date_type == 1}">
					#F0F0F0
					</c:if>
					">${date.day}</td>
					</c:forEach>
					<td style="width:30px;" nowrap align="center">天</td>
					<td style="width:40px;" nowrap align="center">半天</td>
					<td style="width:30px;" nowrap align="center">天</td>
					<td style="width:40px;" nowrap align="center">半天</td>
				</tr>
				<tr>
					<td nowrap align="center">1</td>
					<td nowrap align="center">${SELECT_EMPLOYEE.employee_id}</td>
					<td nowrap align="center">${SELECT_EMPLOYEE.employee_name}</td>
					<c:forEach items="${DATE_LIST}" var="date">
					<td align="center" style="width:40px;height:25px;background-color:
					<c:if test="${date.date_type == 0}">
					white">
					<select id="type_${SELECT_EMPLOYEE.employee_id}_${date.day}" 
					        name="type_${SELECT_EMPLOYEE.employee_id}_${date.day}"
					        onChange="cal('${SELECT_EMPLOYEE.employee_id}', '${date.day}', this.value)"
					        style="width:40px;border:0;">
					<c:forEach items="${VACATION_MARK_TYPE}" var="markType">
						<option value="${markType.column_value}">${markType.column_desc}</option>
					</c:forEach>
					</select>
					</c:if>
					<c:if test="${date.date_type == 1}">
					#F0F0F0">&nbsp;
					</c:if>
					</td>
					</c:forEach>
					<td id="sum_${SELECT_EMPLOYEE.employee_id}_11" align="center">&nbsp;</td>
					<td id="sum_${SELECT_EMPLOYEE.employee_id}_21" align="center">&nbsp;</td>
					<td id="sum_${SELECT_EMPLOYEE.employee_id}_29" align="center">&nbsp;</td>
					<td id="sum_${SELECT_EMPLOYEE.employee_id}_31" align="center">&nbsp;</td>
					<td id="sum_${SELECT_EMPLOYEE.employee_id}_39" align="center">&nbsp;</td>
					<td id="sum_${SELECT_EMPLOYEE.employee_id}_41" align="center">&nbsp;</td>
					<td id="sum_${SELECT_EMPLOYEE.employee_id}_51" align="center">&nbsp;</td>
					<td id="sum_${SELECT_EMPLOYEE.employee_id}_61" align="center">&nbsp;</td>
					<td id="sum_${SELECT_EMPLOYEE.employee_id}_71" align="center">&nbsp;</td>
					<td id="sum_${SELECT_EMPLOYEE.employee_id}_81" align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
			</table>
		</tr>
	</table>
</body>
</html>