<%@page contentType="text/html; charset=utf-8"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	function getDays(strDateStart, strDateEnd){
	   var strSeparator = "-"; //日期分隔符
	   var oDate1;
	   var oDate2;
	   var iDays;
	   oDate1= strDateStart.split(strSeparator);
	   oDate2= strDateEnd.split(strSeparator);
	   var strDateS = new Date(oDate1[0] + "-" + oDate1[1] + "-" + oDate1[2]);
	   var strDateE = new Date(oDate2[0] + "-" + oDate2[1] + "-" + oDate2[2]);
	   
	   if (strDateS.getTime() > strDateE.getTime()) {
		   alert("[起始时间]不能大于[截止时间]");
		   return 0;
	   }
	   
	   iDays = parseInt(Math.abs(strDateS - strDateE ) / 1000 / 60 / 60 /24);//把相差的毫秒数转换为天数 

	   return iDays + 1;
	}
	
	function getNumberOfDays() {
		$(':radio[name=SELECT_DAY_NUMBER]').each(function (i, obj) {
			if (obj.checked) {
				$('#NUMBER_OF_DAYS').val($(obj).val());
			}
		});
	}
	function setDefault() {
		var beginDate = $('#BEGIN_DATE').datebox('getValue');
		if ("" == beginDate) {
			//alert("[起始时间]不能为空");
			return;
		}
		var endDate = $('#END_DATE').datebox('getValue');
		if ("" == endDate) {
			//alert("[截止时间]不能为空");
			return;
		}
		var numberOfDays = getDays(beginDate, endDate);
		var type = $('#TYPE').val();
		if (type == '22' || type == '23' || type == '32' || type == '33') {
			$('#END_DATE').datebox('setValue', beginDate);
			$('#NUMBER_OF_DAYS').val('0.5');
			$('#SPAN_NUMBER_OF_DAYS').html('<font color="red" size="5">0.5天</font>');
		} else {
			$('#NUMBER_OF_DAYS').val(numberOfDays);
			$('#SPAN_NUMBER_OF_DAYS').html('<font color="red" size="5">' + numberOfDays + '天</font>');
		}
		$('#saveBtn').show();
	}
	var sum_${SELECT_EMPLOYEE.employee_id}_11 = 0;
	var sum_${SELECT_EMPLOYEE.employee_id}_21 = 0;
	var sum_${SELECT_EMPLOYEE.employee_id}_29 = 0;
	var sum_${SELECT_EMPLOYEE.employee_id}_31 = 0;
	var sum_${SELECT_EMPLOYEE.employee_id}_39 = 0;
	var sum_${SELECT_EMPLOYEE.employee_id}_41 = 0;
	var sum_${SELECT_EMPLOYEE.employee_id}_51 = 0;
	var sum_${SELECT_EMPLOYEE.employee_id}_61 = 0;
	var sum_${SELECT_EMPLOYEE.employee_id}_71 = 0;
	var sum_${SELECT_EMPLOYEE.employee_id}_81 = 0;
	$(function() {
		$('#BEGIN_DATE').datebox({
		    onSelect: function(date){
		    	setDefault();
		    }
		});
		$('#END_DATE').datebox({
		    onSelect: function(date){
		    	setDefault();
		    }
		});
		$('#saveBtn').bind('click', function () {
			var paramJson = {};
			var beginDate = $('#BEGIN_DATE').datebox('getValue');
			if ("" == beginDate) {
				alert("[起始时间]不能为空");
				return;
			}
			var endDate = $('#END_DATE').datebox('getValue');
			if ("" == endDate) {
				alert("[截止时间]不能为空");
				return;
			}
			var place = "";
			$(":radio[name=PLACE]").each(function(i, obj){
				if (obj.checked) {
					place = $(obj).val();
				}
			});
			if ("" == place) {
				alert("[休假地]不能为空");
				return;
			}
			var reason = $('#REASON').val();
			if ("" == reason) {
				alert("[休假原因]不能为空");
				return;
			}
			paramJson['RECORD_ID'] = $('#RECORD_ID').val();
			paramJson['TYPE'] = $('#TYPE').val();
			paramJson['BEGIN_DATE'] = beginDate;
			paramJson['END_DATE'] = endDate;
			paramJson['NUMBER_OF_DAYS'] = $('#NUMBER_OF_DAYS').val();
			paramJson['PLACE'] = place;
			paramJson['REASON'] = reason;
			//alert(JSON.stringify(paramJson));
			$.ajax({
				type: "POST", 
				url: "<%=basePath%>/erp/vacation/save?OPER_TYPE=" + $('#OPER_TYPE').val(),
				data : paramJson,
				async : false,
				success : function(returnStr) {
					var jsonReturnStr = JSON.parse(returnStr);
					if (jsonReturnStr.STATUS_CODE == '1') {
						alert(jsonReturnStr.STATUS_INFO);
						window.location = "<%=basePath%>/erp/vacation/apply";
					} else {
						alert(jsonReturnStr.STATUS_INFO);
					}
				}
			});
		}).hide();
		$('#report_month_select').combo({
			required : true,
			editable : false
		});
		$('#report_month_list').appendTo($('#report_month_select').combo('panel'));
		$('#report_month_select').combo('setValue', '${REPORT_CURR_MONTH.column_value}').combo('setText',
				'${REPORT_CURR_MONTH.column_desc}').combo('hidePanel');
		$('#report_month_list input').click( function() {
			var v = $(this).val();
			var s = $(this).next('span').text();
			$('#report_month_select').combo('setValue', v).combo('setText', s).combo('hidePanel');
			window.location = '<%=basePath%>/erp/vacation/apply?REPORT_MONTH=' + $(this).val();
		});
		<c:forEach items="${DATE_LIST}" var="date">
		<c:if test="${date.date_type == 0}">
			sum_${SELECT_EMPLOYEE.employee_id}_11 = sum_${SELECT_EMPLOYEE.employee_id}_11 + 1;
		</c:if>
		</c:forEach>
		<c:forEach items="${VACATION_RECORD_LIST}" var="record">
			<c:if test="${record.type == 22 || record.type == 23}">
			sum_${SELECT_EMPLOYEE.employee_id}_29 = sum_${SELECT_EMPLOYEE.employee_id}_29 + ${record.number_of_days};
			</c:if>
			<c:if test="${record.type == 32 || record.type == 33}">
			sum_${SELECT_EMPLOYEE.employee_id}_39 = sum_${SELECT_EMPLOYEE.employee_id}_39 + ${record.number_of_days};
			</c:if>
			<c:if test="${record.type != 22 && record.type != 23 && record.type != 32 && record.type != 33}">
			sum_${SELECT_EMPLOYEE.employee_id}_${record.type} = sum_${SELECT_EMPLOYEE.employee_id}_${record.type} + ${record.number_of_days};
			</c:if>
			var _day = parseInt(${fn:substring(record.begin_date, 8, 10)});
			<c:forEach items="${VACATION_MARK_TYPE}" var="markType">
				<c:if test="${record.type == markType.column_value}">
				$('#type_${SELECT_EMPLOYEE.employee_id}_' + _day).html('${markType.column_desc}');
				</c:if>
			</c:forEach>
		</c:forEach>
		sum_${SELECT_EMPLOYEE.employee_id}_11 = sum_${SELECT_EMPLOYEE.employee_id}_11
		                                      - sum_${SELECT_EMPLOYEE.employee_id}_21
		                                      - sum_${SELECT_EMPLOYEE.employee_id}_29
		                              		  - sum_${SELECT_EMPLOYEE.employee_id}_31
		                            		  - sum_${SELECT_EMPLOYEE.employee_id}_39
		                            		  - sum_${SELECT_EMPLOYEE.employee_id}_41
		                            		  - sum_${SELECT_EMPLOYEE.employee_id}_51
		                            		  - sum_${SELECT_EMPLOYEE.employee_id}_61
		                            		  - sum_${SELECT_EMPLOYEE.employee_id}_71
		                            		  - sum_${SELECT_EMPLOYEE.employee_id}_81;
		$('#sum_${SELECT_EMPLOYEE.employee_id}_11').html(sum_${SELECT_EMPLOYEE.employee_id}_11);
		$('#sum_${SELECT_EMPLOYEE.employee_id}_21').html(sum_${SELECT_EMPLOYEE.employee_id}_21);
		$('#sum_${SELECT_EMPLOYEE.employee_id}_29').html(sum_${SELECT_EMPLOYEE.employee_id}_29);
		$('#sum_${SELECT_EMPLOYEE.employee_id}_31').html(sum_${SELECT_EMPLOYEE.employee_id}_31);
		$('#sum_${SELECT_EMPLOYEE.employee_id}_39').html(sum_${SELECT_EMPLOYEE.employee_id}_39);
		$('#sum_${SELECT_EMPLOYEE.employee_id}_41').html(sum_${SELECT_EMPLOYEE.employee_id}_41);
		$('#sum_${SELECT_EMPLOYEE.employee_id}_51').html(sum_${SELECT_EMPLOYEE.employee_id}_51);
		$('#sum_${SELECT_EMPLOYEE.employee_id}_61').html(sum_${SELECT_EMPLOYEE.employee_id}_61);
		$('#sum_${SELECT_EMPLOYEE.employee_id}_71').html(sum_${SELECT_EMPLOYEE.employee_id}_71);
		$('#sum_${SELECT_EMPLOYEE.employee_id}_81').html(sum_${SELECT_EMPLOYEE.employee_id}_81);
	});
	
	function myformatter(date){
		var y = date.getFullYear();
		var m = date.getMonth()+1;
		var d = date.getDate();
		return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
	}
	function myparser(s){
		if (!s) return new Date();
		var ss = (s.split('-'));
		var y = parseInt(ss[0],10);
		var m = parseInt(ss[1],10);
		var d = parseInt(ss[2],10);
		if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
			return new Date(y,m-1,d);
		} else {
			return new Date();
		}
	}
	
	function onSelect(date) {
		$('#NUMBER_OF_DAYS').val();
		$('#SPAN_NUMBER_OF_DAYS').html('');
		$('#saveBtn').hide();
	}
</script>
</head>
<body>
	<br>
	<br><input type="hidden" id="OPER_TYPE" value="ADD" />
	<input type="hidden" id="RECORD_ID" value="" />
	<table border="1" bordercolor="#7FB0DE" cellpadding="1"
		cellspacing="0" align="center" width="800px" class="content">
		<tr>
			<td class="tdtitle" height="50px">请假类型：</td>
			<td>
				<select id="TYPE" name="TYPE" onChange="setDefault()">
					<c:forEach items="${VACATION_TYPE}" var="vacationType">
						<option value="${vacationType.column_value}">
							${vacationType.column_desc}
						</option>
					</c:forEach>
				</select>
			</td>
			<td class="tdtitle">起始截止时间：</td>
			<td><input id='BEGIN_DATE' class='easyui-datebox'
					data-options="formatter:myformatter,parser:myparser,onSelect:onSelect" />~<input id='END_DATE' class='easyui-datebox'
					data-options="formatter:myformatter,parser:myparser,onSelect:onSelect" /></td>
		</tr>
		<tr>
			<td class="tdtitle" height="50px">请假天数：<input type="hidden" id="NUMBER_OF_DAYS" value="" /></td>
			<td><span id="SPAN_NUMBER_OF_DAYS"></span></td>
			<td class="tdtitle" align="center">休假地：</td>
			<td>
				<c:forEach items="${VACATION_PLACE}" var="vacationPlace">
					<input type="radio" name="PLACE" value="${vacationPlace.column_value}" />
					<span>${vacationPlace.column_desc}</span>&nbsp;&nbsp;
				</c:forEach>
			</td>
		</tr>
		<tr>
			<td class="tdtitle">请假原因：</td>
			<td colspan="3">
				<textarea rows="6" cols='120' id="REASON"></textarea>
				<a href="#" class="easyui-linkbutton"
				id="saveBtn">&nbsp;提&nbsp;&nbsp;交&nbsp;</a>
			</td>
		</tr>
	</table>
	<br>
	<br>
	<table border="0" cellpadding="1"
		cellspacing="0">
		<tr>
			<td>
				<select id="report_month_select" style="width: 150px"></select>
				<div id="report_month_list">
					<input type="radio" name="lang" value="">
						<span>---请选择---</span><br />
					<c:forEach items="${REPORT_MONTH_LIST}" var="reportMonth">
						<input type="radio" name="lang" value="${reportMonth.column_value}">
						<span>${reportMonth.column_desc}</span>
						<br />
					</c:forEach>
				</div>
			</td>
		</tr>
		<tr>
			<table border="1" cellpadding="1"
				cellspacing="0" align="center">
				<tr>
					<td rowspan="2" colspan="3" style="background-color:#E8F2FC">&nbsp;</td>
					<td colspan="${fn:length(DATE_LIST)}" style="background-color:#E8F2FC" align="center">考勤明细</td>
					<td colspan="10" align="center">月统计（天）</td>
					<td rowspan="3" colspan="3" style="width:100px;background-color:#E8F2FC" nowrap align="center">备注</td>
				</tr>
				<tr>
					<td colspan="${fn:length(DATE_LIST)}" style="background-color:#E8F2FC" align="right">出勤△  事假(√,上A,下P）  病假(〇,上㊤,下㊦)  年假□  婚产M  旷工X  迟到L  早退E&nbsp;</td>
					<td rowspan="2" style="width:60px;" nowrap align="center">出勤</td>
					<td colspan="2" nowrap align="center">事假</td>
					<td colspan="2" nowrap align="center">病假</td>
					<td rowspan="2" style="width:60px;" nowrap align="center">年假</td>
					<td rowspan="2" style="width:60px;" nowrap align="center">婚产</td>
					<td rowspan="2" style="width:60px;" nowrap align="center">旷工</td>
					<td rowspan="2" style="width:60px;" nowrap align="center">迟到</td>
					<td rowspan="2" style="width:60px;" nowrap align="center">早退</td>
				</tr>
				<tr>
					<td nowrap style="width:60px;" align="center">序号</td>
					<td nowrap style="width:60px;" align="center">工号</td>
					<td nowrap style="width:60px;" align="center">姓名</td>
					<c:forEach items="${DATE_LIST}" var="date">
					<td align="center" style="width:40px;background-color:
					<c:if test="${date.date_type == 0}">
					white
					</c:if>
					<c:if test="${date.date_type == 1}">
					#F0F0F0
					</c:if>
					">${date.day}</td>
					</c:forEach>
					<td style="width:30px;" nowrap align="center">天</td>
					<td style="width:40px;" nowrap align="center">半天</td>
					<td style="width:30px;" nowrap align="center">天</td>
					<td style="width:40px;" nowrap align="center">半天</td>
				</tr>
				<tr>
					<td nowrap align="center">1</td>
					<td nowrap align="center">${SELECT_EMPLOYEE.employee_id}</td>
					<td nowrap align="center">${SELECT_EMPLOYEE.employee_name}</td>
					<c:forEach items="${DATE_LIST}" var="date">
					<td id="type_${SELECT_EMPLOYEE.employee_id}_${date.day}" 
					align="center" style="width:40px;height:25px;background-color:
					<c:if test="${date.date_type == 0}">
					white">
					</c:if>
					<c:if test="${date.date_type == 1}">
					#F0F0F0">&nbsp;
					</c:if>
					</td>
					</c:forEach>
					<td id="sum_${SELECT_EMPLOYEE.employee_id}_11" align="center">&nbsp;</td>
					<td id="sum_${SELECT_EMPLOYEE.employee_id}_21" align="center">&nbsp;</td>
					<td id="sum_${SELECT_EMPLOYEE.employee_id}_29" align="center">&nbsp;</td>
					<td id="sum_${SELECT_EMPLOYEE.employee_id}_31" align="center">&nbsp;</td>
					<td id="sum_${SELECT_EMPLOYEE.employee_id}_39" align="center">&nbsp;</td>
					<td id="sum_${SELECT_EMPLOYEE.employee_id}_41" align="center">&nbsp;</td>
					<td id="sum_${SELECT_EMPLOYEE.employee_id}_51" align="center">&nbsp;</td>
					<td id="sum_${SELECT_EMPLOYEE.employee_id}_61" align="center">&nbsp;</td>
					<td id="sum_${SELECT_EMPLOYEE.employee_id}_71" align="center">&nbsp;</td>
					<td id="sum_${SELECT_EMPLOYEE.employee_id}_81" align="center">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
			</table>
		</tr>
	</table>
</body>
</html>