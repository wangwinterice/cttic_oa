<%@page contentType="text/html; charset=utf-8"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@ page isELIgnored="false"%>
<%@page import="java.util.List"%>
<%@page import="com.cttic.erp.bean.InfoEmployeeBean"%>
<%@page import="com.cttic.erp.bean.BusiEmployeeVacationRecordBean"%>
<%@page import="com.cttic.erp.bean.SysParamBean"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
	List<InfoEmployeeBean> infoEmployeeBeanList = (List<InfoEmployeeBean>) request.getAttribute("EMPLOYEE_LIST");
	List<BusiEmployeeVacationRecordBean> recordBeanList = (List<BusiEmployeeVacationRecordBean>) request.getAttribute("VACATION_RECORD_LIST");
	List<SysParamBean> sysPrarmBeanList = (List<SysParamBean>) request.getAttribute("VACATION_MARK_TYPE");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	$(function() {
		$('#report_month_select').combo({
			required : true,
			editable : false
		});
		$('#report_month_list').appendTo($('#report_month_select').combo('panel'));
		$('#report_month_select').combo('setValue', '${REPORT_CURR_MONTH.column_value}').combo('setText',
				'${REPORT_CURR_MONTH.column_desc}').combo('hidePanel');
		$('#report_month_list input').click( function() {
			var v = $(this).val();
			var s = $(this).next('span').text();
			$('#report_month_select').combo('setValue', v).combo('setText', s).combo('hidePanel');
			window.location = '<%=basePath%>/erp/vacation/state?REPORT_MONTH=' + $(this).val();
		});
		<%
		for (InfoEmployeeBean employeeBean : infoEmployeeBeanList) {
			for (BusiEmployeeVacationRecordBean recordBean : recordBeanList) {
				if (employeeBean.getEmployee_id() == recordBean.getEmployee_id()) {
					for (SysParamBean paramBean : sysPrarmBeanList) {
						if (recordBean.getType().equals(paramBean.getColumn_value())) {
							%>
							$('#type_<%=employeeBean.getEmployee_id()%>_<%=Integer.parseInt(recordBean.getBegin_date().substring(8, 10))%>').html('<%=paramBean.getColumn_desc()%>');
				<%		
						}
					}
				}
			}
		}
		%>
		
		$('#exportBtn').bind('click', function () {
			$.ajax({
				type : "POST", 
				url : '<%=basePath%>/erp/vacation/generatorExcel',
				async : false,
				data: {
					'REPORT_MONTH' : $('#report_month_select').combo('getValue')
				},
				success : function(returnStr) {
					var jsonReturnStr = JSON.parse(returnStr);
					var url = "<%=basePath%>/erp/vacation/downloadExcel?"
					      + "_FILE_NAME=" + jsonReturnStr.RETURN_INFO;
					var iTop = (window.screen.availHeight-30-200)/2; //获得窗口的垂直位置;
					var iLeft = (window.screen.availWidth-10-270)/2; //获得窗口的水平位置;
					var styleCss = 'height=160, width=270, top='+iTop+', left='+iLeft+', toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, status=no';
					window.open(url, '', styleCss);
				}
			});
		})
	});
	
	function cal(employee_id, day, val) {
		var paramJson = {};
		paramJson['EMPLOYEE_ID'] = employee_id;
		paramJson['TYPE'] = val;
		var reportMonth = '${REPORT_CURR_MONTH.column_value}';
		var dayStr = day.length > 1 ? day : "0" + day;
		var reportDate = reportMonth.substring(0, 4)
		               + "-"
		               + reportMonth.substring(4, 6)
		               + "-"
		               + dayStr;
		paramJson['BEGIN_DATE'] = reportDate;
		paramJson['END_DATE'] = reportDate;
		var numberOfDays = 0;
		if (val == 21 || val == 31 || val == 41 || val == 51 || val == 61 || val == 71 || val == 81) {
			numberOfDays = 1;
		} else if (val == 22 || val == 23 || val == 32 || val == 33) {
			numberOfDays = 0.5;
		}
		paramJson['NUMBER_OF_DAYS'] = numberOfDays;
		//alert(JSON.stringify(paramJson));
		$.ajax({
			type: "POST", 
			url: "<%=basePath%>/erp/vacation/save?OPER_TYPE=REPORT",
			data : paramJson,
			async : false,
			success : function(returnStr) {
				var jsonReturnStr = JSON.parse(returnStr);
				if (jsonReturnStr.STATUS_CODE == '1') {
					alert(jsonReturnStr.STATUS_INFO);
					window.location = "<%=basePath%>/erp/vacation/state";
				} else {
					alert(jsonReturnStr.STATUS_INFO);
				}
			}
		});
	}
</script>
</head>
<body>
	<table border="0" cellpadding="1"
		cellspacing="0">
		<tr>
			<td>
				<select id="report_month_select" style="width: 150px"></select>
				<div id="report_month_list">
					<input type="radio" name="lang" value="">
					<span>---请选择---</span><br />
					<c:forEach items="${REPORT_MONTH_LIST}" var="reportMonth"><input type="radio" name="lang" value="${reportMonth.column_value}"><span>${reportMonth.column_desc}</span><br /></c:forEach>
				</div>
				<a href='#' class='easyui-linkbutton' id='exportBtn'>导出</a>
			</td>
		</tr>
		<tr>
			<table border="1" cellpadding="1"
				cellspacing="0" align="center">
				<tr>
					<td rowspan="2" colspan="3" style="background-color:#E8F2FC">&nbsp;</td>
					<td colspan="${fn:length(DATE_LIST)}" style="background-color:#E8F2FC" align="center">考勤明细</td>
					<td colspan="10" align="center">月统计（天）</td>
					<td rowspan="3" colspan="3" style="width:100px;background-color:#E8F2FC" nowrap align="center">备注</td>
				</tr>
				<tr>
					<td colspan="${fn:length(DATE_LIST)}" style="background-color:#E8F2FC" align="right">出勤△  事假(√,上A,下P）  病假(〇,上㊤,下㊦)  年假□  婚产M  旷工X  迟到L  早退E&nbsp;</td>
					<td rowspan="2" style="width:60px;" nowrap align="center">出勤</td>
					<td colspan="2" nowrap align="center">事假</td>
					<td colspan="2" nowrap align="center">病假</td>
					<td rowspan="2" style="width:60px;" nowrap align="center">年假</td>
					<td rowspan="2" style="width:60px;" nowrap align="center">婚产</td>
					<td rowspan="2" style="width:60px;" nowrap align="center">旷工</td>
					<td rowspan="2" style="width:60px;" nowrap align="center">迟到</td>
					<td rowspan="2" style="width:60px;" nowrap align="center">早退</td>
				</tr>
				<tr>
					<td nowrap style="width:40px;" align="center">序号</td>
					<td nowrap style="width:40px;" align="center">工号</td>
					<td nowrap style="width:60px;" align="center">姓名</td>
					<c:forEach items="${DATE_LIST}" var="date"><td align="center" width="40px" style="width:40px;<c:if test="${date.date_type == 1}">background-color:#F0F0F0</c:if>">${date.day}</td></c:forEach>
					<td style="width:30px;" nowrap align="center">天</td>
					<td style="width:40px;" nowrap align="center">半天</td>
					<td style="width:30px;" nowrap align="center">天</td>
					<td style="width:40px;" nowrap align="center">半天</td>
				</tr>
				<%
				for (int cnt = 0; cnt < infoEmployeeBeanList.size(); cnt ++) {
					InfoEmployeeBean bean = infoEmployeeBeanList.get(cnt);
					String sum_11 = request.getAttribute("sum_" + bean.getEmployee_id() + "_11").toString();
					String sum_21 = request.getAttribute("sum_" + bean.getEmployee_id() + "_21").toString();
					String sum_29 = request.getAttribute("sum_" + bean.getEmployee_id() + "_29").toString();
					String sum_31 = request.getAttribute("sum_" + bean.getEmployee_id() + "_31").toString();
					String sum_39 = request.getAttribute("sum_" + bean.getEmployee_id() + "_39").toString();
					String sum_41 = request.getAttribute("sum_" + bean.getEmployee_id() + "_41").toString();
					String sum_51 = request.getAttribute("sum_" + bean.getEmployee_id() + "_51").toString();
					String sum_61 = request.getAttribute("sum_" + bean.getEmployee_id() + "_61").toString();
					String sum_71 = request.getAttribute("sum_" + bean.getEmployee_id() + "_71").toString();
					String sum_81 = request.getAttribute("sum_" + bean.getEmployee_id() + "_81").toString();
				%>
				<tr>
					<td nowrap align="center"><%=cnt + 1%></td>
					<td nowrap align="center"><%=bean.getEmployee_id()%></td>
					<td nowrap align="center"><%=bean.getEmployee_name()%></td>
					<c:forEach items="${DATE_LIST}" var="date"><td align="center" style="width:40px;height:25px;<c:if test="${date.date_type == 1}">background-color:#F0F0F0</c:if>"><c:if test="${date.date_type == 0}"><div id="type_<%=bean.getEmployee_id()%>_${date.day}" style="width:40px;border:0;">△</div></c:if></td></c:forEach>
					<td align="center"><%=sum_11%></td>
					<td align="center"><%=sum_21%></td>
					<td align="center"><%=sum_29%></td>
					<td align="center"><%=sum_31%></td>
					<td align="center"><%=sum_39%></td>
					<td align="center"><%=sum_41%></td>
					<td align="center"><%=sum_51%></td>
					<td align="center"><%=sum_61%></td>
					<td align="center"><%=sum_71%></td>
					<td align="center"><%=sum_81%></td>
					<td align="center">&nbsp;</td>
				</tr>
				<%
				}
				%>
			</table>
		</tr>
	</table>
</body>
</html>