<%@page contentType="text/html; charset=utf-8"%>
<%@page import="com.cttic.erp.bean.ext.InfoOperBeanExt"%>
<%@page import="com.cttic.erp.common.util.InfoOperUtil"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
	InfoOperBeanExt infoOperBean = InfoOperUtil
			.getCurrOperInfo(request);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	$(function() {
		$('#BTN_QRY').bind('click', function(){
			employeeQuery();
		});
		$('#BTN_ADD').bind('click', function(){
			var rtnValue = window.showModalDialog("<%=basePath%>/erp/employee/edit?OPER_TYPE=ADD",'editemployee',"dialogWidth=800px;dialogHeight=600px");
			if (rtnValue) {
				employeeQuery();
			}
		});
		$('#BTN_EDIT').bind('click', function(){
			var selectRow = $('#dg_employee').datagrid('getSelected');
			if (null == selectRow) {
				messageManager.alert("没有选中行");
				return;
			}
			var rtnValue = window.showModalDialog("<%=basePath%>/erp/employee/edit?OPER_TYPE=EDIT&employee_id=" + selectRow.employee_id,'editemployee',"dialogWidth=800px;dialogHeight=600px");
			if (rtnValue) {
				employeeQuery();
			}
		});
	});

	function employeeQuery() {
		$.ajax({
			type: "POST", 
			url: "<%=basePath%>/erp/employee/employeequery",
			data : {'employee_no' : $('#employee_no').val(),
				'employee_name' : $('#employee_name').val()},
			async : false,
			success : function(returnStr) {
				var jsonReturnStr = JSON.parse(returnStr);
				if (jsonReturnStr.STATUS_CODE == '1') {
					$('#dg_employee').datagrid('loadData', jsonReturnStr.RETURN_INFO);
				} else {
					alert(jsonReturnStr.STATUS_INFO);
				}
			}
		});
	}
</script>
</head>
<body>
	<div class="easyui-panel" title="人员查询" data-options="border:true"
		style="width: auto; height: auto">
		<form id="searchFsForm">
			<table align="center" width="800px" border='0'>
				<tr>
					<td>
						人员编码：<input id='employee_no' style="width: 200px" />
						人员名称：<input id='employee_name' style="width: 200px" />
						<a href="#" id="BTN_QRY"
						class="easyui-linkbutton" iconCls="icon-search">查询</a>
					</td>
				</tr>
			</table>
		</form>
	</div>
	<div style="margin: 10px 0px 0px 0px"></div>
	<table id="dg_employee" class="easyui-datagrid" title="人员列表"
		style="width: auto; height: 480px"
		data-options="fitColumns:true,singleSelect:true,toolbar:'#tb_oper'">
		<thead>
			<tr>
				<th field="employee_id" width="50" hidden="true">人员ID</th>
				<th field="employee_no" width="200">人员工号</th>
				<th field="employee_name" width="100">人员名称</th>
				<th field="status" width="100">人员状态</th>
				<th field="dept_id" width="100">归属部门</th>
				<th field="post" width="100">岗位</th>
				<th field="in_company" width="100">入职时间</th>
				<th field="mobile_phone" width="100">手机</th>
				<th field="email" width="100">email</th>
				<th field="cert_num" width="100">身份证</th>
				<th field="sex" width="100">性别</th>
				<th field="birthday" width="100">生日</th>
				<th field="address" width="100">地址</th>
				<th field="zip" width="100">邮编</th>
			</tr>
		</thead>
	</table>
	<div id="tb_oper" style="padding: 5px; height: auto">
		<table cellspacing="0" cellpadding="0">
			<tr>
				<td><a href="#" id="BTN_ADD" class="easyui-linkbutton"
					iconCls="icon-add" plain="true">新增</a></td>
				<td>
					<div class="datagrid-btn-separator"></div>
				</td>
				<td><a href="#" id="BTN_EDIT" class="easyui-linkbutton"
					iconCls="icon-edit" plain="true">修改</a></td>
			</tr>
		</table>
	</div>
</body>
</html>