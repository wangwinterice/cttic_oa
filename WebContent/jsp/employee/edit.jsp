<%@page contentType="text/html; charset=utf-8"%>
<%@page import="com.cttic.erp.bean.ext.InfoOperBeanExt"%>
<%@page import="com.cttic.erp.common.util.InfoOperUtil"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
	InfoOperBeanExt infoOperBean = InfoOperUtil
			.getCurrOperInfo(request);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	$(function() {
		$('#BTN_SAVE').bind('click', function() {
			$.messager.progress();	// 显示进度条
			var isValid = $(this).form('validate');
			if (!isValid){
				$.messager.progress('close');	// 如果表单是无效的则隐藏进度条
				return;
			}
			$.ajax({
				type : "POST",
				url : "save",
				data : {
					'OPER_TYPE' : '${OPER_TYPE}',
					'employee_id' : $('#employee_id').val(),
					'employee_no' : $('#employee_no').val(),
					'employee_name' : $('#employee_name').val(),
					'mobile_phone' : $('#mobile_phone').val(),
					'email' : $('#email').val(),
					'in_company' : $('#in_company').combo('getValue'),
					'cert_num' : $('#cert_num').val(),
					'birthday' : $('#birthday').combo('getValue'),
					'address' : $('#address').val(),
					'zip' : $('#zip').val()
				},
				async : false,
				success : function(returnStr) {
					$.messager.progress('close');	// 如果提交成功则隐藏进度条
					var jsonReturnStr = JSON.parse(returnStr);
					if (jsonReturnStr.STATUS_CODE == '1') {
						alert(jsonReturnStr.STATUS_INFO);
						window.close();
					} else {
						alert(jsonReturnStr.STATUS_INFO);
					}
				}
			});
		});
	});

	function myformatter(date) {
		var y = date.getFullYear();
		var m = date.getMonth() + 1;
		var d = date.getDate();
		return y + '-' + (m < 10 ? ('0' + m) : m) + '-'
				+ (d < 10 ? ('0' + d) : d);
	}
	function myparser(s) {
		if (!s)
			return new Date();
		var ss = (s.split('-'));
		var y = parseInt(ss[0], 10);
		var m = parseInt(ss[1], 10);
		var d = parseInt(ss[2], 10);
		if (!isNaN(y) && !isNaN(m) && !isNaN(d)) {
			return new Date(y, m - 1, d);
		} else {
			return new Date();
		}
	}
</script>
</head>
<body>
	<div class="easyui-panel" title="人员编辑" data-options="border:true"
		style="width: auto; height: auto">
		<form id="searchFsForm">
			<input id='employee_id' value="${employee.employee_id}" type="hidden" />
			<table align="center" width="100%" border='0'>
				<tr>
					<td align="right" width="40%"><br>人员编码：<font color="red">*</font></td>
					<td width="60%"><input id='employee_no' class="easyui-validatebox"
						value="${employee.employee_no}" style="width: 200px" data-options="required:true" /></td>
				</tr>
				<tr>
					<td align="right"><br>人员名称：<font color="red">*</font></td>
					<td><input id='employee_name' class="easyui-validatebox"
						value="${employee.employee_name}" style="width: 200px" data-options="required:true" /></td>
				</tr>
				<tr>
					<td align="right"><br>手机：<font color="red">*</font></td>
					<td><input id='mobile_phone' class="easyui-validatebox" value="${employee.mobile_phone}"
						style="width: 200px" data-options="required:true" /></td>
				</tr>
				<tr>
					<td align="right"><br>email：<font color="red">*</font></td>
					<td><input id='email' class="easyui-validatebox" value="${employee.email}"
						style="width: 200px" data-options="required:true,validType:'email'" /></td>
				</tr>
				<tr>
					<td align="right"><br> 入职时间： <font color="red">*</font></td>
					<td><input id='in_company' value="${employee.in_company}"
						style="width: 200px" class='easyui-datebox'
						data-options="formatter:myformatter,parser:myparser,required:true" /></td>
				</tr>
				<tr>
					<td align="right"><br> 身份证： <font color="red">*</font></td>
					<td><input id='cert_num' class="easyui-validatebox" value="${employee.cert_num}"
						style="width: 200px" data-options="required:true" /></td>
				</tr>
				<tr>
					<td align="right"><br> 生日：<font color="red">*</font></td>
					<td><input id='birthday' value="${employee.birthday}"
						style="width: 200px" class='easyui-datebox'
						data-options="formatter:myformatter,parser:myparser,required:true" /></td>
				</tr>
				<tr>
					<td align="right"><br> 地址：<font color="red">*</font></td>
					<td><input id='address' class="easyui-validatebox" value="${employee.address}"
						style="width: 200px" data-options="required:true" /></td>
				</tr>
				<tr>
					<td align="right"><br> 邮编： <font color="red">*</font></td>
					<td><input id='zip' class="easyui-validatebox" value="${employee.zip}"
						style="width: 200px" data-options="required:true" /></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><br>
					<a href="#" id="BTN_SAVE" class="easyui-linkbutton"
						iconCls="icon-save">保存</a><br></td>
				</tr>
			</table>
		</form>
	</div>
	<div style="margin: 10px 0px 0px 0px"></div>
</body>
</html>