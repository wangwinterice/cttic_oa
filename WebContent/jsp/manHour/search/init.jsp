<%@page contentType="text/html; charset=utf-8"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<%@ include file="/js/link.jsp"%>
<style type="text/css">
body{ 
	font:14px;
	color:#444;
	/*添加*/
	background-color:#e7ebec;
}
</style>
<script type="text/javascript">
$(function() {
	
	$('#delBtn').bind('click', function () {
		var manHour_id = $(this).attr("manHour_id");
		if (confirm("确认要删除？")) { 
			window.location.href = "<%=basePath%>/erp/manHour/del?manHour_id=" + manHour_id;
        }else{
        	
        }
	});
	
});

</script>
</head>
<body>
	
	<table style="clear:both">
		<tr>
			<td>
				<table border="1" width="800px" bordercolor="#7FB0DE"
					cellpadding="1" cellspacing="0" align="center">
					<tr>
						<td align="center">年份</td>
						<td align="center">月份</td>
						<td align="center">起始~截止日期</td>
						<td align="center">填写状态</td>
						<td align="center">操作</td>
					</tr>
					<c:forEach items="${MANHOUR_LIST}" var="dateList">
						<tr>
							<td name="${dateList.year}">${dateList.year}</td>
							<td name="${dateList.month}">${dateList.month}</td>
							<td>
								${dateList.date_section}
								<c:if test="${dateList.isWeekRecordChange=='true'}">
								<span style="color:red">（周报发生改变，请删除工时重新填写。）</span>
								</c:if>
							</td>
							<td align="center">
								<c:if test="${dateList.isTB==0}">未填报</c:if>
								<c:if test="${dateList.isTB==1 && dateList.isSP==0}">已填报</c:if>
								<c:if test="${dateList.isSP==1}">已审批</c:if>
							</td>
							<td align="center">
								<c:if test="${dateList.isTB==1 && dateList.isSP==0}">
									<a href='<%=basePath%>/erp/manHour/edit?type=VIEW&manHour_id=${ dateList.id}' class='easyui-linkbutton' >查看</a>
									<a href='<%=basePath%>/erp/manHour/edit?type=EDIT&manHour_id=${ dateList.id}' class='easyui-linkbutton' >修改</a> 
									<a href='#' id="delBtn" manHour_id="${ dateList.id}" class='easyui-linkbutton' >删除</a>
								</c:if>
								<c:if test="${dateList.isSP==1}">
									<a href='<%=basePath%>/erp/manHour/appEdit?type=VIEW&manHour_id=${ dateList.id}&from=init' class='easyui-linkbutton' >查看</a>
								</c:if>
								<c:if test="${dateList.isTB!=1}">
									<a href='<%=basePath%>/erp/manHour/edit?type=NEW&week_id=${dateList.week_id}' class='easyui-linkbutton' >补填</a> 
								</c:if>
							</td>
						</tr>
					</c:forEach>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>