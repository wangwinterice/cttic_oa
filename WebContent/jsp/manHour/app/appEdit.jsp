<%@page contentType="text/html; charset=utf-8"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>${DATE_DESC}周报填写</title>

<style type="text/css"> 

.input_width_class{
	width:50px;
}

</style> 

<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	$(function() {
		
		$('#saveBtn').bind('click', function () {
			var result = [];
			var colunms;
			$(".data_tr_class").each(function(){
				
				var prj_id = $(this).find("input[name='prj_id']").val();
				var prj_name = $(this).find("input[name='prj_name']").val();
				
				var manHours = [];
				colunms = 0;
				$(this).find(".manHour_input_class").each(function(){
					var input = $(this).val();
					if(input == ""){
						input = 0;
					}
					manHours.push(input);
					colunms = colunms + 1;
				});
				result.push( prj_id + "," + prj_name + "," + manHours);
			});
			
			
			//验证 input_num_
			for(var i = 0 ; i < colunms ; i++){
				var temp = 0;
				$(".input_num_"+i).each(function(){
					var input = $(this).val();
					if(input == ""){
						input = 0;
					}
					temp = parseInt(temp) + parseInt(input);
				});
				if(temp>24){
					alert("每天的工时合计不能超过8小时。");
					return;
				}
			}
			$.ajax({
				type : "POST", 
				url : '<%=basePath%>/erp/manHour/appSave',
				async : false,
				data: {
					'data' : result.join("|"),
					'id' : $("#manHour_id").val(),
					'begin_date' : $("#begin_date").val(),
					'end_date' : $("#end_date").val(),
					'real_begin_date' : $("#real_begin_date").val(),
					'real_end_date' : $("#real_end_date").val(),
					'week_id' : $("#week_id").val()
				},
				success : function(returnStr) {
					if(returnStr == "1"){
						alert("保存成功。");
						window.location.href = "<%=basePath%>/erp/manHour/appInit";
					}
				}
			});
			
			
		});
		
	});
</script>
</head>

<body >
<br/>
<form id="formId">

<input id="manHour_id" type="hidden" value="${bean.id }" />

<input id="begin_date" type="hidden" value="${bean.begin_date }" />
<input id="end_date" type="hidden" value="${bean.end_date }" />

<input id="real_begin_date" type="hidden" value="${bean.real_begin_date }" />
<input id="real_end_date" type="hidden" value="${bean.real_end_date }" />

<input id="week_id" type="hidden" value="${bean.week_id }" />

<table  style="clear:both">
		<tr>
			<td>
				<table border="1" width="800px" bordercolor="#7FB0DE"
					cellpadding="1" cellspacing="0" align="center">
					<tr>
						<td align="center">项目</td>
						<td align="center">每天工时（${period }）</td>
						<td align="center">审批工时（${period }）</td>
					</tr>
					
					<c:forEach items="${details}" var="detail">
						<tr class="data_tr_class">
							<td align="center">
								${detail.prj_name}
								<input name="prj_name" type="hidden" value="${detail.prj_name}" />
								<input name="prj_id" type="hidden" value="${detail.prj_id}" />
							</td>
							<td align="center">
								<c:forEach items="${detail.man_hour_list}" var="manHour_v" varStatus="status" >
										<input type="text"  class="input_width_class" value="${ manHour_v}" disabled="disabled"  />
								</c:forEach>
							</td>
							<td align="center">
								<c:forEach items="${detail.app_man_hour_list}" var="manHour_v" varStatus="status" >
									<c:if test="${type=='VIEW' }">
										<input type="text"  class="input_width_class manHour_input_class" value="${ manHour_v}" disabled="disabled"  />
									</c:if>
									<c:if test="${type!='VIEW' }">
										<input type="text"  class="input_width_class manHour_input_class easyui-numberbox input_num_${status.index }" data-options="min:0,max:10000,precision:0" value="${ manHour_v}"  />
									</c:if>
								</c:forEach>
							</td>
						</tr>
					</c:forEach>
				</table>
			</td>
		</tr>
	</table>
</form>
<div style="float: left">
<c:if test="${type!='VIEW' }">
	<a href="#" id="saveBtn" class="easyui-linkbutton">审批</a>
</c:if>
<c:if test="${from=='init' }">
	<a href="<%=basePath%>/erp/manHour/init" class="easyui-linkbutton">返回</a>
</c:if>
<c:if test="${from=='appInit' }">
	<a href="<%=basePath%>/erp/manHour/appInit" class="easyui-linkbutton">返回</a>
</c:if>
</div>
</body>
</html>