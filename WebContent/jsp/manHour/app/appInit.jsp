<%@page contentType="text/html; charset=utf-8"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<%@ include file="/js/link.jsp"%>
<style type="text/css">
body{ 
	font:14px;
	color:#444;
	/*添加*/
	background-color:#e7ebec;
}
</style>
<script type="text/javascript">
$(function() {
	//设置查询默认值
	$("#search_year").val($("#defaultYear").val());
	$("#search_week").val($("#defaultWeek").val());
	
	$('#searchBtn').bind('click', function () {
		var search_year = $("#search_year").val();
		var search_week = $("#search_week").val();
		window.location.href = "<%=basePath%>/erp/manHour/appInit?search_year="+search_year+"&search_week="+search_week;
	});
	
	
});

function change(obj){
	
	$.ajax({
		type : "GET", 
		url : '<%=basePath%>/erp/manHour/getWeeksByYear?year='+obj.value,
		async : false,
		success : function(result) {
			var json = eval("("+result+")");
			$("#search_week").empty();
			for(var i=0;i<json.length;i++){
				$("#search_week").append("<option value='"+json[i]+"'>"+json[i]+"</option>");
			}
		}
	});
	
}

</script>
</head>
<body>


<input id="defaultYear" type="hidden" value="${defaultYear }" />
<input id="defaultWeek" type="hidden" value="${defaultWeek }" />

<br/>
<div style="float: left;">

	<span  style="font-size:13px;"> 年份：</span>
	<select id="search_year" onchange="change(this)">
	  <c:forEach items="${years}" var="year">
	  		<option value="${year }">${year }</option>
	  </c:forEach>
	</select>

	<span  style="font-size:13px;"> 周：</span>
	<select id="search_week">
	  <c:forEach items="${yearWeeks}" var="w">
	  		<option value="${w }">${w }</option>
	  </c:forEach>
	</select>
	
	<a href='#' class='easyui-linkbutton' id="searchBtn" >查询</a>
	
</div>  

<br/><br/>
	
	<table style="clear:both">
		<tr>
			<td>
				<table border="1" width="600px" bordercolor="#7FB0DE"
					cellpadding="1" cellspacing="0" align="center">
					<tr>
						<td align="center">年份</td>
						<td align="center">月份</td>
						<td align="center">员工姓名</td>
						<td align="center">起始~截止日期</td>
						<td align="center">填写状态</td>
						<td align="center">操作</td>
					</tr>
					<c:forEach items="${LIST}" var="dateList">
						<tr>
							<td name="${dateList.year}">${dateList.year}</td>
							<td name="${dateList.month}">${dateList.month}</td>
							<td name="${dateList.employee_name}">${dateList.employee_name}</td>
							<td>${dateList.date_section}</td>
							<td align="center">
								<c:if test="${dateList.isSP==0}">未审批</c:if>
								<c:if test="${dateList.isSP==1}">已审批</c:if>
							</td>
							<td align="center">
								<c:if test="${dateList.isSP==0}">
									<a href='<%=basePath%>/erp/manHour/appEdit?manHour_id=${ dateList.id}&from=appInit' class='easyui-linkbutton' >审批</a> 
								</c:if>
								<c:if test="${dateList.isSP==1}">
									<a href='<%=basePath%>/erp/manHour/appEdit?type=VIEW&manHour_id=${ dateList.id}&from=appInit' class='easyui-linkbutton' >查看</a> 
								</c:if>
							</td>
						</tr>
					</c:forEach>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>