<%@page contentType="text/html; charset=utf-8"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>${DATE_DESC}周报填写</title>

<style type="text/css"> 

.manHour_input_class{
	width:50px;
}

</style> 

<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	$(function() {
		
		$('#saveBtn').bind('click', function () {
			var result = [];
			var colunms;
			$(".data_tr_class").each(function(){
				
				var prj_id = $(this).find("input[name='prj_id']").val();
				var prj_name = $(this).find("input[name='prj_name']").val();
				
				var manHours = [];
				colunms = 0;
				$(this).find(".manHour_input_class").each(function(){
					var input = $(this).val();
					if(input == ""){
						input = 0;
					}
					manHours.push(input);
					colunms = colunms + 1;
				});
				result.push( prj_id + "," + prj_name + "," + manHours);
			});
			
			
			//验证 input_num_
			for(var i = 0 ; i < colunms ; i++){
				var temp = 0;
				$(".input_num_"+i).each(function(){
					var input = $(this).val();
					if(input == ""){
						input = 0;
					}
					temp = parseInt(temp) + parseInt(input);
				});
				if(temp>24){
					alert("每天的工时合计不能超过8小时。");
					return;
				}
			}
			$.ajax({
				type : "POST", 
				url : '<%=basePath%>/erp/manHour/save',
				async : false,
				data: {
					'data' : result.join("|"),
					'id' : $("#manHour_id").val(),
					'begin_date' : $("#begin_date").val(),
					'end_date' : $("#end_date").val(),
					'real_begin_date' : $("#real_begin_date").val(),
					'real_end_date' : $("#real_end_date").val(),
					'week_id' : $("#week_id").val()
				},
				success : function(returnStr) {
					alert("保存成功。");
					window.location.href = "<%=basePath%>/erp/manHour/dealCurrent";
				}
			});
			
			
		});
		
	});
</script>
</head>

<body >
<br/>
<form id="formId">

<input id="manHour_id" type="hidden" value="${bean.id }" />

<input id="begin_date" type="hidden" value="${bean.begin_date }" />
<input id="end_date" type="hidden" value="${bean.end_date }" />

<input id="real_begin_date" type="hidden" value="${bean.real_begin_date }" />
<input id="real_end_date" type="hidden" value="${bean.real_end_date }" />

<input id="week_id" type="hidden" value="${bean.week_id }" />
<!-- 
<div style="float: left;font-size:15px;color: black "><b>当前周（${period }）周报填写</b></div>
 -->
<table  style="clear:both">
		<tr>
			<td>
				<table border="1" width="600px" bordercolor="#7FB0DE"
					cellpadding="1" cellspacing="0" align="center">
					<tr>
						<td align="center">项目</td>
						<td align="center">每天工时（${period }）</td>
					</tr>
					
					<c:forEach items="${details}" var="detail">
						<tr class="data_tr_class">
							<td align="center">
								${detail.prj_name}
								<input name="prj_name" type="hidden" value="${detail.prj_name}" />
								<input name="prj_id" type="hidden" value="${detail.prj_id}" />
							</td>
							<td align="center">
								<c:forEach items="${detail.man_hour_list}" var="manHour_v" varStatus="status" >
									<input type="text"  class="manHour_input_class easyui-numberbox input_num_${status.index }" data-options="min:0,max:10000,precision:0" value="${ manHour_v}"  />
								</c:forEach>
							</td>
						</tr>
					</c:forEach>
				</table>
			</td>
		</tr>
	</table>
</form>
<div style="float: left">
	<a href="#" id="saveBtn" class="easyui-linkbutton">保存</a>
</div>
</body>
</html>