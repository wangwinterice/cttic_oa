<%@page contentType="text/html; charset=utf-8"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>${DATE_DESC}周报填写</title>

<style type="text/css"> 

.manHour_input_class{
	width:50px;
}

</style> 

<%@ include file="/js/link.jsp"%>
</head>

<body >
<br/>
<form id="formId">

<input id="manHour_id" type="hidden" value="${bean.id }" />


<table  style="clear:both">
		<tr>
			<td>
				<table border="1" width="600px" bordercolor="#7FB0DE"
					cellpadding="1" cellspacing="0" align="center">
					<tr>
						<td align="center">项目</td>
						<td align="center">每天工时（${period }）</td>
					</tr>
					
					<c:forEach items="${details}" var="detail">
						<tr class="data_tr_class">
							<td align="center">
								${detail.prj_name}
								<input name="prj_name" type="hidden" value="${detail.prj_name}" />
								<input name="prj_id" type="hidden" value="${detail.prj_id}" />
							</td>
							<td align="center">
								<c:forEach items="${detail.man_hour_list}" var="manHour_v" varStatus="status" >
										<input type="text"  class="manHour_input_class" value="${ manHour_v}" disabled="disabled"  />
								</c:forEach>
							</td>
						</tr>
					</c:forEach>
				</table>
			</td>
		</tr>
	</table>
</form>
<div style="float: left">
	<a href="<%=basePath%>/erp/manHour/dealCurrent?type=edit" class="easyui-linkbutton">修改</a>
</div>
</body>
</html>