<%@page contentType="text/html; charset=utf-8"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	$(function() {
		<c:forEach items="${INFO_EVALUATION_DIMENSION}" var="evaluationItem">
			$('td[name=SELF_VALUE_${evaluationItem.dimension_id}]').each(function(i, obj){
				<c:if test="${empty EVALUATION_RECORD}">
					$(obj).append("<input type='number' style='width:40px; height:40px;' id='SELF_VALUE_${evaluationItem.dimension_id}'></input>");
				</c:if>
				<c:if test="${!empty EVALUATION_RECORD}">
					<c:forEach items="${EVALUATION_RESULT}" var="evaluationResult">
						<c:if test="${'SELF_VALUE' == evaluationResult.evaluation_type && evaluationItem.dimension_id == evaluationResult.evaluation_item}">
							<c:if test="${EVALUATION_RECORD.state == '0'}">
								$(obj).append("<input type='number' style='width:40px; height:40px;' id='SELF_VALUE_${evaluationItem.dimension_id}' value='${evaluationResult.evaluation_value}'></input>");	
							</c:if>
							<c:if test="${EVALUATION_RECORD.state == '1'}">
								$(obj).append("${evaluationResult.evaluation_value}");
							</c:if>
						</c:if>
					</c:forEach>
				</c:if>
			});
		</c:forEach>
		$('#saveBtn').bind('click', function () {
			var paramJson = {};
			<c:forEach items="${INFO_EVALUATION_DIMENSION}" var="evaluationItem">
				if ($('#SELF_VALUE_${evaluationItem.dimension_id}').val() == '') {
					alert('【${evaluationItem.dimension_name}】评分不能为空');
					return;
				}	
				if ($('#SELF_VALUE_${evaluationItem.dimension_id}').val() > ${evaluationItem.weight_function} ) {
					alert('【${evaluationItem.dimension_name}】评分不能大于最大分值【${evaluationItem.weight_function}】');
					return;
				}
				if ($('#SELF_VALUE_${evaluationItem.dimension_id}').val() < 0) {
					alert('【${evaluationItem.dimension_name}】评分不能小于零');
					return;
				}
				paramJson['SELF_VALUE_${evaluationItem.dimension_id}'] = $('#SELF_VALUE_${evaluationItem.dimension_id}').val();
			</c:forEach>
			paramJson['EVALUATION_DATE'] = '${EVALUATION_YEAR}${EVALUATION_MONTH}';
			<c:if test="${empty EVALUATION_RECORD}">
				paramJson['OPER_TYPE'] = 'ADD';
			</c:if>
			<c:if test="${!empty EVALUATION_RECORD}">
				paramJson['OPER_TYPE'] = 'EDIT';
			</c:if>
			//alert(JSON.stringify(paramJson));
			$.ajax({ 
				type: "POST", 
				url: "<%=basePath%>/erp/evaluation/selfSave",
				data : paramJson,
				async : false,
				success : function(returnStr) {
					var jsonReturnStr = JSON.parse(returnStr);
					if (jsonReturnStr.STATUS_CODE == '1') {
						alert(jsonReturnStr.STATUS_INFO);
						window.location = "<%=basePath%>/erp/evaluation/selfView?EVALUATION_YEAR=${EVALUATION_YEAR}&EVALUATION_MONTH=${EVALUATION_MONTH}";
					} else {
						alert(jsonReturnStr.STATUS_INFO);
					}
				}
			});
		});
		<c:if test="${!empty EVALUATION_RECORD}">
			<c:if test="${EVALUATION_RECORD.state == '0'}">
				$('#saveBtn').show();
			</c:if>
			<c:if test="${EVALUATION_RECORD.state == '1'}">
				$('#saveBtn').hide();
				alert("${EVALUATION_YEAR}年${EVALUATION_MONTH}月的考核已经结束");
			</c:if>
		</c:if>
	});
</script>
</head>
<body>
		<table border="0" align="center">
			<tr>
				<td align="center" colspan="3">
					<h1>${EVALUATION_YEAR}年度软件工程部员工绩效考核表</h1>
				</td>
			</tr>
			<tr>
				<td align="left">
					&nbsp;&nbsp;&nbsp;&nbsp;姓名：${EVALUATION_EMPLOYEE.employee_name}
				</td>
				<td align="left">
					&nbsp;&nbsp;&nbsp;&nbsp;工号：${EVALUATION_EMPLOYEE.employee_no}
				</td>
				<td align="right">
					考核时间：${EVALUATION_YEAR}年${EVALUATION_MONTH}月&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<table border="1" bordercolor="#7FB0DE"
						cellpadding="1" cellspacing="0" align="center">
						<tr>
							<td width="40px" align="center" nowrap>序号</td>
							<td width="50px" align="center" nowrap>维度</td>
							<td width="50px" align="center" nowrap>维度权重</td>
							<td width="80px" align="center" nowrap>被考核人评分</td>
						</tr>
						<c:forEach items="${INFO_EVALUATION_DIMENSION}" var="evaluationItem">
							<tr>
								<td name="DIMENSION_${evaluationItem.dimension_id}" align="center" nowrap>
									<span>${evaluationItem.dimension_id}</span>
								</td>
								<td name="DIMENSION_NAME_${evaluationItem.dimension_id}" align="center" nowrap>
									<span>${evaluationItem.dimension_name}</span>
								</td>
								<td name="DIMENSION_WEIGHT_${evaluationItem.dimension_id}" align="center" nowrap>
									<span>${evaluationItem.weight_function}%</span>
								</td>
								<td name="SELF_VALUE_${evaluationItem.dimension_id}" align="center" nowrap>
									
								</td>
							</tr>
						</c:forEach>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="3" align="right">
					<a href="#" class="easyui-linkbutton" id="saveBtn">保&nbsp;存</a>
				</td>
			</tr>
		</table>
</body>
</html>