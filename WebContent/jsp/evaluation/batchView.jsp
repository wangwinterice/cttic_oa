<%@page contentType="text/html; charset=utf-8"%>
<%@page import="com.cttic.erp.bean.ext.InfoOperBeanExt"%>
<%@page import="com.cttic.erp.common.util.InfoOperUtil"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
	InfoOperBeanExt infoOperBean = InfoOperUtil.getCurrOperInfo(request);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	$(function() {
		$('#evaluation_date_select').combo({
			required : true,
			editable : false
		});
		$('#evaluation_date_list').appendTo($('#evaluation_date_select').combo('panel'));
		$('#evaluation_date_select').combo('setValue', '${EVALUATION_DATE}').combo('setText',
				'${EVALUATION_DATE}'.substring(0, 4) + "年" + '${EVALUATION_DATE}'.substring(4, 6) + "月").combo('hidePanel');
		$('#evaluation_date_list input').click( function() {
			var v = $(this).val();
			var s = $(this).next('span').text();
			$('#evaluation_date_select').combo('setValue', v).combo('setText',
							s).combo('hidePanel');
			window.location = '<%=basePath%>/erp/evaluation/batchView?EVALUATION_DATE=' + $(this).val();
		});
		<c:forEach items="${INFO_EVALUATION_DIMENSION}" var="dimensionItem">
			$('td[name=DIMENSION_${dimensionItem.dimension_id}]').each(function(i, obj){
				if (i != 0) {
					$(obj).remove();
				} else {
					$(obj).attr("colspan", $('td[name=DIMENSION_${dimensionItem.dimension_id}]').length);
				}
			});
		</c:forEach>
		<c:forEach items="${EVALUATION_RESULT}" var="evaluationResult">
			$('#${evaluationResult.employee_id}_${evaluationResult.evaluation_type}_${evaluationResult.evaluation_item}').html('${evaluationResult.evaluation_value}');
		</c:forEach>
	});
</script>
</head>
<body>
		<table id="reportTable" border="0" align="center">
			<tr>
				<td align="center" colspan="8">
					<h1>${EVALUATION_YEAR}年度软件工程部员工绩效考核表</h1>
				</td>
			</tr>
			<tr>
				<td align="right" colspan="3">
					考核时间：
					<select id="evaluation_date_select" style="width: 150px"></select>
					<div id="evaluation_date_list">
						<div style="color: #99BBE8; background: #fafafa; padding: 5px;">---请选择---</div>
						<c:forEach items="${EVALUATION_DATE_LIST}" var="evaluationDateList">
							<input type="radio" name="lang" value="${evaluationDateList.column_value}">
							<span>${evaluationDateList.column_desc}</span>
							<br />
						</c:forEach>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<table border="1" bordercolor="#7FB0DE"
						cellpadding="1" cellspacing="0" align="center">
						<tr>
							<td colspan="2" align="center">考核维度</td>
							<c:forEach items="${INFO_EVALUATION_DIMENSION}" var="evaluationItem">
								<td align="center">${evaluationItem.dimension_name}</td>
							</c:forEach>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td align="center">序号</td>
							<td align="center">姓名</td>
							<c:forEach items="${INFO_EVALUATION_DIMENSION}" var="evaluationItem">
								<td align="center">${evaluationItem.weight_function}</td>
							</c:forEach>
							<td align="center">考核成绩</td>
							<td align="center">考核等级</td>
						</tr>
						<c:forEach items="${EMPLOYEE_LIST}" var="employeeList">
							<tr>
								<td>${employeeList.employee_id}</td>
								<td>${employeeList.employee_name}</td>
								<c:forEach items="${INFO_EVALUATION_DIMENSION}" var="evaluationItem">
									<td id="${employeeList.employee_id}_CHECK_VALUE_${evaluationItem.dimension_id}" align="center"></td>
								</c:forEach>
								<td id="${employeeList.employee_id}_CHECK_RESULT_VALUE" align="center"></td>
								<td id="${employeeList.employee_id}_CHECK_RESULT_LEVEL" align="center"></td>
							</tr>
						</c:forEach>
					</table>
				</td>
			</tr>
		</table>
</body>
</html>