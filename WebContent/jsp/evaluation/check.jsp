<%@page contentType="text/html; charset=utf-8"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	$(function() {
		$('#employee_select').combo({
			required : true,
			editable : false
		});
		$('#employee_list').appendTo($('#employee_select').combo('panel'));
		<c:if test="${!empty SELECT_EMPLOYEE}">
			$('#employee_select').combo('setValue', '${SELECT_EMPLOYEE.employee_no}').combo('setText',
				'姓名：${SELECT_EMPLOYEE.employee_name}    工号：${SELECT_EMPLOYEE.employee_no}').combo('hidePanel');
		</c:if>
		$('#employee_list input').click( function() {
			var v = $(this).val();
			var s = $(this).next('span').text();
			$('#employee_select').combo('setValue', v).combo('setText',
							s).combo('hidePanel');
			window.location = '<%=basePath%>/erp/evaluation/check?EMPLOYEE_ID=' + $(this).val();
		});
		<c:forEach items="${INFO_EVALUATION_DIMENSION}" var="evaluationItem">
			$('td[name=CHECK_VALUE_${evaluationItem.dimension_id}]').each(function(i, obj){
				<c:if test="${!empty EVALUATION_RECORD}">
					<c:if test="${EVALUATION_RECORD.state == '0'}">
						$(obj).append("<input type='number' style='width:40px; height:40px;' id='CHECK_VALUE_${evaluationItem.dimension_id}'></input>");
					</c:if>	
					<c:if test="${EVALUATION_RECORD.state == '1'}">
						<c:forEach items="${EVALUATION_RESULT}" var="evaluationResult">
							<c:if test="${'CHECK_VALUE' == evaluationResult.evaluation_type && evaluationItem.dimension_id == evaluationResult.evaluation_item}">
								$(obj).append("<input type='number' style='width:40px; height:40px;' id='CHECK_VALUE_${evaluationItem.dimension_id}' value='${evaluationResult.evaluation_value}'></input>");
							</c:if>
						</c:forEach>
					</c:if>	
				</c:if>
			});
		</c:forEach>
		$('#saveBtn').bind('click', function () {
			var paramJson = {};
			<c:forEach items="${INFO_EVALUATION_DIMENSION}" var="evaluationItem">
				if ($('#CHECK_VALUE_${evaluationItem.dimension_id}').val() == '') {
					alert('考核指标【${evaluationItem.dimension_name}】评分不能为空');
					return;
				}	
				if ($('#CHECK_VALUE_${evaluationItem.dimension_id}').val() > ${evaluationItem.weight_function} ) {
					alert('考核指标【${evaluationItem.dimension_name}】评分不能大于最大分值【${evaluationItem.weight_function}】');
					return;
				}
				if ($('#CHECK_VALUE_${evaluationItem.dimension_id}').val() < 0) {
					alert('考核指标【${evaluationItem.dimension_name}】评分不能小于零');
					return;
				}
				paramJson['CHECK_VALUE_${evaluationItem.dimension_id}'] = $('#CHECK_VALUE_${evaluationItem.dimension_id}').val();
			</c:forEach>
			paramJson['RESULT_DESCRIPTION'] = $('#RESULT_DESCRIPTION').val();
			paramJson['EMPLOYEE_ID'] = '${SELECT_EMPLOYEE.employee_id}';
			paramJson['EVALUATION_DATE'] = '${EVALUATION_YEAR}${EVALUATION_MONTH}';
			//alert(JSON.stringify(paramJson));
			$.ajax({ 
				type: "POST", 
				url: "<%=basePath%>/erp/evaluation/checkSave",
				data : paramJson,
				async : false,
				success : function(returnStr) {
					var jsonReturnStr = JSON.parse(returnStr);
					if (jsonReturnStr.STATUS_CODE == '1') {
						alert(jsonReturnStr.STATUS_INFO);
						window.location = "<%=basePath%>/erp/evaluation/checkView?EMPLOYEE_ID=${SELECT_EMPLOYEE.employee_no}&EVALUATION_YEAR=${EVALUATION_YEAR}&EVALUATION_MONTH=${EVALUATION_MONTH}";
					} else {
						alert(jsonReturnStr.STATUS_INFO);
					}
				}
			});
		});
		<c:if test="${empty EVALUATION_RECORD}">
			$('#saveBtn').hide();
			<c:if test="${!empty SELECT_EMPLOYEE}">
				alert("${EVALUATION_YEAR}年${EVALUATION_MONTH}月的考核[${SELECT_EMPLOYEE.employee_name}]还没有进行自评");
			</c:if>
		</c:if>
		<c:if test="${!empty EVALUATION_RECORD}">
			$('#saveBtn').show();
			$("#RESULT_DESCRIPTION").val('${EVALUATION_RECORD.result_description}'.split("<br>").join("\r\n"));
		</c:if>
	});
</script>
</head>
<body>
		<table border="0" align="center">
			<tr>
				<td align="center" colspan="3">
					<h1>${EVALUATION_YEAR}年度软件工程部员工绩效考核表</h1>
				</td>
			</tr>
			<tr>
				<td align="left">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<select id="employee_select" style="width: 300px"></select>
					<div id="employee_list">
						<div style="color: #99BBE8; background: #fafafa; padding: 5px;">---请选择---</div>
						<c:forEach items="${EMPLOYEE_LIST}" var="employee">
							<input type="radio" name="lang" value="${employee.employee_no}">
							<span>姓名：${employee.employee_name}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;工号：${employee.employee_no}</span>
							<br />
						</c:forEach>
					</div>
				</td>
				<td align="right">
					考核时间：${EVALUATION_YEAR}年${EVALUATION_MONTH}月&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<table border="1" bordercolor="#7FB0DE"
						cellpadding="1" cellspacing="0" align="center">
						<tr>
							<td width="40px" align="center" nowrap>序号</td>
							<td width="50px" align="center" nowrap>维度</td>
							<td width="50px" align="center" nowrap>维度权重</td>
							<td width="80px" align="center" nowrap>被考核人评分</td>
							<td width="80px" align="center" nowrap>考核人评分</td>
						</tr>
						<c:forEach items="${INFO_EVALUATION_DIMENSION}" var="evaluationItem">
							<tr>
								<td name="DIMENSION_${evaluationItem.dimension_id}" align="center" nowrap>
									<span>${evaluationItem.dimension_id}</span>
								</td>
								<td name="DIMENSION_NAME_${evaluationItem.dimension_id}" align="center" nowrap>
									<span>${evaluationItem.dimension_name}</span>
								</td>
								<td name="DIMENSION_WEIGHT_${evaluationItem.dimension_id}" align="center" nowrap>
									<span>${evaluationItem.weight_function}%</span>
								</td>
								<td name="SELF_VALUE_${evaluationItem.dimension_id}" align="center" nowrap>
									<c:forEach items="${EVALUATION_RESULT}" var="evaluationResult">
										<c:if test="${'SELF_VALUE' == evaluationResult.evaluation_type && evaluationItem.dimension_id == evaluationResult.evaluation_item}">
											${evaluationResult.evaluation_value}
										</c:if>
									</c:forEach>
								</td>
								<td name="CHECK_VALUE_${evaluationItem.dimension_id}" align="center" nowrap>
									
								</td>
							</tr>
						</c:forEach>
					</table>
					<table border="1" bordercolor="#7FB0DE"
						cellpadding="1" cellspacing="0" align="center">
						<tr>
							<td align="center">被考核人成绩</td>
							<td width="30px" align="center">
								<c:forEach items="${EVALUATION_RESULT}" var="evaluationResult">
									<c:if test="${'SELF_RESULT' == evaluationResult.evaluation_type && 'VALUE' == evaluationResult.evaluation_item}">
										${evaluationResult.evaluation_value}
									</c:if>
								</c:forEach>
							</td>
							<td align="center">被考核人等级</td>
							<td width="30px" align="center">
								<c:forEach items="${EVALUATION_RESULT}" var="evaluationResult">
									<c:if test="${'SELF_RESULT' == evaluationResult.evaluation_type && 'LEVEL' == evaluationResult.evaluation_item}">
										${evaluationResult.evaluation_value}
									</c:if>
								</c:forEach>
							</td>
						</tr>
						<tr>
							<td align="center">绩效总结与改进建议<br>（考核人填写）</td>
							<td colspan="3" align="left">
								<textarea rows="8" cols='27' id="RESULT_DESCRIPTION">${EVALUATION_RECORD.result_description}</textarea>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="3" align="right">
					<a href="#" class="easyui-linkbutton" id="saveBtn">保&nbsp;存</a>
				</td>
			</tr>
		</table>
</body>
</html>