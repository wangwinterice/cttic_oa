<%@page contentType="text/html; charset=utf-8"%>
<%@page import="com.cttic.erp.bean.ext.InfoOperBeanExt"%>
<%@page import="com.cttic.erp.common.util.InfoOperUtil"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
	InfoOperBeanExt infoOperBean = InfoOperUtil.getCurrOperInfo(request);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	$(function() {
		$('#editBtn').bind('click', function () {
			window.location = "<%=basePath%>/erp/evaluation/self";
		});
	});
</script>
</head>
<body>
		<table border="0" align="center">
			<tr>
				<td align="center" colspan="3">
					<h1>${EVALUATION_YEAR}年度软件工程部员工绩效考核表</h1>
				</td>
			</tr>
			<tr>
				<td align="left">
					&nbsp;&nbsp;&nbsp;&nbsp;姓名：<%= infoOperBean.getInfoEmployeeBean().getEmployee_name() %>
				</td>
				<td align="left">
					&nbsp;&nbsp;&nbsp;&nbsp;工号：<%= infoOperBean.getInfoEmployeeBean().getEmployee_no() %>
				</td>
				<td align="right">
					考核时间：${EVALUATION_YEAR}年${EVALUATION_MONTH}月&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<table border="1" bordercolor="#7FB0DE"
					cellpadding="1" cellspacing="0" align="center">
						<tr>
							<td width="40px" align="center" nowrap>序号</td>
							<td width="50px" align="center" nowrap>维度</td>
							<td width="50px" align="center" nowrap>维度权重</td>
							<td width="80px" align="center" nowrap>被考核人评分</td>
						</tr>
						<c:forEach items="${INFO_EVALUATION_DIMENSION}" var="evaluationItem">
							<tr>
								<td name="DIMENSION_${evaluationItem.dimension_id}" align="center" nowrap>
									<span>${evaluationItem.dimension_id}</span>
								</td>
								<td name="DIMENSION_NAME_${evaluationItem.dimension_id}" align="center" nowrap>
									<span>${evaluationItem.dimension_name}</span>
								</td>
								<td name="DIMENSION_WEIGHT_${evaluationItem.dimension_id}" align="center" nowrap>
									<span>${evaluationItem.weight_function}%</span>
								</td>
								<td name="SELF_VALUE_${evaluationItem.dimension_id}" align="center">
									<c:forEach items="${EVALUATION_RESULT}" var="evaluationResult">
										<c:if test="${'SELF_VALUE' == evaluationResult.evaluation_type && evaluationItem.dimension_id == evaluationResult.evaluation_item}">
											${evaluationResult.evaluation_value}
										</c:if>
									</c:forEach>
								</td>
							</tr>
						</c:forEach>
					</table>
					<table border="1" bordercolor="#7FB0DE"
						cellpadding="1" cellspacing="0" align="center">
						<tr>
							<td align="center">被考核人成绩</td>
							<td width="30px" align="center">
								<c:forEach items="${EVALUATION_RESULT}" var="evaluationResult">
									<c:if test="${'SELF_RESULT' == evaluationResult.evaluation_type && 'VALUE' == evaluationResult.evaluation_item}">
										${evaluationResult.evaluation_value}
									</c:if>
								</c:forEach>
							</td>
							<td align="center">被考核人等级</td>
							<td width="30px" align="center">
								<c:forEach items="${EVALUATION_RESULT}" var="evaluationResult">
									<c:if test="${'SELF_RESULT' == evaluationResult.evaluation_type && 'LEVEL' == evaluationResult.evaluation_item}">
										${evaluationResult.evaluation_value}
									</c:if>
								</c:forEach>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="3" align="right">
					<a href="#" class="easyui-linkbutton" id="editBtn">修&nbsp;改</a>
				</td>
			</tr>
		</table>
</body>
</html>