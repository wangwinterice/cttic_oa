<%@page contentType="text/html; charset=utf-8"%>
<%@page import="com.cttic.erp.bean.ext.InfoOperBeanExt"%>
<%@page import="com.cttic.erp.common.util.InfoOperUtil"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
	InfoOperBeanExt infoOperBean = InfoOperUtil
			.getCurrOperInfo(request);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	$(function() {
		var tableWidth = 550 + ${fn:length(EVALUATION_DATE_LIST)}*50;
		$('#tt').datagrid({
			width: tableWidth,
			height: 1330,
			fitColumns: true,
			singleSelect: true,
			remoteSort:true,
			onSortColumn:queryDate,
			columns:
				[
					[
						{field:'employee_id',title:'工号',align:'center',rowspan:2,width:100,sortable:true},
						{field:'employee_name',title:'姓名',align:'center',rowspan:2,width:150},
						<c:forEach items="${EVALUATION_DATE_LIST}" var="dateList">
							{title:'${dateList.column_desc}',colspan:3,width:300},
						</c:forEach>
					],
					[
						<c:forEach items="${EVALUATION_DATE_LIST}" var="dateList">
							{field:'${dateList.column_value}_SELF_RESULT_VALUE',title:'自评',align:'center',width:150,sortable:true},
							{field:'${dateList.column_value}_CHECK_RESULT_VALUE',title:'考核成绩',align:'center',width:150,sortable:true},
							{field:'${dateList.column_value}_CHECK_RESULT_LEVEL',title:'考核等级',align:'center',width:150,sortable:true},
						</c:forEach>
					]
			    ]
		}).datagrid('loadData', queryDate());
	});
	function queryDate(sort, order) {
		sort = sort == null ? 'employee_id' : sort;
		order = order == null ? 'acs' : order;
		$.ajax({
			type: "POST", 
			url: "<%=basePath%>/erp/evaluation/stateQuery",
			data : {'sort' : sort, 'order' : order},
			async : false,
			success : function(returnStr) {
				var jsonReturnStr = JSON.parse(returnStr);
				if (jsonReturnStr.STATUS_CODE == '1') {
					$('#tt').datagrid('loadData', jsonReturnStr.RETURN_INFO);
				} else {
					alert(jsonReturnStr.STATUS_INFO);
				}
			}
		});
		$('#exportBtn').bind('click', function () {
			$.ajax({
				type : "POST", 
				url : '<%=basePath%>/erp/evaluation/generatorExcel',
				async : false,
				data: {
				},
				success : function(returnStr) {
					var jsonReturnStr = JSON.parse(returnStr);
					var url = "<%=basePath%>/erp/evaluation/downloadExcel?"
					      + "&_FILE_NAME=" + jsonReturnStr.RETURN_INFO;
					var iTop = (window.screen.availHeight-30-200)/2; //获得窗口的垂直位置;
					var iLeft = (window.screen.availWidth-10-270)/2; //获得窗口的水平位置;
					var styleCss = 'height=160, width=270, top='+iTop+', left='+iLeft+', toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, status=no';
					window.open(url, '', styleCss);
				}
			});
		});
	}
</script>
</head>
<body>
	<table border="0" align="center">
		<tr>
			<td align="right">
				<a href='#' class='easyui-linkbutton' id='exportBtn'>导出</a>
			</td>
		</tr>
		<tr>
			<td>
				<table id="tt"></table>
			</td>
		</tr>
	</table>
</body>
</html>