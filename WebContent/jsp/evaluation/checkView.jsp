<%@page contentType="text/html; charset=utf-8"%>
<%@page import="com.cttic.erp.bean.ext.InfoOperBeanExt"%>
<%@page import="com.cttic.erp.common.util.InfoOperUtil"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
	InfoOperBeanExt infoOperBean = InfoOperUtil.getCurrOperInfo(request);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	$(function() {
		$('#employee_select').combo({
			required : true,
			editable : false
		});
		$('#employee_list').appendTo($('#employee_select').combo('panel'));
		<c:if test="${!empty SELECT_EMPLOYEE}">
			$('#employee_select').combo('setValue', '${SELECT_EMPLOYEE.employee_no}').combo('setText',
				'姓名：${SELECT_EMPLOYEE.employee_name}    工号：${SELECT_EMPLOYEE.employee_no}').combo('hidePanel');
		</c:if>
		$('#employee_list input').click( function() {
			var v = $(this).val();
			var s = $(this).next('span').text();
			$('#employee_select').combo('setValue', v).combo('setText',
							s).combo('hidePanel');
			window.location = '<%=basePath%>/erp/evaluation/check?EMPLOYEE_ID=' + $(this).val();
		});
		$('#editBtn').bind('click', function () {
			window.location = "<%=basePath%>/erp/evaluation/check?EMPLOYEE_ID=${SELECT_EMPLOYEE.employee_id}";
		});
	});
</script>
</head>
<body>
		<div  style="text-align: center">
		</div>
		<table border="0" align="center">
			<tr>
				<td align="center" colspan="3">
					<h1>${EVALUATION_YEAR}年度软件工程部员工绩效考核表</h1>
				</td>
			</tr>
			<tr>
				<td align="left">
					&nbsp;&nbsp;&nbsp;&nbsp;
					<select id="employee_select" style="width: 300px"></select>
					<div id="employee_list">
						<div style="color: #99BBE8; background: #fafafa; padding: 5px;">---请选择---</div>
						<c:forEach items="${EMPLOYEE_LIST}" var="employee">
							<input type="radio" name="lang" value="${employee.employee_no}">
							<span>姓名：${employee.employee_name}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;工号：${employee.employee_no}</span>
							<br />
						</c:forEach>
					</div>
				</td>
				<td align="right">
					考核时间：${EVALUATION_YEAR}年${EVALUATION_MONTH}月&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<table border="1" bordercolor="#7FB0DE"
						cellpadding="1" cellspacing="0" align="center">
						<tr>
							<td width="40px" align="center" nowrap>序号</td>
							<td width="50px" align="center" nowrap>维度</td>
							<td width="50px" align="center" nowrap>维度权重</td>
							<td width="80px" align="center" nowrap>被考核人评分</td>
							<td width="80px" align="center">考核人评分</td>
						</tr>
						<c:forEach items="${INFO_EVALUATION_DIMENSION}" var="evaluationItem">
							<tr>
								<td name="DIMENSION_${evaluationItem.dimension_id}" align="center">
									<span>${evaluationItem.dimension_id}</span>
								</td>
								<td name="DIMENSION_NAME_${evaluationItem.dimension_id}" align="center">
									<span>${evaluationItem.dimension_name}</span>
								</td>
								<td name="DIMENSION_WEIGHT_${evaluationItem.dimension_id}" align="center">
									<span>${evaluationItem.weight_function}%</span>
								</td>
								<td name="SELF_VALUE_${evaluationItem.dimension_id}" align="center">
									<c:forEach items="${EVALUATION_RESULT}" var="evaluationResult">
										<c:if test="${'SELF_VALUE' == evaluationResult.evaluation_type && evaluationItem.dimension_id == evaluationResult.evaluation_item}">
											${evaluationResult.evaluation_value}
										</c:if>
									</c:forEach>
								</td>
								<td name="CHECK_VALUE_${evaluationItem.dimension_id}" align="center">
									<c:forEach items="${EVALUATION_RESULT}" var="evaluationResult">
										<c:if test="${'CHECK_VALUE' == evaluationResult.evaluation_type && evaluationItem.dimension_id == evaluationResult.evaluation_item}">
											${evaluationResult.evaluation_value}
										</c:if>
									</c:forEach>
								</td>
							</tr>
						</c:forEach>
					</table>
					<table border="1" bordercolor="#7FB0DE"
						cellpadding="1" cellspacing="0" align="center">
						<tr>
							<td align="center">被考核人成绩</td>
							<td width="30px" align="center">
								<c:forEach items="${EVALUATION_RESULT}" var="evaluationResult">
									<c:if test="${'SELF_RESULT' == evaluationResult.evaluation_type && 'VALUE' == evaluationResult.evaluation_item}">
										${evaluationResult.evaluation_value}
									</c:if>
								</c:forEach>
							</td>
							<td align="center">被考核人等级</td>
							<td width="30px" align="center">
								<c:forEach items="${EVALUATION_RESULT}" var="evaluationResult">
									<c:if test="${'SELF_RESULT' == evaluationResult.evaluation_type && 'LEVEL' == evaluationResult.evaluation_item}">
										${evaluationResult.evaluation_value}
									</c:if>
								</c:forEach>
							</td>
						</tr>
						<tr>
							<td align="center">考核人打分成绩</td>
							<td align="center">
								<c:forEach items="${EVALUATION_RESULT}" var="evaluationResult">
									<c:if test="${'CHECK_RESULT' == evaluationResult.evaluation_type && 'VALUE' == evaluationResult.evaluation_item}">
										${evaluationResult.evaluation_value}
									</c:if>
								</c:forEach>
							</td>
							<td align="center">考核人打分等级</td>
							<td align="center">
								<c:forEach items="${EVALUATION_RESULT}" var="evaluationResult">
									<c:if test="${'CHECK_RESULT' == evaluationResult.evaluation_type && 'LEVEL' == evaluationResult.evaluation_item}">
										${evaluationResult.evaluation_value}
									</c:if>
								</c:forEach>
							</td>
						</tr>
						<tr>
							<td align="center">最终考核成绩</td>
							<td align="center">
								<c:forEach items="${EVALUATION_RESULT}" var="evaluationResult">
									<c:if test="${'CHECK_RESULT' == evaluationResult.evaluation_type && 'VALUE' == evaluationResult.evaluation_item}">
										${evaluationResult.evaluation_value}
									</c:if>
								</c:forEach>
							</td>
							<td align="center">最终考核等级</td>
							<td align="center">
								<c:forEach items="${EVALUATION_RESULT}" var="evaluationResult">
									<c:if test="${'CHECK_RESULT' == evaluationResult.evaluation_type && 'LEVEL' == evaluationResult.evaluation_item}">
										${evaluationResult.evaluation_value}
									</c:if>
								</c:forEach>
							</td>
						</tr>
						<tr>
							<td align="center">绩效总结与改进建议<br>（考核人填写）</td>
							<td colspan="3" align="left">
								${EVALUATION_RECORD.result_description}
							</td>
						</tr>
						<tr>
							<td align="center" rowspan="3">签字确认</td>
							<td align="center">被考核人</td>
							<td align="center">考核人</td>
							<td align="center">考核人上级</td>
						</tr>
						<tr>
							<td rowspan="2">&nbsp;</td>
							<td rowspan="2">&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="3" align="right">
					<a href="#" class="easyui-linkbutton" id="editBtn">修&nbsp;改</a>
				</td>
			</tr>
		</table>
</body>
</html>