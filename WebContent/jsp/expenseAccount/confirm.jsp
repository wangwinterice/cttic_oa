<%@page contentType="text/html; charset=utf-8"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	$(function() {
		
	});
	
	function operEvent(recordId, state) {
		$.ajax({
			type: "POST", 
			url: "<%=basePath%>/erp/expenseAccount/save?OPER_TYPE=OPER",
			data : {'RECORD_ID' : recordId, 'STATE' : state},
			async : false,
			success : function(returnStr) {
				var jsonReturnStr = JSON.parse(returnStr);
				if (jsonReturnStr.STATUS_CODE == '1') {
					alert(jsonReturnStr.STATUS_INFO);
					window.location = "<%=basePath%>/erp/expenseAccount/confirm";
				} else {
					alert(jsonReturnStr.STATUS_INFO);
				}
			}
		});
	}
</script>
</head>
<body>
	<br>
	<br>
	<table border="1" bordercolor="#7FB0DE" cellpadding="1"
		cellspacing="0" align="center" width="800px" class="content">
		<tr>
			<td class="tdtitle" colspan="7">
			[报销申请记录]
			</td>
		</tr>
		<tr>
			<td class="tabletitle" width="80px" height="50px">填报项目</td>
			<td class="tabletitle" width="80px">费用类型</td>
			<td class="tabletitle" width="80px">费用金额</td>
			<td class="tabletitle" width="80px">申请时间</td>
			<td class="tabletitle">费用说明</td>
			<td class="tabletitle" width="80px">费用状态</td>
			<td class="tabletitle" width="100px">操作</td>
		</tr>
		<c:forEach items="${EXPENSE_ACCOUNT_RECORD}" var="expenseAccountRecord">
			<tr>
				<td><input type="hidden" id="${expenseAccountRecord.record_id}_PRJ_ID" value="${expenseAccountRecord.prj_id}" />
					<c:forEach items="${PRJ_LIST}" var="prjList">
						<c:if test="${expenseAccountRecord.prj_id == prjList.prj_id}">
							${prjList.prj_name}
						</c:if>
					</c:forEach>
				</td>
				<td>
					<c:forEach items="${EXPENSE_ACCOUNT_EXPENSE_TYPE}" var="expenseTypeSysParam">
						<c:if test="${expenseAccountRecord.expense_type == expenseTypeSysParam.column_value}">
							${expenseTypeSysParam.column_desc}
							<input type="hidden" id="${expenseAccountRecord.record_id}_EXPENSE_TYPE_DESC" value="${expenseTypeSysParam.column_desc}" />
						</c:if>
					</c:forEach>
				</td>
				<td>
					${expenseAccountRecord.expense_amount}
				</td>
				<td>
					<fmt:formatDate value="${expenseAccountRecord.apply_time}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${expenseAccountRecord.expense_desc}
				</td>
				<td>
					<c:forEach items="${EXPENSE_ACCOUNT_STATE}" var="statesSsParam">
						<c:if test="${expenseAccountRecord.state == statesSsParam.column_value}">
							${statesSsParam.column_desc}
						</c:if>
					</c:forEach>
				</td>
				<td align="center">
					<c:if test="${expenseAccountRecord.state == '4'}">
						<a href='#' class='easyui-linkbutton' onClick="operEvent('${expenseAccountRecord.record_id}', '${expenseAccountRecord.state}')">
							<c:forEach items="${EXPENSE_ACCOUNT_OPER}" var="operSysParam">
						<c:if test="${expenseAccountRecord.state == operSysParam.parent_column_value}">
							${operSysParam.column_desc}
						</c:if>
					</c:forEach>
						</a>
					</c:if>
				</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>