<%@page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ page isELIgnored="false"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cttic.erp.bean.ext.BusiExpenseAccountRecordBeanExt"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	$(function() {
		$('#saveBtn').bind('click', function () {
			var prjId = $('#prj_id').val();
			if ("" == prj_id) {
				alert("请选择费用类型");
				return;
			}
			var expenseType = $('#expense_type').val();
			if ("" == expenseType) {
				alert("请选择费用类型");
				return;
			}
			var expenseAmount = $('#expense_amount').val();
			if ("" == expenseAmount) {
				alert("请填写费用金额");
				return;
			}
			var expenseDesc = $('#expense_desc').val();
			if ("" == expenseDesc) {
				alert("请填写费用描述");
				return;
			}
			$.ajax({ 
				type: "POST", 
				url: "<%=basePath%>/erp/expenseAccount/save",
				data : {
					'OPER_TYPE' : $('#OPER_TYPE').val(),
					'RECORD_ID' : $('#RECORD_ID').val(),
					'prj_id' : prjId,
					'expense_type' : expenseType,
					'expense_amount' : expenseAmount,
					'expense_desc' : expenseDesc
				},
				async : false,
				success : function(returnStr) {
					var jsonReturnStr = JSON.parse(returnStr);
					if (jsonReturnStr.STATUS_CODE == '1') {
						alert(jsonReturnStr.STATUS_INFO);
						window.location = "<%=basePath%>/erp/expenseAccount/apply";
					} else {
						alert(jsonReturnStr.STATUS_INFO);
					}
				}
			});
		});
	});
	
	function editEvent(recordId) {
		$('#prj_id').val($('#' + recordId + '_PRJ_ID').val());
		$('#expense_type').val($('#' + recordId + '_EXPENSE_TYPE').val());
		$('#expense_amount').val($('#' + recordId + '_EXPENSE_AMOUNT').val());
		$('#expense_desc').val($('#' + recordId + '_EXPENSE_DESC').val().split("<br>").join("\r\n"));
		$('#OPER_TYPE').val('EDIT');
		$('#RECORD_ID').val(recordId);
	}
	
	function cancelEvent(recordId) {
		$.ajax({
			type: "POST",
			url: "<%=basePath%>/erp/expenseAccount/save?OPER_TYPE=CANCEL",
			data : {'RECORD_ID' : recordId},
			async : false,
			success : function(returnStr) {
				var jsonReturnStr = JSON.parse(returnStr);
				if (jsonReturnStr.STATUS_CODE == '1') {
					alert(jsonReturnStr.STATUS_INFO);
					window.location = "<%=basePath%>/erp/expenseAccount/apply";
				} else {
					alert(jsonReturnStr.STATUS_INFO);
				}
			}
		});
	}
</script>
</head>
<body>
	<table border="1" bordercolor="#7FB0DE" cellpadding="1"
		cellspacing="0" align="center" class="content">
		<tr>
			<td class="tdtitle" colspan="7">[报销统计]</td>
		</tr>
		<tr>
			<td class="tabletitle" width="80px" height="50px">年度</td>
			<td class="tabletitle" width="80px">项目</td>
			<td class="tabletitle" width="80px">费用类型</td>
			<td class="tabletitle" width="80px">费用金额</td>
		</tr>
		<%
		List<Map<String, Object>> resultList = (List<Map<String, Object>>) request.getAttribute("RESULT_LIST");
		
		for (Map<String, Object> timeMap : resultList) {
			String statTime = timeMap.get("stat_time").toString();
			List<Map<String, Object>> prjList = (List<Map<String, Object>>) timeMap.get("prj_list");
			int rowCnt = 0;
			BigDecimal yearAmount = new BigDecimal(0.00);
			for (Map<String, Object> prjMap : prjList) {
				List<BusiExpenseAccountRecordBeanExt> beanList = (List<BusiExpenseAccountRecordBeanExt>) prjMap.get("stat_date");
				for (BusiExpenseAccountRecordBeanExt bean : beanList) {
					rowCnt ++;
					yearAmount = yearAmount.add(bean.getExpense_amount());
				}
				rowCnt ++;
			}
		%>
			<tr>
				<td rowspan="<%=rowCnt + 1%>" align="center">
					<%=statTime%>　
				</td>
		<%
			int i = 0;
			for (Map<String, Object> prjMap : prjList) {
				List<BusiExpenseAccountRecordBeanExt> beanList = (List<BusiExpenseAccountRecordBeanExt>) prjMap.get("stat_date");
				if (i != 0) {
		%>
			<tr>
		<%
				}
		%>
				<td rowspan="<%=beanList.size() + 1%>">
					<%=prjMap.get("stat_prj_name")%>
				</td>
		<%
				i ++;
				int j = 0;
				BigDecimal prjAmount = new BigDecimal(0.00);
				for (BusiExpenseAccountRecordBeanExt bean : beanList) {
					prjAmount = prjAmount.add(bean.getExpense_amount());
					if (j != 0) {
		%>
			<tr>
		<%
					}
		%>
				<td>
					<%=bean.getExpenseTypeName()%>
				</td>
				<td align="right">
					<%=bean.getExpense_amount()%>
				</td>
		<%
					j ++;
				}
		%>
			<tr style="background-color: #E8F2FC">
				<td align="right">合计：</td>
				<td align="right"><%=prjAmount%></td>
			</tr>
		<%	
			}
		%>
			</tr>
			<tr style="background-color: #E8F2FC">
				<td align="right">总计：</td>
				<td colspan="2" align="right"><%=yearAmount%></td>
			</tr>
		<%
		}
		%>
	</table>
</body>
</html>