<%@page contentType="text/html; charset=utf-8"%>
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<%@ include file="/js/link.jsp"%>
<script type="text/javascript">
	$(function() {
		$('#saveBtn').bind('click', function () {
			var prjId = $('#prj_id').val();
			if ("" == prjId) {
				alert("请选择填报项目");
				return;
			}
			var expenseType = $('#expense_type').val();
			if ("" == expenseType) {
				alert("请选择费用类型");
				return;
			}
			var expenseAmount = $('#expense_amount').val();
			if ("" == expenseAmount) {
				alert("请填写费用金额");
				return;
			}
			var expenseDesc = $('#expense_desc').val();
			if ("" == expenseDesc) {
				alert("请填写费用描述");
				return;
			}
			$.ajax({ 
				type: "POST", 
				url: "<%=basePath%>/erp/expenseAccount/save",
				data : {
					'OPER_TYPE' : $('#OPER_TYPE').val(),
					'RECORD_ID' : $('#RECORD_ID').val(),
					'prj_id' : prjId,
					'expense_type' : expenseType,
					'expense_amount' : expenseAmount,
					'expense_desc' : expenseDesc
				},
				async : false,
				success : function(returnStr) {
					var jsonReturnStr = JSON.parse(returnStr);
					if (jsonReturnStr.STATUS_CODE == '1') {
						alert(jsonReturnStr.STATUS_INFO);
						window.location = "<%=basePath%>/erp/expenseAccount/apply";
					} else {
						alert(jsonReturnStr.STATUS_INFO);
					}
				}
			});
		});
	});
	
	function editEvent(recordId) {
		$('#prj_id').val($('#' + recordId + '_PRJ_ID').val());
		$('#expense_type').val($('#' + recordId + '_EXPENSE_TYPE').val());
		$('#expense_amount').val($('#' + recordId + '_EXPENSE_AMOUNT').val());
		$('#expense_desc').val($('#' + recordId + '_EXPENSE_DESC').val().split("<br>").join("\r\n"));
		$('#OPER_TYPE').val('EDIT');
		$('#RECORD_ID').val(recordId);
	}
	
	function cancelEvent(recordId) {
		$.ajax({
			type: "POST",
			url: "<%=basePath%>/erp/expenseAccount/save?OPER_TYPE=CANCEL",
			data : {'RECORD_ID' : recordId},
			async : false,
			success : function(returnStr) {
				var jsonReturnStr = JSON.parse(returnStr);
				if (jsonReturnStr.STATUS_CODE == '1') {
					alert(jsonReturnStr.STATUS_INFO);
					window.location = "<%=basePath%>/erp/expenseAccount/apply";
				} else {
					alert(jsonReturnStr.STATUS_INFO);
				}
			}
		});
	}
</script>
</head>
<body>
	<br>
	<br><input type="hidden" id="OPER_TYPE" value="ADD" />
	<input type="hidden" id="RECORD_ID" value="" />
	<table align="center" width="800px" border='0'>
		<tr>
			<td>
				<table border="1" bordercolor="#7FB0DE" cellpadding="1"
					cellspacing="0" align="center" width="800px" class="content">
					<tr>
						<td class="tdtitle" height="50px">填报项目：</td>
						<td>
							<select id='prj_id' style="width: 150px">
								<option value="">---请选择---</option>
								<c:forEach items="${PRJ_LIST}" var="prjList">
									<option value="${prjList.prj_id}">${prjList.prj_name}</option>
								</c:forEach>
							</select>
						</td>
						<td class="tdtitle" height="50px">费用类型：</td>
						<td>
							<select id="expense_type" style="width: 150px">
								<option value="">---请选择---</option>
								<c:forEach items="${EXPENSE_ACCOUNT_EXPENSE_TYPE}" var="sysParam">
									<option value="${sysParam.column_value}">${sysParam.column_desc}</option>
								</c:forEach>
							</select>
						</td>
						<td class="tdtitle">费用金额：</td>
						<td><input id='expense_amount' style="width: 150px" /></td>
					</tr>
					<tr>
						<td class="tdtitle">费用说明：</td>
						<td colspan="5">
							<textarea rows="4" cols='87' id="expense_desc"></textarea>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="right">
				<a href="#" class="easyui-linkbutton"
						id="saveBtn">&nbsp;提&nbsp;&nbsp;交&nbsp;</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
		</tr>
	</table>
	<br>
	<table border="1" bordercolor="#7FB0DE" cellpadding="1"
		cellspacing="0" align="center" width="800px" class="content">
		<tr>
			<td class="tdtitle" colspan="7">[报销申请记录]</td>
		</tr>
		<tr>
			<td class="tabletitle" width="80px" height="50px">填报项目</td>
			<td class="tabletitle" width="80px">费用类型</td>
			<td class="tabletitle" width="80px">费用金额</td>
			<td class="tabletitle" width="80px">申请时间</td>
			<td class="tabletitle">费用说明</td>
			<td class="tabletitle" width="80px">费用状态</td>
			<td class="tabletitle" width="100px">操作</td>
		</tr>
		<c:forEach items="${EXPENSE_ACCOUNT_RECORD}" var="expenseAccountRecord">
			<tr>
				<td><input type="hidden" id="${expenseAccountRecord.record_id}_PRJ_ID" value="${expenseAccountRecord.prj_id}" />
					<c:forEach items="${PRJ_LIST}" var="prjList">
						<c:if test="${expenseAccountRecord.prj_id == prjList.prj_id}">
							${prjList.prj_name}
						</c:if>
					</c:forEach>
				</td>
				<td><input type="hidden" id="${expenseAccountRecord.record_id}_EXPENSE_TYPE" value="${expenseAccountRecord.expense_type}" />
					<c:forEach items="${EXPENSE_ACCOUNT_EXPENSE_TYPE}" var="sysParam">
						<c:if test="${expenseAccountRecord.expense_type == sysParam.column_value}">
							${sysParam.column_desc}
						</c:if>
					</c:forEach>
				</td>
				<td><input type="hidden" id="${expenseAccountRecord.record_id}_EXPENSE_AMOUNT" value="${expenseAccountRecord.expense_amount}" />
					${expenseAccountRecord.expense_amount}
				</td>
				<td>
					<fmt:formatDate value="${expenseAccountRecord.apply_time}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td><input type="hidden" id="${expenseAccountRecord.record_id}_EXPENSE_DESC" value="${expenseAccountRecord.expense_desc}" />
					${expenseAccountRecord.expense_desc}
				</td>
				<td>
					<c:forEach items="${EXPENSE_ACCOUNT_STATE}" var="sysParam">
						<c:if test="${expenseAccountRecord.state == sysParam.column_value}">
							${sysParam.column_desc}
						</c:if>
					</c:forEach>
				</td>
				<td align="center">
					<c:if test="${expenseAccountRecord.state == '1'}">
						<a href='#' class='easyui-linkbutton' onClick="editEvent('${expenseAccountRecord.record_id}')">修改</a>
						<a href="#" class="easyui-linkbutton" onClick="cancelEvent('${expenseAccountRecord.record_id}')">撤销</a>
					</c:if>
				</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>