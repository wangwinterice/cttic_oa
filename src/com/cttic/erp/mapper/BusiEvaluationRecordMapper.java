package com.cttic.erp.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.cttic.erp.bean.BusiEvaluationRecordBean;

public interface BusiEvaluationRecordMapper {

	@Select("SELECT * FROM busi_evaluation_record "
			+ "where employee_id = #{employee_id} "
			+ "and evaluation_date = #{evaluation_date}")
	public BusiEvaluationRecordBean getBean(
			@Param("employee_id") long employee_id,
			@Param("evaluation_date") String evaluation_date);

	@Insert("insert into busi_evaluation_record(record_id, employee_id, evaluation_date, state, result_description) "
			+ "value(#{busiEvaluationRecordBean.record_id}, #{busiEvaluationRecordBean.employee_id}, #{busiEvaluationRecordBean.evaluation_date}, #{busiEvaluationRecordBean.state}, #{busiEvaluationRecordBean.result_description})")
	public void insertBean(
			@Param("busiEvaluationRecordBean") BusiEvaluationRecordBean busiEvaluationRecordBean);

	@Update("update busi_evaluation_record set state = #{busiEvaluationRecordBean.state}, result_description = #{busiEvaluationRecordBean.result_description}"
			+ " where record_id = #{busiEvaluationRecordBean.record_id}")
	public void updateBean(
			@Param("busiEvaluationRecordBean") BusiEvaluationRecordBean busiEvaluationRecordBean);
}
