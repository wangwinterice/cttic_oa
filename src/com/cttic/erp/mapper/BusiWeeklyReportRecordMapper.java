package com.cttic.erp.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.cttic.erp.bean.BusiWeeklyReportRecordBean;

public interface BusiWeeklyReportRecordMapper {

	@Select("select * from busi_weekly_report_record "
			+ "where employee_id = #{employee_id} "
			+ "and begin_date = #{begin_date} " + "and end_date = #{end_date}")
	public BusiWeeklyReportRecordBean getBean(
			@Param("employee_id") long employee_id,
			@Param("begin_date") String begin_date,
			@Param("end_date") String end_date);
	
	@Select("select * from busi_weekly_report_record "
			+ "where begin_date >= #{begin_date} " + "and end_date <= #{end_date}")
	public List<BusiWeeklyReportRecordBean> getBeanListByDate(
			@Param("begin_date") String begin_date,
			@Param("end_date") String end_date);

	@Select("select * from busi_weekly_report_record "
			+ "where employee_id = #{employee_id} order by end_date desc")
	public List<BusiWeeklyReportRecordBean> getBeans(
			@Param("employee_id") long employee_id);
	
	@Select("select * from busi_weekly_report_record "
			+ "where employee_id = #{employee_id} and DATE_FORMAT(end_date, '%Y%m') = #{select_month} order by end_date desc")
	public List<BusiWeeklyReportRecordBean> getBeanList(
			@Param("employee_id") long employee_id, @Param("select_month") String select_month);

	@Insert("insert into busi_weekly_report_record(record_id, employee_id, begin_date, end_date, oper_time, task_begin_date, task_end_date, plan_begin_date, plan_end_date, approval_flag) "
			+ "values(#{bean.record_id}, #{bean.employee_id}, #{bean.begin_date}, #{bean.end_date}, #{bean.oper_time}, #{bean.task_begin_date}, #{bean.task_end_date}, #{bean.plan_begin_date}, #{bean.plan_end_date}, #{bean.approval_flag})")
	public void insertBean(@Param("bean") BusiWeeklyReportRecordBean bean);
	
	@Update("update busi_weekly_report_record set oper_time = #{bean.oper_time}, task_begin_date = #{bean.task_begin_date}, task_end_date = #{bean.task_end_date}, plan_begin_date = #{bean.plan_begin_date}, plan_end_date = #{bean.plan_end_date}, approval_flag = #{bean.approval_flag} where record_id = #{bean.record_id}")
	public void updateBean(@Param("bean") BusiWeeklyReportRecordBean bean);
	
	@Select(" select * from busi_weekly_report_record r where r.begin_date = #{search.begin_date}  and r.employee_id=#{search.employee_id} ")
	public List<BusiWeeklyReportRecordBean> findWeekRecord(@Param("search") BusiWeeklyReportRecordBean search);
	
	@Select(" select * from busi_weekly_report_record r where r.record_id=#{week_id} ")
	public List<BusiWeeklyReportRecordBean> getBeanById(@Param("week_id") String week_id);
}
