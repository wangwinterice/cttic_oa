package com.cttic.erp.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.cttic.erp.bean.InfoDateBean;

public interface InfoDateMapper {

	@Select("select * from info_date where year = #{year} and month = #{month} order by id")
	public List<InfoDateBean> getBeans(@Param("year") String year, @Param("month") String month);

}
