package com.cttic.erp.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.cttic.erp.bean.BusiEmployeeVacationRecordBean;

public interface BusiEmployeeVacationRecordMapper {

	@Select("select * from busi_employee_vacation_record where employee_id = #{employee_id} order by record_id desc")
	public List<BusiEmployeeVacationRecordBean> getBeansByEmployeeId(@Param("employee_id") long employee_id);
	
	@Select("select * from busi_employee_vacation_record where state = #{state}")
	public List<BusiEmployeeVacationRecordBean> getBeansByState(@Param("state") String state);
	
	@Select("select * from busi_employee_vacation_record where record_id = #{record_id}")
	public BusiEmployeeVacationRecordBean getBeansByRecordId(@Param("record_id") long record_id);
	
	@Select("select * from busi_employee_vacation_record where employee_id = #{employee_id} and DATE_FORMAT(begin_date, '%Y%m') = #{reportMonth} order by record_id")
	public List<BusiEmployeeVacationRecordBean> getBeans(@Param("employee_id") long employee_id, @Param("reportMonth") String reportMonth);
	
	@Select("select * from busi_employee_vacation_record where DATE_FORMAT(begin_date, '%Y%m') = #{reportMonth} order by employee_id, begin_date")
	public List<BusiEmployeeVacationRecordBean> getBeansByReportMonth(@Param("reportMonth") String reportMonth);
	
	@Select("select * from busi_employee_vacation_record where employee_id = #{employee_id} and begin_date = #{beginDate}")
	public BusiEmployeeVacationRecordBean getBean(@Param("employee_id") long employee_id, @Param("beginDate") String beginDate);
	
	@Insert("insert into busi_employee_vacation_record(record_id, employee_id, type, begin_date, end_date, number_of_days, place, reason, state, check_employee_id, check_date) "
			+ "values(#{bean.record_id}, #{bean.employee_id}, #{bean.type}, #{bean.begin_date}, #{bean.end_date}, #{bean.number_of_days}, #{bean.place}, #{bean.reason}, #{bean.state}, #{bean.check_employee_id}, #{bean.check_date})")
	public void insert(@Param("bean") BusiEmployeeVacationRecordBean bean);
	
	@Update("update busi_employee_vacation_record set state = #{bean.state}, check_employee_id = #{bean.check_employee_id}, check_date = #{bean.check_date} "
			+ "where record_id = #{bean.record_id}")
	public void updateCheck(@Param("bean")  BusiEmployeeVacationRecordBean bean);
	
	@Update("update busi_employee_vacation_record set type = #{bean.type}, begin_date = #{bean.begin_date}, end_date = #{bean.end_date}, number_of_days = #{bean.number_of_days}, place = #{bean.place}, reason = #{bean.reason} "
			+ "where record_id = #{bean.record_id}")
	public void updateRecord(@Param("bean")  BusiEmployeeVacationRecordBean bean);
	
	@Update("update busi_employee_vacation_record set state = #{bean.state} "
			+ "where record_id = #{bean.record_id}")
	public void cancel(@Param("bean")  BusiEmployeeVacationRecordBean bean);
}
