package com.cttic.erp.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.cttic.erp.bean.BusiWeeklyReportResultBean;

public interface BusiWeeklyReportResultMapper {

	@Select("select * from busi_weekly_report_result where record_id = #{record_id}")
	public List<BusiWeeklyReportResultBean> getBeans(@Param("record_id") String record_id);
	
	@Select("select * from busi_weekly_report_result where record_id in (select record_id from busi_weekly_report_record where employee_id = #{employee_id})")
	public List<BusiWeeklyReportResultBean> getBeansByEmployeeId(@Param("employee_id") Long employee_id);
	
	@Insert("insert into busi_weekly_report_result(result_id, result_type, result_code, result_value, record_id) "
			+ "values(#{bean.result_id}, #{bean.result_type}, #{bean.result_code}, #{bean.result_value}, #{bean.record_id})")
	public void insertBean(@Param("bean") BusiWeeklyReportResultBean bean);
	
	@Delete("delete from busi_weekly_report_result where record_id = #{record_id}")
	public void deleteBeans(@Param("record_id") String record_id);
	
	@Delete("delete from busi_weekly_report_result "
			+ "where record_id = #{record_id}"
			+ "  and result_code in('WORK_TIME_1', 'WORK_TIME_2', 'WORK_TIME_3', 'WORK_TIME_4', 'WORK_TIME_5', 'WORK_TIME_6', 'WORK_TIME_7')")
	public void deleteWorkTime(@Param("record_id") String record_id);
}
