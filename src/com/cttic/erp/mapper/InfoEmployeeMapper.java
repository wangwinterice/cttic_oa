package com.cttic.erp.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.cttic.erp.bean.InfoEmployeeBean;

public interface InfoEmployeeMapper {

	@Select("select * from info_employee")
	@Options(useCache = true, flushCache = false)
	public List<InfoEmployeeBean> getBeanList();

	@Select("select * from info_employee where dept_id = #{dept_id}")
	@Options(useCache = true, flushCache = false)
	public List<InfoEmployeeBean> getBeanListByDept(@Param("dept_id") long dept_id);

	@Select("select * from info_employee where employee_no = #{employee_no}")
	@Options(useCache = true, flushCache = false)
	public InfoEmployeeBean getBean(@Param("employee_no") long employee_no);

	@Update("update info_employee set employee_pwd = #{employee_pwd} where employee_id = #{employee_id}")
	public void updateEmployeePwd(@Param("employee_pwd") String employee_pwd, @Param("employee_id") long employee_id);

	@Insert("INSERT INTO info_employee(employee_id, employee_name,employee_no,mobile_phone,email,employee_pwd,dept_id,status,cert_num,post,sex,birthday,address,zip,in_company) VALUES(#{infoEmployeeBean.employee_id},#{infoEmployeeBean.employee_name},#{infoEmployeeBean.employee_no},#{infoEmployeeBean.mobile_phone},#{infoEmployeeBean.email},#{infoEmployeeBean.employee_pwd},#{infoEmployeeBean.dept_id},#{infoEmployeeBean.status},#{infoEmployeeBean.cert_num},#{infoEmployeeBean.post},#{infoEmployeeBean.sex},#{infoEmployeeBean.birthday},#{infoEmployeeBean.address},#{infoEmployeeBean.zip},#{infoEmployeeBean.in_company})")
	public void insert(@Param("infoEmployeeBean") InfoEmployeeBean infoEmployeeBean);

	@Update("UPDATE info_employee SET employee_name = #{infoEmployeeBean.employee_name},employee_no = #{infoEmployeeBean.employee_no},mobile_phone = #{infoEmployeeBean.mobile_phone},email = #{infoEmployeeBean.email},dept_id = #{infoEmployeeBean.dept_id},status = #{infoEmployeeBean.status},cert_num = #{infoEmployeeBean.cert_num},post = #{infoEmployeeBean.post},sex = #{infoEmployeeBean.sex},birthday = #{infoEmployeeBean.birthday},address = #{infoEmployeeBean.address},zip = #{infoEmployeeBean.zip},in_company = #{infoEmployeeBean.in_company} WHERE employee_id = #{infoEmployeeBean.employee_id}")
	public void update(@Param("infoEmployeeBean") InfoEmployeeBean infoEmployeeBean);

	@Select("select * from info_employee where 1=1 ${conditions}")
	public List<InfoEmployeeBean> getBeanListByConditions(@Param("conditions") String conditions);
}
