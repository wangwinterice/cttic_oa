package com.cttic.erp.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.cttic.erp.bean.ext.BusiEvaluationBeanExt;

public interface BusiEvaluationMapper {

	@Select("select a.dimension_id, "
			+ "a.dimension_name, "
			+ "a.weight_function, "
			+ "b.kpi_id, "
			+ "b.kpi_name, "
			+ "b.kpi_score, "
			+ "c.standard_name, "
			+ "c.standard_score_scope "
			+ "from info_evaluation_dimension a, "
			+ "info_evaluation_kpi b, "
			+ "info_evaluation_kpi_standard c "
			+ "where a.dimension_id = b.demension_id "
			+ "and b.kpi_id = c.kpi_id and a.template_id = #{template_id}")
	public List<BusiEvaluationBeanExt> getEvaluationStandardList(@Param("template_id") int template_id);
	
	@Select("select a.dimension_id, "
			+ "a.dimension_name, "
			+ "a.weight_function, "
			+ "b.kpi_id, "
			+ "b.kpi_name, "
			+ "b.kpi_score "
			+ "from info_evaluation_dimension a, "
			+ "info_evaluation_kpi b "
			+ "where a.dimension_id = b.demension_id "
			+ "and a.template_id = #{template_id}")
	public List<BusiEvaluationBeanExt> getEvaluationKpiList(@Param("template_id") int template_id);
}
