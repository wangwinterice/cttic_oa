package com.cttic.erp.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.cttic.erp.bean.InfoEvaluationKpiBean;

public interface InfoEvaluationKpiMapper {

	@Select("SELECT * FROM info_evaluation_kpi")
	public List<InfoEvaluationKpiBean> getBeanList();
}
