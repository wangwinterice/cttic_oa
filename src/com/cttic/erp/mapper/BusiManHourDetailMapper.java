package com.cttic.erp.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.cttic.erp.bean.BusiManHourDetailBean;

public interface BusiManHourDetailMapper {


	@Select(" select t.manHour_id,t.prj_id,p.prj_name,t.man_hour,t.total,t.app_man_hour,t.app_total  from busi_man_hour_detail  t, info_project p  where manHour_id = #{manHour_id} and t.prj_id = p.prj_id ")
	public List<BusiManHourDetailBean> getBeans(@Param("manHour_id") long manHour_id);
	
	
	@Insert("insert into busi_man_hour_detail(manHour_id, prj_id,prj_name, man_hour,total, app_man_hour,app_total) "
			+ "value(#{busiManHourDetailBean.manHour_id}, #{busiManHourDetailBean.prj_id},#{busiManHourDetailBean.prj_name}, #{busiManHourDetailBean.man_hour}, #{busiManHourDetailBean.total}, #{busiManHourDetailBean.app_man_hour}, #{busiManHourDetailBean.app_total})")
	public void insertBean(@Param("busiManHourDetailBean") BusiManHourDetailBean busiManHourDetailBean);
	
	
	@Delete("delete from busi_man_hour_detail where manHour_id = #{manHour_id} ")
	public int delete(@Param("manHour_id") long manHour_id);
	

	@Update(" update busi_man_hour_detail d set d.app_man_hour = #{d.app_man_hour} ,d.app_total=#{d.app_total} where d.manHour_id=#{d.manHour_id} and d.prj_id=#{d.prj_id} ")
	public void updateDetailBean(@Param("d") BusiManHourDetailBean d);
	
	
}
