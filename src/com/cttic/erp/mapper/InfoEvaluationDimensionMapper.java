package com.cttic.erp.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.cttic.erp.bean.InfoEvaluationDimensionBean;

public interface InfoEvaluationDimensionMapper {

	@Select("SELECT * FROM info_evaluation_dimension")
	public List<InfoEvaluationDimensionBean> getBeanList();
}
