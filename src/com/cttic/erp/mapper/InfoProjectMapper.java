package com.cttic.erp.mapper;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.cttic.erp.bean.InfoProjectBean;

public interface InfoProjectMapper {

	@Select("select * from info_project")
	public List<InfoProjectBean> getAllBeans();
	
	@Select("select * from info_project where state = '0'")
	public List<InfoProjectBean> getBeans();
	
	@Select("select * from info_project where state = '0' and manager_employee_id = #{manager_employee_id}")
	public List<InfoProjectBean> getBeansByManager(@Param("manager_employee_id") long manager_employee_id);
	
	@Select("select * from info_project where state = '0' and prj_id = #{prj_id}")
	public InfoProjectBean getBean(@Param("prj_id") BigDecimal prj_id);
	
	@Insert("insert into info_project(prj_id, prj_code, prj_name, state, manager_employee_id)"
			+ "VALUES(#{bean.prj_id}, #{bean.prj_code}, #{bean.prj_name}, '0', #{bean.manager_employee_id})")
	public void insert(@Param("bean") InfoProjectBean bean);
	
	@Update("update info_project set prj_code = #{bean.prj_code}, prj_name = #{bean.prj_name}"
			+ ", manager_employee_id = #{bean.manager_employee_id} where prj_id = #{bean.prj_id}")
	public void update(@Param("bean") InfoProjectBean bean);
	
	@Update("update info_project set state = '1'"
			+ " where prj_id = #{prj_id}")
	public void close(@Param("prj_id") BigDecimal prj_id);
}
