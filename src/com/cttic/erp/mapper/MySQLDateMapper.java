package com.cttic.erp.mapper;

import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface MySQLDateMapper {

	@Select("select SYSDATE()")
	@Options(useCache = true, flushCache = false)
	public String getSysDate();
	
	@Select("select LAST_DAY(#{date})")
	@Options(useCache = true, flushCache = false)
	public String getMonthLastDate(@Param("date") String date);
	
	@Select("select date_add(DATE_FORMAT(SYSDATE(), '%Y-%m-%d'), Interval -1 month)")
	public String getBeforeMonthToday();
	
	@Select("select date_add(DATE_FORMAT(SYSDATE(), '%Y-%m-%d'), Interval 1 month)")
	public String getAfterMonthToday();

}
