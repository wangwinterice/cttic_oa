package com.cttic.erp.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.cttic.erp.bean.InfoMenuBean;

public interface InfoOperMapper {

	@Select("select d.* from info_employee_role b, "
			+ "info_role_menu c, "
			+ "info_menu d "
			+ "where b.employee_id = #{employee_id} "
			+ "and b.role_id = c.role_id "
			+ "and c.menu_id = d.menu_id "
			+ "and d.state = 1")
	public List<InfoMenuBean> getInfoMenuBeanList(@Param("employee_id") long employee_id);
}
