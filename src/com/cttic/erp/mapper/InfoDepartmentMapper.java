package com.cttic.erp.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.cttic.erp.bean.InfoDepartmentBean;

public interface InfoDepartmentMapper {

	@Select("select * from info_department")
	public List<InfoDepartmentBean> getBeans();

	@Select("select * from info_department where dept_id = #{dept_id}")
	public InfoDepartmentBean getBeanByDeptId(@Param("dept_id") long dept_id);

	@Select("select * from info_department where parent_dept_id = #{parent_dept_id}")
	public List<InfoDepartmentBean> getChildBeanByDeptId(@Param("parent_dept_id") long parent_dept_id);

	@Select("select * from info_department where manager_employee_id = #{manager_employee_id}")
	public InfoDepartmentBean getBeanByManagerEmployee(@Param("manager_employee_id") long manager_employee_id);
}
