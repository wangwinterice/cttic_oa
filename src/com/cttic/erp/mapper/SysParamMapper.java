package com.cttic.erp.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.cttic.erp.bean.SysParamBean;

public interface SysParamMapper {

	@Select("select * from sys_param where state = '0'")
	public List<SysParamBean> getBeans();
}
