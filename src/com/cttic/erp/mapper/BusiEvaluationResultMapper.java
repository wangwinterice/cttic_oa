package com.cttic.erp.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.cttic.erp.bean.BusiEvaluationResultBean;
import com.cttic.erp.bean.ext.BusiEvaluationResultBeanExt;

public interface BusiEvaluationResultMapper {

	@Select("SELECT * FROM busi_evaluation_result " + "where record_id = #{record_id}")
	public List<BusiEvaluationResultBean> getBeanList(@Param("record_id") long record_id);

	@Insert("insert into busi_evaluation_result(record_id, evaluation_type, evaluation_item, evaluation_value) "
	        + "value(#{busiEvaluationResultBean.record_id}, #{busiEvaluationResultBean.evaluation_type}, #{busiEvaluationResultBean.evaluation_item}, #{busiEvaluationResultBean.evaluation_value})")
	public void insertBean(@Param("busiEvaluationResultBean") BusiEvaluationResultBean busiEvaluationResultBean);

	@Delete("delete from busi_evaluation_result where record_id = #{record_id} and evaluation_type in ('SELF_VALUE', 'SELF_RESULT')")
	public int deleteSelfEvaluationResult(@Param("record_id") long record_id);

	@Delete("delete from busi_evaluation_result where record_id = #{record_id} and evaluation_type in ('CHECK_VALUE', 'CHECK_RESULT')")
	public int deleteCheckEvaluationResult(@Param("record_id") long record_id);

	@Select("select b.employee_id, c.evaluation_type, c.evaluation_item, c.evaluation_value "
	        + "from busi_evaluation_record b, busi_evaluation_result c "
	        + "where b.record_id = c.record_id and b.evaluation_date = #{evaluation_date}")
	public List<BusiEvaluationResultBeanExt> getEvaluationResultBeanList(@Param("evaluation_date") String evaluation_date);

	@Select("select b.employee_id, b.evaluation_date, c.evaluation_type, c.evaluation_item, c.evaluation_value "
	        + "from busi_evaluation_record b, busi_evaluation_result c " + "where b.record_id = c.record_id")
	public List<BusiEvaluationResultBeanExt> getEvaluationState();
}
