package com.cttic.erp.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.cttic.erp.bean.BusiExpenseAccountRecordBean;
import com.cttic.erp.bean.ext.BusiExpenseAccountRecordBeanExt;

public interface BusiExpenseAccountRecordMapper {

	@Select("select * from busi_expense_account_record where employee_id = #{employee_id} order by apply_time desc")
	public List<BusiExpenseAccountRecordBean> getBeansByEmployeeId(@Param("employee_id") long employee_id);
	
	@Select("select * from busi_expense_account_record where state not in (0, 6) order by apply_time desc")
	public List<BusiExpenseAccountRecordBean> getEffectBeans();
	
	@Select("select DATE_FORMAT(apply_time, '%Y') sApplyTime, prj_id, expense_type, sum(expense_amount) expense_amount from busi_expense_account_record where state not in (0, 6) group by DATE_FORMAT(apply_time, '%Y'), prj_id, expense_type order by DATE_FORMAT(apply_time, '%Y'), prj_id, expense_type")
	public List<BusiExpenseAccountRecordBeanExt> getStatBeans();
	
	@Insert("insert into busi_expense_account_record(prj_id, employee_id, expense_type, expense_amount, expense_desc, state, apply_time)"
			+ " value(#{bean.prj_id}, #{bean.employee_id}, #{bean.expense_type}, #{bean.expense_amount}, #{bean.expense_desc}, #{bean.state}, #{bean.apply_time})")
	public int insert(@Param("bean") BusiExpenseAccountRecordBean bean);
	
	@Update("update busi_expense_account_record set prj_id = #{bean.prj_id}, expense_type = #{bean.expense_type}, expense_amount = #{bean.expense_amount}, expense_desc = #{bean.expense_desc} where record_id = #{bean.record_id}")
	public int update(@Param("bean") BusiExpenseAccountRecordBean bean);
	
	@Update("update busi_expense_account_record set state = #{bean.state} where record_id = #{bean.record_id}")
	public int updateState(@Param("bean") BusiExpenseAccountRecordBean bean);
}
