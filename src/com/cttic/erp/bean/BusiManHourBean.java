package com.cttic.erp.bean;

import java.util.List;


public class BusiManHourBean {

	private long id;
	
	private long employee_id;
	
	private String begin_date;
	
	private String end_date;
	
	private String real_begin_date;
	
	private String real_end_date;
	
	private int isTB;
	
	private int isSP;
	
	private String week_id;
	
	//自定义
	private String date_section;
	private String year;
	private String month;
	private String employee_name;
	private String isWeekRecordChange;
	private List<BusiManHourDetailBean> details;
	
	public String getDate_section() {
		return date_section;
	}

	public void setDate_section(String date_section) {
		this.date_section = date_section;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getReal_begin_date() {
		return real_begin_date;
	}

	public void setReal_begin_date(String real_begin_date) {
		this.real_begin_date = real_begin_date;
	}

	public String getReal_end_date() {
		return real_end_date;
	}

	public void setReal_end_date(String real_end_date) {
		this.real_end_date = real_end_date;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(long employee_id) {
		this.employee_id = employee_id;
	}

	public String getBegin_date() {
		return begin_date;
	}

	public void setBegin_date(String begin_date) {
		this.begin_date = begin_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public int getIsTB() {
		return isTB;
	}

	public void setIsTB(int isTB) {
		this.isTB = isTB;
	}

	public int getIsSP() {
		return isSP;
	}

	public void setIsSP(int isSP) {
		this.isSP = isSP;
	}

	public String getWeek_id() {
		return week_id;
	}

	public void setWeek_id(String week_id) {
		this.week_id = week_id;
	}

	public String getEmployee_name() {
		return employee_name;
	}

	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}

	public List<BusiManHourDetailBean> getDetails() {
		return details;
	}

	public void setDetails(List<BusiManHourDetailBean> details) {
		this.details = details;
	}

	public String getIsWeekRecordChange() {
		return isWeekRecordChange;
	}

	public void setIsWeekRecordChange(String isWeekRecordChange) {
		this.isWeekRecordChange = isWeekRecordChange;
	}


}
