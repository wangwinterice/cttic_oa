package com.cttic.erp.bean;

public class WeeklyReportDateBean {

	private String beginDate;
	
	private String beginDateDesc;
	
	private String endDate;
	
	private String endDateDesc;
	
	private String year;
	
	private String month;
	
	private int week;

	public String getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}

	public String getBeginDateDesc() {
		return beginDateDesc;
	}

	public void setBeginDateDesc(String beginDateDesc) {
		this.beginDateDesc = beginDateDesc;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getEndDateDesc() {
		return endDateDesc;
	}

	public void setEndDateDesc(String endDateDesc) {
		this.endDateDesc = endDateDesc;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public int getWeek() {
		return week;
	}

	public void setWeek(int week) {
		this.week = week;
	}
}
