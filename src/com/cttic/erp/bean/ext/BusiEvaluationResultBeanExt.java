package com.cttic.erp.bean.ext;

import com.cttic.erp.bean.BusiEvaluationResultBean;

public class BusiEvaluationResultBeanExt extends BusiEvaluationResultBean {

	private long employee_id;
	
	private String evaluation_date;
	
	public long getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(long employee_id) {
		this.employee_id = employee_id;
	}

	public String getEvaluation_date() {
		return evaluation_date;
	}

	public void setEvaluation_date(String evaluation_date) {
		this.evaluation_date = evaluation_date;
	}
}
