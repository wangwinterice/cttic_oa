package com.cttic.erp.bean.ext;

import com.cttic.erp.bean.BusiExpenseAccountRecordBean;

public class BusiExpenseAccountRecordBeanExt extends
		BusiExpenseAccountRecordBean {

	private String sApplyTime;
	
	private String prjName;
	
	private String expenseTypeName;

	public String getsApplyTime() {
		return sApplyTime;
	}

	public void setsApplyTime(String sApplyTime) {
		this.sApplyTime = sApplyTime;
	}

	public String getPrjName() {
		return prjName;
	}

	public void setPrjName(String prjName) {
		this.prjName = prjName;
	}

	public String getExpenseTypeName() {
		return expenseTypeName;
	}

	public void setExpenseTypeName(String expenseTypeName) {
		this.expenseTypeName = expenseTypeName;
	}
}
