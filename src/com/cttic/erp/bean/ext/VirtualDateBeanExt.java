package com.cttic.erp.bean.ext;

public class VirtualDateBeanExt {

	private String date;
	
	private String day_of_the_week;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDay_of_the_week() {
		return day_of_the_week;
	}

	public void setDay_of_the_week(String day_of_the_week) {
		this.day_of_the_week = day_of_the_week;
	}
}
