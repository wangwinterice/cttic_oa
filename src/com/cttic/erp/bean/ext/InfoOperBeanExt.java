package com.cttic.erp.bean.ext;

import java.util.List;

import com.cttic.erp.bean.InfoEmployeeBean;
import com.cttic.erp.bean.InfoMenuBean;

public class InfoOperBeanExt {

	private InfoEmployeeBean infoEmployeeBean;
	
	private List<InfoMenuBean> infoMenuBeanLists;

	public InfoEmployeeBean getInfoEmployeeBean() {
		return infoEmployeeBean;
	}

	public void setInfoEmployeeBean(InfoEmployeeBean infoEmployeeBean) {
		this.infoEmployeeBean = infoEmployeeBean;
	}

	public List<InfoMenuBean> getInfoMenuBeanLists() {
		return infoMenuBeanLists;
	}

	public void setInfoMenuBeanLists(List<InfoMenuBean> infoMenuBeanLists) {
		this.infoMenuBeanLists = infoMenuBeanLists;
	}

}
