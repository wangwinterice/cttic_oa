package com.cttic.erp.bean.ext;

public class BusiEvaluationBeanExt {

	private int dimension_id;
	
	private String dimension_name;
	
	private String weight_function;
	
	private String kpi_id;
	
	private String kpi_name;
	
	private String kpi_score;
	
	private String standard_name;
	
	private String standard_score_scope;

	public int getDimension_id() {
		return dimension_id;
	}

	public void setDimension_id(int dimension_id) {
		this.dimension_id = dimension_id;
	}

	public String getDimension_name() {
		return dimension_name;
	}

	public void setDimension_name(String dimension_name) {
		this.dimension_name = dimension_name;
	}

	public String getWeight_function() {
		return weight_function;
	}

	public void setWeight_function(String weight_function) {
		this.weight_function = weight_function;
	}

	public String getKpi_id() {
		return kpi_id;
	}

	public void setKpi_id(String kpi_id) {
		this.kpi_id = kpi_id;
	}

	public String getKpi_name() {
		return kpi_name;
	}

	public void setKpi_name(String kpi_name) {
		this.kpi_name = kpi_name;
	}

	public String getKpi_score() {
		return kpi_score;
	}

	public void setKpi_score(String kpi_score) {
		this.kpi_score = kpi_score;
	}

	public String getStandard_name() {
		return standard_name;
	}

	public void setStandard_name(String standard_name) {
		this.standard_name = standard_name;
	}

	public String getStandard_score_scope() {
		return standard_score_scope;
	}

	public void setStandard_score_scope(String standard_score_scope) {
		this.standard_score_scope = standard_score_scope;
	}
}
