package com.cttic.erp.bean;

import java.util.Date;

public class BusiEmployeeVacationRecordBean {

	private long record_id;
	
	private long employee_id;
	
	private String type;
	
	private String begin_date;
	
	private String end_date;
	
	private String number_of_days;
	
	private String place;
	
	private String reason;
	
	private String state;
	
	private long check_employee_id;
	
	private Date check_date;

	public long getRecord_id() {
		return record_id;
	}

	public void setRecord_id(long record_id) {
		this.record_id = record_id;
	}

	public long getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(long employee_id) {
		this.employee_id = employee_id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBegin_date() {
		return begin_date;
	}

	public void setBegin_date(String begin_date) {
		this.begin_date = begin_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public String getNumber_of_days() {
		return number_of_days;
	}

	public void setNumber_of_days(String number_of_days) {
		this.number_of_days = number_of_days;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public long getCheck_employee_id() {
		return check_employee_id;
	}

	public void setCheck_employee_id(long check_employee_id) {
		this.check_employee_id = check_employee_id;
	}

	public Date getCheck_date() {
		return check_date;
	}

	public void setCheck_date(Date check_date) {
		this.check_date = check_date;
	}
}
