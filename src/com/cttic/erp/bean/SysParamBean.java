package com.cttic.erp.bean;

public class SysParamBean {

	private String type_code;
	
	private String param_code;
	
	private String column_value;
	
	private String column_desc;
	
	private String state;
	
	private String parent_type_code;
	
	private String parent_param_code;
	
	private String parent_column_value;

	public String getType_code() {
		return type_code;
	}

	public void setType_code(String type_code) {
		this.type_code = type_code;
	}

	public String getParam_code() {
		return param_code;
	}

	public void setParam_code(String param_code) {
		this.param_code = param_code;
	}

	public String getColumn_value() {
		return column_value;
	}

	public void setColumn_value(String column_value) {
		this.column_value = column_value;
	}

	public String getColumn_desc() {
		return column_desc;
	}

	public void setColumn_desc(String column_desc) {
		this.column_desc = column_desc;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getParent_type_code() {
		return parent_type_code;
	}

	public void setParent_type_code(String parent_type_code) {
		this.parent_type_code = parent_type_code;
	}

	public String getParent_param_code() {
		return parent_param_code;
	}

	public void setParent_param_code(String parent_param_code) {
		this.parent_param_code = parent_param_code;
	}

	public String getParent_column_value() {
		return parent_column_value;
	}

	public void setParent_column_value(String parent_column_value) {
		this.parent_column_value = parent_column_value;
	}
}
