package com.cttic.erp.bean;

public class InfoEvaluationDimensionBean {

	private int dimension_id;
	
	private String Dimension_name;
	
	private String weight_function;

	public int getDimension_id() {
		return dimension_id;
	}

	public void setDimension_id(int dimension_id) {
		this.dimension_id = dimension_id;
	}

	public String getDimension_name() {
		return Dimension_name;
	}

	public void setDimension_name(String dimension_name) {
		Dimension_name = dimension_name;
	}

	public String getWeight_function() {
		return weight_function;
	}

	public void setWeight_function(String weight_function) {
		this.weight_function = weight_function;
	}
}
