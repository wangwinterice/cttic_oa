package com.cttic.erp.bean;

public class BusiEvaluationResultBean {

	private long record_id;
	
	private String evaluation_type;
	
	private String evaluation_item;
	
	private String evaluation_value;

	public long getRecord_id() {
		return record_id;
	}

	public void setRecord_id(long record_id) {
		this.record_id = record_id;
	}

	public String getEvaluation_type() {
		return evaluation_type;
	}

	public void setEvaluation_type(String evaluation_type) {
		this.evaluation_type = evaluation_type;
	}

	public String getEvaluation_item() {
		return evaluation_item;
	}

	public void setEvaluation_item(String evaluation_item) {
		this.evaluation_item = evaluation_item;
	}

	public String getEvaluation_value() {
		return evaluation_value;
	}

	public void setEvaluation_value(String evaluation_value) {
		this.evaluation_value = evaluation_value;
	}
}
