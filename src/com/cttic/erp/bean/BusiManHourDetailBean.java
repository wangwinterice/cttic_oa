package com.cttic.erp.bean;

import java.util.List;


public class BusiManHourDetailBean {

	private long manHour_id;
	
	private long prj_id;
	
	private String prj_name;
	
	private String man_hour;
	
	private int total;
	
	private String app_man_hour;

	private int app_total;
	
	//自定义
	private List<String> man_hour_list;
	private List<String> app_man_hour_list;
	

	public long getManHour_id() {
		return manHour_id;
	}

	public void setManHour_id(long manHour_id) {
		this.manHour_id = manHour_id;
	}
	
	public String getApp_man_hour() {
		return app_man_hour;
	}

	public void setApp_man_hour(String app_man_hour) {
		this.app_man_hour = app_man_hour;
	}

	public int getApp_total() {
		return app_total;
	}

	public void setApp_total(int app_total) {
		this.app_total = app_total;
	}


	public String getPrj_name() {
		return prj_name;
	}

	public void setPrj_name(String prj_name) {
		this.prj_name = prj_name;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public String getMan_hour() {
		return man_hour;
	}

	public void setMan_hour(String man_hour) {
		this.man_hour = man_hour;
	}

	public List<String> getMan_hour_list() {
		return man_hour_list;
	}

	public void setMan_hour_list(List<String> man_hour_list) {
		this.man_hour_list = man_hour_list;
	}

	public long getPrj_id() {
		return prj_id;
	}

	public void setPrj_id(long prj_id) {
		this.prj_id = prj_id;
	}

	public List<String> getApp_man_hour_list() {
		return app_man_hour_list;
	}

	public void setApp_man_hour_list(List<String> app_man_hour_list) {
		this.app_man_hour_list = app_man_hour_list;
	}
	


}
