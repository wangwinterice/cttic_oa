package com.cttic.erp.bean;

import java.math.BigDecimal;
import java.util.List;

public class InfoProjectBean {

	private long id;
	
	private BigDecimal prj_id;
	
	private String prj_code;

	private String prj_name;
	
	private String state;
	
	private String state_desc;
	
	private long manager_employee_id;
	
	private String manager_employee_name;
	
	private List<InfoProjectEmployeeBean> infoProjectEmployeeBeanList;
	
	private List<String> employeeNameList;
	
	private String employeeNames;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public BigDecimal getPrj_id() {
		return prj_id;
	}

	public void setPrj_id(BigDecimal prj_id) {
		this.prj_id = prj_id;
	}

	public String getPrj_code() {
		return prj_code;
	}

	public void setPrj_code(String prj_code) {
		this.prj_code = prj_code;
	}

	public String getPrj_name() {
		return prj_name;
	}

	public void setPrj_name(String prj_name) {
		this.prj_name = prj_name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getState_desc() {
		return state_desc;
	}

	public void setState_desc(String state_desc) {
		this.state_desc = state_desc;
	}

	public long getManager_employee_id() {
		return manager_employee_id;
	}

	public void setManager_employee_id(long manager_employee_id) {
		this.manager_employee_id = manager_employee_id;
	}

	public String getManager_employee_name() {
		return manager_employee_name;
	}

	public void setManager_employee_name(String manager_employee_name) {
		this.manager_employee_name = manager_employee_name;
	}

	public List<InfoProjectEmployeeBean> getInfoProjectEmployeeBeanList() {
		return infoProjectEmployeeBeanList;
	}

	public void setInfoProjectEmployeeBeanList(
			List<InfoProjectEmployeeBean> infoProjectEmployeeBeanList) {
		this.infoProjectEmployeeBeanList = infoProjectEmployeeBeanList;
	}

	public List<String> getEmployeeNameList() {
		return employeeNameList;
	}

	public void setEmployeeNameList(List<String> employeeNameList) {
		this.employeeNameList = employeeNameList;
	}

	public String getEmployeeNames() {
		return employeeNames;
	}

	public void setEmployeeNames(String employeeNames) {
		this.employeeNames = employeeNames;
	}
}
