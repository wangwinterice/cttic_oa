package com.cttic.erp.bean;

public class InfoEvaluationTemplateBean {

	private int template_id;
	
	private String template_name;

	public int getTemplate_id() {
		return template_id;
	}

	public void setTemplate_id(int template_id) {
		this.template_id = template_id;
	}

	public String getTemplate_name() {
		return template_name;
	}

	public void setTemplate_name(String template_name) {
		this.template_name = template_name;
	}
}
