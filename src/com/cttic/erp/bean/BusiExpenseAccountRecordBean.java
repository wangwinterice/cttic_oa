package com.cttic.erp.bean;

import java.math.BigDecimal;
import java.util.Date;

public class BusiExpenseAccountRecordBean {

	private long record_id;
	
	private BigDecimal prj_id;
	
	private long employee_id;
	
	private String expense_type;
	
	private BigDecimal expense_amount;
	
	private String expense_desc;
	
	private int state;
	
	private Date apply_time;

	public long getRecord_id() {
		return record_id;
	}

	public void setRecord_id(long record_id) {
		this.record_id = record_id;
	}

	public BigDecimal getPrj_id() {
		return prj_id;
	}

	public void setPrj_id(BigDecimal prj_id) {
		this.prj_id = prj_id;
	}

	public long getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(long employee_id) {
		this.employee_id = employee_id;
	}

	public String getExpense_type() {
		return expense_type;
	}

	public void setExpense_type(String expense_type) {
		this.expense_type = expense_type;
	}

	public BigDecimal getExpense_amount() {
		return expense_amount;
	}

	public void setExpense_amount(BigDecimal expense_amount) {
		this.expense_amount = expense_amount;
	}

	public String getExpense_desc() {
		return expense_desc;
	}

	public void setExpense_desc(String expense_desc) {
		this.expense_desc = expense_desc;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public Date getApply_time() {
		return apply_time;
	}

	public void setApply_time(Date apply_time) {
		this.apply_time = apply_time;
	}
}
