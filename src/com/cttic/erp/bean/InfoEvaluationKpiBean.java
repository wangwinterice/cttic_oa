package com.cttic.erp.bean;

public class InfoEvaluationKpiBean {

	private int kpi_id;
	
	private String kpi_name;
	
	private String kpi_score;
	
	private int dimension_id;

	public int getKpi_id() {
		return kpi_id;
	}

	public void setKpi_id(int kpi_id) {
		this.kpi_id = kpi_id;
	}

	public String getKpi_name() {
		return kpi_name;
	}

	public void setKpi_name(String kpi_name) {
		this.kpi_name = kpi_name;
	}

	public String getKpi_score() {
		return kpi_score;
	}

	public void setKpi_score(String kpi_score) {
		this.kpi_score = kpi_score;
	}

	public int getDimension_id() {
		return dimension_id;
	}

	public void setDimension_id(int dimension_id) {
		this.dimension_id = dimension_id;
	}
}
