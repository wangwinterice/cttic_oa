package com.cttic.erp.bean;

public class BusiWeeklyReportResultBean {

	private int result_id;
	
	private String result_type;
	
	private String result_code;
	
	private String result_value;
	
	private String record_id;

	public int getResult_id() {
		return result_id;
	}

	public void setResult_id(int result_id) {
		this.result_id = result_id;
	}

	public String getResult_type() {
		return result_type;
	}

	public void setResult_type(String result_type) {
		this.result_type = result_type;
	}

	public String getResult_code() {
		return result_code;
	}

	public void setResult_code(String result_code) {
		this.result_code = result_code;
	}

	public String getResult_value() {
		return result_value;
	}

	public void setResult_value(String result_value) {
		this.result_value = result_value;
	}

	public String getRecord_id() {
		return record_id;
	}

	public void setRecord_id(String record_id) {
		this.record_id = record_id;
	}
}
