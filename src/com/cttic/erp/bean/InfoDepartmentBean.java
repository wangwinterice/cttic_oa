package com.cttic.erp.bean;

public class InfoDepartmentBean {

	private long dept_id;
	
	private String dept_name;
	
	private String dept_code;
	
	private long parent_dept_id;
	
	private long manager_employee_id;

	public long getDept_id() {
		return dept_id;
	}

	public void setDept_id(long dept_id) {
		this.dept_id = dept_id;
	}

	public String getDept_name() {
		return dept_name;
	}

	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}

	public String getDept_code() {
		return dept_code;
	}

	public void setDept_code(String dept_code) {
		this.dept_code = dept_code;
	}

	public long getParent_dept_id() {
		return parent_dept_id;
	}

	public void setParent_dept_id(long parent_dept_id) {
		this.parent_dept_id = parent_dept_id;
	}

	public long getManager_employee_id() {
		return manager_employee_id;
	}

	public void setManager_employee_id(long manager_employee_id) {
		this.manager_employee_id = manager_employee_id;
	}

}
