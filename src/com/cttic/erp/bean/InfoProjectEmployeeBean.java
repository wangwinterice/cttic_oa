package com.cttic.erp.bean;

import java.math.BigDecimal;

public class InfoProjectEmployeeBean {

	private long id;
	
	private BigDecimal prj_id;
	
	private long employee_id;
	
	private String prj_name;
	
	private String employee_name;
	
	private String state;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public BigDecimal getPrj_id() {
		return prj_id;
	}

	public void setPrj_id(BigDecimal prj_id) {
		this.prj_id = prj_id;
	}

	public long getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(long employee_id) {
		this.employee_id = employee_id;
	}

	public String getPrj_name() {
		return prj_name;
	}

	public void setPrj_name(String prj_name) {
		this.prj_name = prj_name;
	}

	public String getEmployee_name() {
		return employee_name;
	}

	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
}
