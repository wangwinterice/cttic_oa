package com.cttic.erp.bean;

public class InfoEvaluationKpiStandardBean {

	private int standard_id;
	
	private String standard_name;
	
	private String standard_score_scope;
	
	private int kpi_id;

	public int getStandard_id() {
		return standard_id;
	}

	public void setStandard_id(int standard_id) {
		this.standard_id = standard_id;
	}

	public String getStandard_name() {
		return standard_name;
	}

	public void setStandard_name(String standard_name) {
		this.standard_name = standard_name;
	}

	public String getStandard_score_scope() {
		return standard_score_scope;
	}

	public void setStandard_score_scope(String standard_score_scope) {
		this.standard_score_scope = standard_score_scope;
	}

	public int getKpi_id() {
		return kpi_id;
	}

	public void setKpi_id(int kpi_id) {
		this.kpi_id = kpi_id;
	}
}
