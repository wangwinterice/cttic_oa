package com.cttic.erp.bean;

public class BusiEvaluationRecordBean {

	private long record_id;
	
	private long employee_id;
	
	private String evaluation_date;
	
	private String state;
	
	private String result_description;

	public long getRecord_id() {
		return record_id;
	}

	public void setRecord_id(long record_id) {
		this.record_id = record_id;
	}

	public long getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(long employee_id) {
		this.employee_id = employee_id;
	}

	public String getEvaluation_date() {
		return evaluation_date;
	}

	public void setEvaluation_date(String evaluation_date) {
		this.evaluation_date = evaluation_date;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getResult_description() {
		return result_description;
	}

	public void setResult_description(String result_description) {
		this.result_description = result_description;
	}
}
