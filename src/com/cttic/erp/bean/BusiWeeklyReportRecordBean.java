package com.cttic.erp.bean;

import java.util.Date;

public class BusiWeeklyReportRecordBean {

	private String record_id;
	
	private long employee_id;
	
	private String begin_date;
	
	private String end_date;
	
	private Date oper_time;
	
	private String task_begin_date;
	
	private String task_end_date;
	
	private String plan_begin_date;
	
	private String plan_end_date;
	
	private String approval_flag;
	
	public String getRecord_id() {
		return record_id;
	}

	public void setRecord_id(String record_id) {
		this.record_id = record_id;
	}

	public long getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(long employee_id) {
		this.employee_id = employee_id;
	}

	public String getBegin_date() {
		return begin_date;
	}

	public void setBegin_date(String begin_date) {
		this.begin_date = begin_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public Date getOper_time() {
		return oper_time;
	}

	public void setOper_time(Date oper_time) {
		this.oper_time = oper_time;
	}

	public String getTask_begin_date() {
		return task_begin_date;
	}

	public void setTask_begin_date(String task_begin_date) {
		this.task_begin_date = task_begin_date;
	}

	public String getTask_end_date() {
		return task_end_date;
	}

	public void setTask_end_date(String task_end_date) {
		this.task_end_date = task_end_date;
	}

	public String getPlan_begin_date() {
		return plan_begin_date;
	}

	public void setPlan_begin_date(String plan_begin_date) {
		this.plan_begin_date = plan_begin_date;
	}

	public String getPlan_end_date() {
		return plan_end_date;
	}

	public void setPlan_end_date(String plan_end_date) {
		this.plan_end_date = plan_end_date;
	}

	public String getApproval_flag() {
		return approval_flag;
	}

	public void setApproval_flag(String approval_flag) {
		this.approval_flag = approval_flag;
	}
}
