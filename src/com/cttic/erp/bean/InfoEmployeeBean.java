package com.cttic.erp.bean;


public class InfoEmployeeBean {

	private long employee_id;

	private String employee_name;

	private long employee_no;

	private String mobile_phone;

	private String email;

	private String employee_pwd;

	private String dept_id;

	private String status;

	private String cert_num;

	private String post;

	private String sex;

	private String birthday;

	private String address;

	private String zip;

	private String in_company;

	public long getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(long employee_id) {
		this.employee_id = employee_id;
	}

	public String getEmployee_name() {
		return employee_name;
	}

	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}

	public long getEmployee_no() {
		return employee_no;
	}

	public void setEmployee_no(long employee_no) {
		this.employee_no = employee_no;
	}

	public String getMobile_phone() {
		return mobile_phone;
	}

	public void setMobile_phone(String mobile_phone) {
		this.mobile_phone = mobile_phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmployee_pwd() {
		return employee_pwd;
	}

	public void setEmployee_pwd(String employee_pwd) {
		this.employee_pwd = employee_pwd;
	}

	public String getDept_id() {
		return dept_id;
	}

	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCert_num() {
		return cert_num;
	}

	public void setCert_num(String cert_num) {
		this.cert_num = cert_num;
	}

	public String getPost() {
		return post;
	}

	public void setPost(String post) {
		this.post = post;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getIn_company() {
		return in_company;
	}

	public void setIn_company(String in_company) {
		this.in_company = in_company;
	}

}
