package com.cttic.erp.servlet;

import javax.servlet.http.HttpServlet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cttic.erp.common.cache.CacheManager;

public class CacheLoaderServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Log log = LogFactory.getLog(CacheLoaderServlet.class);

	@Override
	public void init() {
		try {
			log.debug("初始化缓存开始.....");
			CacheManager.start();
			log.debug("初始化缓存结束.....");
		} catch (Exception e) {
			log.error("初始化TSysParam表数据到缓存失败", e);
			throw new RuntimeException("加载[系统配置参数]缓存数据异常...", e);
		}
	}
}
