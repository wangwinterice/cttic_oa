package com.cttic.erp.service.interfaces;

import java.util.List;

import com.cttic.erp.bean.BusiExpenseAccountRecordBean;
import com.cttic.erp.bean.ext.BusiExpenseAccountRecordBeanExt;

public interface IBusiExpenseAccountRecordService {

	public List<BusiExpenseAccountRecordBean> getBeansByEmployeeId(long employee_id);
	
	public List<BusiExpenseAccountRecordBean> getEffectBeans();
	
	public List<BusiExpenseAccountRecordBeanExt> getStatBeans();
	
	public int insert(BusiExpenseAccountRecordBean bean);
	
	public int update(BusiExpenseAccountRecordBean bean);
	
	public int updateState(BusiExpenseAccountRecordBean bean);
}
