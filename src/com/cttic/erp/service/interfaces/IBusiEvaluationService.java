package com.cttic.erp.service.interfaces;

import java.util.List;

import com.cttic.erp.bean.BusiEvaluationRecordBean;
import com.cttic.erp.bean.BusiEvaluationResultBean;
import com.cttic.erp.bean.ext.BusiEvaluationBeanExt;

public interface IBusiEvaluationService {

	public List<BusiEvaluationBeanExt> getEvaluationStandardList(int template_id);
	
	public List<BusiEvaluationBeanExt> getEvaluationKpiList(int template_id);

	public void saveNewSelfEvaluationResult(
			BusiEvaluationRecordBean busiEvaluationRecordBean,
			List<BusiEvaluationResultBean> busiEvaluationResultBeanlist);

	public void saveCheckEvaluationResult(
			BusiEvaluationRecordBean busiEvaluationRecordBean,
			List<BusiEvaluationResultBean> busiEvaluationResultBeanlist);

	public void saveEditSelfEvaluationResult(
			BusiEvaluationRecordBean busiEvaluationRecordBean,
			List<BusiEvaluationResultBean> busiEvaluationResultBeanlist);
}
