package com.cttic.erp.service.interfaces;

import java.util.List;

import com.cttic.erp.bean.SysParamBean;

public interface ISysParamService {

	public List<SysParamBean> getBeans();
}
