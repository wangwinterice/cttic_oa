package com.cttic.erp.service.interfaces;

import java.util.List;

import com.cttic.erp.bean.BusiWeeklyReportResultBean;

public interface IBusiWeeklyReportResultService {

	public List<BusiWeeklyReportResultBean> getBeans(String record_id);
	
	public List<BusiWeeklyReportResultBean> getBeansByEmployeeId(Long employee_id);
	
	public void insertBean(BusiWeeklyReportResultBean bean);
	
	public void deleteBeans(String record_id);
}
