package com.cttic.erp.service.interfaces;

import java.util.List;

import com.cttic.erp.bean.InfoEvaluationDimensionBean;

public interface IInfoEvaluationDimensionService {

	public List<InfoEvaluationDimensionBean> getBeanList();
}
