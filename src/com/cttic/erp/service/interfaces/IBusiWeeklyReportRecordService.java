package com.cttic.erp.service.interfaces;

import java.util.List;

import com.cttic.erp.bean.BusiWeeklyReportRecordBean;

public interface IBusiWeeklyReportRecordService {
	
	public BusiWeeklyReportRecordBean getBean(long employee_id,
			String begin_date, String end_date);
	
	public List<BusiWeeklyReportRecordBean> getBeanListByDate(String begin_date, String end_date);

	public List<BusiWeeklyReportRecordBean> getBeans(long employee_id);
	
	public List<BusiWeeklyReportRecordBean> getBeans(long employee_id, String select_month);

	public void insertBean(BusiWeeklyReportRecordBean bean);
	
	public void updateBean(BusiWeeklyReportRecordBean bean);
	
	/**
	 * 通过employee_id和begin_date查询记录
	 * @param employee_id
	 * @param current_monday
	 * @return
	 */
	public BusiWeeklyReportRecordBean findWeekRecord(
			BusiWeeklyReportRecordBean search);

	public BusiWeeklyReportRecordBean getBeanById(String week_id);
}
