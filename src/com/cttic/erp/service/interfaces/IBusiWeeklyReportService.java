package com.cttic.erp.service.interfaces;

import java.util.List;

import com.cttic.erp.bean.BusiWeeklyReportRecordBean;
import com.cttic.erp.bean.BusiWeeklyReportResultBean;

public interface IBusiWeeklyReportService {

	public void save(String operType, BusiWeeklyReportRecordBean busiWeeklyReportRecordBean,
			List<BusiWeeklyReportResultBean> busiWeeklyReportResultBeanList);
	
	public void saveCheck(String operType, BusiWeeklyReportRecordBean busiWeeklyReportRecordBean,
			List<BusiWeeklyReportResultBean> busiWeeklyReportResultBeanList);
}
