package com.cttic.erp.service.interfaces;

import java.util.List;

import com.cttic.erp.bean.InfoMenuBean;
import com.cttic.erp.bean.ext.InfoOperBeanExt;

public interface IInfoOperService {

	public List<InfoMenuBean> getBeanList(long employee_id);

	public InfoOperBeanExt getInfoOperBeanExt(long employee_id);
}
