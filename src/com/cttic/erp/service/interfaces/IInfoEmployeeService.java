package com.cttic.erp.service.interfaces;

import java.util.List;

import com.cttic.erp.bean.InfoEmployeeBean;

public interface IInfoEmployeeService {

	public List<InfoEmployeeBean> getBeanList();

	public List<InfoEmployeeBean> getBeanListByDept(long dept_id);

	public InfoEmployeeBean getBean(long employee_no);

	public void updateEmployeePwd(String employee_pwd, long employee_id);

	public void insert(InfoEmployeeBean infoEmployeeBean);

	public void update(InfoEmployeeBean infoEmployeeBean);

	public List<InfoEmployeeBean> getBeanListByConditions(String conditions);
}
