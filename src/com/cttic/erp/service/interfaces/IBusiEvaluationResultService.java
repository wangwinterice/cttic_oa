package com.cttic.erp.service.interfaces;

import java.util.List;

import com.cttic.erp.bean.BusiEvaluationResultBean;
import com.cttic.erp.bean.ext.BusiEvaluationResultBeanExt;

public interface IBusiEvaluationResultService {

	public List<BusiEvaluationResultBean> getBeanList(long record_id);
	
	public List<BusiEvaluationResultBeanExt> getBeanList(String evaluation_date);

	public List<BusiEvaluationResultBeanExt> getEvaluationState();
}
