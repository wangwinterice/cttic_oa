package com.cttic.erp.service.interfaces;

import java.util.List;

import com.cttic.erp.bean.InfoEvaluationKpiBean;

public interface IInfoEvaluationKpiService {

	public List<InfoEvaluationKpiBean> getBeanList();
}
