package com.cttic.erp.service.interfaces;

import com.cttic.erp.bean.BusiEvaluationRecordBean;

public interface IBusiEvaluationRecordService {

	public BusiEvaluationRecordBean getBean(long employee_id,
			String evaluation_date);

	public void insertBean(BusiEvaluationRecordBean busiEvaluationRecordBean);

	public void updateBean(BusiEvaluationRecordBean busiEvaluationRecordBean);
}
