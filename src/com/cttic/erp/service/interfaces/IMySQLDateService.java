package com.cttic.erp.service.interfaces;

import java.util.Calendar;
import java.util.List;

import com.cttic.erp.bean.ext.VirtualDateBeanExt;

public interface IMySQLDateService {

	public Calendar getSysdate() throws Exception;
	
	public String getSysdateStr() throws Exception;
	
	public Calendar getWeekFirstDate(String date) throws Exception;
	
	public String getWeekFirstDateStr(String date) throws Exception;
	
	public Calendar getWeekLastDate(String date) throws Exception;
	
	public String getWeekLastDateStr(String date) throws Exception;
	
	public List<String> getWeekList(String date) throws Exception;
	
	public List<VirtualDateBeanExt> getMonthList(String date) throws Exception;
	
	public Calendar getMonthFirstDate(String date) throws Exception;
	
	public String getMonthFirstDateStr(String date) throws Exception;
	
	public Calendar getMonthLastDate(String date) throws Exception;
	
	public String getMonthLastDateStr(String date) throws Exception;
	
	public String getBeforeMonthToday() throws Exception;
	
	public String getAfterMonthToday() throws Exception;
	
	public String getNextDate(String date) throws Exception;
}
