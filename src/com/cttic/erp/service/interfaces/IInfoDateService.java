package com.cttic.erp.service.interfaces;

import java.util.List;

import com.cttic.erp.bean.InfoDateBean;

public interface IInfoDateService {

	public List<InfoDateBean> getBeans(String year, String month);
}
