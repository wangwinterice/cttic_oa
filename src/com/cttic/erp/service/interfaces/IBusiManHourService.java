package com.cttic.erp.service.interfaces;

import java.util.List;
import java.util.Map;

import com.cttic.erp.bean.BusiManHourBean;
import com.cttic.erp.bean.InfoProjectBean;

public interface IBusiManHourService {
	

	public List<BusiManHourBean> getBeans(long employee_id);
	
	public void insertBean(BusiManHourBean busiManHourBean);
	
	public int delete(long id);

	public List<Map<String, String>> getPeriod(String startDate);

	public List<BusiManHourBean> getBeansById(long id);

	public List<InfoProjectBean> getProjectsByWeekId(String week_id);

	public String getRealBeginDateByWeekId(String week_id);

	public String getRealEndDateByWeekId(String week_id);

	public List<BusiManHourBean> getApprovalBeans();

	public BusiManHourBean getBeanByWeekId(String record_id);
	/**
	 * 获取一个员工所有填报和未填报的工时
	 * @param employee_id
	 * @return
	 */
	public List<BusiManHourBean> getAllBeans(long employee_id);
	/**
	 * 获取工时表中所有的年列表
	 * @return
	 */
	public List<Map<String, String>> findYears();
	/**
	 * 获取一年中所有的周期
	 * @param year
	 * @return
	 */
	public List<Map<String, String>> getWeekByYear(String year);
	
	/**
	 * 获取一个周期下所有未审批的工时
	 * @param replace
	 * @return
	 */
	public List<BusiManHourBean> getApprovalBeansByEndDate(String replace);
	
	/**
	 * 更新工时表状态
	 * @param id
	 */
	public void approval(long id);
	
	

}
