package com.cttic.erp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cttic.erp.bean.BusiEvaluationRecordBean;
import com.cttic.erp.dao.interfaces.IBusiEvaluationRecordDAO;
import com.cttic.erp.service.interfaces.IBusiEvaluationRecordService;

@Service
public class BusiEvaluationRecordServiceImpl implements
		IBusiEvaluationRecordService {

	@Autowired
	private IBusiEvaluationRecordDAO busiEvaluationRecordDAOImpl;
	
	@Override
	public BusiEvaluationRecordBean getBean(long employee_id,
			String evaluation_date) {
		return busiEvaluationRecordDAOImpl.getBean(employee_id, evaluation_date);
	}

	@Override
	public void insertBean(BusiEvaluationRecordBean busiEvaluationRecordBean) {
		busiEvaluationRecordDAOImpl.insertBean(busiEvaluationRecordBean);
	}

	@Override
	public void updateBean(BusiEvaluationRecordBean busiEvaluationRecordBean) {
		busiEvaluationRecordDAOImpl.updateBean(busiEvaluationRecordBean);
	}

}
