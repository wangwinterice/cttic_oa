package com.cttic.erp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cttic.erp.bean.BusiWeeklyReportRecordBean;
import com.cttic.erp.bean.BusiWeeklyReportResultBean;
import com.cttic.erp.dao.interfaces.IBusiWeeklyReportRecordDAO;
import com.cttic.erp.dao.interfaces.IBusiWeeklyReportResultDAO;
import com.cttic.erp.service.interfaces.IBusiWeeklyReportService;

@Service
public class BusiWeeklyReportServiceImpl implements IBusiWeeklyReportService {

	@Autowired
	private IBusiWeeklyReportRecordDAO busiWeeklyReportRecordDAOImpl;
	
	@Autowired
	private IBusiWeeklyReportResultDAO busiWeeklyReportResultDAOImpl;
	
	@Override
	@Transactional
	public void save(String operType,
			BusiWeeklyReportRecordBean busiWeeklyReportRecordBean,
			List<BusiWeeklyReportResultBean> busiWeeklyReportResultBeanList) {
		if ("NEW".equals(operType)) {
			busiWeeklyReportRecordDAOImpl.insertBean(busiWeeklyReportRecordBean);
		}
		if ("EDIT".equals(operType)) {
			busiWeeklyReportRecordDAOImpl.updateBean(busiWeeklyReportRecordBean);
			busiWeeklyReportResultDAOImpl.deleteBeans(busiWeeklyReportRecordBean.getRecord_id());
		}
		for (BusiWeeklyReportResultBean bean : busiWeeklyReportResultBeanList) {
			busiWeeklyReportResultDAOImpl.insertBean(bean);
		}
	}

	public void saveCheck(String operType, BusiWeeklyReportRecordBean busiWeeklyReportRecordBean,
			List<BusiWeeklyReportResultBean> busiWeeklyReportResultBeanList) {
		busiWeeklyReportRecordBean.setApproval_flag("1");
		busiWeeklyReportRecordDAOImpl.updateBean(busiWeeklyReportRecordBean);
		busiWeeklyReportResultDAOImpl.deleteWorkTime(busiWeeklyReportRecordBean.getRecord_id());
		for (BusiWeeklyReportResultBean bean : busiWeeklyReportResultBeanList) {
			busiWeeklyReportResultDAOImpl.insertBean(bean);
		}
	}
}
