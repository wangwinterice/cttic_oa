package com.cttic.erp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cttic.erp.bean.InfoEvaluationKpiBean;
import com.cttic.erp.dao.interfaces.IInfoEvaluationKpiDAO;
import com.cttic.erp.service.interfaces.IInfoEvaluationKpiService;

@Service
public class InfoEvaluationKpiServiceImpl implements IInfoEvaluationKpiService {

	@Autowired
	private IInfoEvaluationKpiDAO infoEvaluationKpiImpl;
	
	@Override
	public List<InfoEvaluationKpiBean> getBeanList() {
		return infoEvaluationKpiImpl.getBeanList();
	}

}
