package com.cttic.erp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cttic.erp.bean.InfoEvaluationDimensionBean;
import com.cttic.erp.dao.interfaces.IInfoEvaluationDimensionDAO;
import com.cttic.erp.service.interfaces.IInfoEvaluationDimensionService;

@Service
public class InfoEvaluationDimensionServiceImpl implements
		IInfoEvaluationDimensionService {

	@Autowired
	private IInfoEvaluationDimensionDAO infoEvaluationDimensionDAOImpl;
	
	@Override
	public List<InfoEvaluationDimensionBean> getBeanList() {
		return infoEvaluationDimensionDAOImpl.getBeanList();
	}

}
