package com.cttic.erp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cttic.erp.bean.BusiWeeklyReportResultBean;
import com.cttic.erp.dao.interfaces.IBusiWeeklyReportResultDAO;
import com.cttic.erp.service.interfaces.IBusiWeeklyReportResultService;

@Service
public class BusiWeeklyReportResultServiceImpl implements
		IBusiWeeklyReportResultService {

	@Autowired
	private IBusiWeeklyReportResultDAO busiWeeklyReportResultDAOImpl;
	
	@Override
	public List<BusiWeeklyReportResultBean> getBeans(String record_id) {
		return busiWeeklyReportResultDAOImpl.getBeans(record_id);
	}

	@Override
	public List<BusiWeeklyReportResultBean> getBeansByEmployeeId(Long employee_id) {
		return busiWeeklyReportResultDAOImpl.getBeansByEmployeeId(employee_id);
	}
	
	@Override
	public void insertBean(BusiWeeklyReportResultBean bean) {
		busiWeeklyReportResultDAOImpl.insertBean(bean);
	}

	@Override
	public void deleteBeans(String record_id) {
		busiWeeklyReportResultDAOImpl.deleteBeans(record_id);
	}

}
