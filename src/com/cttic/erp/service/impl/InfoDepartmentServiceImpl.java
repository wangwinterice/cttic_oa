package com.cttic.erp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cttic.erp.bean.InfoDepartmentBean;
import com.cttic.erp.dao.interfaces.IInfoDepartmentDAO;
import com.cttic.erp.service.interfaces.IInfoDepartmentService;

@Service
public class InfoDepartmentServiceImpl implements IInfoDepartmentService {

	@Autowired
	private IInfoDepartmentDAO infoDepartmentDAOImpl;

	@Override
	public List<InfoDepartmentBean> getBeans() {
		return infoDepartmentDAOImpl.getBeans();
	}

	@Override
	public InfoDepartmentBean getBeanByDeptId(long dept_id) {
		return infoDepartmentDAOImpl.getBeanByDeptId(dept_id);
	}

	@Override
	public List<InfoDepartmentBean> getChildBeanByDeptId(long parent_dept_id) {
		return infoDepartmentDAOImpl.getChildBeanByDeptId(parent_dept_id);
	}

	@Override
	public InfoDepartmentBean getBeanByManagerEmployee(long manager_employee_id) {
		return infoDepartmentDAOImpl.getBeanByManagerEmployee(manager_employee_id);
	}

}
