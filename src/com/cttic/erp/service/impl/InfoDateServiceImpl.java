package com.cttic.erp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cttic.erp.bean.InfoDateBean;
import com.cttic.erp.dao.interfaces.IInfoDateDAO;
import com.cttic.erp.service.interfaces.IInfoDateService;

@Service
public class InfoDateServiceImpl implements IInfoDateService {

	@Autowired
	IInfoDateDAO infoDateDao;
	
	@Override
	public List<InfoDateBean> getBeans(String year, String month) {
		return infoDateDao.getBeans(year, month);
	}

}
