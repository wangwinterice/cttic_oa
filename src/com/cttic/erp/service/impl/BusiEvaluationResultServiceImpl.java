package com.cttic.erp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cttic.erp.bean.BusiEvaluationResultBean;
import com.cttic.erp.bean.ext.BusiEvaluationResultBeanExt;
import com.cttic.erp.dao.interfaces.IBusiEvaluationResultDAO;
import com.cttic.erp.service.interfaces.IBusiEvaluationResultService;

@Service
public class BusiEvaluationResultServiceImpl implements
		IBusiEvaluationResultService {

	@Autowired
	private IBusiEvaluationResultDAO busiEvaluationResultDAOImpl;
	
	@Override
	public List<BusiEvaluationResultBean> getBeanList(long record_id) {
		return busiEvaluationResultDAOImpl.getBeanList(record_id);
	}
	
	@Override
	public List<BusiEvaluationResultBeanExt> getBeanList(String evaluation_date) {
		return busiEvaluationResultDAOImpl.getBeanList(evaluation_date);
	}

	@Override
	public List<BusiEvaluationResultBeanExt> getEvaluationState() {
		return busiEvaluationResultDAOImpl.getEvaluationState();
	}
}
