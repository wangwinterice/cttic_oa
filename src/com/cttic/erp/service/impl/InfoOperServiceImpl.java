package com.cttic.erp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cttic.erp.bean.InfoMenuBean;
import com.cttic.erp.bean.ext.InfoOperBeanExt;
import com.cttic.erp.dao.interfaces.IInfoEmployeeDAO;
import com.cttic.erp.dao.interfaces.IInfoOperDAO;
import com.cttic.erp.service.interfaces.IInfoOperService;

@Service
public class InfoOperServiceImpl implements IInfoOperService {

	@Autowired
	private IInfoOperDAO infoOperDAOImpl;
	
	@Autowired
	private IInfoEmployeeDAO infoEmployeeDAOImpl;
	
	@Override
	public List<InfoMenuBean> getBeanList(long employee_id) {
		return infoOperDAOImpl.getInfoMenuBeanList(employee_id);
	}

	@Override
	public InfoOperBeanExt getInfoOperBeanExt(long employee_id) {
		InfoOperBeanExt infoOperBeanExt = new InfoOperBeanExt();
		infoOperBeanExt.setInfoEmployeeBean(infoEmployeeDAOImpl.getBean(employee_id));
		infoOperBeanExt.setInfoMenuBeanLists(getBeanList(employee_id));
		return infoOperBeanExt;
	}
}
