package com.cttic.erp.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cttic.erp.bean.BusiManHourBean;
import com.cttic.erp.bean.InfoProjectBean;
import com.cttic.erp.common.util.StringUtil;
import com.cttic.erp.dao.interfaces.IBusiManHourDAO;
import com.cttic.erp.service.interfaces.IBusiManHourService;

@Service
public class BusiManHourServiceImpl implements
	IBusiManHourService {

	@Autowired
	private IBusiManHourDAO busiManHourDAOImpl;
	
	@Override
	public List<BusiManHourBean> getBeans(long employee_id) {
		List<BusiManHourBean> list = busiManHourDAOImpl.getBeans(employee_id);
		initAttributes(list);
		return list;
	}
	
	/**
	 * end_date在几月，当前周就属于哪个月的范围
	 * @param list
	 */
	private void initAttributes(List<BusiManHourBean> list) {
		for(BusiManHourBean bean : list){
			String year = bean.getEnd_date().substring(0, 4);
			String month = bean.getEnd_date().substring(4, 6);
			String begin_desc = StringUtil.changeDateFormt(bean.getBegin_date());
			String end_desc = StringUtil.changeDateFormt(bean.getEnd_date());
			
			bean.setYear(year);
			bean.setMonth(month);
			bean.setDate_section(begin_desc+"~"+end_desc);
		}
	}

	@Override
	public void insertBean(BusiManHourBean busiManHourBean) {
		busiManHourDAOImpl.insertBean(busiManHourBean);
	}

	@Override
	public int delete(long id) {
		return busiManHourDAOImpl.delete(id);
	}

	@Override
	public List<Map<String, String>> getPeriod(String startDate) {
		return busiManHourDAOImpl.getPeriod(startDate);
	}

	@Override
	public List<BusiManHourBean> getBeansById(long id) {
		return busiManHourDAOImpl.getBeansById(id);
	}

	@Override
	public List<InfoProjectBean> getProjectsByWeekId(String week_id) {
		return busiManHourDAOImpl.getProjectsByWeekId(week_id);
	}

	@Override
	public String getRealBeginDateByWeekId(String week_id) {
		return busiManHourDAOImpl.getRealBeginDateByWeekId(week_id).replace("-", "");
	}

	@Override
	public String getRealEndDateByWeekId(String week_id) {
		return busiManHourDAOImpl.getRealEndDateByWeekId(week_id).replace("-", "");
	}

	@Override
	public List<BusiManHourBean> getApprovalBeans() {
		List<BusiManHourBean> list = busiManHourDAOImpl.getApprovalBeans();
		initAttributes(list);
		return list;
	}

	@Override
	public BusiManHourBean getBeanByWeekId(String record_id) {
		List<BusiManHourBean> list = busiManHourDAOImpl.getBeanByWeekId(record_id);
		if(list!=null && list.size()>0){
			return list.get(0);
		}
		return null;
	}
	/**
	 * 获取一个员工所有填报和未填报的工时
	 * @param employee_id
	 * @return
	 */
	@Override
	public List<BusiManHourBean> getAllBeans(long employee_id) {
		List<BusiManHourBean> list = busiManHourDAOImpl.getAllBeans(employee_id);
		initAttributes(list);
		return list;
	}

	@Override
	public List<Map<String, String>> findYears() {
		return busiManHourDAOImpl.findYears();
	}

	@Override
	public List<Map<String, String>> getWeekByYear(String year) {
		return busiManHourDAOImpl.getWeekByYear(year);
	}

	@Override
	public List<BusiManHourBean> getApprovalBeansByEndDate(String end_date) {
		List<BusiManHourBean> list = busiManHourDAOImpl.getApprovalBeansByEndDate(end_date);
		initAttributes(list);
		return list;
	}

	@Override
	public void approval(long id) {
		busiManHourDAOImpl.approval(id);
	}


}
