package com.cttic.erp.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cttic.erp.bean.ext.VirtualDateBeanExt;
import com.cttic.erp.mapper.MySQLDateMapper;
import com.cttic.erp.service.interfaces.IMySQLDateService;

@Service
public class MySQLDateServiceImpl implements IMySQLDateService {

	@Autowired
	private MySQLDateMapper mySQLDateMapper;
	
	public Calendar getSysdate() throws Exception {
		String date = mySQLDateMapper.getSysDate();
		Calendar calendar = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		calendar.setTime(dateFormat.parse(date));
		return calendar;
	}
	
	public String getSysdateStr() throws Exception {
		String date = mySQLDateMapper.getSysDate();
		return date;
	}
	
	public Calendar getWeekFirstDate(String date) throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Integer.parseInt(date.split("-")[0]), Integer.parseInt(date.split("-")[1]) - 1, Integer.parseInt(date.split("-")[2]));
		calendar.set(Calendar.DAY_OF_WEEK, 1);
		return calendar;
	}
	
	public String getWeekFirstDateStr(String date) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(sdf.parse(date));
		calendar.set(Calendar.DAY_OF_WEEK, 1);
		return sdf.format(calendar.getTime());
	}
	
	public Calendar getWeekLastDate(String date) throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Integer.parseInt(date.split("-")[0]), Integer.parseInt(date.split("-")[1]) - 1, Integer.parseInt(date.split("-")[2]));
		calendar.set(Calendar.DAY_OF_WEEK, 7);
		return calendar;
	}
	
	public String getWeekLastDateStr(String date) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(sdf.parse(date));
		calendar.set(Calendar.DAY_OF_WEEK, 7);
		return sdf.format(calendar.getTime());
	}
	
	public List<String> getWeekList(String date) throws Exception {
		List<String> weekList = new ArrayList<String>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Calendar calendar = Calendar.getInstance();
		for (int i = 2; i <= 6; i ++) {
			calendar.set(Calendar.DAY_OF_WEEK, i);
			weekList.add(sdf.format(calendar.getTime()));
		}
		return weekList;
	}
	
	@SuppressWarnings("deprecation")
	public List<VirtualDateBeanExt> getMonthList(String date) throws Exception {
		List<VirtualDateBeanExt> monthList = new ArrayList<VirtualDateBeanExt>();
		String[] daysOfTheWeek = new String[]{"", "������", "����һ", "���ڶ�", "������", "������", "������", "������"};
		String monthFirstDateStr = getMonthFirstDateStr(date);
		int monthFirstDateInt = Integer.parseInt(monthFirstDateStr);
		String monthLastDateStr = getMonthLastDateStr(date);
		int monthLastDateInt = Integer.parseInt(monthLastDateStr);
		for (int i = monthFirstDateInt; i <= monthLastDateInt; i ++) {
			String currDate = String.valueOf(i);
			// ���������ڼ�
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date(currDate.substring(0, 4) + "/" + currDate.substring(4, 6) + "/" + currDate.substring(6, 8)));
			VirtualDateBeanExt bean = new VirtualDateBeanExt();
			bean.setDate(String.valueOf(i));
			bean.setDay_of_the_week(daysOfTheWeek[calendar.get(Calendar.DAY_OF_WEEK)]);
			monthList.add(bean);
		}
		return monthList;
	}
	
	public Calendar getMonthFirstDate(String date) throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		return calendar;
	}
	
	public String getMonthFirstDateStr(String date) throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		return sdf.format(calendar.getTime());
	}
	
	public Calendar getMonthLastDate(String date) throws Exception {
		String monthLastDate = mySQLDateMapper.getMonthLastDate(date);
		Calendar calendar = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		calendar.setTime(dateFormat.parse(monthLastDate));
		return calendar;
	}
	
	public String getMonthLastDateStr(String date) throws Exception {
		String monthLastDate = mySQLDateMapper.getMonthLastDate(date);
		return monthLastDate.replace("-", "");
	}
	
	public String getBeforeMonthToday() throws Exception {
		return mySQLDateMapper.getBeforeMonthToday();
	}
	
	public String getAfterMonthToday() throws Exception {
		return mySQLDateMapper.getAfterMonthToday();
	}
	
	public String getNextDate(String date) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(sdf.parse(date));
		calendar.add(Calendar.DATE, 1);
		return sdf.format(calendar.getTime());
	}
}
