package com.cttic.erp.service.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cttic.erp.bean.BusiManHourDetailBean;
import com.cttic.erp.dao.interfaces.IBusiManHourDetailDAO;
import com.cttic.erp.service.interfaces.IBusiManHourDetailService;

@Service
public class BusiManHourDetailServiceImpl implements
	IBusiManHourDetailService {

	@Autowired
	private IBusiManHourDetailDAO busiManHourDetailDAOImpl;
	

	@Override
	public List<BusiManHourDetailBean> getBeans(long manHour_id) {
		List<BusiManHourDetailBean> list = busiManHourDetailDAOImpl.getBeans(manHour_id);
		for(BusiManHourDetailBean bean : list){
			String[] arr = bean.getMan_hour().split(",");
			bean.setMan_hour_list(Arrays.asList(arr));
			
			String[] arr2 = bean.getApp_man_hour().split(",");
			bean.setApp_man_hour_list(Arrays.asList(arr2));
		}
		return list;
	}

	@Override
	public void insertBean(BusiManHourDetailBean busiManHourDetailBean) {
		busiManHourDetailDAOImpl.insertBean(busiManHourDetailBean);
	}

	@Override
	public int delete(long manHour_id) {
		return busiManHourDetailDAOImpl.delete(manHour_id);
	}

	@Override
	public void updateBean(BusiManHourDetailBean d) {
		busiManHourDetailDAOImpl.updateBean(d);
	}

}
