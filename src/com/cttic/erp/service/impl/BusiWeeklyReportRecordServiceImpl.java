package com.cttic.erp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cttic.erp.bean.BusiWeeklyReportRecordBean;
import com.cttic.erp.dao.interfaces.IBusiWeeklyReportRecordDAO;
import com.cttic.erp.service.interfaces.IBusiWeeklyReportRecordService;

@Service
public class BusiWeeklyReportRecordServiceImpl implements
		IBusiWeeklyReportRecordService {

	@Autowired
	private IBusiWeeklyReportRecordDAO busiWeeklyReportRecordDAOImpl;
	
	@Override
	public BusiWeeklyReportRecordBean getBean(long employee_id,
			String begin_date, String end_date) {
		return busiWeeklyReportRecordDAOImpl.getBean(employee_id, begin_date, end_date);
	}

	@Override
	public List<BusiWeeklyReportRecordBean> getBeanListByDate(String begin_date, String end_date) {
		return busiWeeklyReportRecordDAOImpl.getBeanListByDate(begin_date, end_date);
	}

	@Override
	public List<BusiWeeklyReportRecordBean> getBeans(long employee_id) {
		return busiWeeklyReportRecordDAOImpl.getBeans(employee_id);
	}
	
	@Override
	public List<BusiWeeklyReportRecordBean> getBeans(long employee_id, String select_month) {
		return busiWeeklyReportRecordDAOImpl.getBeans(employee_id, select_month);
	}

	@Override
	public void insertBean(BusiWeeklyReportRecordBean bean) {
		busiWeeklyReportRecordDAOImpl.insertBean(bean);
	}
	
	@Override
	public void updateBean(BusiWeeklyReportRecordBean bean) {
		busiWeeklyReportRecordDAOImpl.updateBean(bean);
	}


	@Override
	public BusiWeeklyReportRecordBean findWeekRecord(
			BusiWeeklyReportRecordBean search) {
		List<BusiWeeklyReportRecordBean> list = busiWeeklyReportRecordDAOImpl.findWeekRecord(search);
		if(list!=null && list.size() >0){
			return list.get(0);
		}
		return null;
	}

	@Override
	public BusiWeeklyReportRecordBean getBeanById(String week_id) {
		List<BusiWeeklyReportRecordBean> list = busiWeeklyReportRecordDAOImpl.getBeanById(week_id);
		if(list!=null && list.size() >0){
			return list.get(0);
		}
		return null;
	}

}
