package com.cttic.erp.service.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cttic.erp.bean.InfoProjectBean;
import com.cttic.erp.bean.InfoProjectEmployeeBean;
import com.cttic.erp.dao.interfaces.IInfoProjectDAO;
import com.cttic.erp.service.interfaces.IInfoProjectEmployeeService;
import com.cttic.erp.service.interfaces.IInfoProjectService;

@Service
public class InfoProjectServiceImpl implements IInfoProjectService {

	@Autowired
	private IInfoProjectDAO infoProjectDAOImpl;
	
	@Autowired
	private IInfoProjectEmployeeService infoProjectEmployeeService;
	
	@Override
	public List<InfoProjectBean> getAllBeans() {
		return infoProjectDAOImpl.getAllBeans();
	}
	
	@Override
	public List<InfoProjectBean> getBeans() {
		return infoProjectDAOImpl.getBeans();
	}
	
	@Override
	public List<InfoProjectBean> getBeansByManager(long manager_employee_id) {
		return infoProjectDAOImpl.getBeansByManager(manager_employee_id);
	}

	@Override
	public InfoProjectBean getBean(BigDecimal prj_id) {
		return infoProjectDAOImpl.getBean(prj_id);
	}

	@Override
	public void insert(InfoProjectBean bean) {
		infoProjectDAOImpl.insert(bean);
	}
	
	@Override
	public void update(InfoProjectBean bean) {
		infoProjectDAOImpl.update(bean);
	}
	
	@Override
	public void close(BigDecimal prj_id) {
		infoProjectDAOImpl.close(prj_id);
	}
	
	@Override
	public void synSave(String operType, InfoProjectBean infoProjectBean, List<InfoProjectEmployeeBean> infoProjectEmployeeBeanList) {
		if ("ADD".equals(operType)) {
			SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");//设置日期格式
			BigDecimal prjId = BigDecimal.valueOf(Long.parseLong(df.format(new Date())));
			infoProjectBean.setPrj_id(prjId);
			this.insert(infoProjectBean);
			for (InfoProjectEmployeeBean bean : infoProjectEmployeeBeanList) {
				bean.setPrj_id(prjId);
				infoProjectEmployeeService.insert(bean);
			}
		} else if ("EDIT".equals(operType)) {
			this.update(infoProjectBean);
			List<InfoProjectEmployeeBean> beanList = infoProjectEmployeeService.getBeansByPrjId(infoProjectBean.getPrj_id());
			for (InfoProjectEmployeeBean newBean : infoProjectEmployeeBeanList) {
				int addCnt = 0; 
				for (InfoProjectEmployeeBean oldBean : beanList) {
					if (newBean.getEmployee_id() == oldBean.getEmployee_id()) {
						if ("1".equals(oldBean.getState())) {
							oldBean.setState("0");
							infoProjectEmployeeService.update(oldBean);
							break;
						}
					} else {
						addCnt ++;
					}
				}
				if (addCnt == beanList.size()) {
					newBean.setPrj_id(infoProjectBean.getPrj_id());
					infoProjectEmployeeService.insert(newBean);
				}
			}
			for (InfoProjectEmployeeBean oldBean : beanList) {
				int delCnt = 0; 
				for (InfoProjectEmployeeBean newBean : infoProjectEmployeeBeanList) {
					if (newBean.getEmployee_id() == oldBean.getEmployee_id()) {
						break;
					} else {
						delCnt ++;
					}
				}
				if (delCnt == infoProjectEmployeeBeanList.size()) {
					oldBean.setState("1");
					infoProjectEmployeeService.update(oldBean);
				}
			}
		} else if ("CLOSE".equals(operType)) {
			this.close(infoProjectBean.getPrj_id());
			List<InfoProjectEmployeeBean> beanList = infoProjectEmployeeService.getValidBeansByPrjId(infoProjectBean.getPrj_id());
			for (InfoProjectEmployeeBean bean : beanList) {
				bean.setState("1");
				infoProjectEmployeeService.update(bean);
			}
		}
	}
}
