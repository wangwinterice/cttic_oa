package com.cttic.erp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cttic.erp.bean.BusiEmployeeVacationRecordBean;
import com.cttic.erp.dao.interfaces.IBusiEmployeeVacationRecordDAO;
import com.cttic.erp.service.interfaces.IBusiEmployeeVacationRecordService;

@Service
public class BusiEmployeeVacationRecordServiceImpl implements
		IBusiEmployeeVacationRecordService {

	@Autowired
	private IBusiEmployeeVacationRecordDAO busiEmployeeVacationRecordDAOImpl;
	
	@Override
	public List<BusiEmployeeVacationRecordBean> getBeansByEmployeeId(long employee_id) {
		return busiEmployeeVacationRecordDAOImpl.getBeansByEmployeeId(employee_id);
	}

	@Override
	public List<BusiEmployeeVacationRecordBean> getBeansByState(String state) {
		return busiEmployeeVacationRecordDAOImpl.getBeansByState(state);
	}
	
	@Override
	public List<BusiEmployeeVacationRecordBean> getBeans(long employee_id, String reportMonth) {
		return busiEmployeeVacationRecordDAOImpl.getBeans(employee_id, reportMonth);
	}
	
	@Override
	public List<BusiEmployeeVacationRecordBean> getBeansByReportMonth(String reportMonth) {
		return busiEmployeeVacationRecordDAOImpl.getBeansByReportMonth(reportMonth);
	}
	
	@Override
	public BusiEmployeeVacationRecordBean getBean(long employee_id, String beginDate) {
		return busiEmployeeVacationRecordDAOImpl.getBean(employee_id, beginDate);
	}
	
	@Override
	public BusiEmployeeVacationRecordBean getBeansByRecordId(long record_id) {
		return busiEmployeeVacationRecordDAOImpl.getBeansByRecordId(record_id);
	}

	@Override
	public void insert(BusiEmployeeVacationRecordBean bean) {
		busiEmployeeVacationRecordDAOImpl.insert(bean);
	}

	@Override
	public void updateCheck(BusiEmployeeVacationRecordBean bean) {
		busiEmployeeVacationRecordDAOImpl.updateCheck(bean);
	}

	@Override
	public void updateRecord(BusiEmployeeVacationRecordBean bean) {
		busiEmployeeVacationRecordDAOImpl.updateRecord(bean);
	}
	
	@Override
	public void cancel(BusiEmployeeVacationRecordBean bean) {
		busiEmployeeVacationRecordDAOImpl.cancel(bean);
	}
}
