package com.cttic.erp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cttic.erp.bean.BusiExpenseAccountRecordBean;
import com.cttic.erp.bean.ext.BusiExpenseAccountRecordBeanExt;
import com.cttic.erp.dao.interfaces.IBusiExpenseAccountRecordDAO;
import com.cttic.erp.service.interfaces.IBusiExpenseAccountRecordService;

@Service
public class BusiExpenseAccountRecordServiceImpl implements
		IBusiExpenseAccountRecordService {

	@Autowired
	private IBusiExpenseAccountRecordDAO busiExpenseAccountRecordDAOImpl;
	
	@Override
	public List<BusiExpenseAccountRecordBean> getBeansByEmployeeId(
			long employee_id) {
		return busiExpenseAccountRecordDAOImpl.getBeansByEmployeeId(employee_id);
	}
	
	@Override
	public List<BusiExpenseAccountRecordBean> getEffectBeans() {
		return busiExpenseAccountRecordDAOImpl.getEffectBeans();
	}
	
	@Override
	public List<BusiExpenseAccountRecordBeanExt> getStatBeans() {
		return busiExpenseAccountRecordDAOImpl.getStatBeans();
	}

	@Override
	public int insert(BusiExpenseAccountRecordBean bean) {
		return busiExpenseAccountRecordDAOImpl.insert(bean);
	}

	@Override
	public int update(BusiExpenseAccountRecordBean bean) {
		return busiExpenseAccountRecordDAOImpl.update(bean);
	}
	
	@Override
	public int updateState(BusiExpenseAccountRecordBean bean) {
		return busiExpenseAccountRecordDAOImpl.updateState(bean);
	}
}
