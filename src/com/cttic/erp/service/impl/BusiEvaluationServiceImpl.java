package com.cttic.erp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cttic.erp.bean.BusiEvaluationRecordBean;
import com.cttic.erp.bean.BusiEvaluationResultBean;
import com.cttic.erp.bean.ext.BusiEvaluationBeanExt;
import com.cttic.erp.dao.interfaces.IBusiEvaluationDAO;
import com.cttic.erp.dao.interfaces.IBusiEvaluationRecordDAO;
import com.cttic.erp.dao.interfaces.IBusiEvaluationResultDAO;
import com.cttic.erp.service.interfaces.IBusiEvaluationService;

@Service
public class BusiEvaluationServiceImpl implements IBusiEvaluationService {

	@Autowired
	private IBusiEvaluationDAO infoEvaluationServiceDAOImpl;

	@Autowired
	private IBusiEvaluationRecordDAO busiEvaluationRecordDAOImpl;

	@Autowired
	private IBusiEvaluationResultDAO busiEvaluationResultDAOImpl;

	@Override
	@Transactional
	public void saveNewSelfEvaluationResult(
			BusiEvaluationRecordBean busiEvaluationRecordBean,
			List<BusiEvaluationResultBean> busiEvaluationResultBeanlist) {
		busiEvaluationRecordDAOImpl.insertBean(busiEvaluationRecordBean);
		for (BusiEvaluationResultBean busiEvaluationResultBean : busiEvaluationResultBeanlist) {
			busiEvaluationResultDAOImpl.insertBean(busiEvaluationResultBean);
		}
	}

	@Override
	@Transactional
	public void saveCheckEvaluationResult(
			BusiEvaluationRecordBean busiEvaluationRecordBean,
			List<BusiEvaluationResultBean> busiEvaluationResultBeanlist) {
		busiEvaluationRecordDAOImpl.updateBean(busiEvaluationRecordBean);
		busiEvaluationResultDAOImpl
				.deleteCheckEvaluationResult(busiEvaluationRecordBean
						.getRecord_id());
		for (BusiEvaluationResultBean busiEvaluationResultBean : busiEvaluationResultBeanlist) {
			busiEvaluationResultDAOImpl.insertBean(busiEvaluationResultBean);
		}
	}

	@Override
	@Transactional
	public void saveEditSelfEvaluationResult(
			BusiEvaluationRecordBean busiEvaluationRecordBean,
			List<BusiEvaluationResultBean> busiEvaluationResultBeanlist) {
		busiEvaluationResultDAOImpl
				.deleteSelfEvaluationResult(busiEvaluationRecordBean
						.getRecord_id());
		for (BusiEvaluationResultBean busiEvaluationResultBean : busiEvaluationResultBeanlist) {
			busiEvaluationResultDAOImpl.insertBean(busiEvaluationResultBean);
		}
	}

	@Override
	public List<BusiEvaluationBeanExt> getEvaluationStandardList(int template_id) {
		return infoEvaluationServiceDAOImpl
				.getEvaluationStandardList(template_id);
	}

	@Override
	public List<BusiEvaluationBeanExt> getEvaluationKpiList(int template_id) {
		return infoEvaluationServiceDAOImpl.getEvaluationKpiList(template_id);
	}

}
