package com.cttic.erp.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cttic.erp.bean.InfoEmployeeBean;
import com.cttic.erp.bean.InfoProjectEmployeeBean;
import com.cttic.erp.dao.interfaces.IInfoProjectEmployeeDAO;
import com.cttic.erp.service.interfaces.IInfoProjectEmployeeService;

@Service
public class InfoProjectEmployeeServiceImpl implements
		IInfoProjectEmployeeService {

	@Autowired
	private IInfoProjectEmployeeDAO infoProjectEmployeeDAO;
	
	@Override
	public List<InfoProjectEmployeeBean> getBeans() {
		return infoProjectEmployeeDAO.getBeans();
	}
	
	@Override
	public List<InfoProjectEmployeeBean> getBeansByPrjId(BigDecimal prj_id) {
		return infoProjectEmployeeDAO.getBeansByPrjId(prj_id);
	}

	@Override
	public List<InfoProjectEmployeeBean> getValidBeansByPrjId(BigDecimal prj_id) {
		return infoProjectEmployeeDAO.getValidBeansByPrjId(prj_id);
	}

	@Override
	public List<InfoProjectEmployeeBean> getBeansByState(String state) {
		return infoProjectEmployeeDAO.getBeansByState(state);
	}

	@Override
	public void insert(InfoProjectEmployeeBean bean) {
		infoProjectEmployeeDAO.insert(bean);
	}

	@Override
	public void update(InfoProjectEmployeeBean bean) {
		infoProjectEmployeeDAO.update(bean);
	}

	@Override
	public List<InfoEmployeeBean> getNoPrjEmployee(BigDecimal prj_id) {
		return infoProjectEmployeeDAO.getNoPrjEmployee(prj_id);
	}
	
	@Override
	public List<InfoProjectEmployeeBean> getBeansByManagerEmployeeId(long managerEmployeeId) {
		return infoProjectEmployeeDAO.getBeansByManagerEmployeeId(managerEmployeeId);
	}

	@Override
	public List<InfoProjectEmployeeBean> getBeansByManagerEmployeeIdAndPrj(long managerEmployeeId, long prjId) {
		return infoProjectEmployeeDAO.getBeansByManagerEmployeeIdAndPrj(managerEmployeeId, prjId);
	}
	
	@Override
	public List<InfoProjectEmployeeBean> getBeansByPrj(long prjId) {
		return infoProjectEmployeeDAO.getBeansByPrj(prjId);
	}
}
