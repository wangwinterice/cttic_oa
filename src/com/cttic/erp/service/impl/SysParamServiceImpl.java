package com.cttic.erp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cttic.erp.bean.SysParamBean;
import com.cttic.erp.dao.interfaces.ISysParamDAO;
import com.cttic.erp.service.interfaces.ISysParamService;

@Service
public class SysParamServiceImpl implements ISysParamService {

	@Autowired
	private ISysParamDAO sysParamDAOImpl;
	
	@Override
	public List<SysParamBean> getBeans() {
		return sysParamDAOImpl.getBeans();
	}

}
