package com.cttic.erp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cttic.erp.bean.InfoEmployeeBean;
import com.cttic.erp.dao.interfaces.IInfoEmployeeDAO;
import com.cttic.erp.service.interfaces.IInfoEmployeeService;

@Service
public class InfoEmployeeServiceImpl implements IInfoEmployeeService {

	@Autowired
	private IInfoEmployeeDAO infoEmployeeDAOImpl;

	public List<InfoEmployeeBean> getBeanList() {
		return infoEmployeeDAOImpl.getBeanList();
	}

	public List<InfoEmployeeBean> getBeanListByDept(long dept_id) {
		return infoEmployeeDAOImpl.getBeanListByDept(dept_id);
	}

	public InfoEmployeeBean getBean(long employee_no) {
		return infoEmployeeDAOImpl.getBean(employee_no);
	}

	public void updateEmployeePwd(String employee_pwd, long employee_id) {
		infoEmployeeDAOImpl.updateEmployeePwd(employee_pwd, employee_id);
	}

	public void insert(InfoEmployeeBean infoEmployeeBean) {
		infoEmployeeDAOImpl.insert(infoEmployeeBean);
	}

	public void update(InfoEmployeeBean infoEmployeeBean) {
		infoEmployeeDAOImpl.update(infoEmployeeBean);
	}

	public List<InfoEmployeeBean> getBeanListByConditions(String conditions) {
		return infoEmployeeDAOImpl.getBeanListByConditions(conditions);
	}
}
