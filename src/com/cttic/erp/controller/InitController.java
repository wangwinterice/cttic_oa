package com.cttic.erp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.cttic.erp.bean.ext.InfoOperBeanExt;
import com.cttic.erp.common.util.InfoOperUtil;

@Controller
@RequestMapping(value = "/init")
public class InitController {

	@RequestMapping(value = "/login")
	public ModelAndView login(HttpServletRequest request,
			HttpServletResponse response) {
		return new ModelAndView("login");
	}
	
	@RequestMapping(value = "/index")
	public ModelAndView index(HttpServletRequest request,
			HttpServletResponse response) {
		String forwardUrl = "index";
		InfoOperBeanExt infoOperBean = InfoOperUtil.getCurrOperInfo(request);
		if (null == infoOperBean) {
			forwardUrl = "login";
		}
		return new ModelAndView(forwardUrl);
	}
}
