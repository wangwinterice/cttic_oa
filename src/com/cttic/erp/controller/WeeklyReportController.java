package com.cttic.erp.controller;

import java.io.File;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cttic.erp.bean.BusiWeeklyReportRecordBean;
import com.cttic.erp.bean.BusiWeeklyReportResultBean;
import com.cttic.erp.bean.InfoDepartmentBean;
import com.cttic.erp.bean.InfoEmployeeBean;
import com.cttic.erp.bean.InfoProjectBean;
import com.cttic.erp.bean.InfoProjectEmployeeBean;
import com.cttic.erp.bean.SysParamBean;
import com.cttic.erp.bean.WeeklyReportDateBean;
import com.cttic.erp.bean.ext.InfoOperBeanExt;
import com.cttic.erp.common.comparator.ComparatorDepartment;
import com.cttic.erp.common.constants.BusiConstants;
import com.cttic.erp.common.util.CacheUtil;
import com.cttic.erp.common.util.ExcelUtil;
import com.cttic.erp.common.util.ExceptionUtil;
import com.cttic.erp.common.util.InfoOperUtil;
import com.cttic.erp.common.util.StringUtil;
import com.cttic.erp.mapper.MySQLDateMapper;
import com.cttic.erp.service.interfaces.IBusiWeeklyReportRecordService;
import com.cttic.erp.service.interfaces.IBusiWeeklyReportResultService;
import com.cttic.erp.service.interfaces.IBusiWeeklyReportService;
import com.cttic.erp.service.interfaces.IInfoDepartmentService;
import com.cttic.erp.service.interfaces.IInfoEmployeeService;
import com.cttic.erp.service.interfaces.IInfoProjectEmployeeService;
import com.cttic.erp.service.interfaces.IInfoProjectService;
import com.cttic.erp.service.interfaces.IMySQLDateService;

@Controller
@RequestMapping(value = "/weeklyReport")
public class WeeklyReportController {

	@Resource
	private IMySQLDateService mySQLDateService;
	
	@Autowired
	private MySQLDateMapper mySQLDateMapper;

	@Resource
	private IBusiWeeklyReportRecordService busiWeeklyReportRecordService;

	@Resource
	private IBusiWeeklyReportResultService busiWeeklyReportResultService;

	@Resource
	private IBusiWeeklyReportService busiWeeklyReportService;

	@Resource
	private IInfoEmployeeService infoEmployeeService;
	
	@Resource
	private IInfoDepartmentService infoDepartmentService;
	
	@Resource
	private IInfoProjectService infoProjectService;
	
	@Resource
	private IInfoProjectEmployeeService infoProjectEmployeeService;
	
	private ComparatorDepartment comparator;
	
	@RequestMapping(value = "/save")
	public void save(HttpServletRequest request, HttpServletResponse response,
			ModelMap modelMap) throws Exception {
		JSONObject rtnJson = new JSONObject();
		// 1、获取页面提交信息
		String operType = request.getParameter("OPER_TYPE");
		String beginDate = request.getParameter("BEGIN_DATE");
		String endDate = request.getParameter("END_DATE");
		String inputParam = request.getParameter("INPUT_PARAM");
		JSONObject inputJson = JSONObject.fromString(inputParam);
		String taskBeginDate = inputJson.getString("TASK_BEGIN_DATE");
		String taskEndDate = inputJson.getString("TASK_END_DATE");
		String planBeginDate = inputJson.getString("PLAN_BEGIN_DATE");
		String planEndDate = inputJson.getString("PLAN_END_DATE");
		JSONArray taskJsonArray = inputJson.getJSONArray("TASK");
		JSONArray planJsonArray = inputJson.getJSONArray("PLAN");
		// 2、获取当前操作员信息
		InfoOperBeanExt infoOperBean = InfoOperUtil.getCurrOperInfo(request);
		if (null == infoOperBean) {
			throw new Exception("没有工号登陆信息");
		}
		// 3、获取人员信息
		InfoEmployeeBean infoEmployeeBean = infoOperBean.getInfoEmployeeBean();
		try {
			// 4、获取周报记录
			BusiWeeklyReportRecordBean busiWeeklyReportRecordBean = busiWeeklyReportRecordService
					.getBean(infoEmployeeBean.getEmployee_id(), beginDate, endDate);
			String recordId = "";
			if (null == busiWeeklyReportRecordBean) {
				recordId = "" + infoEmployeeBean.getEmployee_id() + endDate;
				busiWeeklyReportRecordBean = new BusiWeeklyReportRecordBean();
				busiWeeklyReportRecordBean.setRecord_id(recordId);
				busiWeeklyReportRecordBean.setEmployee_id(infoEmployeeBean
						.getEmployee_id());
				busiWeeklyReportRecordBean.setBegin_date(beginDate);
				busiWeeklyReportRecordBean.setEnd_date(endDate);
				busiWeeklyReportRecordBean.setTask_begin_date(taskBeginDate);
				busiWeeklyReportRecordBean.setTask_end_date(taskEndDate);
				busiWeeklyReportRecordBean.setPlan_begin_date(planBeginDate);
				busiWeeklyReportRecordBean.setPlan_end_date(planEndDate);
				busiWeeklyReportRecordBean.setApproval_flag("0");
			} else {
				recordId = busiWeeklyReportRecordBean.getRecord_id();
				busiWeeklyReportRecordBean.setTask_begin_date(taskBeginDate);
				busiWeeklyReportRecordBean.setTask_end_date(taskEndDate);
				busiWeeklyReportRecordBean.setPlan_begin_date(planBeginDate);
				busiWeeklyReportRecordBean.setPlan_end_date(planEndDate);
			}
			List<BusiWeeklyReportResultBean> busiWeeklyReportResultBeanList = new ArrayList<BusiWeeklyReportResultBean>();
			BusiWeeklyReportResultBean busiWeeklyReportResultBean = new BusiWeeklyReportResultBean();
			for (int count = 0; count < taskJsonArray.length(); count++) {
				JSONObject json = (JSONObject) taskJsonArray.get(count);
				if (!StringUtil.isBlank(json.getString("PRJ_ID"))) {
					busiWeeklyReportResultBean = new BusiWeeklyReportResultBean();
					busiWeeklyReportResultBean.setRecord_id(recordId);
					busiWeeklyReportResultBean.setResult_id(json
							.getInt("WBS_ID"));
					busiWeeklyReportResultBean.setResult_type("TASK");
					busiWeeklyReportResultBean.setResult_code("PRJ_ID");
					busiWeeklyReportResultBean.setResult_value(json.getString("PRJ_ID"));
					busiWeeklyReportResultBeanList
							.add(busiWeeklyReportResultBean);
				}
				if (!StringUtil.isBlank(json.getString("TASK_DESC"))) {
					busiWeeklyReportResultBean = new BusiWeeklyReportResultBean();
					busiWeeklyReportResultBean.setRecord_id(recordId);
					busiWeeklyReportResultBean.setResult_id(json
							.getInt("WBS_ID"));
					busiWeeklyReportResultBean.setResult_type("TASK");
					busiWeeklyReportResultBean.setResult_code("TASK_DESC");
					busiWeeklyReportResultBean.setResult_value(json.getString(
							"TASK_DESC").replaceAll("\n", "<br>"));
					busiWeeklyReportResultBeanList
							.add(busiWeeklyReportResultBean);
				}
				for (int i = 1; i <=7; i ++) {
					if (!"0".equals(json.getString("TASK_WORK_TIME_" + i))) {
						busiWeeklyReportResultBean = new BusiWeeklyReportResultBean();
						busiWeeklyReportResultBean.setRecord_id(recordId);
						busiWeeklyReportResultBean.setResult_id(json
								.getInt("WBS_ID"));
						busiWeeklyReportResultBean.setResult_type("TASK");
						busiWeeklyReportResultBean.setResult_code("WORK_TIME_" + i);
						busiWeeklyReportResultBean.setResult_value(json.getString("TASK_WORK_TIME_" + i));
						busiWeeklyReportResultBeanList.add(busiWeeklyReportResultBean);
					}
				}
			}
			for (int count = 0; count < planJsonArray.length(); count++) {
				JSONObject json = (JSONObject) planJsonArray.get(count);
				if (!StringUtil.isBlank(json.getString("PRJ_ID"))) {
					busiWeeklyReportResultBean = new BusiWeeklyReportResultBean();
					busiWeeklyReportResultBean.setRecord_id(recordId);
					busiWeeklyReportResultBean.setResult_id(json
							.getInt("SEQ_NO"));
					busiWeeklyReportResultBean.setResult_type("PLAN");
					busiWeeklyReportResultBean.setResult_code("PRJ_ID");
					busiWeeklyReportResultBean.setResult_value(json.getString("PRJ_ID"));
					busiWeeklyReportResultBeanList
							.add(busiWeeklyReportResultBean);
				}
				if (!StringUtil.isBlank(json.getString("PLAN_DESC"))) {
					busiWeeklyReportResultBean = new BusiWeeklyReportResultBean();
					busiWeeklyReportResultBean.setRecord_id(recordId);
					busiWeeklyReportResultBean.setResult_id(json
							.getInt("SEQ_NO"));
					busiWeeklyReportResultBean.setResult_type("PLAN");
					busiWeeklyReportResultBean.setResult_code("PLAN_DESC");
					busiWeeklyReportResultBean.setResult_value(json.getString(
							"PLAN_DESC").replaceAll("\n", "<br>"));
					busiWeeklyReportResultBeanList
							.add(busiWeeklyReportResultBean);
				}
			}
			busiWeeklyReportService.save(operType,
					busiWeeklyReportRecordBean, busiWeeklyReportResultBeanList);
			rtnJson.put(BusiConstants.AjaxReturn.STATUS_CODE,
					BusiConstants.AjaxStatus.STATUS_SUCCESS);
			rtnJson.put(BusiConstants.AjaxReturn.STATUS_INFO, "保存成功");
		} catch (Exception ex) {
			ex.printStackTrace();
			String retMsg = ex.getCause() == null ? ex.getMessage() : ex
					.getCause().getMessage();
			rtnJson = ExceptionUtil.toJSON(retMsg, ex, request);
		} finally {
			response.setContentType("text/html;charset=utf-8");
			PrintWriter printWriter = response.getWriter();
			printWriter.write(rtnJson.toString());
			printWriter.flush();
			printWriter.close();
		}
	}

	@RequestMapping(value = "/batchView")
	public ModelAndView batchView(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) throws Exception {
		String employeeId = request.getParameter("EMPLOYEE_ID");
		if (!StringUtil.isBlank(employeeId)) {
			List<BusiWeeklyReportRecordBean> busiWeeklyReportRecordBeanList = busiWeeklyReportRecordService
					.getBeans(Long.parseLong(employeeId));
			modelMap.put("WEEKLY_REPORT_RECORD", busiWeeklyReportRecordBeanList);
			modelMap.put("SELECT_EMPLOYEE",
					infoEmployeeService.getBean(Long.parseLong(employeeId)));
		}
		//modelMap.put("WEEKLYREPORT_DATE_LIST", getWeeklyReportDateList());
		List<InfoEmployeeBean> infoEmployeeBeanList = infoEmployeeService
				.getBeanList();
		modelMap.put("EMPLOYEE_LIST", infoEmployeeBeanList);
		return new ModelAndView("weeklyReport/batchView", modelMap);
	}
	
	@RequestMapping(value = "/generatorExcel")
	public void generatorExcel(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		JSONObject json = new JSONObject();
		try {
			String employeeId = request.getParameter("EMPLOYEE_ID");
			// 查询日报记录
			List<BusiWeeklyReportRecordBean> busiWeeklyReportRecordBeanList = busiWeeklyReportRecordService
					.getBeans(Long.parseLong(employeeId));
			// 查询日报明细
			List<BusiWeeklyReportResultBean> busiWeeklyReportResultBeanList = busiWeeklyReportResultService.getBeansByEmployeeId(Long.parseLong(employeeId));
			if (null == busiWeeklyReportRecordBeanList || busiWeeklyReportRecordBeanList.size() == 0) {
				throw new Exception("该员工没有填写任何周报");
			}
			// 获取已填写周报最后结束日期
			String lastEndDate = busiWeeklyReportRecordBeanList.get(0).getEnd_date();
			// 将日报明细转为map，便于遍历查找
			Map<String, List<BusiWeeklyReportResultBean>> resultMap = new HashMap<String, List<BusiWeeklyReportResultBean>>();
			for (BusiWeeklyReportResultBean resultBean : busiWeeklyReportResultBeanList) {
				String recordId = resultBean.getRecord_id();
				if (resultMap.containsKey(recordId)) {
					resultMap.get(recordId).add(resultBean);
				} else {
					List<BusiWeeklyReportResultBean> resultBeanLists = new ArrayList<BusiWeeklyReportResultBean>();
					resultBeanLists.add(resultBean);
					resultMap.put(recordId, resultBeanLists);
				}
			}
			// 目标文件，在模板中填充数据并生成 
			String outFileName = new String(employeeId + "_" + lastEndDate.substring(0, 4) + "-" + lastEndDate.substring(4, 6) + "-" + lastEndDate.substring(6, 8));  
			File targetFile = new File(outFileName);

			if(targetFile.exists())
				targetFile.delete();
			// 目标文件先引入template中的内容，并将以targetFile导出 
			WritableWorkbook target = Workbook.createWorkbook(targetFile);
			WritableSheet ws;
			Label label;
			// 循环周报记录
			for (int recordCount = 0; recordCount < busiWeeklyReportRecordBeanList.size(); recordCount ++) {
				BusiWeeklyReportRecordBean recordBean = busiWeeklyReportRecordBeanList.get(recordCount);
				String recordId = recordBean.getRecord_id();
				String beginDate = recordBean.getBegin_date();
				String endDate = recordBean.getEnd_date();
				//创建一个工作簿
				ws = target.createSheet("工作周报（" + beginDate.substring(0, 4) + "." + beginDate.substring(4, 6) + "." + beginDate.substring(6, 8) + "~" + endDate.substring(0, 4) + "." + endDate.substring(4, 6) + "." + endDate.substring(6, 8) + "）", recordCount);
				// 设置列宽
				ws.setColumnView(0,10);
				ws.setColumnView(1,40);
				ws.setColumnView(2,60);
				ws.setColumnView(3,10);
				ws.setColumnView(4,15);
				ws.setColumnView(5,15);
				ws.setColumnView(6,18);
				// 写标题
				ws.mergeCells(0, 0, 6, 0);
				WritableFont wfc = new WritableFont(WritableFont.ARIAL, 14, WritableFont.NO_BOLD, false, UnderlineStyle.SINGLE, Colour.BLACK);
				WritableCellFormat wcfFormat = new WritableCellFormat(wfc);
				wcfFormat.setAlignment(Alignment.CENTRE); //设置对齐方式
				wcfFormat.setVerticalAlignment(VerticalAlignment.CENTRE);//设置对齐方式
				label = new Label(0, 0, "本周工作总结", wcfFormat);
				ws.setRowView(0, 700);
				ws.addCell(label);
				// 写任务头
				wfc = new WritableFont(WritableFont.ARIAL, 11, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
				wcfFormat = new WritableCellFormat(wfc);
				wcfFormat.setAlignment(Alignment.CENTRE);
				wcfFormat.setVerticalAlignment(VerticalAlignment.CENTRE);//设置对齐方式
				wcfFormat.setBackground(Colour.SKY_BLUE); //背静色
				wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
				label = new Label(0, 1, "WBS ID", wcfFormat);
				ws.addCell(label);
				label = new Label(1, 1, "参与项目", wcfFormat);
				ws.addCell(label);
				label = new Label(2, 1, "任务描述", wcfFormat);
				ws.addCell(label);
				label = new Label(3, 1, "状态", wcfFormat);
				ws.addCell(label);
				label = new Label(4, 1, "开始时间", wcfFormat);
				ws.addCell(label);
				label = new Label(5, 1, "完成时间", wcfFormat);
				ws.addCell(label);
				label = new Label(6, 1, "备注", wcfFormat);
				ws.addCell(label);
				ws.setRowView(1, 700);
				//定义excel的文本单元格
				if (resultMap.containsKey(recordId)) {
					List<BusiWeeklyReportResultBean> resultBeanLists = resultMap.get(recordId);
					// 循环任务
					int taskCount = 0;
					for (int resultCount = 0; resultCount < resultBeanLists.size(); resultCount ++) {
						BusiWeeklyReportResultBean resultBean = resultBeanLists.get(resultCount);
						if ("TASK".equals(resultBean.getResult_type())) {
							int rowId = resultBean.getResult_id() + 1;
							ws.setRowView(rowId, 500);
							wcfFormat = new WritableCellFormat();
							wcfFormat.setAlignment(Alignment.CENTRE);
							wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
							label = new Label(0, rowId, StringUtil.toString(resultBean.getResult_id()), wcfFormat);
							ws.addCell(label);
							if ("PRJ_ID".equals(resultBean.getResult_code())) {
								wcfFormat = new WritableCellFormat();
								wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
								label = new Label(1, rowId, getPrjName(resultBean.getResult_value()), wcfFormat);
								ws.addCell(label);
								taskCount ++;
							}
							if ("TASK_DESC".equals(resultBean.getResult_code())) {
								wcfFormat = new WritableCellFormat();
								wcfFormat.setWrap(true);//可换行的label样式
								wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
								label = new Label(2, rowId, resultBean.getResult_value().replaceAll("<br>", "\n"), wcfFormat);
								ws.addCell(label);
							}
							if ("STATE".equals(resultBean.getResult_code())) {
								wcfFormat = new WritableCellFormat();
								wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
								label = new Label(3, rowId, CacheUtil.getSysParamBeanValueDesc("WEEKLY_REPORT", "STATE", resultBean.getResult_value()), wcfFormat);
								ws.addCell(label);
							}
							if ("BEGIN_TIME".equals(resultBean.getResult_code())) {
								wcfFormat = new WritableCellFormat();
								wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
								label = new Label(4, rowId, resultBean.getResult_value(), wcfFormat);
								ws.addCell(label);
							}
							if ("END_TIME".equals(resultBean.getResult_code())) {
								wcfFormat = new WritableCellFormat();
								wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
								label = new Label(5, rowId, resultBean.getResult_value(), wcfFormat);
								ws.addCell(label);
							}
							if ("OUTPUT".equals(resultBean.getResult_code())) {
								wcfFormat = new WritableCellFormat();
								wcfFormat.setWrap(true);//可换行的label样式
								wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
								label = new Label(6, rowId, resultBean.getResult_value().replaceAll("<br>", "\n"), wcfFormat);
								ws.addCell(label);
							}
						}
					}
					taskCount ++;
					taskCount ++;
					taskCount ++;
					// 写标题
					ws.mergeCells(0, taskCount, 6, taskCount);
					wfc = new WritableFont(WritableFont.ARIAL, 14, WritableFont.NO_BOLD, false, UnderlineStyle.SINGLE, Colour.BLACK);
					wcfFormat = new WritableCellFormat(wfc);
					wcfFormat.setAlignment(Alignment.CENTRE);
					wcfFormat.setVerticalAlignment(VerticalAlignment.CENTRE);//设置对齐方式
					label = new Label(0, taskCount, "下周工作计划", wcfFormat);
					ws.setRowView(taskCount, 700);
					ws.addCell(label);
					taskCount ++;
					// 写计划头
					wfc = new WritableFont(WritableFont.ARIAL, 11, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
					wcfFormat = new WritableCellFormat(wfc);
					wcfFormat.setAlignment(Alignment.CENTRE);
					wcfFormat.setVerticalAlignment(VerticalAlignment.CENTRE);//设置对齐方式
					wcfFormat.setBackground(Colour.SKY_BLUE); //背静色
					wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
					ws.setRowView(taskCount, 700);
					label = new Label(0, taskCount, "序号", wcfFormat);
					ws.addCell(label);
					label = new Label(1, taskCount, "参与项目", wcfFormat);
					ws.addCell(label);
					label = new Label(2, taskCount, "计划描述", wcfFormat);
					ws.addCell(label);
					label = new Label(3, taskCount, "依赖", wcfFormat);
					ws.addCell(label);
					label = new Label(4, taskCount, "计划开始时间", wcfFormat);
					ws.addCell(label);
					label = new Label(5, taskCount, "计划完成时间", wcfFormat);
					ws.addCell(label);
					label = new Label(6, taskCount, "备注", wcfFormat);
					ws.addCell(label);
					
					// 循环计划
					for (int resultCount = 0; resultCount < resultBeanLists.size(); resultCount ++) {
						BusiWeeklyReportResultBean resultBean = resultBeanLists.get(resultCount);
						if ("PLAN".equals(resultBean.getResult_type())) {
							int rowId = resultBean.getResult_id();
							ws.setRowView(taskCount + rowId, 500);
							wcfFormat = new WritableCellFormat();
							wcfFormat.setAlignment(Alignment.CENTRE);
							wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
							label = new Label(0, taskCount + rowId, StringUtil.toString(resultBean.getResult_id()), wcfFormat);
							ws.addCell(label);
							if ("PRJ_ID".equals(resultBean.getResult_code())) {
								wcfFormat = new WritableCellFormat();
								wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
								label = new Label(1, taskCount + rowId, getPrjName(resultBean.getResult_value()), wcfFormat);
								ws.addCell(label);
							}
							if ("PLAN_DESC".equals(resultBean.getResult_code())) {
								wcfFormat = new WritableCellFormat();
								wcfFormat.setWrap(true);//可换行的label样式
								wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
								label = new Label(2, taskCount + rowId, resultBean.getResult_value().replaceAll("<br>", "\n"), wcfFormat);
								ws.addCell(label);
							}
							if ("DEPEND_ON".equals(resultBean.getResult_code())) {
								wcfFormat = new WritableCellFormat();
								wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
								label = new Label(3, taskCount + rowId, resultBean.getResult_value(), wcfFormat);
								ws.addCell(label);
							}
							if ("PLAN_BEGIN_TIME".equals(resultBean.getResult_code())) {
								wcfFormat = new WritableCellFormat();
								wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
								label = new Label(4, taskCount + rowId, resultBean.getResult_value(), wcfFormat);
								ws.addCell(label);
							}
							if ("PLAN_END_TIME".equals(resultBean.getResult_code())) {
								wcfFormat = new WritableCellFormat();
								wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
								label = new Label(5, taskCount + rowId, resultBean.getResult_value(), wcfFormat);
								ws.addCell(label);
							}
							if ("OUTPUT".equals(resultBean.getResult_code())) {
								wcfFormat = new WritableCellFormat();
								wcfFormat.setWrap(true);//可换行的label样式
								wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
								label = new Label(6, taskCount + rowId, resultBean.getResult_value().replaceAll("<br>", "\n"), wcfFormat);
								ws.addCell(label);
							}
						}
					}
				}
			}
			//关闭文件 
			target.write();
			target.close();
			json.put(BusiConstants.AjaxReturn.STATUS_CODE,
					BusiConstants.AjaxStatus.STATUS_SUCCESS);
			json.put(BusiConstants.AjaxReturn.STATUS_INFO, "Excel生成成功");
			json.put(BusiConstants.AjaxReturn.RETURN_INFO, outFileName);
		} catch (Exception e) {
			String retMsg = e.getCause() == null ? e.getMessage() : e
					.getCause().getMessage();
			retMsg = StringUtil.isBlank(retMsg) ? "Excel生成失败" : retMsg;
			json = ExceptionUtil.toJSON(retMsg, e, request);
		} finally {
			response.setContentType("text/html;charset=utf-8");
			PrintWriter printWriter = response.getWriter();
			printWriter.write(json.toString());
			printWriter.flush();
			printWriter.close();
		}
	}

	@RequestMapping(value = "/generatorOneExcel")
	public void generatorOneExcel(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		JSONObject json = new JSONObject();
		try {
			String employeeId = request.getParameter("EMPLOYEE_ID");
			String beginDate = request.getParameter("BEGIN_DATE");
			String endDate = request.getParameter("END_DATE");
			// 查询日报记录
			BusiWeeklyReportRecordBean busiWeeklyReportRecordBean = busiWeeklyReportRecordService
					.getBean(Long.parseLong(employeeId), beginDate, endDate);
			// 查询日报明细
			List<BusiWeeklyReportResultBean> busiWeeklyReportResultBeanList = busiWeeklyReportResultService.getBeans(busiWeeklyReportRecordBean.getRecord_id());
			// 获取已填写周报最后结束日期
			String lastEndDate = busiWeeklyReportRecordBean.getEnd_date();
			// 将日报明细转为map，便于遍历查找
			Map<String, List<BusiWeeklyReportResultBean>> resultMap = new HashMap<String, List<BusiWeeklyReportResultBean>>();
			for (BusiWeeklyReportResultBean resultBean : busiWeeklyReportResultBeanList) {
				String recordId = resultBean.getRecord_id();
				if (resultMap.containsKey(recordId)) {
					resultMap.get(recordId).add(resultBean);
				} else {
					List<BusiWeeklyReportResultBean> resultBeanLists = new ArrayList<BusiWeeklyReportResultBean>();
					resultBeanLists.add(resultBean);
					resultMap.put(recordId, resultBeanLists);
				}
			}
			// 目标文件，在模板中填充数据并生成 
			String outFileName = new String(employeeId + "_" + lastEndDate.substring(0, 4) + "-" + lastEndDate.substring(4, 6) + "-" + lastEndDate.substring(6, 8));  
			File targetFile = new File(outFileName);
			if(targetFile.exists())
				targetFile.delete();
			// 目标文件先引入template中的内容，并将以targetFile导出 
			WritableWorkbook target = Workbook.createWorkbook(targetFile);
			WritableSheet ws;
			Label label;
			// 循环周报记录
			BusiWeeklyReportRecordBean recordBean = busiWeeklyReportRecordBean;
			String recordId = recordBean.getRecord_id();
			beginDate = recordBean.getBegin_date();
			endDate = recordBean.getEnd_date();
			//创建一个工作簿
			ws = target.createSheet("工作周报（" + beginDate.substring(0, 4) + "." + beginDate.substring(4, 6) + "." + beginDate.substring(6, 8) + "~" + endDate.substring(0, 4) + "." + endDate.substring(4, 6) + "." + endDate.substring(6, 8) + "）", 0);
			// 设置列宽
			ws.setColumnView(0,10);
			ws.setColumnView(1,40);
			ws.setColumnView(2,60);
			ws.setColumnView(3,10);
			ws.setColumnView(4,15);
			ws.setColumnView(5,15);
			ws.setColumnView(6,18);
			// 写标题
			ws.mergeCells(0, 0, 6, 0);
			WritableFont wfc = new WritableFont(WritableFont.ARIAL, 14, WritableFont.NO_BOLD, false, UnderlineStyle.SINGLE, Colour.BLACK);
			WritableCellFormat wcfFormat = new WritableCellFormat(wfc);
			wcfFormat.setAlignment(Alignment.CENTRE); //设置对齐方式
			wcfFormat.setVerticalAlignment(VerticalAlignment.CENTRE);//设置对齐方式
			label = new Label(0, 0, "本周工作总结", wcfFormat);
			ws.setRowView(0, 700);
			ws.addCell(label);
			// 写任务头
			wfc = new WritableFont(WritableFont.ARIAL, 11, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
			wcfFormat = new WritableCellFormat(wfc);
			wcfFormat.setAlignment(Alignment.CENTRE);
			wcfFormat.setVerticalAlignment(VerticalAlignment.CENTRE);//设置对齐方式
			wcfFormat.setBackground(Colour.SKY_BLUE); //背静色
			wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
			label = new Label(0, 1, "WBS ID", wcfFormat);
			ws.addCell(label);
			label = new Label(1, 1, "参与项目", wcfFormat);
			ws.addCell(label);
			label = new Label(2, 1, "任务描述", wcfFormat);
			ws.addCell(label);
			label = new Label(3, 1, "状态", wcfFormat);
			ws.addCell(label);
			label = new Label(4, 1, "开始时间", wcfFormat);
			ws.addCell(label);
			label = new Label(5, 1, "完成时间", wcfFormat);
			ws.addCell(label);
			label = new Label(6, 1, "备注", wcfFormat);
			ws.addCell(label);
			ws.setRowView(1, 700);
			//定义excel的文本单元格
			if (resultMap.containsKey(recordId)) {
				List<BusiWeeklyReportResultBean> resultBeanLists = resultMap.get(recordId);
				// 循环任务
				int taskCount = 0;
				for (int resultCount = 0; resultCount < resultBeanLists.size(); resultCount ++) {
					BusiWeeklyReportResultBean resultBean = resultBeanLists.get(resultCount);
					if ("TASK".equals(resultBean.getResult_type())) {
						int rowId = resultBean.getResult_id() + 1;
						ws.setRowView(rowId, 500);
						wcfFormat = new WritableCellFormat();
						wcfFormat.setAlignment(Alignment.CENTRE);
						wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
						label = new Label(0, rowId, StringUtil.toString(resultBean.getResult_id()), wcfFormat);
						ws.addCell(label);
						if ("PRJ_ID".equals(resultBean.getResult_code())) {
							wcfFormat = new WritableCellFormat();
							wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
							label = new Label(1, rowId, getPrjName(resultBean.getResult_value()), wcfFormat);
							ws.addCell(label);
							taskCount ++;
						}
						if ("TASK_DESC".equals(resultBean.getResult_code())) {
							wcfFormat = new WritableCellFormat();
							wcfFormat.setWrap(true);//可换行的label样式
							wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
							label = new Label(2, rowId, resultBean.getResult_value().replaceAll("<br>", "\n"), wcfFormat);
							ws.addCell(label);
						}
						if ("STATE".equals(resultBean.getResult_code())) {
							wcfFormat = new WritableCellFormat();
							wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
							label = new Label(3, rowId, CacheUtil.getSysParamBeanValueDesc("WEEKLY_REPORT", "STATE", resultBean.getResult_value()), wcfFormat);
							ws.addCell(label);
						}
						if ("BEGIN_TIME".equals(resultBean.getResult_code())) {
							wcfFormat = new WritableCellFormat();
							wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
							label = new Label(4, rowId, resultBean.getResult_value(), wcfFormat);
							ws.addCell(label);
						}
						if ("END_TIME".equals(resultBean.getResult_code())) {
							wcfFormat = new WritableCellFormat();
							wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
							label = new Label(5, rowId, resultBean.getResult_value(), wcfFormat);
							ws.addCell(label);
						}
						if ("OUTPUT".equals(resultBean.getResult_code())) {
							wcfFormat = new WritableCellFormat();
							wcfFormat.setWrap(true);//可换行的label样式
							wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
							label = new Label(6, rowId, resultBean.getResult_value().replaceAll("<br>", "\n"), wcfFormat);
							ws.addCell(label);
						}
					}
				}
				taskCount ++;
				taskCount ++;
				taskCount ++;
				// 写标题
				ws.mergeCells(0, taskCount, 6, taskCount);
				wfc = new WritableFont(WritableFont.ARIAL, 14, WritableFont.NO_BOLD, false, UnderlineStyle.SINGLE, Colour.BLACK);
				wcfFormat = new WritableCellFormat(wfc);
				wcfFormat.setAlignment(Alignment.CENTRE);
				wcfFormat.setVerticalAlignment(VerticalAlignment.CENTRE);//设置对齐方式
				label = new Label(0, taskCount, "下周工作计划", wcfFormat);
				ws.setRowView(taskCount, 700);
				ws.addCell(label);
				taskCount ++;
				// 写计划头
				wfc = new WritableFont(WritableFont.ARIAL, 11, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
				wcfFormat = new WritableCellFormat(wfc);
				wcfFormat.setAlignment(Alignment.CENTRE);
				wcfFormat.setVerticalAlignment(VerticalAlignment.CENTRE);//设置对齐方式
				wcfFormat.setBackground(Colour.SKY_BLUE); //背静色
				wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
				ws.setRowView(taskCount, 700);
				label = new Label(0, taskCount, "序号", wcfFormat);
				ws.addCell(label);
				label = new Label(1, taskCount, "参与项目", wcfFormat);
				ws.addCell(label);
				label = new Label(2, taskCount, "计划描述", wcfFormat);
				ws.addCell(label);
				label = new Label(3, taskCount, "依赖", wcfFormat);
				ws.addCell(label);
				label = new Label(4, taskCount, "计划开始时间", wcfFormat);
				ws.addCell(label);
				label = new Label(5, taskCount, "计划完成时间", wcfFormat);
				ws.addCell(label);
				label = new Label(6, taskCount, "备注", wcfFormat);
				ws.addCell(label);
				
				// 循环计划
				for (int resultCount = 0; resultCount < resultBeanLists.size(); resultCount ++) {
					BusiWeeklyReportResultBean resultBean = resultBeanLists.get(resultCount);
					if ("PLAN".equals(resultBean.getResult_type())) {
						int rowId = resultBean.getResult_id();
						ws.setRowView(taskCount + rowId, 500);
						wcfFormat = new WritableCellFormat();
						wcfFormat.setAlignment(Alignment.CENTRE);
						wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
						label = new Label(0, taskCount + rowId, StringUtil.toString(resultBean.getResult_id()), wcfFormat);
						ws.addCell(label);
						if ("PRJ_ID".equals(resultBean.getResult_code())) {
							wcfFormat = new WritableCellFormat();
							wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
							label = new Label(1, taskCount + rowId, getPrjName(resultBean.getResult_value()), wcfFormat);
							ws.addCell(label);
						}
						if ("PLAN_DESC".equals(resultBean.getResult_code())) {
							wcfFormat = new WritableCellFormat();
							wcfFormat.setWrap(true);//可换行的label样式
							wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
							label = new Label(2, taskCount + rowId, resultBean.getResult_value().replaceAll("<br>", "\n"), wcfFormat);
							ws.addCell(label);
						}
						if ("DEPEND_ON".equals(resultBean.getResult_code())) {
							wcfFormat = new WritableCellFormat();
							wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
							label = new Label(3, taskCount + rowId, resultBean.getResult_value(), wcfFormat);
							ws.addCell(label);
						}
						if ("PLAN_BEGIN_TIME".equals(resultBean.getResult_code())) {
							wcfFormat = new WritableCellFormat();
							wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
							label = new Label(4, taskCount + rowId, resultBean.getResult_value(), wcfFormat);
							ws.addCell(label);
						}
						if ("PLAN_END_TIME".equals(resultBean.getResult_code())) {
							wcfFormat = new WritableCellFormat();
							wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
							label = new Label(5, taskCount + rowId, resultBean.getResult_value(), wcfFormat);
							ws.addCell(label);
						}
						if ("OUTPUT".equals(resultBean.getResult_code())) {
							wcfFormat = new WritableCellFormat();
							wcfFormat.setWrap(true);//可换行的label样式
							wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
							label = new Label(6, taskCount + rowId, resultBean.getResult_value().replaceAll("<br>", "\n"), wcfFormat);
							ws.addCell(label);
						}
					}
				}
			}
			//关闭文件 
			target.write();
			target.close();
			json.put(BusiConstants.AjaxReturn.STATUS_CODE,
					BusiConstants.AjaxStatus.STATUS_SUCCESS);
			json.put(BusiConstants.AjaxReturn.STATUS_INFO, "Excel生成成功");
			json.put(BusiConstants.AjaxReturn.RETURN_INFO, outFileName);
		} catch (Exception e) {
			String retMsg = e.getCause() == null ? e.getMessage() : e
					.getCause().getMessage();
			retMsg = StringUtil.isBlank(retMsg) ? "Excel生成失败" : retMsg;
			json = ExceptionUtil.toJSON(retMsg, e, request);
		} finally {
			response.setContentType("text/html;charset=utf-8");
			PrintWriter printWriter = response.getWriter();
			printWriter.write(json.toString());
			printWriter.flush();
			printWriter.close();
		}
	}
	
	@RequestMapping(value = "/batchExport")
	public ModelAndView batchExport(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) throws Exception {
		//modelMap.put("WEEKLYREPORT_DATE_LIST", getWeeklyReportDateList());
		return new ModelAndView("weeklyReport/batchExport", modelMap);
	}
	
	@RequestMapping(value = "/batchGeneratorExcel")
	public void batchGeneratorExcel(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		JSONObject json = new JSONObject();
		try {
			String beginDate = request.getParameter("BEGIN_DATE");
			String endDate = request.getParameter("END_DATE");
			List<InfoEmployeeBean> infoEmployeeBeanList = infoEmployeeService
					.getBeanList();
			String outFileName = new String(endDate.substring(0, 4) + "-" + endDate.substring(4, 6) + "-" + endDate.substring(6, 8));
			// 目标文件，在模板中填充数据并生成 
			File targetFile = new File(outFileName);

			if(targetFile.exists())
				targetFile.delete();
			// 目标文件先引入template中的内容，并将以targetFile导出 
			WritableWorkbook target = Workbook.createWorkbook(targetFile);
			WritableSheet ws;
			Label label;
//			for (InfoEmployeeBean bean : infoEmployeeBeanList) {
//				if (!"4".equals(bean.getDept_id()) && !"5".equals(bean.getDept_id())) {
//					bean.setDept_id("999999999");
//				}
//			}
			setComparator(new ComparatorDepartment());
			Collections.sort(infoEmployeeBeanList, getComparator());
			for (int employeeCount = 0; employeeCount < infoEmployeeBeanList.size(); employeeCount ++) {
				InfoEmployeeBean infoEmployeeBean = infoEmployeeBeanList.get(employeeCount);
				long employeeId = infoEmployeeBean.getEmployee_id();
				String deptId = infoEmployeeBean.getDept_id();
				InfoDepartmentBean infoDepartmentBean = infoDepartmentService.getBeanByDeptId(Long.parseLong(deptId));
				String sheetName = infoDepartmentBean.getDept_name();
//				if (!"CRM".equals(sheetName) && !"BILLING".equals(sheetName)) {
//					sheetName = "其他";
//				}
				// 查询日报记录
				BusiWeeklyReportRecordBean busiWeeklyReportRecordBean = busiWeeklyReportRecordService
						.getBean(employeeId, beginDate, endDate);
				if (null != busiWeeklyReportRecordBean) {
					// 查询日报明细
					List<BusiWeeklyReportResultBean> busiWeeklyReportResultBeanList = busiWeeklyReportResultService.getBeans(busiWeeklyReportRecordBean.getRecord_id());
					// 将日报明细转为map，便于遍历查找
					Map<String, List<BusiWeeklyReportResultBean>> resultMap = new HashMap<String, List<BusiWeeklyReportResultBean>>();
					for (BusiWeeklyReportResultBean resultBean : busiWeeklyReportResultBeanList) {
						String recordId = resultBean.getRecord_id();
						if (resultMap.containsKey(recordId)) {
							resultMap.get(recordId).add(resultBean);
						} else {
							List<BusiWeeklyReportResultBean> resultBeanLists = new ArrayList<BusiWeeklyReportResultBean>();
							resultBeanLists.add(resultBean);
							resultMap.put(recordId, resultBeanLists);
						}
					}
					// 循环周报记录
					BusiWeeklyReportRecordBean recordBean = busiWeeklyReportRecordBean;
					String recordId = recordBean.getRecord_id();
					//创建一个工作簿
					ws = target.createSheet(sheetName + "-" + infoEmployeeBean.getEmployee_name(), employeeCount);
					// 设置列宽
					ws.setColumnView(0,10);
					ws.setColumnView(1,40);
					ws.setColumnView(2,60);
					ws.setColumnView(3,10);
					ws.setColumnView(4,15);
					ws.setColumnView(5,15);
					ws.setColumnView(6,18);
					// 写标题
					ws.mergeCells(0, 0, 5, 0);
					WritableFont wfc = new WritableFont(WritableFont.ARIAL, 14, WritableFont.NO_BOLD, false, UnderlineStyle.SINGLE, Colour.BLACK);
					WritableCellFormat wcfFormat = new WritableCellFormat(wfc);
					wcfFormat.setAlignment(Alignment.CENTRE); //设置对齐方式
					wcfFormat.setVerticalAlignment(VerticalAlignment.CENTRE);//设置对齐方式
					label = new Label(0, 0, "本周工作总结", wcfFormat);
					ws.setRowView(0, 700);
					ws.addCell(label);
					// 写任务头
					wfc = new WritableFont(WritableFont.ARIAL, 11, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
					wcfFormat = new WritableCellFormat(wfc);
					wcfFormat.setAlignment(Alignment.CENTRE);
					wcfFormat.setVerticalAlignment(VerticalAlignment.CENTRE);//设置对齐方式
					wcfFormat.setBackground(Colour.SKY_BLUE); //背静色
					wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
					label = new Label(0, 1, "WBS ID", wcfFormat);
					ws.addCell(label);
					label = new Label(1, 1, "参与项目", wcfFormat);
					ws.addCell(label);
					label = new Label(2, 1, "任务描述", wcfFormat);
					ws.addCell(label);
					label = new Label(3, 1, "状态", wcfFormat);
					ws.addCell(label);
					label = new Label(4, 1, "开始时间", wcfFormat);
					ws.addCell(label);
					label = new Label(5, 1, "完成时间", wcfFormat);
					ws.addCell(label);
					label = new Label(6, 1, "备注", wcfFormat);
					ws.addCell(label);
					ws.setRowView(1, 700);
					//定义excel的文本单元格
					if (resultMap.containsKey(recordId)) {
						List<BusiWeeklyReportResultBean> resultBeanLists = resultMap.get(recordId);
						// 循环任务
						int taskCount = 0;
						for (int resultCount = 0; resultCount < resultBeanLists.size(); resultCount ++) {
							BusiWeeklyReportResultBean resultBean = resultBeanLists.get(resultCount);
							if ("TASK".equals(resultBean.getResult_type())) {
								int rowId = resultBean.getResult_id() + 1;
								ws.setRowView(rowId, 500);
								wcfFormat = new WritableCellFormat();
								wcfFormat.setAlignment(Alignment.CENTRE);
								wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
								label = new Label(0, rowId, StringUtil.toString(resultBean.getResult_id()), wcfFormat);
								ws.addCell(label);
								if ("PRJ_ID".equals(resultBean.getResult_code())) {
									wcfFormat = new WritableCellFormat();
									wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
									label = new Label(1, rowId, getPrjName(resultBean.getResult_value()), wcfFormat);
									ws.addCell(label);
									taskCount ++;
								}
								if ("TASK_DESC".equals(resultBean.getResult_code())) {
									wcfFormat = new WritableCellFormat();
									wcfFormat.setWrap(true);//可换行的label样式
									wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
									label = new Label(2, rowId, resultBean.getResult_value().replaceAll("<br>", "\n"), wcfFormat);
									ws.addCell(label);
								}
								if ("STATE".equals(resultBean.getResult_code())) {
									wcfFormat = new WritableCellFormat();
									wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
									label = new Label(3, rowId, CacheUtil.getSysParamBeanValueDesc("WEEKLY_REPORT", "STATE", resultBean.getResult_value()), wcfFormat);
									ws.addCell(label);
								}
								if ("BEGIN_TIME".equals(resultBean.getResult_code())) {
									wcfFormat = new WritableCellFormat();
									wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
									label = new Label(4, rowId, resultBean.getResult_value(), wcfFormat);
									ws.addCell(label);
								}
								if ("END_TIME".equals(resultBean.getResult_code())) {
									wcfFormat = new WritableCellFormat();
									wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
									label = new Label(5, rowId, resultBean.getResult_value(), wcfFormat);
									ws.addCell(label);
								}
								if ("OUTPUT".equals(resultBean.getResult_code())) {
									wcfFormat = new WritableCellFormat();
									wcfFormat.setWrap(true);//可换行的label样式
									wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
									label = new Label(6, rowId, resultBean.getResult_value().replaceAll("<br>", "\n"), wcfFormat);
									ws.addCell(label);
								}
							}
						}
						taskCount ++;
						taskCount ++;
						taskCount ++;
						// 写标题
						ws.mergeCells(0, taskCount, 4, taskCount);
						wfc = new WritableFont(WritableFont.ARIAL, 14, WritableFont.NO_BOLD, false, UnderlineStyle.SINGLE, Colour.BLACK);
						wcfFormat = new WritableCellFormat(wfc);
						wcfFormat.setAlignment(Alignment.CENTRE);
						wcfFormat.setVerticalAlignment(VerticalAlignment.CENTRE);//设置对齐方式
						label = new Label(0, taskCount, "下周工作计划", wcfFormat);
						ws.setRowView(taskCount, 700);
						ws.addCell(label);
						taskCount ++;
						// 写计划头
						wfc = new WritableFont(WritableFont.ARIAL, 11, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
						wcfFormat = new WritableCellFormat(wfc);
						wcfFormat.setAlignment(Alignment.CENTRE);
						wcfFormat.setVerticalAlignment(VerticalAlignment.CENTRE);//设置对齐方式
						wcfFormat.setBackground(Colour.SKY_BLUE); //背静色
						wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
						ws.setRowView(taskCount, 700);
						label = new Label(0, taskCount, "序号", wcfFormat);
						ws.addCell(label);
						label = new Label(1, taskCount, "参与项目", wcfFormat);
						ws.addCell(label);
						label = new Label(2, taskCount, "计划描述", wcfFormat);
						ws.addCell(label);
						label = new Label(3, taskCount, "依赖", wcfFormat);
						ws.addCell(label);
						label = new Label(4, taskCount, "计划开始时间", wcfFormat);
						ws.addCell(label);
						label = new Label(5, taskCount, "计划完成时间", wcfFormat);
						ws.addCell(label);
						label = new Label(6, taskCount, "备注", wcfFormat);
						ws.addCell(label);
						
						// 循环计划
						for (int resultCount = 0; resultCount < resultBeanLists.size(); resultCount ++) {
							BusiWeeklyReportResultBean resultBean = resultBeanLists.get(resultCount);
							if ("PLAN".equals(resultBean.getResult_type())) {
								int rowId = resultBean.getResult_id();
								ws.setRowView(taskCount + rowId, 500);
								wcfFormat = new WritableCellFormat();
								wcfFormat.setAlignment(Alignment.CENTRE);
								wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
								label = new Label(0, taskCount + rowId, StringUtil.toString(resultBean.getResult_id()), wcfFormat);
								ws.addCell(label);
								if ("PRJ_ID".equals(resultBean.getResult_code())) {
									wcfFormat = new WritableCellFormat();
									wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
									label = new Label(1, taskCount + rowId, getPrjName(resultBean.getResult_value()), wcfFormat);
									ws.addCell(label);
								}
								if ("PLAN_DESC".equals(resultBean.getResult_code())) {
									wcfFormat = new WritableCellFormat();
									wcfFormat.setWrap(true);//可换行的label样式
									wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
									label = new Label(2, taskCount + rowId, resultBean.getResult_value().replaceAll("<br>", "\n\t"), wcfFormat);
									ws.addCell(label);
								}
								if ("DEPEND_ON".equals(resultBean.getResult_code())) {
									wcfFormat = new WritableCellFormat();
									wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
									label = new Label(3, taskCount + rowId, resultBean.getResult_value(), wcfFormat);
									ws.addCell(label);
								}
								if ("PLAN_BEGIN_TIME".equals(resultBean.getResult_code())) {
									wcfFormat = new WritableCellFormat();
									wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
									label = new Label(4, taskCount + rowId, resultBean.getResult_value(), wcfFormat);
									ws.addCell(label);
								}
								if ("PLAN_END_TIME".equals(resultBean.getResult_code())) {
									wcfFormat = new WritableCellFormat();
									wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
									label = new Label(5, taskCount + rowId, resultBean.getResult_value(), wcfFormat);
									ws.addCell(label);
								}
								if ("OUTPUT".equals(resultBean.getResult_code())) {
									wcfFormat = new WritableCellFormat();
									wcfFormat.setWrap(true);//可换行的label样式
									wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
									label = new Label(2, taskCount + rowId, resultBean.getResult_value().replaceAll("<br>", "\n\t"), wcfFormat);
									ws.addCell(label);
								}
							}
						}
					}
				}
			}
			//关闭文件 
			if (target.getNumberOfSheets() > 0) {
				target.write();
			}
			target.close();
			json.put(BusiConstants.AjaxReturn.STATUS_CODE,
					BusiConstants.AjaxStatus.STATUS_SUCCESS);
			json.put(BusiConstants.AjaxReturn.STATUS_INFO, "Excel生成成功");
			json.put(BusiConstants.AjaxReturn.RETURN_INFO, outFileName);
		} catch (Exception e) {
			String retMsg = e.getCause() == null ? e.getMessage() : e
					.getCause().getMessage();
			retMsg = StringUtil.isBlank(retMsg) ? "Excel生成失败" : retMsg;
			json = ExceptionUtil.toJSON(retMsg, e, request);
		} finally {
			response.setContentType("text/html;charset=utf-8");
			PrintWriter printWriter = response.getWriter();
			printWriter.write(json.toString());
			printWriter.flush();
			printWriter.close();
		}
	}
	
	@RequestMapping(value = "/downloadExcel")
	public void downloadExcel(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		JSONObject json = new JSONObject();
		try {
			String employeeId = request.getParameter("EMPLOYEE_ID");
			String rometeFileName = request.getParameter("_FILE_NAME");
			String localFileName = "";
			if (!StringUtil.isBlank(employeeId)) {
				InfoEmployeeBean infoEmployeeBean = infoEmployeeService.getBean(Long.parseLong(employeeId));
				localFileName = "工作周报_" + infoEmployeeBean.getEmployee_name() + ".xls";
			} else {
				localFileName = "工作周报" + "（" + rometeFileName + "）.xls";
			}
			// 下载Excel文件
			ExcelUtil.download(response, localFileName, "",
					rometeFileName);
			json.put(BusiConstants.AjaxReturn.STATUS_CODE,
					BusiConstants.AjaxStatus.STATUS_SUCCESS);
			json.put(BusiConstants.AjaxReturn.STATUS_INFO, "Excel下载成功");
			json.put(BusiConstants.AjaxReturn.RETURN_INFO, localFileName);
		} catch (Exception e) {
			String retMsg = e.getCause() == null ? e.getMessage() : e
					.getCause().getMessage();
			retMsg = StringUtil.isBlank(retMsg) ? "Excel下载失败" : retMsg;
			json = ExceptionUtil.toJSON(retMsg, e, request);
		} finally {
			response.setContentType("text/html;charset=utf-8");
			PrintWriter printWriter = response.getWriter();
			printWriter.write(json.toString());
			printWriter.flush();
			printWriter.close();
		}
	}

	private List<WeeklyReportDateBean> getSelectMonthDateList(String year, String month) throws Exception {
		List<WeeklyReportDateBean> weeklyReportDateBeanList = new ArrayList<WeeklyReportDateBean>();
		WeeklyReportDateBean weeklyReportDateBean = new WeeklyReportDateBean();
		// 1、设定系统初始日期
		String initDateStr = year + "-" + month + "-01";
		// 2、获取初始日期所在周的第一天
		String releaseFirstDateStr = mySQLDateService.getWeekFirstDateStr(initDateStr);
		// 4、获取当前日期所在周的最后一天
		String releaseLastDateStr = mySQLDateMapper.getMonthLastDate(initDateStr);
		// 5、计算所有的周一和周五
		Calendar beginCalendar = new GregorianCalendar();
		Calendar endCalendar = new GregorianCalendar();
		beginCalendar.set(Integer.parseInt(releaseFirstDateStr.split("-")[0]),
				Integer.parseInt(releaseFirstDateStr.split("-")[1]) - 1,
				Integer.parseInt(releaseFirstDateStr.split("-")[2])); // Calendar的月从0-11，所以月要减一.
		endCalendar.set(Integer.parseInt(releaseLastDateStr.split("-")[0]),
				Integer.parseInt(releaseLastDateStr.split("-")[1]) - 1,
				Integer.parseInt(releaseLastDateStr.split("-")[2])); // Calendar的月从0-11，所以所以月要减一.
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String time = sdf.format(endCalendar.getTime());
		endCalendar.set(Calendar.DAY_OF_WEEK, 2);
		time = sdf.format(endCalendar.getTime());
//		endCalendar.add(Calendar.DAY_OF_YEAR, -7);
		while (endCalendar.compareTo(beginCalendar) >= 0) {
			weeklyReportDateBean = new WeeklyReportDateBean();
			endCalendar.set(Calendar.DAY_OF_WEEK, 2);
			time = sdf.format(endCalendar.getTime());
			weeklyReportDateBean.setBeginDate(time.replaceAll("-", ""));
			weeklyReportDateBean.setBeginDateDesc(time.replaceAll("-", "."));
			endCalendar.set(Calendar.DAY_OF_WEEK, 6);
			time = sdf.format(endCalendar.getTime());
			weeklyReportDateBean.setEndDate(time.replaceAll("-", ""));
			weeklyReportDateBean.setEndDateDesc(time.replaceAll("-", "."));
			weeklyReportDateBean.setYear(time.split("-")[0]);
			weeklyReportDateBean.setMonth(time.split("-")[1]);
			weeklyReportDateBean.setWeek(endCalendar.get(Calendar.WEEK_OF_MONTH));
			weeklyReportDateBeanList.add(weeklyReportDateBean);
			endCalendar.add(Calendar.DAY_OF_YEAR, -7);
		}
		return weeklyReportDateBeanList;
	}

	private List<SysParamBean> getWeeklyReportStateList() {
		return CacheUtil.getSysParamBeanList("WEEKLY_REPORT", "STATE");
	}

	public ComparatorDepartment getComparator() {
		return comparator;
	}

	public void setComparator(ComparatorDepartment comparator) {
		this.comparator = comparator;
	}
	
	private String getPrjName(String prjId) {
		String prjName = "";
		InfoProjectBean infoProjectBean = infoProjectService.getBean(BigDecimal.valueOf(Long.parseLong(prjId)));
		if (null == infoProjectBean) {
			prjName = prjId;
		} else {
			prjName = infoProjectBean.getPrj_name();
		}
		return prjName;
	}
	
	private List<WeeklyReportDateBean> getDelectWeekList() throws Exception {
		List<SysParamBean> sysParamBeanList = getSelectMonthList();
		List<WeeklyReportDateBean> weeklyReportDateBeanList = new ArrayList<WeeklyReportDateBean>();
		for (SysParamBean sysParamBean : sysParamBeanList) {
			String columnValue = sysParamBean.getColumn_value();
			weeklyReportDateBeanList.addAll(getSelectMonthDateList(columnValue.substring(0, 4), columnValue.substring(4, 6)));
		}
		return weeklyReportDateBeanList;
	}
	
	private List<SysParamBean> getSelectMonthList() throws Exception {
		List<SysParamBean> sysParamBeanList = new ArrayList<SysParamBean>();
		SysParamBean sysParamBean = new SysParamBean();
		String initYearStr = "2016";
		String initMonthStr = "01";
		int initYear = Integer.parseInt(initYearStr);
		int initMonth = Integer.parseInt(initMonthStr);
		String currDate = mySQLDateService.getSysdateStr();
		String currYearStr = currDate.split("-")[0];
		String currMonthStr = currDate.split("-")[1];
		int currYear = Integer.parseInt(currYearStr);
		int currMonth = Integer.parseInt(currMonthStr);
		for (int year = currYear; year >= initYear; year--) {
			for (int month = 12; month >= 1; month--) {
				if (year == initYear) {
					if (month < initMonth) {
						continue;
					}
				}
				if (year == currYear) {
					if (month > currMonth) {
						continue;
					}
				}
				sysParamBean = new SysParamBean();
				sysParamBean.setType_code("EVALUATION");
				sysParamBean.setParam_code("DATE_LIST");
				String monthStr = "";
				if (String.valueOf(month).length() == 1) {
					monthStr = "0" + month;
				} else {
					monthStr = "" + month;
				}
				sysParamBean.setColumn_value(year + monthStr);
				sysParamBean.setColumn_desc(year + "年" + monthStr + "月");
				sysParamBeanList.add(sysParamBean);
			}
		}
		return sysParamBeanList;
	}

	@RequestMapping(value = "/init")
	public ModelAndView init(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) throws Exception {
		// 1、获取当前操作员信息
		InfoOperBeanExt infoOperBean = InfoOperUtil.getCurrOperInfo(request);
		// 2、获取人员信息
		InfoEmployeeBean infoEmployeeBean = infoOperBean.getInfoEmployeeBean();
		modelMap.put("SELECT_EMPLOYEE", infoEmployeeBean);
		// 3、获取当前选择月份
		List<WeeklyReportDateBean> weeklyReportDateBeanList = getDelectWeekList();
		String currDate = mySQLDateService.getSysdateStr().substring(0, 10);
		String selectDate = request.getParameter("SELECT_DATE");
		String currYearStr = currDate.split("-")[0];
		String currMonthStr = currDate.split("-")[1];
		String currDayStr = currDate.split("-")[2];
		if (!StringUtils.isBlank(selectDate)) {
			currDate = selectDate.substring(0, 8);
			currYearStr = currDate.substring(0, 4);
			currMonthStr = currDate.substring(4, 6);
			currDayStr = currDate.substring(6, 8);
		} else {
			currYearStr = currDate.split("-")[0];
			currMonthStr = currDate.split("-")[1];
			currDayStr = currDate.split("-")[2];
		}
		Calendar calendar = new GregorianCalendar();
		calendar.set(Integer.parseInt(currYearStr),
				Integer.parseInt(currMonthStr) - 1,
				Integer.parseInt(currDayStr)); // Calendar的月从0-11，所以月要减一.
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String time = sdf.format(calendar.getTime());
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		String beginDate = sdf.format(calendar.getTime());
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
		String endDate = sdf.format(calendar.getTime());
		SysParamBean sysParamBean = new SysParamBean();
		sysParamBean.setType_code("REPORT");
		sysParamBean.setParam_code("CURR_DATE");
		for (WeeklyReportDateBean weeklyReportDateBean : weeklyReportDateBeanList) {
			if (beginDate.equals(weeklyReportDateBean.getBeginDate())) {
				sysParamBean.setColumn_value(weeklyReportDateBean.getBeginDate() + weeklyReportDateBean.getEndDate());
				sysParamBean.setColumn_desc(weeklyReportDateBean.getYear() + "年" + weeklyReportDateBean.getMonth() + "月第" + weeklyReportDateBean.getWeek() + "周" + weeklyReportDateBean.getBeginDateDesc() + "~" + weeklyReportDateBean.getEndDateDesc());
			}
		}
		
		modelMap.put("CURR_DATE", sysParamBean);
		// 4、获取周报记录
		BusiWeeklyReportRecordBean busiWeeklyReportRecordBean = busiWeeklyReportRecordService
				.getBean(infoEmployeeBean.getEmployee_id(), beginDate, endDate);
		modelMap.put("WEEKLY_REPORT_RECORD", busiWeeklyReportRecordBean);
		// 5、获取月份列表
//		modelMap.put("MONTH_LIST", getSelectMonthList());
//		modelMap.put("WEEKLYREPORT_DATE_LIST", getSelectMonthDateList(currYearStr, monthStr));
		modelMap.put("DATE_LIST", weeklyReportDateBeanList);
		return new ModelAndView("weeklyReport/init", modelMap);
	}

	@RequestMapping(value = "/edit")
	public ModelAndView edit(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) {
		try {
			// 1、获取页面提交信息
			String operType = request.getParameter("OPER_TYPE");
			String beginDate = request.getParameter("BEGIN_DATE");
			String endDate = request.getParameter("END_DATE");
			String taskBeginDate = "";
			String taskEndDate = "";
			String planBeginDate = "";
			String planEndDate = "";
			// 2、获取当前操作员信息
			InfoOperBeanExt infoOperBean = InfoOperUtil.getCurrOperInfo(request);
			// 3、获取人员信息
			InfoEmployeeBean infoEmployeeBean = infoOperBean.getInfoEmployeeBean();
			// 4、获取周报记录
			BusiWeeklyReportRecordBean busiWeeklyReportRecordBean = busiWeeklyReportRecordService
					.getBean(infoEmployeeBean.getEmployee_id(), beginDate, endDate);
			modelMap.put("WEEKLY_REPORT_RECORD", busiWeeklyReportRecordBean);
			// 5、获取周报结果
			if (null != busiWeeklyReportRecordBean) {
				List<BusiWeeklyReportResultBean> busiWeeklyReportResultBeanList = busiWeeklyReportResultService
						.getBeans(busiWeeklyReportRecordBean.getRecord_id());
				modelMap.put("WEEKLY_REPORT_RESULT", busiWeeklyReportResultBeanList);
				taskBeginDate = busiWeeklyReportRecordBean.getTask_begin_date();
				taskEndDate = busiWeeklyReportRecordBean.getTask_end_date();
				planBeginDate = busiWeeklyReportRecordBean.getPlan_begin_date();
				planEndDate = busiWeeklyReportRecordBean.getPlan_end_date();
			} else {
				SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");
				SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd");
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(sdf1.parse(beginDate));
				taskBeginDate = sdf3.format(calendar.getTime());
				calendar.add(Calendar.DAY_OF_YEAR, 7);
				planBeginDate = sdf3.format(calendar.getTime());
				calendar.setTime(sdf1.parse(endDate));
				taskEndDate = sdf3.format(calendar.getTime());
				calendar.add(Calendar.DAY_OF_YEAR, 7);
				planEndDate = sdf3.format(calendar.getTime());
			}
			modelMap.put("OPER_TYPE", operType);
			modelMap.put("BEGIN_DATE", beginDate);
			modelMap.put("END_DATE", endDate);
			modelMap.put("TASK_BEGIN_DATE", taskBeginDate);
			modelMap.put("TASK_END_DATE", taskEndDate);
			modelMap.put("PLAN_BEGIN_DATE", planBeginDate);
			modelMap.put("PLAN_END_DATE", planEndDate);
			modelMap.put("PRJ_LIST", infoProjectService.getBeans());
			modelMap.put("WEEKLY_REPORT_STATE", getWeeklyReportStateList());
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");
			SimpleDateFormat sdf2 = new SimpleDateFormat("MM-dd");
			List<String> taskDateList = new ArrayList<String>();
			for (int d = 0; d < 7; d ++) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(sdf1.parse(beginDate));
				calendar.add(Calendar.DAY_OF_YEAR, d);
				taskDateList.add(sdf2.format(calendar.getTime()));
			}
			modelMap.put("TASK_DATE_LIST", taskDateList);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return new ModelAndView("weeklyReport/edit", modelMap);
	}

	@RequestMapping(value = "/checkinit")
	public ModelAndView checkinit(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) throws Exception {
		String prjId = request.getParameter("SELECT_PRJ");
		modelMap.put("SELECT_PRJ", prjId);
		// 1、获取当前操作员信息
		InfoOperBeanExt infoOperBean = InfoOperUtil.getCurrOperInfo(request);
		// 2、获取人员信息
		InfoEmployeeBean infoEmployeeBean = infoOperBean.getInfoEmployeeBean();
		List<InfoProjectEmployeeBean> employeeList = new ArrayList<InfoProjectEmployeeBean>();
		if (!StringUtils.isBlank(prjId)) {
			employeeList = infoProjectEmployeeService.getBeansByManagerEmployeeIdAndPrj(infoEmployeeBean.getEmployee_id(), Long.parseLong(prjId));
		} else {
			employeeList = infoProjectEmployeeService.getBeansByManagerEmployeeId(infoEmployeeBean.getEmployee_id());
		}
		modelMap.put("EMPLOYEE_LIST", employeeList);
		// 3、获取当前选择月份
		List<WeeklyReportDateBean> weeklyReportDateBeanList = getDelectWeekList();
		String currDate = mySQLDateService.getSysdateStr().substring(0, 10);
		String selectDate = request.getParameter("SELECT_DATE");
		String currYearStr = currDate.split("-")[0];
		String currMonthStr = currDate.split("-")[1];
		String currDayStr = currDate.split("-")[2];
		if (!StringUtils.isBlank(selectDate)) {
			currDate = selectDate.substring(0, 8);
			currYearStr = currDate.substring(0, 4);
			currMonthStr = currDate.substring(4, 6);
			currDayStr = currDate.substring(6, 8);
		} else {
			currYearStr = currDate.split("-")[0];
			currMonthStr = currDate.split("-")[1];
			currDayStr = currDate.split("-")[2];
		}
		Calendar calendar = new GregorianCalendar();
		calendar.set(Integer.parseInt(currYearStr),
				Integer.parseInt(currMonthStr) - 1,
				Integer.parseInt(currDayStr)); // Calendar的月从0-11，所以月要减一.
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String time = sdf.format(calendar.getTime());
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		String beginDate = sdf.format(calendar.getTime());
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
		String endDate = sdf.format(calendar.getTime());
		SysParamBean sysParamBean = new SysParamBean();
		sysParamBean.setType_code("REPORT");
		sysParamBean.setParam_code("CURR_DATE");
		for (WeeklyReportDateBean weeklyReportDateBean : weeklyReportDateBeanList) {
			if (beginDate.equals(weeklyReportDateBean.getBeginDate())) {
				sysParamBean.setColumn_value(weeklyReportDateBean.getBeginDate() + weeklyReportDateBean.getEndDate());
				sysParamBean.setColumn_desc(weeklyReportDateBean.getYear() + "年" + weeklyReportDateBean.getMonth() + "月第" + weeklyReportDateBean.getWeek() + "周" + weeklyReportDateBean.getBeginDateDesc() + "~" + weeklyReportDateBean.getEndDateDesc());
			}
		}
		
		modelMap.put("CURR_DATE", sysParamBean);
		// 5、获取月份列表
		modelMap.put("DATE_LIST", weeklyReportDateBeanList);
		modelMap.put("PRJ_LIST", infoProjectService.getBeansByManager(infoEmployeeBean.getEmployee_id()));
		return new ModelAndView("weeklyReport/checkinit", modelMap);
	}

	@RequestMapping(value = "/checkedit")
	public ModelAndView checkedit(HttpServletRequest request,
		HttpServletResponse response, ModelMap modelMap) throws Exception {
		modelMap = getDetail(request, modelMap);
		return new ModelAndView("weeklyReport/checkedit", modelMap);
	}

	@RequestMapping(value = "/checksave")
	public void checksave(HttpServletRequest request, HttpServletResponse response,
			ModelMap modelMap) throws Exception {
		JSONObject rtnJson = new JSONObject();
		// 1、获取页面提交信息
		String operType = request.getParameter("OPER_TYPE");
		String beginDate = request.getParameter("BEGIN_DATE");
		String endDate = request.getParameter("END_DATE");
		String employeeId = request.getParameter("SELECT_EMPLOYEE");
		String inputParam = request.getParameter("INPUT_PARAM");
		JSONObject inputJson = JSONObject.fromString(inputParam);
		JSONArray taskJsonArray = inputJson.getJSONArray("TASK");
		try {
			// 4、获取周报记录
			BusiWeeklyReportRecordBean busiWeeklyReportRecordBean = busiWeeklyReportRecordService
					.getBean(Long.parseLong(employeeId), beginDate, endDate);
			String recordId = busiWeeklyReportRecordBean.getRecord_id();
			List<BusiWeeklyReportResultBean> busiWeeklyReportResultBeanList = new ArrayList<BusiWeeklyReportResultBean>();
			BusiWeeklyReportResultBean busiWeeklyReportResultBean = new BusiWeeklyReportResultBean();
			for (int count = 0; count < taskJsonArray.length(); count++) {
				JSONObject json = (JSONObject) taskJsonArray.get(count);
				for (int i = 1; i <=7; i ++) {
					if (!"0".equals(json.getString("TASK_WORK_TIME_" + i))) {
						busiWeeklyReportResultBean = new BusiWeeklyReportResultBean();
						busiWeeklyReportResultBean.setRecord_id(recordId);
						busiWeeklyReportResultBean.setResult_id(json
								.getInt("WBS_ID"));
						busiWeeklyReportResultBean.setResult_type("TASK");
						busiWeeklyReportResultBean.setResult_code("WORK_TIME_" + i);
						busiWeeklyReportResultBean.setResult_value(json.getString("TASK_WORK_TIME_" + i));
						busiWeeklyReportResultBeanList.add(busiWeeklyReportResultBean);
					}
				}
			}
			busiWeeklyReportService.saveCheck(operType,
					busiWeeklyReportRecordBean, busiWeeklyReportResultBeanList);
			rtnJson.put(BusiConstants.AjaxReturn.STATUS_CODE,
					BusiConstants.AjaxStatus.STATUS_SUCCESS);
			rtnJson.put(BusiConstants.AjaxReturn.STATUS_INFO, "保存成功");
		} catch (Exception ex) {
			ex.printStackTrace();
			String retMsg = ex.getCause() == null ? ex.getMessage() : ex
					.getCause().getMessage();
			rtnJson = ExceptionUtil.toJSON(retMsg, ex, request);
		} finally {
			response.setContentType("text/html;charset=utf-8");
			PrintWriter printWriter = response.getWriter();
			printWriter.write(rtnJson.toString());
			printWriter.flush();
			printWriter.close();
		}
	}

	@RequestMapping(value = "/viewinit")
	public ModelAndView viewinit(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) throws Exception {
		String prjId = request.getParameter("SELECT_PRJ");
		modelMap.put("SELECT_PRJ", prjId);
		String taskBeginDate = "";
		String taskEndDate = "";
		String planBeginDate = "";
		String planEndDate = "";
		// 1、获取当前操作员信息
		InfoOperBeanExt infoOperBean = InfoOperUtil.getCurrOperInfo(request);
		// 2、获取人员信息
		InfoEmployeeBean infoEmployeeBean = infoOperBean.getInfoEmployeeBean();
		if (!StringUtils.isBlank(prjId)) {
			modelMap.put("EMPLOYEE_LIST", infoProjectEmployeeService.getBeansByManagerEmployeeIdAndPrj(infoEmployeeBean.getEmployee_id(), Long.parseLong(prjId)));
		} else {
			modelMap.put("EMPLOYEE_LIST", infoEmployeeService.getBeanList());
		}
		// 3、获取当前选择月份
		List<WeeklyReportDateBean> weeklyReportDateBeanList = getDelectWeekList();
		String currDate = mySQLDateService.getSysdateStr().substring(0, 10);
		String selectDate = request.getParameter("SELECT_DATE");
		String currYearStr = currDate.split("-")[0];
		String currMonthStr = currDate.split("-")[1];
		String currDayStr = currDate.split("-")[2];
		if (!StringUtils.isBlank(selectDate)) {
			currDate = selectDate.substring(0, 8);
			currYearStr = currDate.substring(0, 4);
			currMonthStr = currDate.substring(4, 6);
			currDayStr = currDate.substring(6, 8);
		} else {
			currYearStr = currDate.split("-")[0];
			currMonthStr = currDate.split("-")[1];
			currDayStr = currDate.split("-")[2];
		}
		Calendar calendar = new GregorianCalendar();
		calendar.set(Integer.parseInt(currYearStr),
				Integer.parseInt(currMonthStr) - 1,
				Integer.parseInt(currDayStr)); // Calendar的月从0-11，所以月要减一.
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String time = sdf.format(calendar.getTime());
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		String beginDate = sdf.format(calendar.getTime());
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
		String endDate = sdf.format(calendar.getTime());
		SysParamBean sysParamBean = new SysParamBean();
		sysParamBean.setType_code("REPORT");
		sysParamBean.setParam_code("CURR_DATE");
		for (WeeklyReportDateBean weeklyReportDateBean : weeklyReportDateBeanList) {
			if (beginDate.equals(weeklyReportDateBean.getBeginDate())) {
				sysParamBean.setColumn_value(weeklyReportDateBean.getBeginDate() + weeklyReportDateBean.getEndDate());
				sysParamBean.setColumn_desc(weeklyReportDateBean.getYear() + "年" + weeklyReportDateBean.getMonth() + "月第" + weeklyReportDateBean.getWeek() + "周" + weeklyReportDateBean.getBeginDateDesc() + "~" + weeklyReportDateBean.getEndDateDesc());
			}
		}
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat sdf2 = new SimpleDateFormat("MM-dd");
		SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd");
		calendar.setTime(sdf1.parse(beginDate));
		taskBeginDate = sdf3.format(calendar.getTime());
		calendar.add(Calendar.DAY_OF_YEAR, 7);
		planBeginDate = sdf3.format(calendar.getTime());
		calendar.setTime(sdf1.parse(endDate));
		taskEndDate = sdf3.format(calendar.getTime());
		calendar.add(Calendar.DAY_OF_YEAR, 7);
		planEndDate = sdf3.format(calendar.getTime());
		modelMap.put("CURR_DATE", sysParamBean);
		modelMap.put("DATE_LIST", weeklyReportDateBeanList);
		modelMap.put("PRJ_LIST", infoProjectService.getBeans());
		modelMap.put("BEGIN_DATE", beginDate);
		modelMap.put("END_DATE", endDate);
		modelMap.put("TASK_BEGIN_DATE", taskBeginDate);
		modelMap.put("TASK_END_DATE", taskEndDate);
		modelMap.put("PLAN_BEGIN_DATE", planBeginDate);
		modelMap.put("PLAN_END_DATE", planEndDate);
		List<String> taskDateList = new ArrayList<String>();
		for (int d = 0; d < 7; d ++) {
			calendar = Calendar.getInstance();
			calendar.setTime(sdf1.parse(beginDate));
			calendar.add(Calendar.DAY_OF_YEAR, d);
			taskDateList.add(sdf2.format(calendar.getTime()));
		}
		modelMap.put("TASK_DATE_LIST", taskDateList);
		return new ModelAndView("weeklyReport/viewinit", modelMap);
	}

	@RequestMapping(value = "/detail")
	public ModelAndView detail(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) throws Exception {
		modelMap = getDetail(request, modelMap);
		return new ModelAndView("weeklyReport/detail", modelMap);
	}

	private ModelMap getDetail(HttpServletRequest request, ModelMap modelMap)
			throws ParseException {
		// 1、获取页面提交信息
		String beginDate = request.getParameter("BEGIN_DATE");
		String endDate = request.getParameter("END_DATE");
		String employeeId = request.getParameter("SELECT_EMPLOYEE");
		String taskBeginDate = "";
		String taskEndDate = "";
		String planBeginDate = "";
		String planEndDate = "";
		InfoEmployeeBean infoEmployeeBean = infoEmployeeService.getBean(Long.parseLong(employeeId));
		modelMap.put("SELECT_EMPLOYEE", infoEmployeeBean);
		// 4、获取周报记录
		BusiWeeklyReportRecordBean busiWeeklyReportRecordBean = busiWeeklyReportRecordService
				.getBean(infoEmployeeBean.getEmployee_id(), beginDate, endDate);
		modelMap.put("WEEKLY_REPORT_RECORD", busiWeeklyReportRecordBean);
		// 5、获取周报结果
		if (null != busiWeeklyReportRecordBean) {
			List<BusiWeeklyReportResultBean> busiWeeklyReportResultBeanList = busiWeeklyReportResultService
					.getBeans(busiWeeklyReportRecordBean.getRecord_id());
			taskBeginDate = busiWeeklyReportRecordBean.getTask_begin_date();
			taskEndDate = busiWeeklyReportRecordBean.getTask_end_date();
			planBeginDate = busiWeeklyReportRecordBean.getPlan_begin_date();
			planEndDate = busiWeeklyReportRecordBean.getPlan_end_date();
			List<InfoProjectBean> infoProjectBeanList = infoProjectService.getBeans();
			for (BusiWeeklyReportResultBean busiWeeklyReportResultBean : busiWeeklyReportResultBeanList) {
				if ("PRJ_ID".equals(busiWeeklyReportResultBean.getResult_code())) {
					for (InfoProjectBean infoProjectBean : infoProjectBeanList) {
						if (busiWeeklyReportResultBean.getResult_value().equals(infoProjectBean.getPrj_id().toString())) {
							busiWeeklyReportResultBean.setResult_value(infoProjectBean.getPrj_name());
						}
					}
				}
			}
			modelMap.put("WEEKLY_REPORT_RESULT", busiWeeklyReportResultBeanList);
		} else {
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");
			SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd");
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(sdf1.parse(beginDate));
			taskBeginDate = sdf3.format(calendar.getTime());
			calendar.add(Calendar.DAY_OF_YEAR, 7);
			planBeginDate = sdf3.format(calendar.getTime());
			calendar.setTime(sdf1.parse(endDate));
			taskEndDate = sdf3.format(calendar.getTime());
			calendar.add(Calendar.DAY_OF_YEAR, 7);
			planEndDate = sdf3.format(calendar.getTime());
		}
		modelMap.put("BEGIN_DATE", beginDate);
		modelMap.put("END_DATE", endDate);
		modelMap.put("TASK_BEGIN_DATE", taskBeginDate);
		modelMap.put("TASK_END_DATE", taskEndDate);
		modelMap.put("PLAN_BEGIN_DATE", planBeginDate);
		modelMap.put("PLAN_END_DATE", planEndDate);
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat sdf2 = new SimpleDateFormat("MM-dd");
		List<String> taskDateList = new ArrayList<String>();
		for (int d = 0; d < 7; d ++) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(sdf1.parse(beginDate));
			calendar.add(Calendar.DAY_OF_YEAR, d);
			taskDateList.add(sdf2.format(calendar.getTime()));
		}
		modelMap.put("TASK_DATE_LIST", taskDateList);
		return modelMap;
	}

	@RequestMapping(value = "/view")
	public ModelAndView view(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) throws Exception {
		String queryType = request.getParameter("queryType");
		modelMap.put("queryType", queryType);
		modelMap.put("MONTH_LIST", getSelectMonthList());
		modelMap.put("PRJ_LIST", infoProjectService.getBeans());
		String selectMonth = request.getParameter("selectMonth");
		String selectWeek = request.getParameter("selectWeek");
		String selectPrj = request.getParameter("selectPrj");
		modelMap.put("SELECT_MONTH", selectMonth);
		modelMap.put("SELECT_WEEK", selectWeek);
		modelMap.put("SELECT_PRJ", selectPrj);
		if (!StringUtil.isBlank(selectMonth)) {
			modelMap.put("WEEK_LIST_SELECT", getSelectMonthWeekList(selectMonth));
			List<WeeklyReportDateBean> weekList = new ArrayList<WeeklyReportDateBean>();
			if (StringUtil.isBlank(selectWeek)) {
				weekList = getSelectMonthWeekList(selectMonth);
			} else if (!StringUtil.isBlank(selectWeek)) {
				WeeklyReportDateBean dateBean = new WeeklyReportDateBean();
				dateBean.setYear(selectMonth.substring(0, 4));
				dateBean.setMonth(selectMonth.substring(4, 6));
				String beginDate = selectWeek.substring(0, 8);
				dateBean.setBeginDate(beginDate);
				dateBean.setBeginDateDesc(beginDate.substring(0, 4) + "." + beginDate.substring(4, 6) + "." + beginDate.substring(6, 8));
				String endDate = selectWeek.substring(8, 16);
				dateBean.setEndDate(endDate);
				dateBean.setEndDateDesc(endDate.substring(0, 4) + "." + endDate.substring(4, 6) + "." + endDate.substring(6, 8));
				weekList.add(dateBean);
			}
			List<String> taskDateList = new ArrayList<String>();
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");
			SimpleDateFormat sdf2 = new SimpleDateFormat("MM-dd");
			List<WeeklyReportDateBean> sortWeekList = new ArrayList<WeeklyReportDateBean>();
			for (int i = weekList.size() - 1; i >= 0; i --) {
				sortWeekList.add(weekList.get(i));
				for (int d = 0; d < 7; d ++) {
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(sdf1.parse(weekList.get(i).getBeginDate()));
					calendar.add(Calendar.DAY_OF_YEAR, d);
					taskDateList.add(sdf2.format(calendar.getTime()));
				}
			}
			modelMap.put("WEEK_LIST", sortWeekList);
			modelMap.put("TASK_DATE_LIST", taskDateList);
			if (!StringUtils.isBlank(selectPrj)) {
				modelMap.put("EMPLOYEE_LIST", infoProjectEmployeeService.getBeansByPrj(Long.parseLong(selectPrj)));
			} else {
				modelMap.put("EMPLOYEE_LIST", infoEmployeeService.getBeanList());
			}
			modelMap.put("queryType", queryType);
			
			// 4、获取周报记录
			List<BusiWeeklyReportRecordBean> busiWeeklyReportRecordBeanList = busiWeeklyReportRecordService
					.getBeanListByDate(weekList.get(weekList.size() - 1).getBeginDate(), weekList.get(0).getEndDate());
			modelMap.put("WEEKLY_REPORT_RECORD", busiWeeklyReportRecordBeanList);
			// 5、获取周报结果
			if (null != busiWeeklyReportRecordBeanList) {
				List<InfoProjectBean> infoProjectBeanList = infoProjectService.getBeans();
				List<BusiWeeklyReportResultBean> busiWeeklyReportResultBeanList = new ArrayList<BusiWeeklyReportResultBean>();
				for (BusiWeeklyReportRecordBean busiWeeklyReportRecordBean : busiWeeklyReportRecordBeanList) {
					List<BusiWeeklyReportResultBean> resultBeanList = busiWeeklyReportResultService.getBeans(busiWeeklyReportRecordBean.getRecord_id());
					for (BusiWeeklyReportResultBean bean : resultBeanList) {
						if (!"plan".equals(queryType)) {
							if (!"PLAN".equals(bean.getResult_type())) {
								busiWeeklyReportResultBeanList.add(bean);
							}
						} else {
							if ("PLAN".equals(bean.getResult_type())) {
								busiWeeklyReportResultBeanList.add(bean);
							}
						}
					}
				}
				List<String> list = new ArrayList<String>();
				List<BusiWeeklyReportResultBean> _busiWeeklyReportResultBeanList = new ArrayList<BusiWeeklyReportResultBean>();
				for (BusiWeeklyReportResultBean busiWeeklyReportResultBean : busiWeeklyReportResultBeanList) {
					if ("PRJ_ID".equals(busiWeeklyReportResultBean.getResult_code())) {
						if (!StringUtils.isBlank(selectPrj) && selectPrj.equals(busiWeeklyReportResultBean.getResult_value())) {
							int cnt = 0;
							for (String str : list) {
								if (str.equals(busiWeeklyReportResultBean.getRecord_id() + "_" + busiWeeklyReportResultBean.getResult_id())) {
									cnt ++;
									break;
								}
							}
							if (cnt == 0) {
								list.add(busiWeeklyReportResultBean.getRecord_id() + "_" + busiWeeklyReportResultBean.getResult_id());
							}
						}
					}
					for (InfoProjectBean infoProjectBean : infoProjectBeanList) {
						if (busiWeeklyReportResultBean.getResult_value().equals(infoProjectBean.getPrj_id().toString())) {
							busiWeeklyReportResultBean.setResult_value(infoProjectBean.getPrj_name());
						}
					}
				}
				for (String str : list) {
					for (BusiWeeklyReportResultBean busiWeeklyReportResultBean : busiWeeklyReportResultBeanList) {
						if (str.equals(busiWeeklyReportResultBean.getRecord_id() + "_" + busiWeeklyReportResultBean.getResult_id())) {
							_busiWeeklyReportResultBeanList.add(busiWeeklyReportResultBean);
						}
					}
				}
				if (!StringUtil.isBlank(selectPrj)) {
					modelMap.put("WEEKLY_REPORT_RESULT", _busiWeeklyReportResultBeanList);
				} else {
					modelMap.put("WEEKLY_REPORT_RESULT", busiWeeklyReportResultBeanList);
				}
			}
		}
		return new ModelAndView("weeklyReport/" + queryType, modelMap);
	}
	
	@RequestMapping(value = "/getweek")
	@ResponseBody
	public List<WeeklyReportDateBean> getSelectMonthWeekList(String yearMonth) throws Exception {
		List<WeeklyReportDateBean> weeklyReportDateBeanList = new ArrayList<WeeklyReportDateBean>();
		WeeklyReportDateBean weeklyReportDateBean = new WeeklyReportDateBean();
		// 1、设定系统初始日期
		String initDateStr = yearMonth.substring(0, 4) + "-" + yearMonth.substring(4, 6) + "-01";
		// 2、获取初始日期所在周的第一天
		String releaseFirstDateStr = mySQLDateService.getWeekFirstDateStr(initDateStr);
		// 4、获取当前日期所在周的最后一天
		String releaseLastDateStr = mySQLDateMapper.getMonthLastDate(initDateStr);
		// 5、计算所有的周一和周五
		Calendar beginCalendar = new GregorianCalendar();
		Calendar endCalendar = new GregorianCalendar();
		beginCalendar.set(Integer.parseInt(releaseFirstDateStr.split("-")[0]),
				Integer.parseInt(releaseFirstDateStr.split("-")[1]) - 1,
				Integer.parseInt(releaseFirstDateStr.split("-")[2])); // Calendar的月从0-11，所以月要减一.
		endCalendar.set(Integer.parseInt(releaseLastDateStr.split("-")[0]),
				Integer.parseInt(releaseLastDateStr.split("-")[1]) - 1,
				Integer.parseInt(releaseLastDateStr.split("-")[2])); // Calendar的月从0-11，所以所以月要减一.
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String time = sdf.format(endCalendar.getTime());
		endCalendar.set(Calendar.DAY_OF_WEEK, 2);
		time = sdf.format(endCalendar.getTime());
		while (endCalendar.compareTo(beginCalendar) >= 0) {
			weeklyReportDateBean = new WeeklyReportDateBean();
			endCalendar.set(Calendar.DAY_OF_WEEK, 2);
			time = sdf.format(endCalendar.getTime());
			weeklyReportDateBean.setBeginDate(time.replaceAll("-", ""));
			weeklyReportDateBean.setBeginDateDesc(time.replaceAll("-", "."));
			endCalendar.set(Calendar.DAY_OF_WEEK, 6);
			time = sdf.format(endCalendar.getTime());
			weeklyReportDateBean.setEndDate(time.replaceAll("-", ""));
			weeklyReportDateBean.setEndDateDesc(time.replaceAll("-", "."));
			weeklyReportDateBean.setYear(time.split("-")[0]);
			weeklyReportDateBean.setMonth(time.split("-")[1]);
			weeklyReportDateBean.setWeek(endCalendar.get(Calendar.WEEK_OF_MONTH));
			weeklyReportDateBeanList.add(weeklyReportDateBean);
			endCalendar.add(Calendar.DAY_OF_YEAR, -7);
		}
		return weeklyReportDateBeanList;
	}
}
