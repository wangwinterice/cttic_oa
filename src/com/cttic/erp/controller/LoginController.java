package com.cttic.erp.controller;

import java.io.PrintWriter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.cttic.erp.bean.InfoEmployeeBean;
import com.cttic.erp.bean.ext.InfoOperBeanExt;
import com.cttic.erp.common.constants.BusiConstants;
import com.cttic.erp.common.util.ExceptionUtil;
import com.cttic.erp.common.util.InfoOperUtil;
import com.cttic.erp.service.interfaces.IInfoEmployeeService;
import com.cttic.erp.service.interfaces.IInfoOperService;

@Controller
@RequestMapping(value = "/login")
public class LoginController {
	
	@Resource
	private IInfoEmployeeService infoEmployeeServiceImpl;
	
	@Resource
	private IInfoOperService infoOperServiceImpl;

	@RequestMapping(value = "/loginIn")
	public void loginIn(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		JSONObject rtnJson = new JSONObject();

		String employeeId = request.getParameter("EMPLOYEE_NO");
		String employeePwd = request.getParameter("EMPLOYEE_PWD");
		try {
			InfoEmployeeBean infoEmployeeBean = infoEmployeeServiceImpl.getBean(Long.parseLong(employeeId));
			if (null == infoEmployeeBean) {
				throw new Exception("该工号不是有效的人员");
			}
			if (!employeePwd.equals(infoEmployeeBean.getEmployee_pwd())) {
				throw new Exception("该工号密码不正确");
			}
			InfoOperBeanExt infoOperBean = infoOperServiceImpl.getInfoOperBeanExt(Long.parseLong(employeeId));
			InfoOperUtil.setCurrOperInfo(request, infoOperBean);
			rtnJson.put(BusiConstants.AjaxReturn.STATUS_CODE,
					BusiConstants.AjaxStatus.STATUS_SUCCESS);
			rtnJson.put(BusiConstants.AjaxReturn.STATUS_INFO, "提交成功");
		} catch (Exception ex) {
			ex.printStackTrace();
			String retMsg = ex.getCause() == null ? ex.getMessage() : ex
					.getCause().getMessage();
			rtnJson = ExceptionUtil.toJSON(retMsg, ex, request);
		} finally {
			response.setContentType("text/html;charset=utf-8");
			PrintWriter printWriter = response.getWriter();
			printWriter.write(rtnJson.toString());
			printWriter.flush();
			printWriter.close();
		}
	}
	
	@RequestMapping(value = "/changePwd")
	public ModelAndView changePwd(ModelMap modelMap) {
		return new ModelAndView("changePwd", modelMap);
	}

	@RequestMapping(value = "/savePwd")
	public void savePwd(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		JSONObject rtnJson = new JSONObject();
		InfoOperBeanExt infoOperBean = InfoOperUtil.getCurrOperInfo(request);
		if (null == infoOperBean) {
			throw new Exception("没有工号登陆信息");
		}
		long employeeId = infoOperBean.getInfoEmployeeBean().getEmployee_id();
		String oldEmployeePwd = request.getParameter("OLD_EMPLOYEE_PWD");
		String newEmployeePwd = request.getParameter("NEW_EMPLOYEE_PWD");
		try {
			InfoEmployeeBean infoEmployeeBean = infoEmployeeServiceImpl.getBean(employeeId);
			if (null == infoEmployeeBean) {
				throw new Exception("该工号没有有效的人员信息");
			}
			if (!oldEmployeePwd.equals(infoEmployeeBean.getEmployee_pwd())) {
				throw new Exception("该工号密码不正确");
			}
			infoEmployeeServiceImpl.updateEmployeePwd(newEmployeePwd, employeeId);
			rtnJson.put(BusiConstants.AjaxReturn.STATUS_CODE,
					BusiConstants.AjaxStatus.STATUS_SUCCESS);
			rtnJson.put(BusiConstants.AjaxReturn.STATUS_INFO, "提交成功");
		} catch (Exception ex) {
			ex.printStackTrace();
			String retMsg = ex.getCause() == null ? ex.getMessage() : ex
					.getCause().getMessage();
			rtnJson = ExceptionUtil.toJSON(retMsg, ex, request);
		} finally {
			response.setContentType("text/html;charset=utf-8");
			PrintWriter printWriter = response.getWriter();
			printWriter.write(rtnJson.toString());
			printWriter.flush();
			printWriter.close();
		}
	}
	
	@RequestMapping(value = "/loginOut")
	public void loginOut(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		JSONObject rtnJson = new JSONObject();
		try {
			InfoOperUtil.removeCurrOperInfo(request);
			rtnJson.put(BusiConstants.AjaxReturn.STATUS_CODE,
					BusiConstants.AjaxStatus.STATUS_SUCCESS);
			rtnJson.put(BusiConstants.AjaxReturn.STATUS_INFO, "退出成功");
		} catch (Exception ex) {
			ex.printStackTrace();
			String retMsg = ex.getCause() == null ? ex.getMessage() : ex
					.getCause().getMessage();
			rtnJson = ExceptionUtil.toJSON(retMsg, ex, request);
		} finally {
			response.setContentType("text/html;charset=utf-8");
			PrintWriter printWriter = response.getWriter();
			printWriter.write(rtnJson.toString());
			printWriter.flush();
			printWriter.close();
		}
	}
}
