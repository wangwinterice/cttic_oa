package com.cttic.erp.controller;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cttic.erp.bean.InfoEmployeeBean;
import com.cttic.erp.bean.InfoProjectBean;
import com.cttic.erp.bean.InfoProjectEmployeeBean;
import com.cttic.erp.bean.SysParamBean;
import com.cttic.erp.common.constants.BusiConstants;
import com.cttic.erp.common.util.CacheUtil;
import com.cttic.erp.common.util.ExceptionUtil;
import com.cttic.erp.service.interfaces.IInfoEmployeeService;
import com.cttic.erp.service.interfaces.IInfoProjectEmployeeService;
import com.cttic.erp.service.interfaces.IInfoProjectService;

@Controller
@RequestMapping(value = "/project")
public class ProjectController {

	@Resource
	private IInfoProjectService infoProjectService;
	
	@Resource
	private IInfoProjectEmployeeService infoProjectEmployeeService;
	
	@Resource
	private IInfoEmployeeService infoEmployeeService;
	
	@RequestMapping(value = "/init")
	public ModelAndView init(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) throws Exception {
		List<SysParamBean> sysParamBeanList = getProjectState();
		modelMap.put("VACATION_TYPE", sysParamBeanList);
		List<InfoProjectBean> infoProjectBeanList = infoProjectService.getAllBeans();
		for (InfoProjectBean infoProjectBean : infoProjectBeanList) {
			for (SysParamBean sysParamBean : sysParamBeanList) {
				if (infoProjectBean.getState().equals(sysParamBean.getColumn_value())) {
					infoProjectBean.setState_desc(sysParamBean.getColumn_desc());
				}
			}
			InfoEmployeeBean employeeBean = infoEmployeeService.getBean(infoProjectBean.getManager_employee_id());
			String managerEmployeeName = "";
			if (null != employeeBean) {
				managerEmployeeName = employeeBean.getEmployee_name();
			}
			infoProjectBean.setManager_employee_name(managerEmployeeName);
			List<InfoProjectEmployeeBean> infoProjectEmployeeBeanList = new ArrayList<InfoProjectEmployeeBean>();
			if ("0".equals(infoProjectBean.getState())) {
				infoProjectEmployeeBeanList = infoProjectEmployeeService.getValidBeansByPrjId(infoProjectBean.getPrj_id());
			} else if ("1".equals(infoProjectBean.getState())) {
				infoProjectEmployeeBeanList = infoProjectEmployeeService.getBeansByPrjId(infoProjectBean.getPrj_id());
			}
			infoProjectBean.setInfoProjectEmployeeBeanList(infoProjectEmployeeBeanList);
			List<String> employeeNameList = new ArrayList<String>(); 
			for (InfoProjectEmployeeBean infoProjectEmployeeBean : infoProjectEmployeeBeanList) {
				employeeNameList.add(infoProjectEmployeeBean.getEmployee_name());
			}
			infoProjectBean.setEmployeeNameList(employeeNameList);
			infoProjectBean.setEmployeeNames(employeeNameList.size() > 0 ? employeeNameList.toString() : "");
		}
		modelMap.put("PROJECT_LIST", infoProjectBeanList);
		modelMap.put("EMPLOYEE_LIST", infoEmployeeService.getBeanList());
		return new ModelAndView("project/init", modelMap);
	}
	
	private List<SysParamBean> getProjectState() {
		return CacheUtil.getSysParamBeanList("PROJECT", "STATE");
	}
	
	@RequestMapping(value = "/save", method = { RequestMethod.POST })
	public void save(HttpServletRequest request,
			HttpServletResponse response, InfoProjectBean infoProjectBean) throws Exception {
		JSONObject rtnJson = new JSONObject();
		try {
			String operType = request.getParameter("OPER_TYPE");
			long managerEmployeeId = infoProjectBean.getManager_employee_id();
			List<InfoProjectEmployeeBean> infoProjectEmployeeBeanList = new ArrayList<InfoProjectEmployeeBean>();
			if (!"CLOSE".equals(operType)) {
				boolean ifAddManager = true;
				String signEmployee = request.getParameter("sign_employee");
				String[] signEmployees = signEmployee.split(",");
				if (signEmployees.length > 0) {
					for (String employeeId : signEmployees) {
						if (!StringUtils.isBlank(employeeId)) {
							if (managerEmployeeId == Long.parseLong(employeeId)) {
								ifAddManager = false;
							}
						}
					}
					if (ifAddManager == true) {
						InfoProjectEmployeeBean bean = new InfoProjectEmployeeBean();
						bean.setEmployee_id(managerEmployeeId);
						infoProjectEmployeeBeanList.add(bean);
					}
					for (String employeeId : signEmployees) {
						if (!StringUtils.isBlank(employeeId)) {
							InfoProjectEmployeeBean bean = new InfoProjectEmployeeBean();
							bean.setEmployee_id(Long.parseLong(employeeId));
							infoProjectEmployeeBeanList.add(bean);
						}
					}
				}
			}
			infoProjectService.synSave(operType, infoProjectBean, infoProjectEmployeeBeanList);
			// 3、获取页面提交信息
			rtnJson.put(BusiConstants.AjaxReturn.STATUS_CODE,
					BusiConstants.AjaxStatus.STATUS_SUCCESS);
			rtnJson.put(BusiConstants.AjaxReturn.STATUS_INFO, "提交成功");
		} catch (Exception ex) {
			ex.printStackTrace();
			String retMsg = ex.getCause() == null ? ex.getMessage() : ex
					.getCause().getMessage();
			rtnJson = ExceptionUtil.toJSON(retMsg, ex, request);
		} finally {
			response.setContentType("text/html;charset=utf-8");
			PrintWriter printWriter = response.getWriter();
			printWriter.write(rtnJson.toString());
			printWriter.flush();
			printWriter.close();
		}
	}
	
	@RequestMapping(value = "/get", method = { RequestMethod.POST })
	public void get(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		JSONObject rtnJson = new JSONObject();
		try {
			String prjId = request.getParameter("prj_id");
			InfoProjectBean infoProjectBean = infoProjectService.getBean(BigDecimal.valueOf(Long.parseLong(prjId)));
			InfoEmployeeBean infoEmployeeBean = infoEmployeeService.getBean(infoProjectBean.getManager_employee_id());
			String managerEmployeeName = "";
			if (null != infoEmployeeBean) {
				managerEmployeeName = infoEmployeeBean.getEmployee_name();
			}
			List<InfoProjectEmployeeBean> infoProjectEmployeeBeanList = infoProjectEmployeeService.getValidBeansByPrjId(infoProjectBean.getPrj_id());
			infoProjectBean.setInfoProjectEmployeeBeanList(infoProjectEmployeeBeanList);
			List<String> employeeNameList = new ArrayList<String>(); 
			for (InfoProjectEmployeeBean infoProjectEmployeeBean : infoProjectEmployeeBeanList) {
				employeeNameList.add(infoProjectEmployeeBean.getEmployee_name());
			}
			infoProjectBean.setEmployeeNameList(employeeNameList);
			infoProjectBean.setEmployeeNames(employeeNameList.size() > 0 ? employeeNameList.toString() : "");
			infoProjectBean.setManager_employee_name(managerEmployeeName);
			rtnJson.put(BusiConstants.AjaxReturn.STATUS_CODE,
					BusiConstants.AjaxStatus.STATUS_SUCCESS);
			rtnJson.put("RETURN_INFO", infoProjectBean);
		} catch (Exception ex) {
			ex.printStackTrace();
			String retMsg = ex.getCause() == null ? ex.getMessage() : ex
					.getCause().getMessage();
			rtnJson = ExceptionUtil.toJSON(retMsg, ex, request);
		} finally {
			response.setContentType("text/html;charset=utf-8");
			PrintWriter printWriter = response.getWriter();
			printWriter.write(rtnJson.toString());
			printWriter.flush();
			printWriter.close();
		}
	}
	
	@RequestMapping(value = "/getEmployee", method = { RequestMethod.POST })
	public void getEmployee(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		JSONObject rtnJson = new JSONObject();
		try {
			String prj_id = request.getParameter("prj_id");
			List<InfoProjectEmployeeBean> infoProjectEmployeeBeanList = new ArrayList<InfoProjectEmployeeBean>();
			List<InfoEmployeeBean> infoEmployeeBeanList = new ArrayList<InfoEmployeeBean>();
			if (!StringUtils.isBlank(prj_id)) {
				// 1、查询项目已有成员
				infoProjectEmployeeBeanList = infoProjectEmployeeService.getValidBeansByPrjId(BigDecimal.valueOf(Long.parseLong(prj_id)));
				infoEmployeeBeanList = infoProjectEmployeeService.getNoPrjEmployee(BigDecimal.valueOf(Long.parseLong(prj_id)));
			} else {
				infoEmployeeBeanList = infoEmployeeService.getBeanList();
			}
			rtnJson.put("SIGN_EMPLOYEE", infoProjectEmployeeBeanList);
			rtnJson.put(BusiConstants.AjaxReturn.STATUS_CODE,
					BusiConstants.AjaxStatus.STATUS_SUCCESS);
			rtnJson.put("UNSIGN_EMPLOYEE", infoEmployeeBeanList);
		} catch (Exception ex) {
			ex.printStackTrace();
			String retMsg = ex.getCause() == null ? ex.getMessage() : ex
					.getCause().getMessage();
			rtnJson = ExceptionUtil.toJSON(retMsg, ex, request);
		} finally {
			response.setContentType("text/html;charset=utf-8");
			PrintWriter printWriter = response.getWriter();
			printWriter.write(rtnJson.toString());
			printWriter.flush();
			printWriter.close();
		}
	}
}
