package com.cttic.erp.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.cttic.erp.bean.InfoEmployeeBean;
import com.cttic.erp.common.constants.BusiConstants;
import com.cttic.erp.common.util.ExceptionUtil;
import com.cttic.erp.common.util.MD5Util;
import com.cttic.erp.common.util.StringUtil;
import com.cttic.erp.service.interfaces.IInfoEmployeeService;

@Controller
@RequestMapping(value = "/employee")
public class EmployeeController {

	@Resource
	private IInfoEmployeeService iInfoEmployeeService;

	SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");

	@RequestMapping(value = "/init")
	public ModelAndView self(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) {
		return new ModelAndView("employee/init", modelMap);
	}

	@RequestMapping(value = "/employeequery")
	public void employeeQuery(HttpServletRequest request, HttpServletResponse response) throws IOException {
		JSONObject rtnJson = new JSONObject();
		try {
			String employeeNo = request.getParameter("employee_no");
			String employeeName = request.getParameter("employee_name");
			String conditions = "";
			if (!StringUtil.isBlank(employeeNo)) {
				conditions += " and employee_no = '" + employeeNo + "'";
			}
			if (!StringUtil.isBlank(employeeName)) {
				conditions += " and employee_name like '%" + employeeName + "%'";
			}

			List<InfoEmployeeBean> infoEmployeeBeans = iInfoEmployeeService.getBeanListByConditions(conditions);
			rtnJson.put(BusiConstants.AjaxReturn.STATUS_CODE, BusiConstants.AjaxStatus.STATUS_SUCCESS);
			rtnJson.put(BusiConstants.AjaxReturn.STATUS_INFO, "提交成功");
			rtnJson.put(BusiConstants.AjaxReturn.RETURN_INFO, infoEmployeeBeans);
		}
		catch (Exception ex) {
			ex.printStackTrace();
			String retMsg = ex.getCause() == null ? ex.getMessage() : ex.getCause().getMessage();
			rtnJson = ExceptionUtil.toJSON(retMsg, ex, request);
		}
		finally {
			response.setContentType("text/html;charset=utf-8");
			PrintWriter printWriter = response.getWriter();
			printWriter.write(rtnJson.toString());
			printWriter.flush();
			printWriter.close();
		}
	}

	@RequestMapping(value = "/edit")
	public ModelAndView edit(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) {
		String operType = request.getParameter("OPER_TYPE");
		if ("ADD".equals(operType)) {

		} else if ("EDIT".equals(operType)) {
			String employeeId = request.getParameter("employee_id");
			InfoEmployeeBean employeeBean = iInfoEmployeeService.getBean(Long.parseLong(employeeId));
			modelMap.put("employee", employeeBean);
		}
		modelMap.put("OPER_TYPE", operType);
		return new ModelAndView("employee/edit", modelMap);
	}

	@RequestMapping(value = "/save")
	public void save(HttpServletRequest request, HttpServletResponse response) throws IOException {
		JSONObject rtnJson = new JSONObject();
		try {
			String operType = request.getParameter("OPER_TYPE");
			String employeeNo = request.getParameter("employee_no");
			String employeeName = request.getParameter("employee_name");
			String mobilePhone = request.getParameter("mobile_phone");
			String email = request.getParameter("email");
			String inCompany = request.getParameter("in_company");
			String certNum = request.getParameter("cert_num");
			String birthday = request.getParameter("birthday");
			String address = request.getParameter("address");
			String zip = request.getParameter("zip");
			InfoEmployeeBean infoEmployeeBean = new InfoEmployeeBean();
			infoEmployeeBean.setEmployee_no(Long.parseLong(employeeNo));
			infoEmployeeBean.setEmployee_name(employeeName);
			infoEmployeeBean.setMobile_phone(mobilePhone);
			infoEmployeeBean.setEmail(email);
			infoEmployeeBean.setEmployee_pwd(MD5Util.encode("123456"));
			infoEmployeeBean.setIn_company(inCompany);
			infoEmployeeBean.setCert_num(certNum);
			infoEmployeeBean.setBirthday(birthday);
			infoEmployeeBean.setAddress(address);
			infoEmployeeBean.setZip(zip);
			if ("ADD".equals(operType)) {
				infoEmployeeBean.setEmployee_id(infoEmployeeBean.getEmployee_no());
				iInfoEmployeeService.insert(infoEmployeeBean);
			} else if ("EDIT".equals(operType)) {
				iInfoEmployeeService.update(infoEmployeeBean);
			}
			rtnJson.put(BusiConstants.AjaxReturn.STATUS_CODE, BusiConstants.AjaxStatus.STATUS_SUCCESS);
			rtnJson.put(BusiConstants.AjaxReturn.STATUS_INFO, "保存成功");
		}
		catch (Exception ex) {
			ex.printStackTrace();
			String retMsg = ex.getCause() == null ? ex.getMessage() : ex.getCause().getMessage();
			rtnJson = ExceptionUtil.toJSON(retMsg, ex, request);
		}
		finally {
			response.setContentType("text/html;charset=utf-8");
			PrintWriter printWriter = response.getWriter();
			printWriter.write(rtnJson.toString());
			printWriter.flush();
			printWriter.close();
		}
	}
}
