package com.cttic.erp.controller;

import java.io.File;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.cttic.erp.bean.BusiEvaluationRecordBean;
import com.cttic.erp.bean.BusiEvaluationResultBean;
import com.cttic.erp.bean.InfoDepartmentBean;
import com.cttic.erp.bean.InfoEmployeeBean;
import com.cttic.erp.bean.InfoEvaluationDimensionBean;
import com.cttic.erp.bean.InfoEvaluationKpiBean;
import com.cttic.erp.bean.SysParamBean;
import com.cttic.erp.bean.ext.BusiEvaluationBeanExt;
import com.cttic.erp.bean.ext.BusiEvaluationResultBeanExt;
import com.cttic.erp.bean.ext.InfoOperBeanExt;
import com.cttic.erp.common.comparator.ComparatorEvaluation;
import com.cttic.erp.common.constants.BusiConstants;
import com.cttic.erp.common.util.CacheUtil;
import com.cttic.erp.common.util.ExcelUtil;
import com.cttic.erp.common.util.ExceptionUtil;
import com.cttic.erp.common.util.InfoOperUtil;
import com.cttic.erp.common.util.StringUtil;
import com.cttic.erp.service.interfaces.IBusiEvaluationRecordService;
import com.cttic.erp.service.interfaces.IBusiEvaluationResultService;
import com.cttic.erp.service.interfaces.IBusiEvaluationService;
import com.cttic.erp.service.interfaces.IInfoDepartmentService;
import com.cttic.erp.service.interfaces.IInfoEmployeeService;
import com.cttic.erp.service.interfaces.IInfoEvaluationDimensionService;
import com.cttic.erp.service.interfaces.IInfoEvaluationKpiService;
import com.cttic.erp.service.interfaces.IMySQLDateService;

@Controller
@RequestMapping(value = "/evaluation")
public class EvaluationController {

	@Resource
	private IInfoEmployeeService infoEmployeeServiceImpl;

	@Resource
	private IInfoDepartmentService infoDepartmentServiceImpl;

	@Resource
	private IInfoEvaluationDimensionService infoEvaluationDimensionServiceImpl;

	@Resource
	private IInfoEvaluationKpiService infoEvaluationKpiServiceImpl;

	@Resource
	private IBusiEvaluationService busiEvaluationServiceImpl;

	@Resource
	private IBusiEvaluationRecordService busiEvaluationRecordServiceImpl;

	@Resource
	private IBusiEvaluationResultService busiEvaluationResultServiceImpl;

	@Resource
	private IMySQLDateService mySQLDateServiceImpl;

	private ComparatorEvaluation comparator;

	@RequestMapping(value = "/self")
	public ModelAndView self(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) {
		// 1、获取考核维度信息
		List<InfoEvaluationDimensionBean> infoEvaluationDimensionBeanList = infoEvaluationDimensionServiceImpl.getBeanList();
		modelMap.put("INFO_EVALUATION_DIMENSION", infoEvaluationDimensionBeanList);
		// 2、获取考核指标信息
//		List<InfoEvaluationKpiBean> infoEvaluationKpiBeanList = infoEvaluationKpiServiceImpl.getBeanList();
//		modelMap.put("INFO_EVALUATION_KPI", infoEvaluationKpiBeanList);
		// 3、获取考核考核全量信息
//		List<BusiEvaluationBeanExt> infoEvaluationBeanExtList = busiEvaluationServiceImpl.getEvaluationStandardList(10000000);
//		modelMap.put("EVALUATION_ITEM", infoEvaluationBeanExtList);
		// 4、获取当前登录工号对应的人员信息
		InfoOperBeanExt infoOperBean = InfoOperUtil.getCurrOperInfo(request);
		long employeeId = infoOperBean.getInfoEmployeeBean().getEmployee_id();
		modelMap.put("EVALUATION_EMPLOYEE", infoOperBean.getInfoEmployeeBean());
		// 5、获取考核期，默认取上个月的月份
		String evaluationDate = "";
		try {
			String evaluationModel = CacheUtil.getSysParamBeanListSingle("EVALUATION", "MODEL").getColumn_value();
			if ("0".equals(evaluationModel)) {
				evaluationDate = mySQLDateServiceImpl.getBeforeMonthToday();
			} else if ("1".equals(evaluationModel)) {
				evaluationDate = mySQLDateServiceImpl.getSysdateStr();
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		modelMap.put("EVALUATION_YEAR", evaluationDate.split("-")[0]);
		modelMap.put("EVALUATION_MONTH", evaluationDate.split("-")[1]);
		evaluationDate = evaluationDate.split("-")[0] + evaluationDate.split("-")[1];
		// 6、获取当前人员在考核期内的考核记录
		BusiEvaluationRecordBean busiEvaluationRecordBean = busiEvaluationRecordServiceImpl.getBean(employeeId, evaluationDate);
		modelMap.put("EVALUATION_RECORD", busiEvaluationRecordBean);
		if (null != busiEvaluationRecordBean) {
			List<BusiEvaluationResultBean> infoEvaluationResultBeanList = busiEvaluationResultServiceImpl.getBeanList(busiEvaluationRecordBean
			        .getRecord_id());
			modelMap.put("EVALUATION_RESULT", infoEvaluationResultBeanList);
		}
		return new ModelAndView("evaluation/self", modelMap);
	}

	@RequestMapping(value = "/selfSave")
	public void selfSave(HttpServletRequest request, HttpServletResponse response) throws Exception {
		JSONObject rtnJson = new JSONObject();
		try {
			// 1、获取当前操作员信息
			InfoOperBeanExt infoOperBean = InfoOperUtil.getCurrOperInfo(request);
			long employeeId = infoOperBean.getInfoEmployeeBean().getEmployee_id();
			// 2、获取页面操作标示
			String operType = request.getParameter("OPER_TYPE");
			BusiEvaluationRecordBean busiEvaluationRecordBean = new BusiEvaluationRecordBean();
			String recordId = "";
			// 2.1、根据操作类型进行判断：如果为'ADD'，则为新增操作
			if ("ADD".equals(operType)) {
				DateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
				String currTime = dateFormat.format(new Date());
				recordId = employeeId + currTime;
				busiEvaluationRecordBean.setRecord_id(Long.parseLong(recordId));
				busiEvaluationRecordBean.setEmployee_id(infoOperBean.getInfoEmployeeBean().getEmployee_id());
				busiEvaluationRecordBean.setEvaluation_date(request.getParameter("EVALUATION_DATE"));
				busiEvaluationRecordBean.setState("0");
			} else if ("EDIT".equals(operType)) {
				String evaluationDate = "";
				String evaluationModel = CacheUtil.getSysParamBeanListSingle("EVALUATION", "MODEL").getColumn_value();
				if ("0".equals(evaluationModel)) {
					evaluationDate = mySQLDateServiceImpl.getBeforeMonthToday();
				} else if ("1".equals(evaluationModel)) {
					evaluationDate = mySQLDateServiceImpl.getSysdateStr();
				}
				busiEvaluationRecordBean = busiEvaluationRecordServiceImpl.getBean(employeeId,
				                                                                   evaluationDate.split("-")[0] + evaluationDate.split("-")[1]);
				recordId = String.valueOf(busiEvaluationRecordBean.getRecord_id());
			}
			// 3、获取页面提交信息
			List<InfoEvaluationDimensionBean> infoEvaluationDimensionBeanList = infoEvaluationDimensionServiceImpl.getBeanList();
			List<BusiEvaluationResultBean> infoEvaluationResultBeanList = new ArrayList<BusiEvaluationResultBean>();
			BusiEvaluationResultBean infoEvaluationResultBean = new BusiEvaluationResultBean();
			for (InfoEvaluationDimensionBean infoEvaluationDimensionBean : infoEvaluationDimensionBeanList) {
				infoEvaluationResultBean = new BusiEvaluationResultBean();
				infoEvaluationResultBean.setRecord_id(Long.parseLong(recordId));
				infoEvaluationResultBean.setEvaluation_type("SELF_VALUE");
				infoEvaluationResultBean.setEvaluation_item(StringUtil.toString(infoEvaluationDimensionBean.getDimension_id()));
				infoEvaluationResultBean.setEvaluation_value(request.getParameter("SELF_VALUE_" + infoEvaluationDimensionBean.getDimension_id()));
				infoEvaluationResultBeanList.add(infoEvaluationResultBean);
			}
			// 4、计算总评分
			int totalValue = 0;
			for (BusiEvaluationResultBean bean : infoEvaluationResultBeanList) {
				totalValue += Integer.parseInt(bean.getEvaluation_value());
			}
			infoEvaluationResultBean = new BusiEvaluationResultBean();
			infoEvaluationResultBean.setRecord_id(Long.parseLong(recordId));
			infoEvaluationResultBean.setEvaluation_type("SELF_RESULT");
			infoEvaluationResultBean.setEvaluation_item("VALUE");
			infoEvaluationResultBean.setEvaluation_value(StringUtil.toString(totalValue));
			infoEvaluationResultBeanList.add(infoEvaluationResultBean);
			// 5、计算等级
			String level = "";
			if (totalValue >= 95) {
				level = "A";
			} else if (totalValue >= 85 && totalValue < 95) {
				level = "B";
			} else if (totalValue >= 75 && totalValue < 85) {
				level = "C";
			} else if (totalValue >= 60 && totalValue < 75) {
				level = "D";
			} else if (totalValue < 60) {
				level = "E";
			}
			infoEvaluationResultBean = new BusiEvaluationResultBean();
			infoEvaluationResultBean.setRecord_id(Long.parseLong(recordId));
			infoEvaluationResultBean.setEvaluation_type("SELF_RESULT");
			infoEvaluationResultBean.setEvaluation_item("LEVEL");
			infoEvaluationResultBean.setEvaluation_value(level);
			infoEvaluationResultBeanList.add(infoEvaluationResultBean);

			if ("ADD".equals(operType)) {
				busiEvaluationServiceImpl.saveNewSelfEvaluationResult(busiEvaluationRecordBean, infoEvaluationResultBeanList);
			} else if ("EDIT".equals(operType)) {
				busiEvaluationServiceImpl.saveEditSelfEvaluationResult(busiEvaluationRecordBean, infoEvaluationResultBeanList);
			}
			rtnJson.put(BusiConstants.AjaxReturn.STATUS_CODE, BusiConstants.AjaxStatus.STATUS_SUCCESS);
			rtnJson.put(BusiConstants.AjaxReturn.STATUS_INFO, "提交成功");
		}
		catch (Exception ex) {
			ex.printStackTrace();
			String retMsg = ex.getCause() == null ? ex.getMessage() : ex.getCause().getMessage();
			rtnJson = ExceptionUtil.toJSON(retMsg, ex, request);
		}
		finally {
			response.setContentType("text/html;charset=utf-8");
			PrintWriter printWriter = response.getWriter();
			printWriter.write(rtnJson.toString());
			printWriter.flush();
			printWriter.close();
		}
	}

	@RequestMapping(value = "/selfView")
	public ModelAndView selfView(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) {
		InfoOperBeanExt infoOperBean = InfoOperUtil.getCurrOperInfo(request);
		List<InfoEvaluationDimensionBean> infoEvaluationDimensionBeanList = infoEvaluationDimensionServiceImpl.getBeanList();
		List<InfoEvaluationKpiBean> infoEvaluationKpiBeanList = infoEvaluationKpiServiceImpl.getBeanList();
		List<BusiEvaluationBeanExt> infoEvaluationBeanExtList = busiEvaluationServiceImpl.getEvaluationStandardList(10000000);
		BusiEvaluationRecordBean busiEvaluationRecordBean = busiEvaluationRecordServiceImpl.getBean(infoOperBean.getInfoEmployeeBean()
		        .getEmployee_id(), request.getParameter("EVALUATION_YEAR") + request.getParameter("EVALUATION_MONTH"));
		if (null != busiEvaluationRecordBean) {
			List<BusiEvaluationResultBean> infoEvaluationResultBeanList = busiEvaluationResultServiceImpl.getBeanList(busiEvaluationRecordBean
			        .getRecord_id());
			modelMap.put("EVALUATION_RESULT", infoEvaluationResultBeanList);
		}
		modelMap.put("INFO_EVALUATION_DIMENSION", infoEvaluationDimensionBeanList);
		modelMap.put("INFO_EVALUATION_KPI", infoEvaluationKpiBeanList);
		modelMap.put("EVALUATION_ITEM", infoEvaluationBeanExtList);
		modelMap.put("EVALUATION_YEAR", request.getParameter("EVALUATION_YEAR"));
		modelMap.put("EVALUATION_MONTH", request.getParameter("EVALUATION_MONTH"));
		return new ModelAndView("evaluation/selfView", modelMap);
	}

	@RequestMapping(value = "/check")
	public ModelAndView check(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) {
		String employeeId = request.getParameter("EMPLOYEE_ID");
		String evaluationDate = "";
		try {
			String evaluationModel = CacheUtil.getSysParamBeanListSingle("EVALUATION", "MODEL").getColumn_value();
			if ("0".equals(evaluationModel)) {
				evaluationDate = mySQLDateServiceImpl.getBeforeMonthToday();
			} else if ("1".equals(evaluationModel)) {
				evaluationDate = mySQLDateServiceImpl.getSysdateStr();
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<InfoEvaluationDimensionBean> infoEvaluationDimensionBeanList = infoEvaluationDimensionServiceImpl.getBeanList();
//		List<InfoEvaluationKpiBean> infoEvaluationKpiBeanList = infoEvaluationKpiServiceImpl.getBeanList();
//		List<BusiEvaluationBeanExt> infoEvaluationBeanExtList = busiEvaluationServiceImpl.getEvaluationStandardList(10000000);
		modelMap.put("INFO_EVALUATION_DIMENSION", infoEvaluationDimensionBeanList);
//		modelMap.put("INFO_EVALUATION_KPI", infoEvaluationKpiBeanList);
//		modelMap.put("EVALUATION_ITEM", infoEvaluationBeanExtList);
		modelMap.put("EVALUATION_YEAR", evaluationDate.split("-")[0]);
		modelMap.put("EVALUATION_MONTH", evaluationDate.split("-")[1]);
		if (!StringUtil.isBlank(employeeId)) {
			BusiEvaluationRecordBean busiEvaluationRecordBean = busiEvaluationRecordServiceImpl.getBean(Long.parseLong(employeeId),
			                                                                                            evaluationDate.split("-")[0]
			                                                                                                    + evaluationDate.split("-")[1]);
			if (null != busiEvaluationRecordBean) {
				List<BusiEvaluationResultBean> infoEvaluationResultBeanList = busiEvaluationResultServiceImpl.getBeanList(busiEvaluationRecordBean
				        .getRecord_id());
				modelMap.put("EVALUATION_RECORD", busiEvaluationRecordBean);
				modelMap.put("EVALUATION_RESULT", infoEvaluationResultBeanList);
			}
			modelMap.put("EMPLOYEE_ID", employeeId);
			modelMap.put("SELECT_EMPLOYEE", infoEmployeeServiceImpl.getBean(Long.parseLong(employeeId)));
		}
		// 获取当前操作员
		InfoOperBeanExt infoOperBean = InfoOperUtil.getCurrOperInfo(request);
		// 将当前人员作为部门管理者
		long managerEmployeeId = infoOperBean.getInfoEmployeeBean().getEmployee_id();
		// 获取所管理部门
		InfoDepartmentBean infoDepartmentBean = infoDepartmentServiceImpl.getBeanByManagerEmployee(managerEmployeeId);
		if (null != infoDepartmentBean) {
			// 获取该部门下所有部门
			List<InfoDepartmentBean> infoDepartmentBeanList = getDeptTree(infoDepartmentBean.getDept_id());
			String conditions = " and dept_id in (" + infoDepartmentBean.getDept_id();
			for (InfoDepartmentBean bean : infoDepartmentBeanList) {
				conditions += ", " + bean.getDept_id();
			}
			conditions += ")";
			List<InfoEmployeeBean> infoEmployeeBeanList = infoEmployeeServiceImpl.getBeanListByConditions(conditions);
			modelMap.put("EMPLOYEE_LIST", infoEmployeeBeanList);
		}
		return new ModelAndView("evaluation/check", modelMap);
	}

	private List<InfoDepartmentBean> getDeptTree(long root) {
		List<InfoDepartmentBean> infoDepartmentBeanList = new ArrayList<InfoDepartmentBean>();

		List<InfoDepartmentBean> _list = infoDepartmentServiceImpl.getChildBeanByDeptId(root);
		if (_list.size() > 0) {
			infoDepartmentBeanList.addAll(_list);
			for (int i = 0; i < _list.size(); i++) {
				List<InfoDepartmentBean> __list = getDeptTree(_list.get(i).getDept_id());
				if (__list.size() > 0) {
					infoDepartmentBeanList.addAll(__list);
				}
			}
		}
		return infoDepartmentBeanList;
	}

	@RequestMapping(value = "/checkSave")
	public void checkSave(HttpServletRequest request, HttpServletResponse response) throws Exception {
		JSONObject rtnJson = new JSONObject();
		try {
			// 获取个人考核结果
			BusiEvaluationRecordBean busiEvaluationRecordBean = busiEvaluationRecordServiceImpl.getBean(Integer.parseInt(request
			        .getParameter("EMPLOYEE_ID")), request.getParameter("EVALUATION_DATE"));
			// 更新考核记录
			List<InfoEvaluationDimensionBean> infoEvaluationDimensionBeanList = infoEvaluationDimensionServiceImpl.getBeanList();
			busiEvaluationRecordBean.setState("1");
			busiEvaluationRecordBean.setResult_description(request.getParameter("RESULT_DESCRIPTION").replaceAll("\n", "<br>"));
			// 增加上级考核结果
			List<BusiEvaluationResultBean> infoEvaluationResultBeanList = new ArrayList<BusiEvaluationResultBean>();
			BusiEvaluationResultBean infoEvaluationResultBean = new BusiEvaluationResultBean();
			for (InfoEvaluationDimensionBean infoEvaluationDimensionBean : infoEvaluationDimensionBeanList) {
				infoEvaluationResultBean = new BusiEvaluationResultBean();
				infoEvaluationResultBean.setRecord_id(busiEvaluationRecordBean.getRecord_id());
				infoEvaluationResultBean.setEvaluation_type("CHECK_VALUE");
				infoEvaluationResultBean.setEvaluation_item(StringUtil.toString(infoEvaluationDimensionBean.getDimension_id()));
				infoEvaluationResultBean.setEvaluation_value(request.getParameter("CHECK_VALUE_" + infoEvaluationDimensionBean.getDimension_id()));
				infoEvaluationResultBeanList.add(infoEvaluationResultBean);
			}
			// 计算总评分
			int totalValue = 0;
			for (BusiEvaluationResultBean bean : infoEvaluationResultBeanList) {
				totalValue += Integer.parseInt(bean.getEvaluation_value());
			}
			infoEvaluationResultBean = new BusiEvaluationResultBean();
			infoEvaluationResultBean.setRecord_id(busiEvaluationRecordBean.getRecord_id());
			infoEvaluationResultBean.setEvaluation_type("CHECK_RESULT");
			infoEvaluationResultBean.setEvaluation_item("VALUE");
			infoEvaluationResultBean.setEvaluation_value(StringUtil.toString(totalValue));
			infoEvaluationResultBeanList.add(infoEvaluationResultBean);
			// 计算等级
			String level = "";
			if (totalValue >= 95) {
				level = "A";
			} else if (totalValue >= 85 && totalValue < 95) {
				level = "B";
			} else if (totalValue >= 75 && totalValue < 85) {
				level = "C";
			} else if (totalValue >= 60 && totalValue < 75) {
				level = "D";
			} else if (totalValue < 60) {
				level = "E";
			}
			infoEvaluationResultBean = new BusiEvaluationResultBean();
			infoEvaluationResultBean.setRecord_id(busiEvaluationRecordBean.getRecord_id());
			infoEvaluationResultBean.setEvaluation_type("CHECK_RESULT");
			infoEvaluationResultBean.setEvaluation_item("LEVEL");
			infoEvaluationResultBean.setEvaluation_value(level);
			infoEvaluationResultBeanList.add(infoEvaluationResultBean);
			busiEvaluationServiceImpl.saveCheckEvaluationResult(busiEvaluationRecordBean, infoEvaluationResultBeanList);
			rtnJson.put(BusiConstants.AjaxReturn.STATUS_CODE, BusiConstants.AjaxStatus.STATUS_SUCCESS);
			rtnJson.put(BusiConstants.AjaxReturn.STATUS_INFO, "提交成功");
		}
		catch (Exception ex) {
			ex.printStackTrace();
			String retMsg = ex.getCause() == null ? ex.getMessage() : ex.getCause().getMessage();
			rtnJson = ExceptionUtil.toJSON(retMsg, ex, request);
		}
		finally {
			response.setContentType("text/html;charset=utf-8");
			PrintWriter printWriter = response.getWriter();
			printWriter.write(rtnJson.toString());
			printWriter.flush();
			printWriter.close();
		}
	}

	@RequestMapping(value = "/checkView")
	public ModelAndView checkView(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) {
		String employeeId = request.getParameter("EMPLOYEE_ID");
		List<InfoEvaluationDimensionBean> infoEvaluationDimensionBeanList = infoEvaluationDimensionServiceImpl.getBeanList();
		List<InfoEvaluationKpiBean> infoEvaluationKpiBeanList = infoEvaluationKpiServiceImpl.getBeanList();
		List<BusiEvaluationBeanExt> infoEvaluationBeanExtList = busiEvaluationServiceImpl.getEvaluationStandardList(10000000);
		BusiEvaluationRecordBean busiEvaluationRecordBean = busiEvaluationRecordServiceImpl
		        .getBean(Long.parseLong(employeeId), request.getParameter("EVALUATION_YEAR") + request.getParameter("EVALUATION_MONTH"));
		if (null != busiEvaluationRecordBean) {
			List<BusiEvaluationResultBean> infoEvaluationResultBeanList = busiEvaluationResultServiceImpl.getBeanList(busiEvaluationRecordBean
			        .getRecord_id());
			modelMap.put("EVALUATION_RECORD", busiEvaluationRecordBean);
			modelMap.put("EVALUATION_RESULT", infoEvaluationResultBeanList);
		}
		modelMap.put("INFO_EVALUATION_DIMENSION", infoEvaluationDimensionBeanList);
		modelMap.put("INFO_EVALUATION_KPI", infoEvaluationKpiBeanList);
		modelMap.put("EVALUATION_ITEM", infoEvaluationBeanExtList);
		modelMap.put("EVALUATION_YEAR", request.getParameter("EVALUATION_YEAR"));
		modelMap.put("EVALUATION_MONTH", request.getParameter("EVALUATION_MONTH"));
		// 获取当前操作员
		InfoOperBeanExt infoOperBean = InfoOperUtil.getCurrOperInfo(request);
		// 将当前人员作为部门管理者
		long managerEmployeeId = infoOperBean.getInfoEmployeeBean().getEmployee_id();
		// 获取所管理部门
		InfoDepartmentBean infoDepartmentBean = infoDepartmentServiceImpl.getBeanByManagerEmployee(managerEmployeeId);
		if (null != infoDepartmentBean) {
			// 获取该部门下所有部门
			List<InfoDepartmentBean> infoDepartmentBeanList = getDeptTree(infoDepartmentBean.getDept_id());
			String conditions = " and dept_id in (0";
			for (InfoDepartmentBean bean : infoDepartmentBeanList) {
				conditions += ", " + bean.getDept_id();
			}
			conditions += ")";
			List<InfoEmployeeBean> infoEmployeeBeanList = infoEmployeeServiceImpl.getBeanListByConditions(conditions);
			modelMap.put("EMPLOYEE_LIST", infoEmployeeBeanList);
		}
		modelMap.put("EMPLOYEE_ID", employeeId);
		modelMap.put("SELECT_EMPLOYEE", infoEmployeeServiceImpl.getBean(Long.parseLong(employeeId)));
		return new ModelAndView("evaluation/checkView", modelMap);
	}

	@RequestMapping(value = "/signView")
	public ModelAndView signView(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) {
		InfoOperBeanExt infoOperBean = InfoOperUtil.getCurrOperInfo(request);
		List<InfoEvaluationDimensionBean> infoEvaluationDimensionBeanList = infoEvaluationDimensionServiceImpl.getBeanList();
//		List<InfoEvaluationKpiBean> infoEvaluationKpiBeanList = infoEvaluationKpiServiceImpl.getBeanList();
//		List<BusiEvaluationBeanExt> infoEvaluationBeanExtList = busiEvaluationServiceImpl.getEvaluationStandardList(10000000);
		String evaluationDate = request.getParameter("EVALUATION_DATE");
		if (StringUtil.isBlank(evaluationDate)) {
			try {
				String evaluationModel = CacheUtil.getSysParamBeanListSingle("EVALUATION", "MODEL").getColumn_value();
				if ("0".equals(evaluationModel)) {
					evaluationDate = mySQLDateServiceImpl.getBeforeMonthToday();
				} else if ("1".equals(evaluationModel)) {
					evaluationDate = mySQLDateServiceImpl.getSysdateStr();
				}
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			evaluationDate = evaluationDate.split("-")[0] + evaluationDate.split("-")[1];
		}
		BusiEvaluationRecordBean busiEvaluationRecordBean = busiEvaluationRecordServiceImpl.getBean(infoOperBean.getInfoEmployeeBean()
		        .getEmployee_id(), evaluationDate);
		if (null != busiEvaluationRecordBean) {
			List<BusiEvaluationResultBean> infoEvaluationResultBeanList = busiEvaluationResultServiceImpl.getBeanList(busiEvaluationRecordBean
			        .getRecord_id());
			modelMap.put("EVALUATION_RECORD", busiEvaluationRecordBean);
			modelMap.put("EVALUATION_RESULT", infoEvaluationResultBeanList);
		}
		modelMap.put("INFO_EVALUATION_DIMENSION", infoEvaluationDimensionBeanList);
//		modelMap.put("INFO_EVALUATION_KPI", infoEvaluationKpiBeanList);
//		modelMap.put("EVALUATION_ITEM", infoEvaluationBeanExtList);
		modelMap.put("EVALUATION_DATE", evaluationDate);
		modelMap.put("EVALUATION_DATE_LIST", getEvaluationDateList());
		return new ModelAndView("evaluation/signView", modelMap);
	}

	@RequestMapping(value = "/batchView")
	public ModelAndView batchView(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) {
		List<InfoEmployeeBean> infoEmployeeBeanList = infoEmployeeServiceImpl.getBeanList();
		List<InfoEvaluationDimensionBean> infoEvaluationDimensionBeanList = infoEvaluationDimensionServiceImpl.getBeanList();
		List<BusiEvaluationBeanExt> infoEvaluationBeanExtList = busiEvaluationServiceImpl.getEvaluationKpiList(10000000);
		String evaluationDate = request.getParameter("EVALUATION_DATE");
		if (StringUtil.isBlank(evaluationDate)) {
			try {
				String evaluationModel = CacheUtil.getSysParamBeanListSingle("EVALUATION", "MODEL").getColumn_value();
				if ("0".equals(evaluationModel)) {
					evaluationDate = mySQLDateServiceImpl.getBeforeMonthToday();
				} else if ("1".equals(evaluationModel)) {
					evaluationDate = mySQLDateServiceImpl.getSysdateStr();
				}
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			evaluationDate = evaluationDate.split("-")[0] + evaluationDate.split("-")[1];
		}
		List<BusiEvaluationResultBeanExt> busiEvaluationResultBeanExtList = busiEvaluationResultServiceImpl.getBeanList(evaluationDate);
		modelMap.put("EMPLOYEE_LIST", infoEmployeeBeanList);
		modelMap.put("INFO_EVALUATION_DIMENSION", infoEvaluationDimensionBeanList);
		modelMap.put("EVALUATION_ITEM", infoEvaluationBeanExtList);
		modelMap.put("EVALUATION_RESULT", busiEvaluationResultBeanExtList);
		modelMap.put("EVALUATION_DATE", evaluationDate);
		modelMap.put("EVALUATION_DATE_LIST", getEvaluationDateList());
		return new ModelAndView("evaluation/batchView", modelMap);
	}

	@RequestMapping(value = "/state")
	public ModelAndView state(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) {
		modelMap.put("EVALUATION_DATE_LIST", getEvaluationDateList());
		return new ModelAndView("evaluation/state", modelMap);
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/stateQuery")
	public void stateQuery(HttpServletRequest request, HttpServletResponse response) throws Exception {
		JSONObject rtnJson = new JSONObject();
		try {
			String sort = request.getParameter("sort");
			String order = request.getParameter("order");
			List<InfoEmployeeBean> infoEmployeeBeanList = infoEmployeeServiceImpl.getBeanList();
			List<BusiEvaluationResultBeanExt> busiEvaluationResultBeanExtList = busiEvaluationResultServiceImpl.getEvaluationState();
			List<JSONObject> jsonList = new ArrayList<JSONObject>();
			JSONObject jsonObject = new JSONObject();
			for (InfoEmployeeBean infoEmployeeBean : infoEmployeeBeanList) {
				long employeeId = infoEmployeeBean.getEmployee_id();
				jsonObject = new JSONObject();
				jsonObject.put("employee_id", employeeId);
				jsonObject.put("employee_name", infoEmployeeBean.getEmployee_name());
				for (BusiEvaluationResultBeanExt busiEvaluationResultBeanExt : busiEvaluationResultBeanExtList) {
					if (employeeId == busiEvaluationResultBeanExt.getEmployee_id()
					        && "SELF_RESULT".equals(busiEvaluationResultBeanExt.getEvaluation_type())
					        && "VALUE".equals(busiEvaluationResultBeanExt.getEvaluation_item())) {
						jsonObject.put(busiEvaluationResultBeanExt.getEvaluation_date() + "_SELF_RESULT_VALUE",
						               busiEvaluationResultBeanExt.getEvaluation_value());
					}
					if (employeeId == busiEvaluationResultBeanExt.getEmployee_id()
					        && "CHECK_RESULT".equals(busiEvaluationResultBeanExt.getEvaluation_type())
					        && "VALUE".equals(busiEvaluationResultBeanExt.getEvaluation_item())) {
						jsonObject.put(busiEvaluationResultBeanExt.getEvaluation_date() + "_CHECK_RESULT_VALUE",
						               busiEvaluationResultBeanExt.getEvaluation_value());
					}
					if (employeeId == busiEvaluationResultBeanExt.getEmployee_id()
					        && "CHECK_RESULT".equals(busiEvaluationResultBeanExt.getEvaluation_type())
					        && "LEVEL".equals(busiEvaluationResultBeanExt.getEvaluation_item())) {
						jsonObject.put(busiEvaluationResultBeanExt.getEvaluation_date() + "_CHECK_RESULT_LEVEL",
						               busiEvaluationResultBeanExt.getEvaluation_value());
					}
				}
				jsonList.add(jsonObject);
			}
			setComparator(new ComparatorEvaluation());
			getComparator().setJsonKey(sort);
			getComparator().setOrder(order);
			Collections.sort(jsonList, getComparator());
			rtnJson.put(BusiConstants.AjaxReturn.STATUS_CODE, BusiConstants.AjaxStatus.STATUS_SUCCESS);
			rtnJson.put(BusiConstants.AjaxReturn.STATUS_INFO, "提交成功");
			rtnJson.put(BusiConstants.AjaxReturn.RETURN_INFO, jsonList.toString());
		}
		catch (Exception ex) {
			ex.printStackTrace();
			String retMsg = ex.getCause() == null ? ex.getMessage() : ex.getCause().getMessage();
			rtnJson = ExceptionUtil.toJSON(retMsg, ex, request);
		}
		finally {
			response.setContentType("text/html;charset=utf-8");
			PrintWriter printWriter = response.getWriter();
			printWriter.write(rtnJson.toString());
			printWriter.flush();
			printWriter.close();
		}
	}

	@RequestMapping(value = "/generatorExcel")
	public void generatorExcel(HttpServletRequest request, HttpServletResponse response) throws Exception {
		JSONObject json = new JSONObject();
		try {
			List<InfoEmployeeBean> infoEmployeeBeanList = infoEmployeeServiceImpl.getBeanList();
			List<BusiEvaluationResultBeanExt> busiEvaluationResultBeanExtList = busiEvaluationResultServiceImpl.getEvaluationState();
			List<JSONObject> jsonList = new ArrayList<JSONObject>();
			JSONObject jsonObject = new JSONObject();
			for (InfoEmployeeBean infoEmployeeBean : infoEmployeeBeanList) {
				long employeeId = infoEmployeeBean.getEmployee_id();
				jsonObject = new JSONObject();
				jsonObject.put("employee_id", employeeId);
				jsonObject.put("employee_name", infoEmployeeBean.getEmployee_name());
				for (BusiEvaluationResultBeanExt busiEvaluationResultBeanExt : busiEvaluationResultBeanExtList) {
					if (employeeId == busiEvaluationResultBeanExt.getEmployee_id()
					        && "SELF_RESULT".equals(busiEvaluationResultBeanExt.getEvaluation_type())
					        && "VALUE".equals(busiEvaluationResultBeanExt.getEvaluation_item())) {
						jsonObject.put(busiEvaluationResultBeanExt.getEvaluation_date() + "_SELF_RESULT_VALUE",
						               busiEvaluationResultBeanExt.getEvaluation_value());
					}
					if (employeeId == busiEvaluationResultBeanExt.getEmployee_id()
					        && "CHECK_RESULT".equals(busiEvaluationResultBeanExt.getEvaluation_type())
					        && "VALUE".equals(busiEvaluationResultBeanExt.getEvaluation_item())) {
						jsonObject.put(busiEvaluationResultBeanExt.getEvaluation_date() + "_CHECK_RESULT_VALUE",
						               busiEvaluationResultBeanExt.getEvaluation_value());
					}
					if (employeeId == busiEvaluationResultBeanExt.getEmployee_id()
					        && "CHECK_RESULT".equals(busiEvaluationResultBeanExt.getEvaluation_type())
					        && "LEVEL".equals(busiEvaluationResultBeanExt.getEvaluation_item())) {
						jsonObject.put(busiEvaluationResultBeanExt.getEvaluation_date() + "_CHECK_RESULT_LEVEL",
						               busiEvaluationResultBeanExt.getEvaluation_value());
					}
				}
				jsonList.add(jsonObject);
			}
			String outFileName = new String("test");
			File targetFile = new File(outFileName);
			WritableWorkbook target = Workbook.createWorkbook(targetFile);
			WritableSheet ws = target.createSheet("绩效考核结果", 0);
			ws.mergeCells(0, 0, 0, 1);
			ws.mergeCells(1, 0, 1, 1);
			Label label;
			WritableFont wfc = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
			WritableCellFormat wcfFormat = new WritableCellFormat(wfc);
			wcfFormat.setAlignment(Alignment.CENTRE); // 设置对齐方式
			wcfFormat.setVerticalAlignment(VerticalAlignment.CENTRE);// 设置对齐方式
			label = new Label(0, 0, "工号", wcfFormat);
			ws.addCell(label);
			label = new Label(1, 0, "姓名", wcfFormat);
			ws.addCell(label);

			List<SysParamBean> sysParamList = getEvaluationDateList();
			for (int count = 0; count < sysParamList.size(); count++) {
				ws.mergeCells(2 + count * 3, 0, (2 + count * 3) + 2, 0);
				label = new Label(2 + count * 3, 0, sysParamList.get(count).getColumn_desc(), wcfFormat);
				ws.addCell(label);
				label = new Label(2 + count * 3, 1, "自评", wcfFormat);
				ws.addCell(label);
				label = new Label((2 + count * 3) + 1, 1, "考核成绩", wcfFormat);
				ws.addCell(label);
				label = new Label((2 + count * 3) + 2, 1, "考核等级", wcfFormat);
				ws.addCell(label);
			}

			for (int row = 0; row < jsonList.size(); row++) {
				JSONObject _json = jsonList.get(row);
				label = new Label(0, row + 2, _json.getString("employee_id"), wcfFormat);
				ws.addCell(label);
				label = new Label(1, row + 2, _json.getString("employee_name"), wcfFormat);
				ws.addCell(label);
				for (int count = 0; count < sysParamList.size(); count++) {
					String date = sysParamList.get(count).getColumn_value();
					String selfValue = _json.has(date + "_SELF_RESULT_VALUE") ? _json.getString(date + "_SELF_RESULT_VALUE") : "";
					String checkValue = _json.has(date + "_CHECK_RESULT_VALUE") ? _json.getString(date + "_CHECK_RESULT_VALUE") : "";
					String checkLevel = _json.has(date + "_CHECK_RESULT_LEVEL") ? _json.getString(date + "_CHECK_RESULT_LEVEL") : "";
					label = new Label(2 + count * 3, row + 2, selfValue, wcfFormat);
					ws.addCell(label);
					label = new Label((2 + count * 3) + 1, row + 2, checkValue, wcfFormat);
					ws.addCell(label);
					label = new Label((2 + count * 3) + 2, row + 2, checkLevel, wcfFormat);
					ws.addCell(label);
				}
			}

			wcfFormat = new WritableCellFormat();
			wcfFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
			// 关闭文件
			target.write();
			target.close();
			json.put(BusiConstants.AjaxReturn.STATUS_CODE, BusiConstants.AjaxStatus.STATUS_SUCCESS);
			json.put(BusiConstants.AjaxReturn.STATUS_INFO, "Excel生成成功");
			json.put(BusiConstants.AjaxReturn.RETURN_INFO, outFileName);
		}
		catch (Exception e) {
			String retMsg = e.getCause() == null ? e.getMessage() : e.getCause().getMessage();
			retMsg = StringUtil.isBlank(retMsg) ? "Excel生成失败" : retMsg;
			json = ExceptionUtil.toJSON(retMsg, e, request);
		}
		finally {
			response.setContentType("text/html;charset=utf-8");
			PrintWriter printWriter = response.getWriter();
			printWriter.write(json.toString());
			printWriter.flush();
			printWriter.close();
		}
	}

	@RequestMapping(value = "/downloadExcel")
	public void downloadExcel(HttpServletRequest request, HttpServletResponse response) throws Exception {
		JSONObject json = new JSONObject();
		try {
			// 下载Excel文件
			String rometeFileName = request.getParameter("_FILE_NAME");
			String localFileName = "人员评估结果统计表.xls";
			ExcelUtil.download(response, localFileName, "", rometeFileName);
			json.put(BusiConstants.AjaxReturn.STATUS_CODE, BusiConstants.AjaxStatus.STATUS_SUCCESS);
			json.put(BusiConstants.AjaxReturn.STATUS_INFO, "Excel下载成功");
			json.put(BusiConstants.AjaxReturn.RETURN_INFO, localFileName);
		}
		catch (Exception e) {
			String retMsg = e.getCause() == null ? e.getMessage() : e.getCause().getMessage();
			retMsg = StringUtil.isBlank(retMsg) ? "Excel下载失败" : retMsg;
			json = ExceptionUtil.toJSON(retMsg, e, request);
		}
		finally {
			response.setContentType("text/html;charset=utf-8");
			PrintWriter printWriter = response.getWriter();
			printWriter.write(json.toString());
			printWriter.flush();
			printWriter.close();
		}
	}

	private List<SysParamBean> getEvaluationDateList() {
		List<SysParamBean> sysParamBeanList = new ArrayList<SysParamBean>();
		SysParamBean sysParamBean = new SysParamBean();
		String initYearStr = "2016";
		String initMonthStr = "01";
		int initYear = Integer.parseInt(initYearStr);
		int initMonth = Integer.parseInt(initMonthStr);
		String currDate = "";
		try {
			currDate = mySQLDateServiceImpl.getSysdateStr();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String currYearStr = currDate.split("-")[0];
		String currMonthStr = currDate.split("-")[1];
		int currYear = Integer.parseInt(currYearStr);
		int currMonth = Integer.parseInt(currMonthStr);
		for (int year = currYear; year >= initYear; year--) {
			for (int month = 12; month >= 1; month--) {
				if (year == initYear) {
					if (month < initMonth) {
						continue;
					}
				}
				if (year == currYear) {
					if (month > currMonth) {
						continue;
					}
				}
				sysParamBean = new SysParamBean();
				sysParamBean.setType_code("EVALUATION");
				sysParamBean.setParam_code("DATE_LIST");
				String monthStr = "";
				if (String.valueOf(month).length() == 1) {
					monthStr = "0" + month;
				} else {
					monthStr = "" + month;
				}
				sysParamBean.setColumn_value(year + monthStr);
				sysParamBean.setColumn_desc(year + "年" + monthStr + "月");
				sysParamBeanList.add(sysParamBean);
			}
		}
		return sysParamBeanList;
	}

	public ComparatorEvaluation getComparator() {
		return comparator;
	}

	public void setComparator(ComparatorEvaluation comparator) {
		this.comparator = comparator;
	}
}
