package com.cttic.erp.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cttic.erp.bean.InfoEmployeeBean;
import com.cttic.erp.dao.interfaces.IInfoEmployeeDAO;
import com.cttic.erp.mapper.InfoEmployeeMapper;

@Repository
public class InfoEmployeeDAOImpl implements IInfoEmployeeDAO {

	@Autowired
	private InfoEmployeeMapper infoEmployeeMapper;

	@Override
	public List<InfoEmployeeBean> getBeanList() {
		return infoEmployeeMapper.getBeanList();
	}

	@Override
	public List<InfoEmployeeBean> getBeanListByDept(long dept_id) {
		return infoEmployeeMapper.getBeanListByDept(dept_id);
	}

	@Override
	public InfoEmployeeBean getBean(long employee_no) {
		return infoEmployeeMapper.getBean(employee_no);
	}

	@Override
	public void updateEmployeePwd(String employee_pwd, long employee_id) {
		infoEmployeeMapper.updateEmployeePwd(employee_pwd, employee_id);
	}

	@Override
	public void insert(InfoEmployeeBean infoEmployeeBean) {
		infoEmployeeMapper.insert(infoEmployeeBean);
	}

	@Override
	public void update(InfoEmployeeBean infoEmployeeBean) {
		infoEmployeeMapper.update(infoEmployeeBean);
	}

	@Override
	public List<InfoEmployeeBean> getBeanListByConditions(String conditions) {
		return infoEmployeeMapper.getBeanListByConditions(conditions);
	}
}
