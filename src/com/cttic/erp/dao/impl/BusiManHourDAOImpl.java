package com.cttic.erp.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cttic.erp.bean.BusiManHourBean;
import com.cttic.erp.bean.InfoProjectBean;
import com.cttic.erp.dao.interfaces.IBusiManHourDAO;
import com.cttic.erp.mapper.BusiManHourMapper;

@Repository
public class BusiManHourDAOImpl implements
IBusiManHourDAO {

	@Autowired
	private BusiManHourMapper busiManHourMapper;

	@Override
	public List<BusiManHourBean> getBeans(long employee_id) {
		return busiManHourMapper.getBeans(employee_id);
	}

	@Override
	public void insertBean(BusiManHourBean busiManHourBean) {
		busiManHourMapper.insertBean(busiManHourBean);
	}

	@Override
	public int delete(long id) {
		return busiManHourMapper.delete(id);
	}

	@Override
	public List<Map<String, String>> getPeriod(String startDate) {
		return busiManHourMapper.getPeriod(startDate);
	}

	@Override
	public List<BusiManHourBean> getBeansById(long id) {
		return busiManHourMapper.getBeansById(id);
	}

	@Override
	public List<InfoProjectBean> getProjectsByWeekId(String week_id) {
		return busiManHourMapper.getProjectsByWeekId(week_id);
	}

	@Override
	public String getRealEndDateByWeekId(String week_id) {
		return busiManHourMapper.getRealEndDateByWeekId(week_id);
	}

	@Override
	public String getRealBeginDateByWeekId(String week_id) {
		return busiManHourMapper.getRealBeginDateByWeekId(week_id);
	}

	@Override
	public List<BusiManHourBean> getApprovalBeans() {
		return busiManHourMapper.getApprovalBeans();
	}

	@Override
	public List<BusiManHourBean> getBeanByWeekId(String record_id) {
		return busiManHourMapper.getBeanByWeekId(record_id);
	}

	@Override
	public List<BusiManHourBean> getAllBeans(long employee_id) {
		return busiManHourMapper.getAllBeans(employee_id);
	}

	@Override
	public List<Map<String, String>> findYears() {
		return busiManHourMapper.findYears();
	}

	@Override
	public List<Map<String, String>> getWeekByYear(String year) {
		return busiManHourMapper.getWeekByYear("%"+year+"%");
	}

	@Override
	public List<BusiManHourBean> getApprovalBeansByEndDate(String end_date) {
		return busiManHourMapper.getApprovalBeansByEndDate(end_date);
	}

	@Override
	public void approval(long id) {
		busiManHourMapper.approval(id);
	}

}
