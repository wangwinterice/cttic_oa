package com.cttic.erp.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cttic.erp.bean.BusiEmployeeVacationRecordBean;
import com.cttic.erp.dao.interfaces.IBusiEmployeeVacationRecordDAO;
import com.cttic.erp.mapper.BusiEmployeeVacationRecordMapper;

@Repository
public class BusiEmployeeVacationRecordDAOImpl implements
		IBusiEmployeeVacationRecordDAO {

	@Autowired
	private BusiEmployeeVacationRecordMapper busiEmployeeVacationRecordMapper;
	
	@Override
	public List<BusiEmployeeVacationRecordBean> getBeansByEmployeeId(long employee_id) {
		return busiEmployeeVacationRecordMapper.getBeansByEmployeeId(employee_id);
	}
	
	@Override
	public BusiEmployeeVacationRecordBean getBeansByRecordId(long record_id) {
		return busiEmployeeVacationRecordMapper.getBeansByRecordId(record_id);
	}
	
	@Override
	public List<BusiEmployeeVacationRecordBean> getBeans(long employee_id, String reportMonth) {
		return busiEmployeeVacationRecordMapper.getBeans(employee_id, reportMonth);
	}
	
	@Override
	public List<BusiEmployeeVacationRecordBean> getBeansByReportMonth(String reportMonth) {
		return busiEmployeeVacationRecordMapper.getBeansByReportMonth(reportMonth);
	}
	
	@Override
	public BusiEmployeeVacationRecordBean getBean(long employee_id, String beginDate) {
		return busiEmployeeVacationRecordMapper.getBean(employee_id, beginDate);
	}
	
	@Override
	public List<BusiEmployeeVacationRecordBean> getBeansByState(String state) {
		return busiEmployeeVacationRecordMapper.getBeansByState(state);
	}

	@Override
	public void insert(BusiEmployeeVacationRecordBean bean) {
		busiEmployeeVacationRecordMapper.insert(bean);
	}

	@Override
	public void updateCheck(BusiEmployeeVacationRecordBean bean) {
		busiEmployeeVacationRecordMapper.updateCheck(bean);
	}

	@Override
	public void updateRecord(BusiEmployeeVacationRecordBean bean) {
		busiEmployeeVacationRecordMapper.updateRecord(bean);
	}
	
	@Override
	public void cancel(BusiEmployeeVacationRecordBean bean) {
		busiEmployeeVacationRecordMapper.cancel(bean);
	}
}
