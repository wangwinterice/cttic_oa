package com.cttic.erp.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cttic.erp.bean.InfoDepartmentBean;
import com.cttic.erp.dao.interfaces.IInfoDepartmentDAO;
import com.cttic.erp.mapper.InfoDepartmentMapper;

@Repository
public class InfoDepartmentDAOImpl implements IInfoDepartmentDAO {

	@Autowired
	private InfoDepartmentMapper infoDepartmentMapper;

	@Override
	public List<InfoDepartmentBean> getBeans() {
		return infoDepartmentMapper.getBeans();
	}

	@Override
	public InfoDepartmentBean getBeanByDeptId(long dept_id) {
		return infoDepartmentMapper.getBeanByDeptId(dept_id);
	}

	@Override
	public List<InfoDepartmentBean> getChildBeanByDeptId(long parent_dept_id) {
		return infoDepartmentMapper.getChildBeanByDeptId(parent_dept_id);
	}

	@Override
	public InfoDepartmentBean getBeanByManagerEmployee(long manager_employee_id) {
		return infoDepartmentMapper.getBeanByManagerEmployee(manager_employee_id);
	}

}
