package com.cttic.erp.dao.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cttic.erp.bean.InfoProjectBean;
import com.cttic.erp.dao.interfaces.IInfoProjectDAO;
import com.cttic.erp.mapper.InfoProjectMapper;

@Repository
public class InfoProjectDAOImpl implements IInfoProjectDAO {

	@Autowired
	private InfoProjectMapper infoProjectMapper;
	
	@Override
	public List<InfoProjectBean> getAllBeans() {
		return infoProjectMapper.getAllBeans();
	}
	
	@Override
	public List<InfoProjectBean> getBeans() {
		return infoProjectMapper.getBeans();
	}
	
	@Override
	public List<InfoProjectBean> getBeansByManager(long manager_employee_id) {
		return infoProjectMapper.getBeansByManager(manager_employee_id);
	}

	@Override
	public InfoProjectBean getBean(BigDecimal prj_id) {
		return infoProjectMapper.getBean(prj_id);
	}
	
	@Override
	public void insert(InfoProjectBean bean) {
		infoProjectMapper.insert(bean);
	}
	
	@Override
	public void update(InfoProjectBean bean) {
		infoProjectMapper.update(bean);
	}
	
	@Override
	public void close(BigDecimal prj_id) {
		infoProjectMapper.close(prj_id);
	}
}
