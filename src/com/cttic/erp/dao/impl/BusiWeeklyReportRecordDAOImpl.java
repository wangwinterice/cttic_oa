package com.cttic.erp.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cttic.erp.bean.BusiWeeklyReportRecordBean;
import com.cttic.erp.dao.interfaces.IBusiWeeklyReportRecordDAO;
import com.cttic.erp.mapper.BusiWeeklyReportRecordMapper;

@Repository
public class BusiWeeklyReportRecordDAOImpl implements
		IBusiWeeklyReportRecordDAO {

	@Autowired
	private BusiWeeklyReportRecordMapper busiWeeklyReportRecordMapper;
	
	@Override
	public BusiWeeklyReportRecordBean getBean(long employee_id,
			String begin_date, String end_date) {
		return busiWeeklyReportRecordMapper.getBean(employee_id, begin_date, end_date);
	}
	
	@Override
	public List<BusiWeeklyReportRecordBean> getBeanListByDate(String begin_date, String end_date) {
		return busiWeeklyReportRecordMapper.getBeanListByDate(begin_date, end_date);
	}

	@Override
	public List<BusiWeeklyReportRecordBean> getBeans(long employee_id) {
		return busiWeeklyReportRecordMapper.getBeans(employee_id);
	}
	
	@Override
	public List<BusiWeeklyReportRecordBean> getBeans(long employee_id, String select_month) {
		return busiWeeklyReportRecordMapper.getBeanList(employee_id, select_month);
	}

	@Override
	public void insertBean(BusiWeeklyReportRecordBean bean) {
		busiWeeklyReportRecordMapper.insertBean(bean);
	}

	@Override
	public void updateBean(BusiWeeklyReportRecordBean bean) {
		busiWeeklyReportRecordMapper.updateBean(bean);
	}

	@Override
	public List<BusiWeeklyReportRecordBean> findWeekRecord(
			BusiWeeklyReportRecordBean search) {
		return busiWeeklyReportRecordMapper.findWeekRecord(search);
	}

	@Override
	public List<BusiWeeklyReportRecordBean> getBeanById(String week_id) {
		return busiWeeklyReportRecordMapper.getBeanById(week_id);
	}

}
