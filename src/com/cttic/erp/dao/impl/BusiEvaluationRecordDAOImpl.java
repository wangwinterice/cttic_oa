package com.cttic.erp.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cttic.erp.bean.BusiEvaluationRecordBean;
import com.cttic.erp.dao.interfaces.IBusiEvaluationRecordDAO;
import com.cttic.erp.mapper.BusiEvaluationRecordMapper;

@Repository
public class BusiEvaluationRecordDAOImpl implements IBusiEvaluationRecordDAO {

	@Autowired
	private BusiEvaluationRecordMapper busiEvaluationRecordMapper;
	
	@Override
	public BusiEvaluationRecordBean getBean(long employee_id,
			String evaluation_date) {
		return busiEvaluationRecordMapper.getBean(employee_id, evaluation_date);
	}

	@Override
	public void insertBean(BusiEvaluationRecordBean busiEvaluationRecordBean) {
		busiEvaluationRecordMapper.insertBean(busiEvaluationRecordBean);
	}

	@Override
	public void updateBean(BusiEvaluationRecordBean busiEvaluationRecordBean) {
		busiEvaluationRecordMapper.updateBean(busiEvaluationRecordBean);
	}

}
