package com.cttic.erp.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cttic.erp.bean.InfoDateBean;
import com.cttic.erp.dao.interfaces.IInfoDateDAO;
import com.cttic.erp.mapper.InfoDateMapper;

@Repository
public class InfoDateDAOImpl implements IInfoDateDAO {

	@Autowired
	InfoDateMapper infoDateMapper;
	
	@Override
	public List<InfoDateBean> getBeans(String year, String month) {
		return infoDateMapper.getBeans(year, month);
	}

}
