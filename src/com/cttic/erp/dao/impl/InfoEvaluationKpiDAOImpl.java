package com.cttic.erp.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cttic.erp.bean.InfoEvaluationKpiBean;
import com.cttic.erp.dao.interfaces.IInfoEvaluationKpiDAO;
import com.cttic.erp.mapper.InfoEvaluationKpiMapper;

@Repository
public class InfoEvaluationKpiDAOImpl implements IInfoEvaluationKpiDAO {

	@Autowired
	private InfoEvaluationKpiMapper infoEvaluationKpiMapper;
	
	@Override
	public List<InfoEvaluationKpiBean> getBeanList() {
		return infoEvaluationKpiMapper.getBeanList();
	}

}
