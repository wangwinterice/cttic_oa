package com.cttic.erp.dao.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cttic.erp.bean.InfoEmployeeBean;
import com.cttic.erp.bean.InfoProjectEmployeeBean;
import com.cttic.erp.dao.interfaces.IInfoProjectEmployeeDAO;
import com.cttic.erp.mapper.InfoProjectEmployeeMapper;

@Repository
public class InfoProjectEmployeeDAOImpl implements IInfoProjectEmployeeDAO {
	@Autowired
	private InfoProjectEmployeeMapper infoProjectEmployeeMapper;
	
	@Override
	public List<InfoProjectEmployeeBean> getBeans() {
		return infoProjectEmployeeMapper.getBeans();
	}

	@Override
	public List<InfoProjectEmployeeBean> getBeansByPrjId(BigDecimal prj_id) {
		return infoProjectEmployeeMapper.getBeansByPrjId(prj_id);
	}

	@Override
	public List<InfoProjectEmployeeBean> getValidBeansByPrjId(BigDecimal prj_id) {
		return infoProjectEmployeeMapper.getValidBeansByPrjId(prj_id);
	}

	@Override
	public List<InfoProjectEmployeeBean> getBeansByState(String state) {
		return infoProjectEmployeeMapper.getBeansByState(state);
	}

	@Override
	public void insert(InfoProjectEmployeeBean bean) {
		infoProjectEmployeeMapper.insert(bean);
	}

	@Override
	public void update(InfoProjectEmployeeBean bean) {
		infoProjectEmployeeMapper.update(bean);
	}

	@Override
	public List<InfoEmployeeBean> getNoPrjEmployee(BigDecimal prj_id) {
		return infoProjectEmployeeMapper.getNoPrjEmployee(prj_id);
	}
	
	@Override
	public List<InfoProjectEmployeeBean> getBeansByManagerEmployeeId(long managerEmployeeId) {
		return infoProjectEmployeeMapper.getBeansByManagerEmployeeId(managerEmployeeId);
	}
	
	@Override
	public List<InfoProjectEmployeeBean> getBeansByManagerEmployeeIdAndPrj(long managerEmployeeId, long prjId) {
		return infoProjectEmployeeMapper.getBeansByManagerEmployeeIdAndPrj(managerEmployeeId, prjId);
	}
	
	@Override
	public List<InfoProjectEmployeeBean> getBeansByPrj(long prjId) {
		return infoProjectEmployeeMapper.getBeansByPrj(prjId);
	}
}
