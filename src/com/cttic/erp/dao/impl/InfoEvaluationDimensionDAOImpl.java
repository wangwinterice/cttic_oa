package com.cttic.erp.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cttic.erp.bean.InfoEvaluationDimensionBean;
import com.cttic.erp.dao.interfaces.IInfoEvaluationDimensionDAO;
import com.cttic.erp.mapper.InfoEvaluationDimensionMapper;

@Repository
public class InfoEvaluationDimensionDAOImpl implements
		IInfoEvaluationDimensionDAO {

	@Autowired
	private InfoEvaluationDimensionMapper infoEvaluationDimensionMapper;
	
	@Override
	public List<InfoEvaluationDimensionBean> getBeanList() {
		return infoEvaluationDimensionMapper.getBeanList();
	}

}
