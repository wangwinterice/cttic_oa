package com.cttic.erp.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cttic.erp.bean.BusiEvaluationResultBean;
import com.cttic.erp.bean.ext.BusiEvaluationResultBeanExt;
import com.cttic.erp.dao.interfaces.IBusiEvaluationResultDAO;
import com.cttic.erp.mapper.BusiEvaluationResultMapper;

@Repository
public class BusiEvaluationResultDAOImpl implements IBusiEvaluationResultDAO {

	@Autowired
	private BusiEvaluationResultMapper busiEvaluationResultMapper;
	
	@Override
	public List<BusiEvaluationResultBean> getBeanList(long record_id) {
		return busiEvaluationResultMapper.getBeanList(record_id);
	}

	@Override
	public void insertBean(BusiEvaluationResultBean busiEvaluationResultBean) {
		busiEvaluationResultMapper.insertBean(busiEvaluationResultBean);
	}

	@Override
	public void deleteSelfEvaluationResult(long record_id) {
		busiEvaluationResultMapper.deleteSelfEvaluationResult(record_id);
	}

	@Override
	public void deleteCheckEvaluationResult(long record_id) {
		busiEvaluationResultMapper.deleteCheckEvaluationResult(record_id);
	}
	
	@Override
	public List<BusiEvaluationResultBeanExt> getBeanList(String evaluation_date) {
		return busiEvaluationResultMapper.getEvaluationResultBeanList(evaluation_date);
	}
	
	@Override
	public List<BusiEvaluationResultBeanExt> getEvaluationState() {
		return busiEvaluationResultMapper.getEvaluationState();
	}
}
