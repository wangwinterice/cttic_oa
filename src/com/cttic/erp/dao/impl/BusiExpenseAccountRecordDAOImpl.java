package com.cttic.erp.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cttic.erp.bean.BusiExpenseAccountRecordBean;
import com.cttic.erp.bean.ext.BusiExpenseAccountRecordBeanExt;
import com.cttic.erp.dao.interfaces.IBusiExpenseAccountRecordDAO;
import com.cttic.erp.mapper.BusiExpenseAccountRecordMapper;

@Repository
public class BusiExpenseAccountRecordDAOImpl implements
		IBusiExpenseAccountRecordDAO {

	@Autowired
	private BusiExpenseAccountRecordMapper busiExpenseAccountRecordMapper;
	
	@Override
	public List<BusiExpenseAccountRecordBean> getBeansByEmployeeId(
			long employee_id) {
		return busiExpenseAccountRecordMapper.getBeansByEmployeeId(employee_id);
	}
	
	@Override
	public List<BusiExpenseAccountRecordBean> getEffectBeans() {
		return busiExpenseAccountRecordMapper.getEffectBeans();
	}
	
	@Override
	public List<BusiExpenseAccountRecordBeanExt> getStatBeans() {
		return busiExpenseAccountRecordMapper.getStatBeans();
	}

	@Override
	public int insert(BusiExpenseAccountRecordBean bean) {
		return busiExpenseAccountRecordMapper.insert(bean);
	}

	@Override
	public int update(BusiExpenseAccountRecordBean bean) {
		return busiExpenseAccountRecordMapper.update(bean);
	}
	
	@Override
	public int updateState(BusiExpenseAccountRecordBean bean) {
		return busiExpenseAccountRecordMapper.updateState(bean);
	}
}
