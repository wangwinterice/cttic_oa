package com.cttic.erp.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cttic.erp.bean.SysParamBean;
import com.cttic.erp.dao.interfaces.ISysParamDAO;
import com.cttic.erp.mapper.SysParamMapper;

@Repository
public class SysParamDAOImpl implements ISysParamDAO {

	@Autowired
	private SysParamMapper sysBeanMapper;
	
	@Override
	public List<SysParamBean> getBeans() {
		return sysBeanMapper.getBeans();
	}

}
