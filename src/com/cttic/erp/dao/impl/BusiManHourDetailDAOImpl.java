package com.cttic.erp.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cttic.erp.bean.BusiManHourDetailBean;
import com.cttic.erp.dao.interfaces.IBusiManHourDetailDAO;
import com.cttic.erp.mapper.BusiManHourDetailMapper;

@Repository
public class BusiManHourDetailDAOImpl implements
IBusiManHourDetailDAO {

	@Autowired
	private BusiManHourDetailMapper busiManHourDetailMapper;

	@Override
	public List<BusiManHourDetailBean> getBeans(long manHour_id) {
		return busiManHourDetailMapper.getBeans(manHour_id);
	}

	@Override
	public void insertBean(BusiManHourDetailBean busiManHourDetailBean) {
		busiManHourDetailMapper.insertBean(busiManHourDetailBean);
		
	}

	@Override
	public int delete(long manHour_id) {
		return busiManHourDetailMapper.delete(manHour_id);
	}

	@Override
	public void updateBean(BusiManHourDetailBean d) {
		busiManHourDetailMapper.updateDetailBean(d);
	}


}
