package com.cttic.erp.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cttic.erp.bean.ext.BusiEvaluationBeanExt;
import com.cttic.erp.dao.interfaces.IBusiEvaluationDAO;
import com.cttic.erp.mapper.BusiEvaluationMapper;

@Repository
public class BusiEvaluationDAOImpl implements IBusiEvaluationDAO {

	@Autowired
	private BusiEvaluationMapper busiEvaluationMapper;
	
	@Override
	public List<BusiEvaluationBeanExt> getEvaluationStandardList(int template_id) {
		return busiEvaluationMapper.getEvaluationStandardList(template_id);
	}

	@Override
	public List<BusiEvaluationBeanExt> getEvaluationKpiList(int template_id) {
		return busiEvaluationMapper.getEvaluationKpiList(template_id);
	}
}
