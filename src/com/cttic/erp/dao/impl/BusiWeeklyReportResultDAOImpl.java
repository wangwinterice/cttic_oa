package com.cttic.erp.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cttic.erp.bean.BusiWeeklyReportResultBean;
import com.cttic.erp.dao.interfaces.IBusiWeeklyReportResultDAO;
import com.cttic.erp.mapper.BusiWeeklyReportResultMapper;

@Repository
public class BusiWeeklyReportResultDAOImpl implements
		IBusiWeeklyReportResultDAO {

	@Autowired
	private BusiWeeklyReportResultMapper busiWeeklyReportResultMapper;
	
	@Override
	public List<BusiWeeklyReportResultBean> getBeans(String record_id) {
		return busiWeeklyReportResultMapper.getBeans(record_id);
	}
	
	@Override
	public List<BusiWeeklyReportResultBean> getBeansByEmployeeId(Long employee_id) {
		return busiWeeklyReportResultMapper.getBeansByEmployeeId(employee_id);
	}

	@Override
	public void insertBean(BusiWeeklyReportResultBean bean) {
		busiWeeklyReportResultMapper.insertBean(bean);
	}

	@Override
	public void deleteBeans(String record_id) {
		busiWeeklyReportResultMapper.deleteBeans(record_id);
	}

	@Override
	public void deleteWorkTime(String record_id) {
		busiWeeklyReportResultMapper.deleteWorkTime(record_id);
	}
}
