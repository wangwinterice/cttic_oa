package com.cttic.erp.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cttic.erp.bean.InfoMenuBean;
import com.cttic.erp.dao.interfaces.IInfoOperDAO;
import com.cttic.erp.mapper.InfoOperMapper;

@Repository
public class InfoOperDAOImpl implements IInfoOperDAO {

	@Autowired
	private InfoOperMapper infoOperMapper;
	@Override
	public List<InfoMenuBean> getInfoMenuBeanList(long employee_id) {
		return infoOperMapper.getInfoMenuBeanList(employee_id);
	}

}
