package com.cttic.erp.dao.interfaces;

import java.util.List;

import com.cttic.erp.bean.BusiEvaluationResultBean;
import com.cttic.erp.bean.ext.BusiEvaluationResultBeanExt;

public interface IBusiEvaluationResultDAO {

	public List<BusiEvaluationResultBean> getBeanList(long record_id);

	public void insertBean(BusiEvaluationResultBean busiEvaluationResultBean);

	public void deleteSelfEvaluationResult(long record_id);

	public void deleteCheckEvaluationResult(long record_id);
	
	public List<BusiEvaluationResultBeanExt> getBeanList(String evaluation_date);
	
	public List<BusiEvaluationResultBeanExt> getEvaluationState();
}
