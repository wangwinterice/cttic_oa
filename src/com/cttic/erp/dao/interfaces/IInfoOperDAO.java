package com.cttic.erp.dao.interfaces;

import java.util.List;

import com.cttic.erp.bean.InfoMenuBean;

public interface IInfoOperDAO {

	public List<InfoMenuBean> getInfoMenuBeanList(long employee_id);
}
