package com.cttic.erp.dao.interfaces;

import java.util.List;

import com.cttic.erp.bean.ext.BusiEvaluationBeanExt;

public interface IBusiEvaluationDAO {

	public List<BusiEvaluationBeanExt> getEvaluationStandardList(int template_id);
	
	public List<BusiEvaluationBeanExt> getEvaluationKpiList(int template_id);
}
