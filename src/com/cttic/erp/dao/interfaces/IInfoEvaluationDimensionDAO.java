package com.cttic.erp.dao.interfaces;

import java.util.List;

import com.cttic.erp.bean.InfoEvaluationDimensionBean;

public interface IInfoEvaluationDimensionDAO {

	public List<InfoEvaluationDimensionBean> getBeanList();
}
