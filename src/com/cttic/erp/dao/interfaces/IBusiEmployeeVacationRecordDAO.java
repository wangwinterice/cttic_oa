package com.cttic.erp.dao.interfaces;

import java.util.List;

import com.cttic.erp.bean.BusiEmployeeVacationRecordBean;

public interface IBusiEmployeeVacationRecordDAO {

	public List<BusiEmployeeVacationRecordBean> getBeansByEmployeeId(long employee_id);

	public List<BusiEmployeeVacationRecordBean> getBeansByState(String state);
	
	public List<BusiEmployeeVacationRecordBean> getBeans(long employee_id, String reportMonth);
	
	public List<BusiEmployeeVacationRecordBean> getBeansByReportMonth(String reportMonth);
	
	public BusiEmployeeVacationRecordBean getBean(long employee_id, String beginDate);
	
	public BusiEmployeeVacationRecordBean getBeansByRecordId(long record_id);
	
	public void insert(BusiEmployeeVacationRecordBean bean);

	public void updateCheck(BusiEmployeeVacationRecordBean bean);
	
	public void updateRecord(BusiEmployeeVacationRecordBean bean);
	
	public void cancel(BusiEmployeeVacationRecordBean bean);
}
