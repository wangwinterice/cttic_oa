package com.cttic.erp.dao.interfaces;

import java.util.List;

import com.cttic.erp.bean.InfoDepartmentBean;

public interface IInfoDepartmentDAO {

	public List<InfoDepartmentBean> getBeans();

	public InfoDepartmentBean getBeanByDeptId(long dept_id);

	public List<InfoDepartmentBean> getChildBeanByDeptId(long parent_dept_id);

	public InfoDepartmentBean getBeanByManagerEmployee(long manager_employee_id);
}
