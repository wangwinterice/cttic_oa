package com.cttic.erp.dao.interfaces;

import java.util.List;

import com.cttic.erp.bean.BusiWeeklyReportRecordBean;

public interface IBusiWeeklyReportRecordDAO {

	public BusiWeeklyReportRecordBean getBean(long employee_id,
			String begin_date, String end_date);
	
	public List<BusiWeeklyReportRecordBean> getBeanListByDate(String begin_date, String end_date);
	
	public List<BusiWeeklyReportRecordBean> getBeans(long employee_id);
	
	public List<BusiWeeklyReportRecordBean> getBeans(long employee_id, String select_month);

	public void insertBean(BusiWeeklyReportRecordBean bean);

	public void updateBean(BusiWeeklyReportRecordBean bean);

	public List<BusiWeeklyReportRecordBean> findWeekRecord(
			BusiWeeklyReportRecordBean search);

	public List<BusiWeeklyReportRecordBean> getBeanById(String week_id);
}
