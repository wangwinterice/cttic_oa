package com.cttic.erp.dao.interfaces;

import java.util.List;

import com.cttic.erp.bean.BusiManHourDetailBean;

public interface IBusiManHourDetailDAO {

	public List<BusiManHourDetailBean> getBeans(long manHour_id);
	
	public void insertBean(BusiManHourDetailBean busiManHourDetailBean);
	
	public int delete(long manHour_id);

	public void updateBean(BusiManHourDetailBean d);
	
}
