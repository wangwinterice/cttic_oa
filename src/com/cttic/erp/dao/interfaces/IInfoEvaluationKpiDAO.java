package com.cttic.erp.dao.interfaces;

import java.util.List;

import com.cttic.erp.bean.InfoEvaluationKpiBean;

public interface IInfoEvaluationKpiDAO {

	public List<InfoEvaluationKpiBean> getBeanList();
}
