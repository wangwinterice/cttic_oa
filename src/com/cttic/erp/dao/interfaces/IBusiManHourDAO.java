package com.cttic.erp.dao.interfaces;

import java.util.List;
import java.util.Map;

import com.cttic.erp.bean.BusiManHourBean;
import com.cttic.erp.bean.InfoProjectBean;

public interface IBusiManHourDAO {

	public List<BusiManHourBean> getBeans(long employee_id);
	
	public void insertBean(BusiManHourBean busiManHourBean);
	
	public int delete(long id);

	public List<Map<String, String>> getPeriod(String startDate);

	public List<BusiManHourBean> getBeansById(long id);

	public List<InfoProjectBean> getProjectsByWeekId(String week_id);

	public String getRealEndDateByWeekId(String week_id);

	public String getRealBeginDateByWeekId(String week_id);

	public List<BusiManHourBean> getApprovalBeans();

	public List<BusiManHourBean> getBeanByWeekId(String record_id);

	public List<BusiManHourBean> getAllBeans(long employee_id);

	public List<Map<String, String>> findYears();

	public List<Map<String, String>> getWeekByYear(String year);

	public List<BusiManHourBean> getApprovalBeansByEndDate(String end_date);

	public void approval(long id);
	
}
