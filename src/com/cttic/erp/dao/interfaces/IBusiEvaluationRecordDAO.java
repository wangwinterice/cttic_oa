package com.cttic.erp.dao.interfaces;

import com.cttic.erp.bean.BusiEvaluationRecordBean;

public interface IBusiEvaluationRecordDAO {

	public BusiEvaluationRecordBean getBean(long employee_id,
			String evaluation_date);

	public void insertBean(BusiEvaluationRecordBean busiEvaluationRecordBean);

	public void updateBean(BusiEvaluationRecordBean busiEvaluationRecordBean);
}
