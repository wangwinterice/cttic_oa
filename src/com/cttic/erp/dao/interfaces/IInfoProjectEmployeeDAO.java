package com.cttic.erp.dao.interfaces;

import java.math.BigDecimal;
import java.util.List;

import com.cttic.erp.bean.InfoEmployeeBean;
import com.cttic.erp.bean.InfoProjectEmployeeBean;

public interface IInfoProjectEmployeeDAO {

	public List<InfoProjectEmployeeBean> getBeans();
	
	public List<InfoProjectEmployeeBean> getBeansByPrjId(BigDecimal prj_id);
	
	public List<InfoProjectEmployeeBean> getValidBeansByPrjId(BigDecimal prj_id);
	
	public List<InfoProjectEmployeeBean> getBeansByState(String state);
	
	public void insert(InfoProjectEmployeeBean bean);
	
	public void update(InfoProjectEmployeeBean bean);
	
	public List<InfoEmployeeBean> getNoPrjEmployee(BigDecimal prj_id);
	
	public List<InfoProjectEmployeeBean> getBeansByManagerEmployeeId(long managerEmployeeId);
	
	public List<InfoProjectEmployeeBean> getBeansByManagerEmployeeIdAndPrj(long managerEmployeeId, long prjId);
	
	public List<InfoProjectEmployeeBean> getBeansByPrj(long prjId);
}
