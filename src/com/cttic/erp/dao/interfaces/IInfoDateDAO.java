package com.cttic.erp.dao.interfaces;

import java.util.List;

import com.cttic.erp.bean.InfoDateBean;

public interface IInfoDateDAO {

	public List<InfoDateBean> getBeans(String year, String month);
}
