package com.cttic.erp.dao.interfaces;

import java.util.List;

import com.cttic.erp.bean.SysParamBean;

public interface ISysParamDAO {

	public List<SysParamBean> getBeans();
}
