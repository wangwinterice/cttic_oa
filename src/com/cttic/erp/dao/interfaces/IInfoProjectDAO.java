package com.cttic.erp.dao.interfaces;

import java.math.BigDecimal;
import java.util.List;

import com.cttic.erp.bean.InfoProjectBean;

public interface IInfoProjectDAO {

	public List<InfoProjectBean> getAllBeans();
	
	public List<InfoProjectBean> getBeans();
	
	public List<InfoProjectBean> getBeansByManager(long manager_employee_id);
	
	public InfoProjectBean getBean(BigDecimal prj_id);
	
	public void insert(InfoProjectBean bean);
	
	public void update(InfoProjectBean bean);
	
	public void close(BigDecimal prj_id);
}
