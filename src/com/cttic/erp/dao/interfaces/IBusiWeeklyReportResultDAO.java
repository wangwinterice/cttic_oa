package com.cttic.erp.dao.interfaces;

import java.util.List;

import com.cttic.erp.bean.BusiWeeklyReportResultBean;

public interface IBusiWeeklyReportResultDAO {

	public List<BusiWeeklyReportResultBean> getBeans(String record_id);
	
	public List<BusiWeeklyReportResultBean> getBeansByEmployeeId(Long employee_id);
	
	public void insertBean(BusiWeeklyReportResultBean bean);
	
	public void deleteBeans(String record_id);
	
	public void deleteWorkTime(String record_id);
}
