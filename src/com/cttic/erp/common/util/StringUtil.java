package com.cttic.erp.common.util;

public class StringUtil {

    public static boolean isBlank(String str) {
        if (null == str) {
            return true;
        }
        if ("".equals(str.trim())) {
            return true;
        }

        return false;
    }

    public static String toString(Object obj) {
        if (obj == null) {
            return "";
        }
        return obj.toString();
    }

    public static String restrictLength(String strSrc, int iMaxLength) {
        if (strSrc == null) {
            return null;
        }
        if (iMaxLength <= 0) {
            return strSrc;
        }
        String strResult = strSrc;
        byte[] b = null;
        int iLength = strSrc.length();
        if (iLength > iMaxLength) {
            strResult = strResult.substring(0, iMaxLength);
            iLength = iMaxLength;
        }
        while (true) {
            b = strResult.getBytes();
            if (b.length <= iMaxLength) {
                break;
            }
            iLength--;
            strResult = strResult.substring(0, iLength);
        }
        return strResult;
    }
    
    /**
     * 20160215转化为2016.02.15
     * @param src
     * @return
     */
	public static String changeDateFormt(String src) {
		
		return src.substring(0, 4)+"."+src.substring(4, 6)+"."+src.substring(6, 8);
	}
	/**
     * 2016.02.15转化为20160215
     * @param src
     * @return
     */
	public static String changeDateFormt2(String src) {
		return src.replace(".", "");
	}
	
	public static void main(String[] args) {
		System.out.println(changeDateFormt2("2016.02.15"));
	}
}
