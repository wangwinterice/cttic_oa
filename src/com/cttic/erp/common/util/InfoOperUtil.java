package com.cttic.erp.common.util;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.cttic.erp.bean.ext.InfoOperBeanExt;

public class InfoOperUtil {

	public final static String INFOOPER_IN_SESSION = "__INFOOPER_IN_SESSION";
	
	public static InfoOperBeanExt getCurrOperInfo(ServletRequest req) {
        HttpServletRequest httpRequest = (HttpServletRequest) req;
        HttpSession session = httpRequest.getSession();
        InfoOperBeanExt infoOperBean = null;
        if (session.getAttribute(INFOOPER_IN_SESSION) != null)
        	infoOperBean = (InfoOperBeanExt) session.getAttribute(INFOOPER_IN_SESSION);
        else {
        	// 压力测试用代码，压力测试完毕后删除
            session.setAttribute(INFOOPER_IN_SESSION, infoOperBean);
        }
        return infoOperBean;
    }
	
	public static void setCurrOperInfo(ServletRequest req, InfoOperBeanExt infoOperBean) {
        HttpServletRequest httpRequest = (HttpServletRequest) req;
        HttpSession session = httpRequest.getSession();
        session.setAttribute(INFOOPER_IN_SESSION, infoOperBean);
    }
	
	public static void removeCurrOperInfo(ServletRequest req) {
        HttpServletRequest httpRequest = (HttpServletRequest) req;
        HttpSession session = httpRequest.getSession();
        session.removeAttribute(INFOOPER_IN_SESSION);
    }
}
