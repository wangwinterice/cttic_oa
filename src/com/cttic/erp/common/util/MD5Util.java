package com.cttic.erp.common.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Title: sysframe <br>
 * Description: MD5加密算法 <br>
 * Date: 2014-4-10 <br>
 * Copyright (c) 2014 CTTIC <br>
 */
public class MD5Util {

	public static String byteToHexString(byte[] b) {
		StringBuffer hexString = new StringBuffer();
		for (int i = 0; i < b.length; i++) {
			String hex = Integer.toHexString(b[i] & 0xFF);
			if (hex.length() == 1) {
				hex = "0" + hex;
			}
			hexString.append(hex.toLowerCase());
		}
		return hexString.toString();
	}

	public static String encode(String input) {
		byte[] digesta = null;
		try {
			MessageDigest alga = MessageDigest.getInstance("MD5");
			try {
				alga.update(input.getBytes("utf-8"));
			}
			catch (UnsupportedEncodingException ex) {
			}
			digesta = alga.digest();
		}
		catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return byteToHexString(digesta);
	}

	public static void main(String[] args) {
		try {
			MD5Util md5 = new MD5Util();// �Զ�����Կ
			System.out.println("-----test-----");
			String test = "123%aa";
			System.out.println("MD5加密" + test);
			String jiami = md5.encode(test);
			System.out.println(jiami);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
