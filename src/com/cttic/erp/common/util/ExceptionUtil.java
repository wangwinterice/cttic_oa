package com.cttic.erp.common.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import com.cttic.erp.common.constants.BusiConstants;
import com.cttic.erp.common.exception.SystemException;

public class ExceptionUtil {

    public static JSONObject toJSON(String message, Exception exception, HttpServletRequest request) {
        return toJSON(message, exception);
    }

    public static JSONObject toJSON(String message, Exception ex) {
        ex.printStackTrace();
        String excepPrintex = getTrace(ex);
        String retMsg = toString(ex);
        retMsg = StringUtil.isBlank(retMsg) ? "系统错误，请联系管理员解决" : retMsg;
        message = StringUtil.isBlank(message) ? retMsg : message;
        String excepType = getExceptionType(ex);
        JSONObject json = new JSONObject();
        json.put(BusiConstants.AjaxReturn.STATUS_CODE, BusiConstants.AjaxStatus.STATUS_FAILURE);
        json.put(BusiConstants.AjaxReturn.STATUS_INFO, message);
        json.put("EXCEP_TYPE", excepType);
        if (SystemException.SYS_EXCEPTION.equals(excepType)) {
            json.put("SHOW_DETAIL", "SHOW_DETAIL");
        }
        json.put("EXCEP_PRINTEX", excepPrintex);
       
        return json;
    }

    public static JSONObject toJSON(Exception ex) {
        ex.printStackTrace();
        String excepPrintex = getTrace(ex);
        String retMsg = toString(ex);
        retMsg = StringUtil.isBlank(retMsg) ? "系统错误，请联系管理员解决" : retMsg;
        String excepType = getExceptionType(ex);
        JSONObject json = new JSONObject();
        json.put(BusiConstants.AjaxReturn.STATUS_CODE, BusiConstants.AjaxStatus.STATUS_FAILURE);
        json.put(BusiConstants.AjaxReturn.STATUS_INFO, retMsg);
        json.put("EXCEP_TYPE", excepType);
        if (SystemException.SYS_EXCEPTION.equals(excepType)) {
            json.put("SHOW_DETAIL", "SHOW_DETAIL");
        }
        json.put("EXCEP_PRINTEX", excepPrintex); 
        return json;
    }

    private static String getExceptionType(Exception ex) {
        SystemException sysEx = null;
        if (ex.getCause() instanceof InvocationTargetException) {
            InvocationTargetException invocationTargetException = (InvocationTargetException) ex
                    .getCause();
            if (invocationTargetException.getTargetException() instanceof SystemException) {
                sysEx = (SystemException) invocationTargetException.getTargetException();
            } else
                sysEx = new SystemException(invocationTargetException.getTargetException()
                        .getMessage(), invocationTargetException);
        } else if (ex instanceof InvocationTargetException) {
            InvocationTargetException invocationTargetException = (InvocationTargetException) ex;
            if (invocationTargetException.getTargetException() instanceof SystemException) {
                sysEx = (SystemException) invocationTargetException.getTargetException();
            } else
                sysEx = new SystemException(invocationTargetException.getTargetException()
                        .getMessage(), invocationTargetException);
        } else {
            sysEx = new SystemException(ex.getMessage(), ex);
        }
        String excepType = StringUtil.isBlank(sysEx.getExcepType()) ? SystemException.SYS_EXCEPTION
                : sysEx.getExcepType();
        return excepType;
    }

    public static String toString(Exception ex) {
        String retMsg = ex.getCause() == null ? ex.getMessage() : ex.getCause().getMessage();
        return retMsg;
    }

    public static String getTrace(Throwable t) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        try  
        {
	        t.printStackTrace(printWriter);
	        return stringWriter.toString();
        }  
        finally
        {  
        	printWriter.close();  
        }  
    }
}
