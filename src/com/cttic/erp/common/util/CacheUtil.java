package com.cttic.erp.common.util;

import java.util.ArrayList;
import java.util.List;

import com.cttic.erp.bean.SysParamBean;
import com.cttic.erp.common.cache.CacheManager;

public class CacheUtil {

    /**
     * 从缓存中获取配置参数信息:忽略参数类型的大小写
     * 
     * @param type_code:忽略大小写
     * @param param_code:忽略大小写
     * @return
     * @author wangdong
     */
    public static List<SysParamBean> getSysParamBeanList(String type_code, String param_code) {
    	type_code = null == type_code ? "" : type_code;
    	param_code = null == param_code ? "" : param_code;
        String key = (type_code + CacheManager.LINK_CHAR + param_code).toLowerCase();
        try {
            if (CacheManager.sysParamCacheMap.containsKey(key)) {
                return (List<SysParamBean>) CacheManager.sysParamCacheMap.get(key);
            }
        } catch (Exception e) {
            throw new RuntimeException("获取系统配置参数出错[" + key + "]", e);
        }
        return new ArrayList<SysParamBean>();
    }
    
    /**
     * 从缓存中获取配置参数信息:忽略参数类型的大小写
     * 
     * @param type_code:忽略大小写
     * @param param_code:忽略大小写
     * @return
     * @author wangdong
     */
    public static SysParamBean getSysParamBeanListSingle(String type_code, String param_code) {
        type_code = null == type_code ? "" : type_code;
        param_code = null == param_code ? "" : param_code;
        String key = (type_code + CacheManager.LINK_CHAR + param_code).toLowerCase();
        try {
            if (CacheManager.sysParamCacheMap.containsKey(key)) {
                return ((List<SysParamBean>) CacheManager.sysParamCacheMap.get(key)).get(0);
            }
        } catch (Exception e) {
            throw new RuntimeException("获取系统配置参数出错[" + key + "]", e);
        }
        return new SysParamBean();
    }    
    
    /**
     * 从缓存中获取配置参数信息:忽略参数类型大小写
     * 
     * @param type_code:忽略大小写
     * @param param_code:忽略大小写
     * @param parent_type_code:忽略大小写
     * @param parent_param_code:忽略大小写
     * @param parent_column_value:忽略大小写
     * @return
     * @author wangdong
     */
    public static List<SysParamBean> getSysParamBeanList(String type_code, String param_code,
            String parent_type_code, String parent_param_code, String parent_column_value) {
    	parent_type_code = null == parent_type_code ? "" : parent_type_code;
    	parent_param_code = null == parent_param_code ? "" : parent_param_code;
    	parent_column_value = null == parent_column_value ? "" : parent_column_value;
        List<SysParamBean> sysParams = getSysParamBeanList(type_code, param_code);
        if ("".equals(parent_type_code) || "".equals(parent_param_code) || "".equals(parent_column_value)) {
            // 入参subparam_code为空，则认为不需要通过subparam_code字段过滤
            return sysParams;
        } else {
            List<SysParamBean> subSysParams = new ArrayList<SysParamBean>();
            for (int index = 0; index < sysParams.size(); index++) {
                // 忽略大小写
                if (parent_type_code.equalsIgnoreCase(sysParams.get(index).getParent_type_code())
                		&& parent_param_code.equalsIgnoreCase(sysParams.get(index).getParent_param_code())
                		&& parent_column_value.equalsIgnoreCase(sysParams.get(index).getParent_column_value())) {
                    subSysParams.add(sysParams.get(index));
                }
            }
            return subSysParams;
        }
    }

    /**
     * 从缓存中获取某配置参数值详细信息
     * 
     * @param type_code:忽略大小写
     * @param param_code:忽略大小写
     * @param column_value
     * @return
     * @author wangdong
     */
    public static SysParamBean getSysParamBean(String type_code, String param_code, String column_value) {
    	column_value = null == column_value ? "" : column_value;
        List<SysParamBean> sysParams = getSysParamBeanList(type_code, param_code);
        for (int index = 0; index < sysParams.size(); index++) {
            if (column_value.equals(sysParams.get(index).getColumn_value())) {
                return sysParams.get(index);
            }
        }
        return null;
    }

    /**
     * 从缓存中获取某配置参数值的描述信息
     * 
     * @param type_code:忽略大小写
     * @param param_code:忽略大小写
     * @param column_value
     * @return
     * @author wangdong
     */
    public static String getSysParamBeanValueDesc(String type_code, String param_code, String column_value) {
    	SysParamBean sysParam = getSysParamBean(type_code, param_code, column_value);
        return (null == sysParam) ? "" : (null == sysParam.getColumn_desc()? "" : sysParam
        		.getColumn_desc());
    }

}
