package com.cttic.erp.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

	public static final String yyyyMMdd_HHmmss = "yyyy-MM-dd HH:mm:ss";

	public static final String yyyyMMdd = "yyyy-MM-dd";

	public static String formatDate(Date reqDate, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(reqDate);
	}

	/**
	 * 当前时间减i天
	 * 
	 * @param i
	 * @return
	 */
	public static String minus(int i) {
		return minus(i, "yyyy-MM-dd");
	}

	/**
	 * 当前时间减i天
	 * 
	 * @param i
	 * @return
	 */
	public static String minus(int i, String format) {
		SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
		Date beginDate = new Date();
		Calendar date = Calendar.getInstance();
		date.setTime(beginDate);
		date.set(Calendar.DATE, date.get(Calendar.DATE) - i);
		Date endDate = null;
		try {
			endDate = dft.parse(dft.format(date.getTime()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return formatDate(endDate, format);
	}

	/**
	 * 根据传入时间，减i天
	 * 
	 * @param i
	 * @return
	 */
	public static String minus(Date beginDate, int i) {
		SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
		Calendar date = Calendar.getInstance();
		date.setTime(beginDate);
		date.set(Calendar.DATE, date.get(Calendar.DATE) - i);
		Date endDate = null;
		try {
			endDate = dft.parse(dft.format(date.getTime()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return formatDate(endDate, "yyyy-MM-dd");
	}

	/**
	 * 根据传入时间，加i天
	 * 
	 * @param i
	 * @return
	 */
	public static String add(Date beginDate, int i) {
		SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
		Calendar date = Calendar.getInstance();
		date.setTime(beginDate);
		date.set(Calendar.DATE, date.get(Calendar.DATE) + i);
		Date endDate = null;
		try {
			endDate = dft.parse(dft.format(date.getTime()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return formatDate(endDate, "yyyy-MM-dd");
	}

	/**
	 * 使用参数Format将字符串转为Date
	 */
	public static Date parse(String strDate, String pattern) {
		try {
			return new SimpleDateFormat(pattern).parse(strDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}


	/**
	 * 两个时间相减，得到天数(end-begin)
	 * 
	 * @param begin
	 * @param end
	 * @return
	 */
	public static int minus(Date begin, Date end) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			begin = sdf.parse(sdf.format(begin));
			end = sdf.parse(sdf.format(end));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(begin);
		long time1 = cal.getTimeInMillis();
		cal.setTime(end);
		long time2 = cal.getTimeInMillis();
		long between_days = (time2 - time1) / (1000 * 3600 * 24);

		return Integer.parseInt(String.valueOf(between_days))+1;
	}
	
	/**
	 * 获取指定日期的周一
	 * @param time
	 * @return
	 */
	public static String getMonday(Date time) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // 设置时间格式
		Calendar cal = Calendar.getInstance();
		cal.setTime(time);

		// 判断要计算的日期是否是周日，如果是则减一天计算周六的，否则会出问题，计算到下一周去了
		int dayWeek = cal.get(Calendar.DAY_OF_WEEK);// 获得当前日期是一个星期的第几天
		if (1 == dayWeek) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
		}

		cal.setFirstDayOfWeek(Calendar.MONDAY);// 设置一个星期的第一天，按中国的习惯一个星期的第一天是星期一

		int day = cal.get(Calendar.DAY_OF_WEEK);// 获得当前日期是一个星期的第几天
		cal.add(Calendar.DATE, cal.getFirstDayOfWeek() - day);// 根据日历的规则，给当前日期减去星期几与一个星期第一天的差值
		return sdf.format(cal.getTime());
	}
	
	public static void main(String[] args) {
		Date begin = DateUtil.parse("20160229", "yyyyMMdd");
		Date end = DateUtil.parse("20160301", "yyyyMMdd");
		int days = DateUtil.minus(begin,end);
		System.out.println(days);
	}

}
