package com.cttic.erp.common.comparator;

import java.util.Comparator;

import net.sf.json.JSONObject;

public class ComparatorEvaluation implements Comparator {

	private String jsonKey;
	
	private String order;
	
	public String getJsonKey() {
		return jsonKey;
	}
	
	public void setJsonKey(String jsonKey) {
		this.jsonKey = jsonKey;
	}
	
	public String getOrder() {
		return order;
	}
	
	public void setOrder(String order) {
		this.order = order;
	}
	
	@Override
	public int compare(Object o1, Object o2) {
		JSONObject json1 = (JSONObject) o1;
		JSONObject json2 = (JSONObject) o2;
		String str1 = json1.has(jsonKey) ? json1.getString(jsonKey) : "";
		String str2 = json2.has(jsonKey) ? json2.getString(jsonKey) : "";
		int result = 0;
		if ("asc".equals(order)) {
			result = str1.compareTo(str2);
		} else if ("desc".equals(order)) {
			result = str2.compareTo(str1);
		}
		return result;
	}

}
