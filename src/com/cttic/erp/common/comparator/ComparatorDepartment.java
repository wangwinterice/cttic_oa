package com.cttic.erp.common.comparator;

import java.util.Comparator;

import com.cttic.erp.bean.InfoEmployeeBean;

public class ComparatorDepartment implements Comparator {

	private String deptId;
	
	public String getDeptId() {
		return deptId;
	}

	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}

	@Override
	public int compare(Object str1, Object str2) {
		
		return ((InfoEmployeeBean) str1).getDept_id().compareTo(((InfoEmployeeBean) str2).getDept_id());
	}

}
