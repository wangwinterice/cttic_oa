package com.cttic.erp.common.exception;

import java.io.Serializable;

public class SystemException extends RuntimeException implements Serializable {

	private static final long serialVersionUID = 1L;

    public static final String BUSI_EXCEPTION = "1";

    public static final String SYS_EXCEPTION = "2";

    private String excepType;

    private Exception exception;

    private String message = "";

    private String detail = "";

    public SystemException(String message) {
        super(message);
        this.message = message;
        this.excepType = BUSI_EXCEPTION;
    }

    public SystemException(String message, Exception e) {
        super(message, e);
        this.message = message;
        this.excepType = e instanceof SystemException ? ((SystemException) e).getExcepType()
                : SYS_EXCEPTION;
    }
    
    public SystemException(Exception e) {
        super(e);
        this.excepType = e instanceof SystemException ? ((SystemException) e).getExcepType()
                : SYS_EXCEPTION;
    }

    public SystemException(Throwable e) {
        super(e);
        this.excepType = e instanceof SystemException ? ((SystemException) e).getExcepType()
                : SYS_EXCEPTION;
    }

    public String getExcepType() {
        return excepType;
    }

    public void setExcepType(String excepType) {
        this.excepType = excepType;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
