package com.cttic.erp.common.constants;

public class BusiConstants {

    public static class AjaxReturn {

        public final static String STATUS_CODE = "STATUS_CODE";

        public final static String STATUS_INFO = "STATUS_INFO";
        
        public final static String RETURN_CODE = "RETURN_CODE";

        public final static String RETURN_INFO = "RETURN_INFO";
    }
    
    public static class AjaxStatus {

        public final static String STATUS_SUCCESS = "1";

        public final static String STATUS_FAILURE = "0";
    }
    
}
