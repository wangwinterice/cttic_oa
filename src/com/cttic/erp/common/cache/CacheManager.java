package com.cttic.erp.common.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.cttic.erp.bean.SysParamBean;
import com.cttic.erp.service.interfaces.ISysParamService;

public class CacheManager {

	private static final Log log = LogFactory.getLog(CacheManager.class);

	public static final String LINK_CHAR = ".";
	public static HashMap<String, List<SysParamBean>> sysParamCacheMap = new HashMap<String, List<SysParamBean>>();

	public static void start() {
		setSysParamCache();
	}

	public static void setSysParamCache() {
		log.debug("加载[系统配置参数]缓存数据......start......");
		try {
			ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml"); 
			ISysParamService sysParamServiceImpl = ctx.getBean(ISysParamService.class);
			List<SysParamBean> sysParamBeanList =  sysParamServiceImpl.getBeans();
			String key = null;
			for (SysParamBean bean : sysParamBeanList) {
				key = bean.getType_code().toLowerCase();
				if (!sysParamCacheMap.containsKey(key)) {
					sysParamCacheMap.put(key, new ArrayList<SysParamBean>());
				}
				sysParamCacheMap.get(key).add(bean);
				key = (bean.getType_code() + LINK_CHAR + bean
						.getParam_code()).toLowerCase();
				if (!sysParamCacheMap.containsKey(key)) {
					sysParamCacheMap.put(key, new ArrayList<SysParamBean>());
				}
				sysParamCacheMap.get(key).add(bean);
			}
		} catch (Exception e) {
			throw new RuntimeException("加载[系统配置参数]缓存数据异常...", e);
		}
		log.debug("加载[系统配置参数]缓存数据......end......");
		log.debug(sysParamCacheMap.size());
	}
}
